sap.ui.define(["com/doehler/PricingMasterFile/model/base", "sap/ui/core/format/DateFormat"], function (base, DateFormat) {
	"use strict";

	return {

		SHFillingQuantity: function (matnr, VFQnty) {
			if (matnr && VFQnty) {
				if (matnr.length >= 9 || VFQnty === "X") {
					return true;
				} else {
					return false;
				}
			}
		},

		getDecimal: function (value) {
			var oFloatFormatter = sap.ui.core.format.NumberFormat.getFloatInstance({
				decimals: 2,
				groupingEnabled: true,
				groupingSeparator: '.',
				decimalSeparator: ',',
				parseAsString: true
			});
			return oFloatFormatter.format(value);
		},

		/* Chart status in pr dd icon */
		getChartStatusColor: function (value) {
			if (value) {
				if (value === "2") {
					return "Success"; // green
				} else if (value === "3") {
					return "Error"; // red
				} else
					return "None"; // no color

			}
			//return value;
		},

		getListStatusColor: function (value) {
			if (value === null) return "Reject"; //green
			return value ? "Accept" : "Reject";
		},

		getApprovalStatus: function (value, name) {
			if (value === "2") {
				return "Approved by" + " " + name;
			} else {
				if (value === "3") {
					return "Rejected by" + " " + name;
				} else
					return "";
			}
		},

		getApprovalNeededText: function (value) {
			if (value === null) return "";
			return value === "" ? "No" : "Yes";
		},

		getApprovalNeededColor: function (value) {
			if (value === null) return "#139B49"; //green
			return value === "X" ? "#EB3109" : "#139B49";
		},

		getBUName: function (value) {
			if (!value) return "";
			return sap.ui.getCore().getModel("buActionDD").getData().filter(function (o) {
				return o.DOMVALUE_L === value;
			})[0].DDTEXT;
		},

		checkCM1Value: function (value) {
			if (!value) return "None";

			return parseFloat(value) > 0 ? "None" : "Error";
		},

		checkGapToTargetPerc: function (value) {
			if (!value) return "None";

			return parseFloat(value) > 0 ? "None" : "Error";
		},

		checkGapToTargetValue: function (value) {
			if (!value) return "None";

			return parseFloat(value) > 0 ? "None" : "Error";
		},

		_getLastPriceChangeIcon: function (value) {
			if (value === null || value === undefined) return "#139B49"; //green
			return value > 0 ? "sap-icon://arrow-bottom" : value < 0 ? "sap-icon://arrow-top" : "";
		},

		_getLastPriceChangeIconColor: function (value) {
			if (value === null || value === undefined) return "#139B49"; //green
			return value > 0 ? "#EB3109" : value < 0 ? "#139B49" : "";
		},

		getDateStringFormat: function (jsonDate) {
			if (!jsonDate || jsonDate === "00000000") {
				return "";
			}

			return jsonDate.substring(6, 8) + "." + jsonDate.substring(4, 6) + "." + jsonDate.substring(0, 4);
		},

		conversionExistingNew: function (value) {
			if (!value && value !== "") {
				return "";
			}

			return value === "X" ? "Existing" : "New";
		},

		conversionB2BL1: function (value) {
			if (!value) {
				return "";
			}

			if (value !== null) {
				if (value.length > 1) {
					return value[value.length - 1];
				} else return value;
			}
		},

		conversionB2BL2: function (value) {
			if (!value) {
				return "";
			}

			if (value !== null) {
				if (value.length > 1) {
					return value[value.length - 2] + value[value.length - 1];
				} else return value;
			}
		},

		conversionB2BL3: function (value) {
			if (!value) {
				return "";
			}

			if (value !== null) {
				if (value.length > 1) {
					return value[value.length - 3] + value[value.length - 2] + value[value.length - 1];
				} else return value;
			}
		},

		conversionB2BL4: function (value) {
			if (!value) {
				return "";
			}

			if (value !== null) {
				if (value.length > 1) {
					return value[value.length - 4] + value[value.length - 3] + value[value.length - 2] + value[value.length - 1];
				} else return value;
			}
		},

		conversionB2BL5: function (value) {
			if (!value) {
				return "";
			}

			if (value !== null) {
				if (value.length > 1) {
					return value[value.length - 5] + value[value.length - 4] + value[value.length - 3] + value[value.length - 2] + value[value.length -
						1];
				} else return value;
			}
		},
		setDescWidth: function (desc) {
			if (desc === "") {
				return "50%";
			} else {
				return "100%";
			}
		},

		setDescWidth48: function (desc) {
			if (desc === "") {
				return "48%";
			} else {
				return "96%";
			}
		},

	};

	// return {
	// 	/** 
	// 	 * Gets unformatted date and returns fromatted date object.
	// 	 * @public
	// 	 * @param {date} jsonDate - Object of the source control. 
	// 	 * @returns {date} Formatted date object.
	// 	 */
	// 	getDateFormat: function (jsonDate) {
	// 		if (!jsonDate) {
	// 			return "";
	// 		}
	// 		if (jsonDate == "0000-00-00" || jsonDate == "1970-01-01") {
	// 			return null;
	// 		}
	// 		var df = base.getI18NDateFormat();
	// 		var oDateFormat = DateFormat.getDateInstance({
	// 			pattern: df
	// 		});
	// 		if (jsonDate.toString().indexOf("/Date(") > -1) {
	// 			var jsonDate1 = jsonDate.match(/\d/g);
	// 			jsonDate1 = jsonDate1.join("");
	// 			var res = oDateFormat.format(new Date(Number(jsonDate1)));
	// 			return res;
	// 		} else {
	// 			var res1 = oDateFormat.format(new Date(jsonDate.toString()));
	// 			return res1;
	// 		}
	// 		return "";
	// 	},
	// 	/* ~~~~~~~~~~~~~~~  Article Header  Page ~~~~~~~~~~~~~~~~~ */
	// 	/* Aritcle Master list status */
	// 	status: function (s) {
	// 		if (s == "W" || s == "I") {
	// 			return "Warning";
	// 		} else if (s == "E") {
	// 			return "Error";
	// 		} else {
	// 			return "None";
	// 		}
	// 	},
	// 	/* Aritcle Master list icon status */
	// 	setIconStatus: function (s) {
	// 		if (s == "W" || s == "I") {
	// 			return "sap-icon://alert";
	// 		} else if (s == "E") {
	// 			return "sap-icon://alert";
	// 		} else {
	// 			return "";
	// 		}
	// 	},
	// 	/* Run Costing Status */
	// 	runCostEnabled: function (s) {
	// 		if (s == "E") {
	// 			return true;
	// 		}
	// 		return false;
	// 	},

	// 	getHarbourValue: function (value) {
	// 		if (value !== null && value !== undefined) {
	// 			if (parseInt(value) === 0) {
	// 				return "";
	// 			} else {
	// 				return parseInt(value);
	// 			}
	// 		}

	// 		return "";
	// 	},
	// 	/* Completion status in pr dd price setting */
	// 	getCompletionStatus: function (value) {
	// 		if (value) {
	// 			if (value === "I") {
	// 				return "Error";
	// 			} else if (value === "R") {
	// 				return "Success";
	// 			}
	// 		}
	// 		//  return value;
	// 	},
	// 	/* Completion status for button in pr dd price setting */
	// 	getCompletionStatusBtn: function (value) {
	// 		if (value) {
	// 			if (value === "I") {
	// 				return "Reject";
	// 			} else if (value === "R") {
	// 				return "Accept";
	// 			}
	// 		}
	// 		//  return value;
	// 	},
	// 	/* Completion stauts in pr dd price setting icon */
	// 	getCompletionStatusIcon: function (value) {
	// 		if (value) {
	// 			if (value === "I") {
	// 				return "sap-icon://status-error";
	// 			} else if (value === "R") {
	// 				return "sap-icon://complete";
	// 			}
	// 		}
	// 		//   return value;
	// 	},

	// 	/* approved status for button in pr dd price setting */
	// 	getApprovalStatusBtn: function (value) {
	// 		if (value) {
	// 			if (value === "G") {
	// 				return "Accept";
	// 			} else if (value === "R") {
	// 				return "Reject";
	// 			}
	// 		}
	// 		//  return value;
	// 	},

	// 	//getWebGuiLinkForQuotation ex_dogus 04.03.2020
	// 	getWebGuiLinkForQuotation: function (quoNumb) {
	// 		var link = "";
	// 		if (quoNumb) {
	// 			link = sap.ui.getCore().getModel("globalModel").getProperty("/tcodeHostName");
	// 			link = link + "&~transaction=VA23+vbak-vbeln=" + quoNumb + ";&~OKCODE=ENT2#";
	// 			return link;
	// 		}
	// 	},

	// 	/* approved stauts in pr dd price setting icon */
	// 	getApprovalStatusIcon: function (value) {
	// 		if (value) {
	// 			if (value === "N") {
	// 				return "sap-icon://status-error";
	// 			} else if (value === "Y") {
	// 				return "sap-icon://complete";
	// 			}
	// 		}
	// 		//   return value;
	// 	},

	// 	getCompletionStatusTT: function (value) {
	// 		if (value) {
	// 			if (value === "I") {
	// 				return "Incomplete";
	// 			} else if (value === "R") {
	// 				return "Ready";
	// 			}
	// 		}
	// 		//  return value;
	// 	},
	// 	/*SOC: Ex_kenguvj Task-181*/
	// 	/*Drop down enable for alternative unit of measure*/
	// 	alternativeUOM: function (matnr, xvfkz) {
	// 		if (matnr) {
	// 			if (xvfkz === undefined) {
	// 				return (matnr.length >= 9);
	// 			} else {
	// 				return !(xvfkz === 'X' || matnr.length < 9);
	// 			}
	// 		}
	// 	},
	// 	SHFillingQuantity: function (matnr, VFQnty) {
	// 		if (matnr && VFQnty) {
	// 			if (matnr.length >= 9 || VFQnty === "X") {
	// 				return true;
	// 			} else {
	// 				return false;
	// 			}
	// 		}
	// 	},
	// 	/*EOC: Ex_kenguvj Task-181*/
	// 	/*SOC: Ex_kenguvj Task-51*/
	// 	/* Quoted */
	// 	getQuotedStatus: function (value, name) {
	// 		if (value === "Q") {
	// 			return "Quoted by" + " " + name;;
	// 		}
	// 	},
	// 	/*EOC: Ex_kenguvj Task-51*/
	// 	/*SOC: Ex_kenguvj Task-171*/
	// 	STOtherMaterialCosts: function (nLos, nPkg, nPlt, nRM) {
	// 		var nSubTotal = nLos + nPkg + nPlt + nRM;
	// 		return nSubTotal.toFixed(2);
	// 	},
	// 	/*EOC: Ex_kenguvj Task-171*/
	// 	/* Chart status in pr dd */
	// 	getChartStatusText: function (value) {
	// 		if (value) {
	// 			if (value === "A") {
	// 				return "Approved by";
	// 			} else if (value === "R") {
	// 				return "Rejected by";
	// 			} else if (value === "B") {
	// 				return "";
	// 			} else if (value === "P") {
	// 				return "Pending by";
	// 			}
	// 		}
	// 		//   return value;
	// 	},
	// 	/* Chart status in pr dd icon */
	// 	getChartStatusColor: function (value) {
	// 		if (value) {
	// 			if (value === "A" || value === "Q") {
	// 				return "Success"; // green
	// 			} else if (value === "R") {
	// 				return "Error"; // red
	// 			} else if (value === "B") {
	// 				return "None"; // no color
	// 			} else if (value === "P") {
	// 				return "Warning"; // orange
	// 			}
	// 		}
	// 		//return value;
	// 	},
	// 	/* Message log status */
	// 	getMsgTypeIcon: function (value) {
	// 		if (value) {
	// 			if (value === "S") {
	// 				return "sap-icon://sys-enter";
	// 			} else if (value === "E") {
	// 				return "sap-icon://message-error";
	// 			}
	// 		}
	// 		// return value;
	// 	},
	// 	/* Message log status */
	// 	getMsgTypeState: function (value) {
	// 		if (value) {
	// 			if (value === "S") {
	// 				return "Success";
	// 			} else if (value === "E") {
	// 				return "Error";
	// 			}
	// 		}
	// 		// return value;
	// 	},
	// 	/* BOM Raw material status */
	// 	getGRYStatus: function (value) {
	// 		if (value) {
	// 			if (value === "G") {
	// 				return "Success";
	// 			} else if (value === "R") {
	// 				return "Error";
	// 			} else if (value === "Y") {
	// 				return "Warning";
	// 			}
	// 		}
	// 		// return value;
	// 	},
	// 	/* BOM Raw material status icon */
	// 	getGRYStatusIcon: function (value) {
	// 		if (value) {
	// 			if (value === "G") {
	// 				return "sap-icon://sys-enter";
	// 			} else if (value === "R") {
	// 				return "sap-icon://message-error";
	// 			} else if (value === "Y") {
	// 				return "sap-icon://message-warning";
	// 			}
	// 		}
	// 		//return value;
	// 	},
	// 	/* get attachment icon */
	// 	getAttachmentIcon: function (fileType) {
	// 		if (fileType) {
	// 			if (fileType === "application/pdf") {
	// 				return "sap-icon://pdf-attachment";
	// 			} else if (fileType === "image/jpeg" || fileType === "image/png") {
	// 				return "sap-icon://attachment-photo";
	// 			} else if (fileType === "text/html") {
	// 				return "sap-icon://attachment-html";
	// 			} else if (fileType === "text/plain") {
	// 				return "sap-icon://attachment-text-file";
	// 			} else if (fileType === "application/msword") {
	// 				return "sap-icon://doc-attachment";
	// 			} else if (fileType === "text/csv" || fileType === "application/vnd.ms-excel" || fileType ===
	// 				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
	// 				return "sap-icon://excel-attachment";
	// 			} else if (fileType === "application/vnd.ms-powerpoint" || fileType ===
	// 				"application/vnd.openxmlformats-officedocument.presentationml.presentation" || fileType ===
	// 				"application/vnd.openxmlformats-officedocument.pres") {
	// 				return "sap-icon://ppt-attachment";
	// 			} else if (fileType === "application/zip") {
	// 				return "sap-icon://attachment-zip-file";
	// 			} else {
	// 				return "sap-icon://document";
	// 			}
	// 		}
	// 	},
	// 	/* if 9 digit then packaging in display mode in PMS*/
	// 	is6Digit: function (value) {
	// 		if (value) {
	// 			var patt1 = /[0-9A-Za-z]/g;
	// 			var length = value.match(patt1).join("").length;
	// 			if (length == "6") {
	// 				return true;
	// 			}
	// 		}
	// 		return false;
	// 	},
	// 	/* if input desc is empty then put width*/

	// 	/* if input desc is empty then put width*/
	// 	setDescWidth48: function (desc) {
	// 		if (desc === "") {
	// 			return "48%";
	// 		} else {
	// 			return "96%";
	// 		}
	// 	},

	// 	setDescWidth48ForPrice: function (desc) {
	// 		if (desc === 0) {
	// 			return "48%";
	// 		} else {
	// 			return "96%";
	// 		}
	// 	},

	// 	setDescWidth48ForPlanCost: function (desc) {
	// 		if (desc === 0) {
	// 			return "48%";
	// 		} else {
	// 			return "96%";
	// 		}
	// 	},

	// 	setDescWidth48ForRawMaterialCost: function (desc) {
	// 		if (desc === 0) {
	// 			return "48%";
	// 		} else {
	// 			return "96%";
	// 		}
	// 	},

	// 	checkEmptyExistingFieldForAgreementType: function (value) {
	// 		if (value === "0") {
	// 			return "";
	// 		} else if (value !== undefined) {
	// 			return sap.ui.getCore().getModel("priceRecM").getData()["ZEXST_BUSS"] + " " + sap.ui.getCore().getModel("priceRecM").getData()[
	// 				"ZEXST_BUSS_T"];
	// 		} else {
	// 			return "";
	// 		}
	// 	},

	// 	checkEmptyExistingFieldForCurrentPrice: function (value) {
	// 		if (value === "0,00") {
	// 			return "";
	// 		} else if (value !== null) {
	// 			return value; //sap.ui.getCore().getModel("priceRecM").getData()["ZEXST_PRC"];
	// 		} else {
	// 			return "";
	// 		}
	// 	},

	// 	checkEmptyExistingFieldForCurrentPriceDesc: function (value) {
	// 		if (value === 0) {
	// 			return "";
	// 		} else if (value !== undefined) {
	// 			return sap.ui.getCore().getModel("priceRecM").getData()["ZEXST_CURR"] + " / " + sap.ui.getCore().getModel("priceRecM").getData()[
	// 				"ZEXST_PQU"] + " " + sap.ui.getCore().getModel("priceRecM").getData()["ZEXST_PUOM"];
	// 		} else {
	// 			return "";
	// 		}
	// 	},

	// 	checkEmptyExistingFieldForPlanCostChange: function (value) {
	// 		if (value === "0,00") {
	// 			return "";
	// 		} else if (value !== null) {
	// 			return sap.ui.getCore().getModel("priceRecM").getData()["ZEXST_COSTEXGROSS"];
	// 		} else {
	// 			return "";
	// 		}
	// 	},

	// 	checkEmptyExistingFieldForPlanCostChangeDesc: function (value) {
	// 		if (value === 0) {
	// 			return "";
	// 		} else if (value !== undefined) {
	// 			return sap.ui.getCore().getModel("priceRecM").getData()["ZWAERS2Q"] + "/" + sap.ui.getCore().getModel("priceRecM").getData()["MEINS"];
	// 		} else {
	// 			return "";
	// 		}
	// 	},

	// 	checkEmptyExistingFieldForRawMaterialCostChange: function (value) {
	// 		if (value === "0,00") {
	// 			return "";
	// 		} else if (value !== null) {
	// 			return sap.ui.getCore().getModel("priceRecM").getData()["ZEXST_COSTEXNET"];
	// 		} else {
	// 			return "";
	// 		}
	// 	},

	// 	checkEmptyExistingFieldForRawMaterialCostChangeDesc: function (value) {
	// 		if (value === 0) {
	// 			return "";
	// 		} else if (value !== undefined) {
	// 			return sap.ui.getCore().getModel("priceRecM").getData()["ZWAERS2Q"] + "/" + sap.ui.getCore().getModel("priceRecM").getData()["MEINS"];
	// 		} else {
	// 			return "";
	// 		}
	// 	},

	// 	/* ~~~~~~~~~~~~~~ End Article Header Page ~~~~~~~~~~~~~~~~~ */
	// 	/*~~~~~~~~~~~~~~~~~~~~ PRICE RECORD ~~~~~~~~~~~~~~~~~~~~~~~~*/
	// 	getCheckbox: function (x) {
	// 		if (x === "x" || x === "X") {
	// 			return true;
	// 		} else {
	// 			return false;
	// 		}
	// 	},
	// 	/* CM1 chart color */
	// 	getChartColor: function (value) {
	// 		if (value) {
	// 			value = parseFloat(value);
	// 			if (value >= 70) {
	// 				return "Good"; // green
	// 			} else if (value >= 40) {
	// 				return "#FFFF00"; // yellow
	// 			} else if (value < 40) {
	// 				return "Error"; // red
	// 			}
	// 		}
	// 		//return value;
	// 	},
	// 	/** 
	// 	 * Gets unformatted time and returns fromatted time format.
	// 	 * @public
	// 	 * @param {date} jsonDate - Object of the source control. 
	// 	 * @returns {date} Formatted time object.
	// 	 */
	// 	getTimeFormat: function (jsonDate) {
	// 		if (jsonDate) {
	// 			if (typeof jsonDate === "object") {
	// 				var cdate = new Date(jsonDate.ms);
	// 				return cdate.getHours() + ":" + cdate.getMinutes() + ":" + cdate.getSeconds();
	// 			} else {
	// 				var cdate1 = (jsonDate.match(/(\d{2})H(\d{2})M(\d{2})S$/).slice(-3).join(":"));
	// 				return cdate1;
	// 			}
	// 		}
	// 		return null;
	// 	},
	// 	/*Ex_ravulav Task 64 Place of fulfillment changes*/
	// 	/* Make place of fulfillment field editable basing on Incoterms field value */
	// 	/*        setPfEditable: function(incoTerms) {        	
	// 	        	return !(incoTerms === "EXW" || incoTerms === "FCA");
	// 	        }
	// 	*/
	// };
});