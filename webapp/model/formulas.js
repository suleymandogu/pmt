sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/resource/ResourceModel"
], function (JSONModel, ResourceModel) {
	"use strict";
	return {
		_calculateRowsForKPIs: function (data) {
			var that = this;
			data.forEach(function (row) {

				if (row.PRICE_MASTER.DPRSVVJ > 0) {
					if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
								row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
								(((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
									.DRATE5) + ((row.PRICE_MASTER.DVV093PY + row.PRICE_MASTER.DVVTILPY) / (row.PRICE_MASTER.DPRSVVJ) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) /
								(row.PRICE_MASTER.DPRSVVJ)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER
								.DPRSVVJ) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
								row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 + (((
										row
										.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row
										.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
									.DRATE5) + ((row.PRICE_MASTER.DVV093PY + row.PRICE_MASTER.DVVTILPY) / (row.PRICE_MASTER.DPRSVVJ) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) /
								(row.PRICE_MASTER.DPRSVVJ)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER
								.DPRSVVJ) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
						}
					} else {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
									row
									.PRICE_MASTER
									.DRATE5 + (((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC /
											row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
										row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
									row
									.PRICE_MASTER
									.DRATE5 + (((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC /
											row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
										row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER.DUNITQ;
						}
					}
				}
				if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) > 0) {
					if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
									row
									.PRICE_MASTER
									.DRATE5 + row.PRICE_MASTER
									.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
									(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
											.DRPSFRTYPY *
											row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (
											row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) /
										row
										.PRICE_MASTER
										.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVV093PY - row.PRICE_MASTER.DVV093YTDPY +
										row.PRICE_MASTER.DVVTILYTD + row.PRICE_MASTER.DVVTILPY - row.PRICE_MASTER.DVVTILYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row
										.PRICE_MASTER
										.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) / (
									1 - row.PRICE_MASTER
									.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
									.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ -
									row.PRICE_MASTER.DPRSVYTDPY)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER
										.DPRSRSC3PPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) /
									row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER
								.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
									row
									.PRICE_MASTER
									.DRATE5 + row.PRICE_MASTER
									.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
									(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
												.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) +
											(row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) /
										row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVV093PY - row.PRICE_MASTER
										.DVV093YTDPY + row.PRICE_MASTER.DVVTILYTD + row.PRICE_MASTER.DVVTILPY - row.PRICE_MASTER.DVVTILYTDPY) / (row.PRICE_MASTER
										.DPRSVYTD +
										row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER
									.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
									.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ -
									row.PRICE_MASTER.DPRSVYTDPY)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER
										.DPRSRSC3PPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) /
									row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER
								.DUNITQ;
						}
					} else {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
									row
									.PRICE_MASTER
									.DRATE5 +
									(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
											.DRPSFRTYPY *
											row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (
											row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) /
										row
										.PRICE_MASTER
										.DRATE1 * row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST *
									row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER
								.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
									row
									.PRICE_MASTER
									.DRATE5 +
									(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
											.DRPSFRTYPY *
											row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (
											row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) /
										row
										.PRICE_MASTER
										.DRATE1 * row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST *
									row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER
								.DUNITQ;
						}
					}
				}
				if (row.PRICE_MASTER.DPRSVYTD > 0) {
					if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
								row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
								(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
									.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVVTILYTD) / (row.PRICE_MASTER.DPRSVYTD) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) /
								(row.PRICE_MASTER.DPRSVYTD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER
								.DPRSVYTD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
								row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 + (((
										row
										.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row
										.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
									.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVVTILYTD) / (row.PRICE_MASTER.DPRSVYTD) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) /
								(row.PRICE_MASTER.DPRSVYTD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER
								.DPRSVYTD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
						}
					} else {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
									row
									.PRICE_MASTER
									.DRATE5 + (((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC /
											row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
										row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
									row
									.PRICE_MASTER
									.DRATE5 + (((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC /
											row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
										row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER.DUNITQ;
						}
					}
				}
				if (row.PRICE_MASTER.DPRSVATD > 0) {
					if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
								row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
								(((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
									.DRATE5) + ((row.PRICE_MASTER.DVV093ATD + row.PRICE_MASTER.DVVTILATD) / (row.PRICE_MASTER.DPRSVATD) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) /
								(row.PRICE_MASTER.DPRSVATD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER
								.DPRSVATD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
								row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 + (((
										row
										.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
									.DRATE5) + ((row.PRICE_MASTER.DVV093ATD + row.PRICE_MASTER.DVVTILATD) / (row.PRICE_MASTER.DPRSVATD) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) /
								(row.PRICE_MASTER.DPRSVATD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER
								.DPRSVATD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
						}
					} else {
						if (row.PRICE_MASTER.DISTR_CHAN === "60") {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) *
									row
									.PRICE_MASTER
									.DRATE5 + (((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
											row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
										row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER.DUNITQ;
						} else {
							row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) *
									row
									.PRICE_MASTER
									.DRATE5 + (((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
											row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
										row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
								row.PRICE_MASTER.DUNITQ;
						}
					}
				}
				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = 0;
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = 0;
				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.DPRSCPAIN / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
					.DUNITQ *
					row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = row.PRICE_MASTER.PRICE3 / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = that._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE);
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = that._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE);
				row.PRICE_MASTER.PRICE3 = that._setInitialValueNeeded(row.PRICE_MASTER.PRICE3);
				row.PRICE_MASTER.PRICE3 = parseFloat(row.PRICE_MASTER.PRICE3.toFixed(2));
			});
			return data;
		},
		_calculatePrices: function (row) {
			//Red Position (forecast)
			row.PRICE_MASTER.RED_POS_FC = "";
			row.PRICE_MASTER.PERC_CM1_FC = 0;
			row.PRICE_MASTER.GAPTOTARGET_PERC_FC = 0;
			row.PRICE_MASTER.DUNITQ_S = row.PRICE_MASTER.DUNITQ.toString();
			row.PRICE_MASTER.DCCHGRCURKG_CURR_UNIT = row.PRICE_MASTER.DCCHGRCURKG * parseFloat(row.PRICE_MASTER.DCONDUNIT);
			//18.12.2020
			/**********************************************volume*******************************************************************/
			row.PRICE_MASTER.VOLUME_VALUE = row.PRICE_MASTER.DPRSVATD;
			row.PRICE_MASTER.VOLUME_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.VOLUME_VALUE);
			row.PRICE_MASTER.VOLUME_VALUE = parseFloat(row.PRICE_MASTER.VOLUME_VALUE.toFixed(2));

			row.PRICE_MASTER.VOLUME_YTD_VALUE = row.PRICE_MASTER.DPRSVYTD;
			row.PRICE_MASTER.VOLUME_YTD_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.VOLUME_YTD_VALUE);
			row.PRICE_MASTER.VOLUME_YTD_VALUE = parseFloat(row.PRICE_MASTER.VOLUME_YTD_VALUE.toFixed(2));

			row.PRICE_MASTER.VOLUME_L12M_VALUE = row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY;
			row.PRICE_MASTER.VOLUME_L12M_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.VOLUME_L12M_VALUE);
			row.PRICE_MASTER.VOLUME_L12M_VALUE = parseFloat(row.PRICE_MASTER.VOLUME_L12M_VALUE.toFixed(2));

			row.PRICE_MASTER.VOLUME_PY_VALUE = row.PRICE_MASTER.DPRSVVJ;
			row.PRICE_MASTER.VOLUME_PY_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.VOLUME_PY_VALUE);
			row.PRICE_MASTER.VOLUME_PY_VALUE = parseFloat(row.PRICE_MASTER.VOLUME_PY_VALUE.toFixed(2));

			/************************************************sales gross****************************************************************/
			// row.PRICE_MASTER.SALESGROSS_VALUE = row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY;
			row.PRICE_MASTER.SALESGROSS_VALUE = row.PRICE_MASTER.DPRSRSGSATD;
			row.PRICE_MASTER.SALESGROSS_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.SALESGROSS_VALUE);
			row.PRICE_MASTER.SALESGROSS_VALUE = parseFloat(row.PRICE_MASTER.SALESGROSS_VALUE.toFixed(2));

			row.PRICE_MASTER.SALESGROSS_YTD_VALUE = row.PRICE_MASTER.DPRSRSGSA;
			row.PRICE_MASTER.SALESGROSS_YTD_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.SALESGROSS_YTD_VALUE);
			row.PRICE_MASTER.SALESGROSS_YTD_VALUE = parseFloat(row.PRICE_MASTER.SALESGROSS_YTD_VALUE.toFixed(2));

			row.PRICE_MASTER.SALESGROSS_L12M_VALUE = row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY;
			row.PRICE_MASTER.SALESGROSS_L12M_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.SALESGROSS_L12M_VALUE);
			row.PRICE_MASTER.SALESGROSS_L12M_VALUE = parseFloat(row.PRICE_MASTER.SALESGROSS_L12M_VALUE.toFixed(2));

			row.PRICE_MASTER.SALESGROSS_PY_VALUE = row.PRICE_MASTER.DPRSRSGSAPY;
			row.PRICE_MASTER.SALESGROSS_PY_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.SALESGROSS_PY_VALUE);
			row.PRICE_MASTER.SALESGROSS_PY_VALUE = parseFloat(row.PRICE_MASTER.SALESGROSS_PY_VALUE.toFixed(2));

			/************************************************GM**********************************************************************************/
			row.PRICE_MASTER.GM_VALUE = row.PRICE_MASTER.DPRSGMATD * row.PRICE_MASTER.DPRSVATD;
			row.PRICE_MASTER.GM_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_VALUE);
			row.PRICE_MASTER.GM_VALUE = parseFloat(row.PRICE_MASTER.GM_VALUE.toFixed(2));

			row.PRICE_MASTER.GM_YTD_VALUE = row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER.DPRSVYTD;
			row.PRICE_MASTER.GM_YTD_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_YTD_VALUE);
			row.PRICE_MASTER.GM_YTD_VALUE = parseFloat(row.PRICE_MASTER.GM_YTD_VALUE.toFixed(2));

			row.PRICE_MASTER.GM_L12M_VALUE = (row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER
				.DPRSVVJ -
				row.PRICE_MASTER.DPRSGMYPY * row.PRICE_MASTER.DPRSVYTDPY);
			row.PRICE_MASTER.GM_L12M_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_L12M_VALUE);
			row.PRICE_MASTER.GM_L12M_VALUE = parseFloat(row.PRICE_MASTER.GM_L12M_VALUE.toFixed(2));

			row.PRICE_MASTER.GM_PY_VALUE = row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER.DPRSVVJ;
			row.PRICE_MASTER.GM_PY_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_PY_VALUE);
			row.PRICE_MASTER.GM_PY_VALUE = parseFloat(row.PRICE_MASTER.GM_PY_VALUE.toFixed(2));

			/****************************************************************CM1*********************************************************/
			row.PRICE_MASTER.CM1_VALUE = row.PRICE_MASTER.DPRSRSCM1ATD;
			row.PRICE_MASTER.CM1_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_VALUE);
			row.PRICE_MASTER.CM1_VALUE = parseFloat(row.PRICE_MASTER.CM1_VALUE.toFixed(2));

			row.PRICE_MASTER.CM1_YTD_VALUE = row.PRICE_MASTER.DPRSRSCM1;
			row.PRICE_MASTER.CM1_YTD_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_YTD_VALUE);
			row.PRICE_MASTER.CM1_YTD_VALUE = parseFloat(row.PRICE_MASTER.CM1_YTD_VALUE.toFixed(2));

			row.PRICE_MASTER.CM1_L12M_VALUE = (row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY);
			row.PRICE_MASTER.CM1_L12M_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_L12M_VALUE);
			row.PRICE_MASTER.CM1_L12M_VALUE = parseFloat(row.PRICE_MASTER.CM1_L12M_VALUE.toFixed(2));

			row.PRICE_MASTER.CM1_PY_VALUE = row.PRICE_MASTER.DPRSRSCM1PY;
			row.PRICE_MASTER.CM1_PY_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_PY_VALUE);
			row.PRICE_MASTER.CM1_PY_VALUE = parseFloat(row.PRICE_MASTER.CM1_PY_VALUE.toFixed(2));

			/*****************************************************GM_EUR_KG*************************************************************************/
			row.PRICE_MASTER.GM_EUR_KG_VALUE = row.PRICE_MASTER.DPRSGMATD;
			row.PRICE_MASTER.GM_EUR_KG_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_EUR_KG_VALUE);
			row.PRICE_MASTER.GM_EUR_KG_VALUE = parseFloat(row.PRICE_MASTER.GM_EUR_KG_VALUE.toFixed(2));

			row.PRICE_MASTER.GM_EUR_KG_YTD_VALUE = row.PRICE_MASTER.DPRSGM;
			row.PRICE_MASTER.GM_EUR_KG_YTD_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_EUR_KG_YTD_VALUE);
			row.PRICE_MASTER.GM_EUR_KG_YTD_VALUE = parseFloat(row.PRICE_MASTER.GM_EUR_KG_YTD_VALUE.toFixed(2));

			row.PRICE_MASTER.GM_EUR_KG_L12M_VALUE = (row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER
				.DPRSVVJ - row.PRICE_MASTER.DPRSGMYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ -
				row.PRICE_MASTER
				.DPRSVYTDPY);
			row.PRICE_MASTER.GM_EUR_KG_L12M_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_EUR_KG_L12M_VALUE);
			row.PRICE_MASTER.GM_EUR_KG_L12M_VALUE = parseFloat(row.PRICE_MASTER.GM_EUR_KG_L12M_VALUE.toFixed(2));

			row.PRICE_MASTER.GM_EUR_KG_PY_VALUE = row.PRICE_MASTER.DPRSGMPY;
			row.PRICE_MASTER.GM_EUR_KG_PY_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.GM_EUR_KG_PY_VALUE);
			row.PRICE_MASTER.GM_EUR_KG_PY_VALUE = parseFloat(row.PRICE_MASTER.GM_EUR_KG_PY_VALUE.toFixed(2));

			/*****************************************************CM1_EUR_KG*******************************************************************************/
			row.PRICE_MASTER.CM1_EUR_KG_VALUE = (row.PRICE_MASTER.DPRSRSCM1ATD) / (row.PRICE_MASTER.DPRSVATD);
			row.PRICE_MASTER.CM1_EUR_KG_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_VALUE);

			row.PRICE_MASTER.CM1_CURR_UNIT = row.PRICE_MASTER.CM1_EUR_KG_VALUE * row.PRICE_MASTER.DUNITQ / row.PRICE_MASTER.DRATE3;
			row.PRICE_MASTER.CM1_CURR_UNIT = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_CURR_UNIT);
			row.PRICE_MASTER.CM1_CURR_UNIT = parseFloat(row.PRICE_MASTER.CM1_CURR_UNIT.toFixed(2));

			row.PRICE_MASTER.CM1_EUR_KG_VALUE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_VALUE.toFixed(2));

			row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE = (row.PRICE_MASTER.DPRSRSCM1) / (row.PRICE_MASTER.DPRSVYTD);
			row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE);

			row.PRICE_MASTER.CM1_YTD_CURR_UNIT = row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE * row.PRICE_MASTER.DUNITQ / row.PRICE_MASTER.DRATE3;
			row.PRICE_MASTER.CM1_YTD_CURR_UNIT = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_YTD_CURR_UNIT);
			row.PRICE_MASTER.CM1_YTD_CURR_UNIT = parseFloat(row.PRICE_MASTER.CM1_YTD_CURR_UNIT.toFixed(2));

			row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE.toFixed(2));

			row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE = (row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY) /
				(row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);
			row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE);

			row.PRICE_MASTER.CM1_L12M_CURR_UNIT = row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE * row.PRICE_MASTER.DUNITQ / row.PRICE_MASTER.DRATE3;
			row.PRICE_MASTER.CM1_L12M_CURR_UNIT = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_L12M_CURR_UNIT);
			row.PRICE_MASTER.CM1_L12M_CURR_UNIT = parseFloat(row.PRICE_MASTER.CM1_L12M_CURR_UNIT.toFixed(2));

			row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE.toFixed(2));

			row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE = (row.PRICE_MASTER.DPRSRSCM1PY) / (row.PRICE_MASTER.DPRSVVJ);
			row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE);

			row.PRICE_MASTER.CM1_PY_CURR_UNIT = row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE * row.PRICE_MASTER.DUNITQ / row.PRICE_MASTER.DRATE3;
			row.PRICE_MASTER.CM1_PY_CURR_UNIT = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_PY_CURR_UNIT);
			row.PRICE_MASTER.CM1_PY_CURR_UNIT = parseFloat(row.PRICE_MASTER.CM1_PY_CURR_UNIT.toFixed(2));

			row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE.toFixed(2));

			var DRATE5_Buffered = 0,
				OthCostInc_curr_kg = 0,
				FrgtInc_curr_kg = 0,
				CostChangeRaw = 0,
				costChangeGross = 0,
				costChangeGrossTotalEUR = 0,
				costChangeGrossDecrEUR = 0,
				costChangeGrossIncrEUR = 0;

			/*****************************************************************4 sub parts*************************************************************************************/
			var LV_PERIOD_TRIGGER = sap.ui.getCore().getModel("globalModel").getProperty("/periodSelectedIndex");

			//condition 4.1
			if (row.PRICE_MASTER.DPRSVVJ > 0) {
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnTextCM1", "PY");
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnText", "Full last year");
				row.PRICE_MASTER.rdBtnTextCM1 = "PY";
				row.PRICE_MASTER.rdBtnText = "Full last year";
				row.PRICE_MASTER.gapToTargetPercDetail = ((row.PRICE_MASTER.DPRSRSCM1PY) /
					(row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) * 100 - row.PRICE_MASTER.DPRSTMPRO);
				row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.DPRSCM1PERC;
				row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.gapToTargetPercDetail;

				row.PRICE_MASTER.cm1L12m = (row.PRICE_MASTER.DPRSRSCM1PY) / (row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
					.DPRSVVJ) * 100;
				row.PRICE_MASTER.cm1L12m = this._setInitialValueNeeded(row.PRICE_MASTER.cm1L12m);
				row.PRICE_MASTER.cm1L12m = parseFloat(row.PRICE_MASTER.cm1L12m.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVVJ) > 0) {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				} else {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "---";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "---";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				}

				row.PRICE_MASTER.gapToTargetPercDetail = this._setInitialValueNeeded(row.PRICE_MASTER.gapToTargetPercDetail);
				row.PRICE_MASTER.gapToTargetPercDetail = parseFloat(row.PRICE_MASTER.gapToTargetPercDetail.toFixed(2));

				/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>costChangeGross>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

				CostChangeRaw = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				if (row.PRICE_MASTER.DPRDIVIS === 0) CostChangeRaw = 0;
				if (row.PRICE_MASTER.DCUKYCP === "USD" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 + 0.03;
				else if (row.PRICE_MASTER.DCUKYCP === "GBP" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.05);
				else
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.02);

				OthCostInc_curr_kg = row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;
				FrgtInc_curr_kg = row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;
				if (CostChangeRaw < 0) {
					costChangeGross = CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg) / (1 - (row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ +
						row.PRICE_MASTER
						.DPRSRSC3PPY) / (row.PRICE_MASTER.DPRSRSGSAPY));
				} else {
					costChangeGross = (CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg)) / (1 - (row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
						.DPRSVVJ +
						row.PRICE_MASTER
						.DPRSRSC3PPY) / (row.PRICE_MASTER.DPRSRSGSAPY));
				}

				if (isNaN(costChangeGross) || costChangeGross === Infinity || costChangeGross === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGross = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGross = "";

				costChangeGross = this._setInitialValueNeeded(costChangeGross);
				row.PRICE_MASTER.costChangeGross = parseFloat(costChangeGross.toFixed(2));

				costChangeGrossTotalEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				if (isNaN(costChangeGrossTotalEUR) || costChangeGrossTotalEUR === Infinity || costChangeGrossTotalEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "";
				costChangeGrossTotalEUR = this._setInitialValueNeeded(costChangeGrossTotalEUR);
				row.PRICE_MASTER.costChangeGrossTotalEUR = parseFloat(costChangeGrossTotalEUR.toFixed(2));
				row.PRICE_MASTER.costChangeGrossTotalEUR = costChangeGrossTotalEUR;

				if (row.PRICE_MASTER.costChangeGross < 0) {
					costChangeGrossDecrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossDecrEUR) || costChangeGrossDecrEUR === Infinity || costChangeGrossDecrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "";
				costChangeGrossDecrEUR = this._setInitialValueNeeded(costChangeGrossDecrEUR);
				row.PRICE_MASTER.costChangeGrossDecrEUR = parseFloat(costChangeGrossDecrEUR.toFixed(2));

				if (row.PRICE_MASTER.costChangeGross < 0) {
					costChangeGrossIncrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossIncrEUR) || costChangeGrossIncrEUR === Infinity || costChangeGrossIncrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "";
				costChangeGrossIncrEUR = this._setInitialValueNeeded(costChangeGrossIncrEUR);
				row.PRICE_MASTER.costChangeGrossIncrEUR = parseFloat(costChangeGrossIncrEUR.toFixed(2));

				//Cost change gross in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DCONDUNIT * row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross);
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross.toFixed(2));

				row.PRICE_MASTER.costChangeGrossInSalesCurrency = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossInSalesCurrency);
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = parseFloat(row.PRICE_MASTER.costChangeGrossInSalesCurrency.toFixed(2));

				row.PRICE_MASTER.costChangeGrossEURKG = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.costChangeGrossEURKG = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossEURKG);
				row.PRICE_MASTER.costChangeGrossEURKG = parseFloat(row.PRICE_MASTER.costChangeGrossEURKG.toFixed(2));

				/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<costChangeGross<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
				var planMaterialCostChange = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange = this._setInitialValueNeeded(planMaterialCostChange);
				planMaterialCostChange = parseFloat(planMaterialCostChange.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange = 0;
				row.PRICE_MASTER.planMaterialCostChange = planMaterialCostChange;

				//Plan material cost changes in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange);
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange
					.toFixed(
						2));

				var planMaterialCostChangeABS = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS = this._setInitialValueNeeded(planMaterialCostChangeABS);
				planMaterialCostChangeABS = parseFloat(planMaterialCostChangeABS.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS = planMaterialCostChangeABS;

				var planMaterialCostChange_EUR = row.PRICE_MASTER.DPRSCEXGC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange_EUR = this._setInitialValueNeeded(planMaterialCostChange_EUR);
				planMaterialCostChange_EUR = parseFloat(planMaterialCostChange_EUR.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange_EUR = 0;
				row.PRICE_MASTER.planMaterialCostChange_EUR = planMaterialCostChange_EUR;

				var planMaterialCostChangeABS_EUR = row.PRICE_MASTER.planMaterialCostChange_EUR * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS_EUR = this._setInitialValueNeeded(planMaterialCostChangeABS_EUR);
				planMaterialCostChangeABS_EUR = parseFloat(planMaterialCostChangeABS_EUR.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS_EUR = planMaterialCostChangeABS_EUR;

				if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
							.PRICE_MASTER
							.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
							(((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row
									.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
								.DRATE5) + ((row.PRICE_MASTER.DVV093PY + row.PRICE_MASTER.DVVTILPY) / (row.PRICE_MASTER.DPRSVVJ) / row.PRICE_MASTER.DRATE1 *
								row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) /
							(row.PRICE_MASTER.DPRSVVJ)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER
							.DPRSVVJ) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
							.PRICE_MASTER
							.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 + (((
									row
									.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
									.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
								.DRATE5) + ((row.PRICE_MASTER.DVV093PY + row.PRICE_MASTER.DVVTILPY) / (row.PRICE_MASTER.DPRSVVJ) / row.PRICE_MASTER.DRATE1 *
								row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) /
							(row.PRICE_MASTER.DPRSVVJ)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER
							.DPRSVVJ) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
					}

				} else {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
								.PRICE_MASTER
								.DRATE5 + (((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
								.PRICE_MASTER
								.DRATE5 + (((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER.DUNITQ;
					}
				}

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = 0;
				row.PRICE_MASTER.HIGHEST_PRICE = 0;

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.DPRSCPAIN / parseFloat(
						row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = row.PRICE_MASTER.PRICE3 / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE);
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE);

				row.PRICE_MASTER.GAPTOTARGET_PERC = ((row.PRICE_MASTER.DPRSRSCM1PY) / (row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSSALDEPY *
					row.PRICE_MASTER.DPRSVVJ) * 100 - row.PRICE_MASTER.DPRSTMPRO);

				row.PRICE_MASTER.GAPTOTARGET_VALUE = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;
				row.PRICE_MASTER.GAPTOTARGET_VALUE = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL = row.PRICE_MASTER.GAPTOTARGET_VALUE;
				row.PRICE_MASTER.GAPTOTARGET_PERC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC.toFixed(1));

				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR;

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER.DUNITQ;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR);
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR.toFixed(2));

				row.PRICE_MASTER.HIGHEST_PRICE = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;

				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP7 - row.PRICE_MASTER
							.DMBULKSTZZP7) * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ)) -
						((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
							.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER
							.DPRSVVJ);

				} else {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP4 - row.PRICE_MASTER
							.DMBULKSTZZP4) * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ)) -
						((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
							.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER
							.DPRSVVJ);
				}

				row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER
					.DRATE4 -
					row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS - (row.PRICE_MASTER.DVV093PY + row.PRICE_MASTER.DVVTILPY) / (row.PRICE_MASTER
						.DPRSVVJ);

				row.PRICE_MASTER.PRICE3 = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE3);
				row.PRICE_MASTER.PRICE3 = parseFloat(row.PRICE_MASTER.PRICE3.toFixed(2));

				row.PRICE_MASTER.PRICE3_EXCEL = row.PRICE_MASTER.PRICE3;

				if (row.PRICE_MASTER.DPRDIVIS !== 0) {
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_EUR_KG.toFixed(2));
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = row.PRICE_MASTER.DPRSCPATC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_CURR_KG.toFixed(2));
					row.PRICE_MASTER.MAT_COST_CURR_KG = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.MAT_COST_CURR_KG = parseFloat(row.PRICE_MASTER.MAT_COST_CURR_KG.toFixed(2));
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = row.PRICE_MASTER.DCOSTINCTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = parseFloat(row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG.toFixed(2));
				}

				if ((row.PRICE_MASTER.DPRSVVJ) !== 0) {
					row.PRICE_MASTER.GM_EUR_KG = (row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ);

					row.PRICE_MASTER.CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1PY) / (row.PRICE_MASTER.DPRSVVJ);

					row.PRICE_MASTER.gapToTargetValDetail = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;

					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "";
				} else {
					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "---";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "---";
					row.PRICE_MASTER.gapToTargetValDetail = 0;
					row.PRICE_MASTER.GM_EUR_KG = 0;
					row.PRICE_MASTER.CM1_EUR_KG = 0;
				}

				row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
				row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				if ((row.PRICE_MASTER.DPRSVVJ) !== 0) {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSAPY) / (row.PRICE_MASTER.DPRSVVJ);

					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;

					row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
					row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				} else {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSAPY) / (row.PRICE_MASTER.DPRSVVJ);
					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
					if (isNaN(row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE)) {
						row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "---";
					}
					if (isNaN(row.PRICE_MASTER.CURR_PRICE_AVERAGE)) {
						row.PRICE_MASTER.CURR_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "---";
					}
				}

				row = this._calculatePricesWithNewPrice(row);

				row.PRICE_MASTER.GAPTOTARGET_PERC_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_PERC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_VALUE;

				//Red Position (forecast) and Forecasted CM1 (%) and  Forecasted Gap to Target (%)
				if (row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE - row.PRICE_MASTER.costChangeGrossEURKG < 0) {
					row.PRICE_MASTER.RED_POS_FC = "X";
				} else {
					row.PRICE_MASTER.RED_POS_FC = "X";
				}

				row.PRICE_MASTER.PERC_CM1_FC = ((row.PRICE_MASTER.CM1_EUR_KG_PY_VALUE - row.PRICE_MASTER.costChangeGrossEURKG) / row.PRICE_MASTER.DPRSNEXPY) *
					100;

				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = row.PRICE_MASTER.PERC_CM1_FC - row.PRICE_MASTER.DPRSTMPRO;

				row.PRICE_MASTER.PERC_CM1_FC = this._setInitialValueNeeded(row.PRICE_MASTER.PERC_CM1_FC);
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_PERC_FC);
				row.PRICE_MASTER.PERC_CM1_FC = parseFloat(row.PRICE_MASTER.PERC_CM1_FC.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC_FC.toFixed(2));

			}
			//condition 3.1
			if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) > 0) {
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnTextCM1", "L12M");
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnText", "Last 12 months");
				row.PRICE_MASTER.rdBtnTextCM1 = "L12M";
				row.PRICE_MASTER.rdBtnText = "Last 12 months";
				row.PRICE_MASTER.gapToTargetPercDetail = ((row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY) /
					(row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY - row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
						.DPRSVYTD - row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ + row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY
					) * 100 - row.PRICE_MASTER.DPRSTMPRO);
				row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.DPRSCM1PERC;
				row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.gapToTargetPercDetail;

				row.PRICE_MASTER.cm1L12m = (row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY) / (row.PRICE_MASTER
					.DPRSRSGSA + row.PRICE_MASTER
					.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY - row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD - row.PRICE_MASTER.DPRSSALDEPY *
					row.PRICE_MASTER.DPRSVVJ + row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) * 100;

				row.PRICE_MASTER.cm1L12m = this._setInitialValueNeeded(row.PRICE_MASTER.cm1L12m);
				row.PRICE_MASTER.cm1L12m = parseFloat(row.PRICE_MASTER.cm1L12m.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) > 0) {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				} else {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "---";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "---";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				}

				row.PRICE_MASTER.gapToTargetPercDetail = this._setInitialValueNeeded(row.PRICE_MASTER.gapToTargetPercDetail);
				row.PRICE_MASTER.gapToTargetPercDetail = parseFloat(row.PRICE_MASTER.gapToTargetPercDetail.toFixed(2));

				/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>costChangeGross>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

				CostChangeRaw = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				if (row.PRICE_MASTER.DPRDIVIS === 0) CostChangeRaw = 0;
				if (row.PRICE_MASTER.DCUKYCP === "USD" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 + 0.03;
				else if (row.PRICE_MASTER.DCUKYCP === "GBP" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.05);
				else
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.02);

				OthCostInc_curr_kg = row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;
				FrgtInc_curr_kg = row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;

				if (CostChangeRaw < 0) {
					costChangeGross = CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg) / (1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD +
						row.PRICE_MASTER
						.DPRSRSC3P + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER.DPRSSALDECYPY *
						row.PRICE_MASTER.DPRSVYTDPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY -
						row.PRICE_MASTER
						.DPRSRSGSAYPY));
				} else {
					costChangeGross = (CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg)) / (1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
						.DPRSVYTD +
						row.PRICE_MASTER
						.DPRSRSC3P + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER.DPRSSALDECYPY *
						row.PRICE_MASTER.DPRSVYTDPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY -
						row.PRICE_MASTER
						.DPRSRSGSAYPY));
				}

				if (isNaN(costChangeGross) || costChangeGross === Infinity || costChangeGross === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGross = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGross = "";

				costChangeGross = this._setInitialValueNeeded(costChangeGross);
				row.PRICE_MASTER.costChangeGross = parseFloat(costChangeGross.toFixed(2));

				costChangeGrossTotalEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				if (isNaN(costChangeGrossTotalEUR) || costChangeGrossTotalEUR === Infinity || costChangeGrossTotalEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "";
				costChangeGrossTotalEUR = this._setInitialValueNeeded(costChangeGrossTotalEUR);
				row.PRICE_MASTER.costChangeGrossTotalEUR = parseFloat(costChangeGrossTotalEUR.toFixed(2));
				row.PRICE_MASTER.costChangeGrossTotalEUR = costChangeGrossTotalEUR;

				if (row.PRICE_MASTER.costChangeGross < 0) {
					costChangeGrossDecrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossDecrEUR) || costChangeGrossDecrEUR === Infinity || costChangeGrossDecrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "";
				costChangeGrossDecrEUR = this._setInitialValueNeeded(costChangeGrossDecrEUR);
				row.PRICE_MASTER.costChangeGrossDecrEUR = parseFloat(costChangeGrossDecrEUR.toFixed(2));

				if (row.PRICE_MASTER.costChangeGross < 0) {
					costChangeGrossIncrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossIncrEUR) || costChangeGrossIncrEUR === Infinity || costChangeGrossIncrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "";
				costChangeGrossIncrEUR = this._setInitialValueNeeded(costChangeGrossIncrEUR);
				row.PRICE_MASTER.costChangeGrossIncrEUR = parseFloat(costChangeGrossIncrEUR.toFixed(2));

				//Cost change gross in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DCONDUNIT * row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross);
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross.toFixed(2));
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossInSalesCurrency);
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = parseFloat(row.PRICE_MASTER.costChangeGrossInSalesCurrency.toFixed(2));

				row.PRICE_MASTER.costChangeGrossEURKG = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.costChangeGrossEURKG = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossEURKG);
				row.PRICE_MASTER.costChangeGrossEURKG = parseFloat(row.PRICE_MASTER.costChangeGrossEURKG.toFixed(2));

				/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<costChangeGross<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
				var planMaterialCostChange = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange = this._setInitialValueNeeded(planMaterialCostChange);
				planMaterialCostChange = parseFloat(planMaterialCostChange.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange = 0;
				row.PRICE_MASTER.planMaterialCostChange = planMaterialCostChange;

				//Plan material cost changes in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange);
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange
					.toFixed(
						2));

				var planMaterialCostChangeABS = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS = this._setInitialValueNeeded(planMaterialCostChangeABS);
				planMaterialCostChangeABS = parseFloat(planMaterialCostChangeABS.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS = planMaterialCostChangeABS;

				var planMaterialCostChange_EUR = row.PRICE_MASTER.DPRSCEXGC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange_EUR = this._setInitialValueNeeded(planMaterialCostChange_EUR);
				planMaterialCostChange_EUR = parseFloat(planMaterialCostChange_EUR.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange_EUR = 0;
				row.PRICE_MASTER.planMaterialCostChange_EUR = planMaterialCostChange_EUR;

				var planMaterialCostChangeABS_EUR = row.PRICE_MASTER.planMaterialCostChange_EUR * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS_EUR = this._setInitialValueNeeded(planMaterialCostChangeABS_EUR);
				planMaterialCostChangeABS_EUR = parseFloat(planMaterialCostChangeABS_EUR.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS_EUR = planMaterialCostChangeABS_EUR;

				if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER
								.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
								(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
										.DRPSFRTYPY *
										row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (row.PRICE_MASTER
										.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) / row
									.PRICE_MASTER
									.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVV093PY - row.PRICE_MASTER.DVV093YTDPY +
									row.PRICE_MASTER.DVVTILYTD + row.PRICE_MASTER.DVVTILPY - row.PRICE_MASTER.DVVTILYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER
									.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) / (
								1 - row.PRICE_MASTER
								.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
								.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ -
								row.PRICE_MASTER.DPRSVYTDPY)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER
									.DPRSRSC3PPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) /
								row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER
							.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
								.PRICE_MASTER
								.DRATE5 + row.PRICE_MASTER
								.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
								(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
											.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) +
										(row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) /
									row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVV093PY - row.PRICE_MASTER
									.DVV093YTDPY + row.PRICE_MASTER.DVVTILYTD + row.PRICE_MASTER.DVVTILPY - row.PRICE_MASTER.DVVTILYTDPY) / (row.PRICE_MASTER.DPRSVYTD +
									row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER
								.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
								.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ -
								row.PRICE_MASTER.DPRSVYTDPY)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER
									.DPRSRSC3PPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) /
								row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER
							.DUNITQ;
					}

				} else {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
								.PRICE_MASTER
								.DRATE5 +
								(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
										.DRPSFRTYPY *
										row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (row.PRICE_MASTER
										.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) / row
									.PRICE_MASTER
									.DRATE1 * row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST *
								row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER
							.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
								.PRICE_MASTER
								.DRATE5 +
								(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
										.DRPSFRTYPY *
										row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (row.PRICE_MASTER
										.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) / row
									.PRICE_MASTER
									.DRATE1 * row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST *
								row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER
							.DUNITQ;
					}

				}

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = 0;
				row.PRICE_MASTER.HIGHEST_PRICE = 0;

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.DPRSCPAIN / parseFloat(
						row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = row.PRICE_MASTER.PRICE3 / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER
					.DRATE3;

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE);
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE);

				row.PRICE_MASTER.GAPTOTARGET_PERC = ((row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY) /
					(
						row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY - row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
						.DPRSVYTD - row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ + row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY
					) * 100 - row.PRICE_MASTER.DPRSTMPRO);

				row.PRICE_MASTER.GAPTOTARGET_VALUE = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;
				row.PRICE_MASTER.GAPTOTARGET_VALUE = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL = row.PRICE_MASTER.GAPTOTARGET_VALUE;
				row.PRICE_MASTER.GAPTOTARGET_PERC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC.toFixed(1));

				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR;

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER.DUNITQ;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR);
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR.toFixed(2));

				row.PRICE_MASTER.HIGHEST_PRICE = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;

				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP7 - row.PRICE_MASTER
							.DMBULKSTZZP7) * row.PRICE_MASTER
						.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
						.DPRSVYTDPY)) - ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
						.DPRSVYTDPY) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY -
						row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER
						.DPRSVYTD +
						row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);

				} else {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP4 - row.PRICE_MASTER
							.DMBULKSTZZP4) * row.PRICE_MASTER
						.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
						.DPRSVYTDPY)) - ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
						.DPRSVYTDPY) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY -
						row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER
						.DPRSVYTD +
						row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);

				}

				row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER
					.DRATE4 - row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS - (row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVV093PY - row
						.PRICE_MASTER
						.DVV093YTDPY + row.PRICE_MASTER.DVVTILYTD + row.PRICE_MASTER.DVVTILPY - row.PRICE_MASTER.DVVTILYTDPY) / (row.PRICE_MASTER.DPRSVYTD +
						row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);

				row.PRICE_MASTER.PRICE3 = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE3);
				row.PRICE_MASTER.PRICE3 = parseFloat(row.PRICE_MASTER.PRICE3.toFixed(2));

				row.PRICE_MASTER.PRICE3_EXCEL = row.PRICE_MASTER.PRICE3;

				if (row.PRICE_MASTER.DPRDIVIS !== 0) {
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_EUR_KG.toFixed(2));
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = row.PRICE_MASTER.DPRSCPATC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_CURR_KG.toFixed(2));
					row.PRICE_MASTER.MAT_COST_CURR_KG = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.MAT_COST_CURR_KG = parseFloat(row.PRICE_MASTER.MAT_COST_CURR_KG.toFixed(2));
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = row.PRICE_MASTER.DCOSTINCTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = parseFloat(row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG.toFixed(2));
				}

				if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) !== 0) {
					row.PRICE_MASTER.GM_EUR_KG = (row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DPRSGMYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
						.DPRSVYTDPY);
					row.PRICE_MASTER.CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY) / (row.PRICE_MASTER
						.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);

					row.PRICE_MASTER.gapToTargetValDetail = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;

					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "";
				} else {
					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "---";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "---";
					row.PRICE_MASTER.gapToTargetValDetail = 0;
					row.PRICE_MASTER.GM_EUR_KG = 0;
					row.PRICE_MASTER.CM1_EUR_KG = 0;
				}

				row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
				row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) !== 0) {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY) /
						(row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);

					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;

					row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
					row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				} else {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY) /
						(row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);
					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
					if (isNaN(row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE)) {
						row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "---";
					}
					if (isNaN(row.PRICE_MASTER.CURR_PRICE_AVERAGE)) {
						row.PRICE_MASTER.CURR_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "---";
					}
				}

				row = this._calculatePricesWithNewPrice(row);
				//Red Position (forecast) and Forecasted CM1 (%) and  Forecasted Gap to Target (%)
				if ((row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE - row.PRICE_MASTER.costChangeGrossEURKG) < 0) {
					row.PRICE_MASTER.RED_POS_FC = "X";
				} else {
					row.PRICE_MASTER.RED_POS_FC = "X";
				}

				row.PRICE_MASTER.PERC_CM1_FC = ((row.PRICE_MASTER.CM1_EUR_KG_L12M_VALUE - row.PRICE_MASTER.costChangeGrossEURKG) / ((row.PRICE_MASTER
					.DPRSNEX * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSNEXPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSNEXYPY * row
					.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY))) * 100;
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = row.PRICE_MASTER.PERC_CM1_FC - row.PRICE_MASTER.DPRSTMPRO;
				row.PRICE_MASTER.PERC_CM1_FC = this._setInitialValueNeeded(row.PRICE_MASTER.PERC_CM1_FC);
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_PERC_FC);
				row.PRICE_MASTER.PERC_CM1_FC = parseFloat(row.PRICE_MASTER.PERC_CM1_FC.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC_FC.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_PERC_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_PERC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_VALUE;

			}
			//condition 2.1
			if (row.PRICE_MASTER.DPRSVYTD > 0) {
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnTextCM1", "YTD");
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnText", "YTD");
				row.PRICE_MASTER.rdBtnTextCM1 = "YTD";
				row.PRICE_MASTER.rdBtnText = "YTD";
				row.PRICE_MASTER.gapToTargetPercDetail = ((row.PRICE_MASTER.DPRSRSCM1) /
					(row.PRICE_MASTER.DPRSRSGSA - row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) * 100 - row.PRICE_MASTER.DPRSTMPRO);
				row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.DPRSCM1PERC;
				row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.gapToTargetPercDetail;

				row.PRICE_MASTER.cm1L12m = (row.PRICE_MASTER.DPRSRSCM1) / (row.PRICE_MASTER.DPRSRSGSA - row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
					.DPRSVYTD) * 100;
				row.PRICE_MASTER.cm1L12m = this._setInitialValueNeeded(row.PRICE_MASTER.cm1L12m);
				row.PRICE_MASTER.cm1L12m = parseFloat(row.PRICE_MASTER.cm1L12m.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVYTD) > 0) {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				} else {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "---";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "---";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				}

				row.PRICE_MASTER.gapToTargetPercDetail = this._setInitialValueNeeded(row.PRICE_MASTER.gapToTargetPercDetail);
				row.PRICE_MASTER.gapToTargetPercDetail = parseFloat(row.PRICE_MASTER.gapToTargetPercDetail.toFixed(2));

				/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>costChangeGross>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

				CostChangeRaw = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				if (row.PRICE_MASTER.DPRDIVIS === 0) CostChangeRaw = 0;
				if (row.PRICE_MASTER.DCUKYCP === "USD" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 + 0.03;
				else if (row.PRICE_MASTER.DCUKYCP === "GBP" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.05);
				else
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.02);

				OthCostInc_curr_kg = row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;
				FrgtInc_curr_kg = row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;

				if (CostChangeRaw < 0) {
					costChangeGross = CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg) / (1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD +
						row.PRICE_MASTER
						.DPRSRSC3P) / (row.PRICE_MASTER.DPRSRSGSA));
				} else {
					costChangeGross = (CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg)) / (1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
						.DPRSVYTD +
						row.PRICE_MASTER
						.DPRSRSC3P) / (row.PRICE_MASTER.DPRSRSGSA));
				}

				if (isNaN(costChangeGross) || costChangeGross === Infinity || costChangeGross === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGross = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGross = "";

				costChangeGross = this._setInitialValueNeeded(costChangeGross);
				row.PRICE_MASTER.costChangeGross = parseFloat(costChangeGross.toFixed(2));

				costChangeGrossTotalEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				if (isNaN(costChangeGrossTotalEUR) || costChangeGrossTotalEUR === Infinity || costChangeGrossTotalEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "";
				costChangeGrossTotalEUR = this._setInitialValueNeeded(costChangeGrossTotalEUR);
				row.PRICE_MASTER.costChangeGrossTotalEUR = parseFloat(costChangeGrossTotalEUR.toFixed(2));
				row.PRICE_MASTER.costChangeGrossTotalEUR = costChangeGrossTotalEUR;

				if (row.PRICE_MASTER.costChangeGross < 0) {
					costChangeGrossDecrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossDecrEUR) || costChangeGrossDecrEUR === Infinity || costChangeGrossDecrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "";
				costChangeGrossDecrEUR = this._setInitialValueNeeded(costChangeGrossDecrEUR);
				row.PRICE_MASTER.costChangeGrossDecrEUR = parseFloat(costChangeGrossDecrEUR.toFixed(2));

				if (row.PRICE_MASTER.costChangeGross < 0) {
					costChangeGrossIncrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossIncrEUR) || costChangeGrossIncrEUR === Infinity || costChangeGrossIncrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "";
				costChangeGrossIncrEUR = this._setInitialValueNeeded(costChangeGrossIncrEUR);
				row.PRICE_MASTER.costChangeGrossIncrEUR = parseFloat(costChangeGrossIncrEUR.toFixed(2));

				//Cost change gross in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DCONDUNIT * row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross);
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross.toFixed(2));
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossInSalesCurrency);
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = parseFloat(row.PRICE_MASTER.costChangeGrossInSalesCurrency.toFixed(2));

				row.PRICE_MASTER.costChangeGrossEURKG = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.costChangeGrossEURKG = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossEURKG);
				row.PRICE_MASTER.costChangeGrossEURKG = parseFloat(row.PRICE_MASTER.costChangeGrossEURKG.toFixed(2));

				/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<costChangeGross<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
				var planMaterialCostChange = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange = this._setInitialValueNeeded(planMaterialCostChange);
				planMaterialCostChange = parseFloat(planMaterialCostChange.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange = 0;
				row.PRICE_MASTER.planMaterialCostChange = planMaterialCostChange;

				//Plan material cost changes in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange);
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange
					.toFixed(
						2));

				var planMaterialCostChangeABS = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS = this._setInitialValueNeeded(planMaterialCostChangeABS);
				planMaterialCostChangeABS = parseFloat(planMaterialCostChangeABS.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS = planMaterialCostChangeABS;

				var planMaterialCostChange_EUR = row.PRICE_MASTER.DPRSCEXGC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange_EUR = this._setInitialValueNeeded(planMaterialCostChange_EUR);
				planMaterialCostChange_EUR = parseFloat(planMaterialCostChange_EUR.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange_EUR = 0;
				row.PRICE_MASTER.planMaterialCostChange_EUR = planMaterialCostChange_EUR;

				var planMaterialCostChangeABS_EUR = row.PRICE_MASTER.planMaterialCostChange_EUR * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS_EUR = this._setInitialValueNeeded(planMaterialCostChangeABS_EUR);
				planMaterialCostChangeABS_EUR = parseFloat(planMaterialCostChangeABS_EUR.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS_EUR = planMaterialCostChangeABS_EUR;

				if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
							.PRICE_MASTER
							.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
							(((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row
									.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
								.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVVTILYTD) / (row.PRICE_MASTER.DPRSVYTD) / row.PRICE_MASTER.DRATE1 *
								row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) /
							(row.PRICE_MASTER.DPRSVYTD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER
							.DPRSVYTD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
							.PRICE_MASTER
							.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 + (((
									row
									.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
									.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
								.DRATE5) + ((row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVVTILYTD) / (row.PRICE_MASTER.DPRSVYTD) / row.PRICE_MASTER.DRATE1 *
								row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) /
							(row.PRICE_MASTER.DPRSVYTD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER
							.DPRSVYTD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
					}

				} else {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
								.PRICE_MASTER
								.DRATE5 + (((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
								.PRICE_MASTER
								.DRATE5 + (((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER.DUNITQ;
					}
				}

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = 0;
				row.PRICE_MASTER.HIGHEST_PRICE = 0;
				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.DPRSCPAIN / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = row.PRICE_MASTER.PRICE3 / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE);
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE);

				row.PRICE_MASTER.GAPTOTARGET_PERC = ((row.PRICE_MASTER.DPRSRSCM1) / (row.PRICE_MASTER.DPRSRSGSA - row.PRICE_MASTER.DPRSSALDEC *
					row.PRICE_MASTER
					.DPRSVYTD) * 100 - row.PRICE_MASTER.DPRSTMPRO);

				row.PRICE_MASTER.GAPTOTARGET_VALUE = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;
				row.PRICE_MASTER.GAPTOTARGET_VALUE = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL = row.PRICE_MASTER.GAPTOTARGET_VALUE;
				row.PRICE_MASTER.GAPTOTARGET_PERC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC.toFixed(1));

				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR;

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER.DUNITQ;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR);
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR.toFixed(2));

				row.PRICE_MASTER.HIGHEST_PRICE = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;

				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP7 - row.PRICE_MASTER
							.DMBULKSTZZP7) * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD)) -
						((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
							.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER
							.DPRSVYTD);

				} else {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP4 - row.PRICE_MASTER
							.DMBULKSTZZP4) * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD)) -
						((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
							.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER
							.DPRSVYTD);
				}

				row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER
					.DRATE4 -
					row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS - (row.PRICE_MASTER.DVV093YTD + row.PRICE_MASTER.DVVTILYTD) / (row.PRICE_MASTER
						.DPRSVYTD);

				row.PRICE_MASTER.PRICE3 = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE3);
				row.PRICE_MASTER.PRICE3 = parseFloat(row.PRICE_MASTER.PRICE3.toFixed(2));

				row.PRICE_MASTER.PRICE3_EXCEL = row.PRICE_MASTER.PRICE3;

				if (row.PRICE_MASTER.DPRDIVIS !== 0) {
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_EUR_KG.toFixed(2));
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = row.PRICE_MASTER.DPRSCPATC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_CURR_KG.toFixed(2));
					row.PRICE_MASTER.MAT_COST_CURR_KG = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.MAT_COST_CURR_KG = parseFloat(row.PRICE_MASTER.MAT_COST_CURR_KG.toFixed(2));
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = row.PRICE_MASTER.DCOSTINCTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = parseFloat(row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG.toFixed(2));
				}

				if ((row.PRICE_MASTER.DPRSVYTD) !== 0) {
					row.PRICE_MASTER.GM_EUR_KG = (row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD);

					row.PRICE_MASTER.CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1) / (row.PRICE_MASTER.DPRSVYTD);

					row.PRICE_MASTER.gapToTargetValDetail = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;

					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "";
				} else {
					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "---";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "---";
					row.PRICE_MASTER.gapToTargetValDetail = 0;
					row.PRICE_MASTER.GM_EUR_KG = 0;
					row.PRICE_MASTER.CM1_EUR_KG = 0;
				}

				row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
				row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				if ((row.PRICE_MASTER.DPRSVYTD) !== 0) {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSA) / (row.PRICE_MASTER.DPRSVYTD);

					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;

					row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
					row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				} else {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSA) / (row.PRICE_MASTER.DPRSVYTD);
					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
					if (isNaN(row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE)) {
						row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "---";
					}
					if (isNaN(row.PRICE_MASTER.CURR_PRICE_AVERAGE)) {
						row.PRICE_MASTER.CURR_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "---";
					}
				}

				row = this._calculatePricesWithNewPrice(row);
				//Red Position (forecast) and Forecasted CM1 (%) and  Forecasted Gap to Target (%)
				if ((row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE - row.PRICE_MASTER.costChangeGrossEURKG) < 0) {
					row.PRICE_MASTER.RED_POS_FC = "X";
				} else {
					row.PRICE_MASTER.RED_POS_FC = "X";
				}

				row.PRICE_MASTER.PERC_CM1_FC = ((row.PRICE_MASTER.CM1_EUR_KG_YTD_VALUE - row.PRICE_MASTER.costChangeGrossEURKG) / row.PRICE_MASTER.DPRSNEX) *
					100;
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = row.PRICE_MASTER.PERC_CM1_FC - row.PRICE_MASTER.DPRSTMPRO;
				row.PRICE_MASTER.PERC_CM1_FC = this._setInitialValueNeeded(row.PRICE_MASTER.PERC_CM1_FC);
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_PERC_FC);
				row.PRICE_MASTER.PERC_CM1_FC = parseFloat(row.PRICE_MASTER.PERC_CM1_FC.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC_FC.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_PERC_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_PERC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_VALUE;

			}
			//condition 1.1
			if (row.PRICE_MASTER.DPRSVATD > 0) {
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnTextCM1", "Since last PA");
				sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnText", "Since last price validity");
				row.PRICE_MASTER.rdBtnTextCM1 = "Since last PA";
				row.PRICE_MASTER.rdBtnText = "Since last price validity";
				row.PRICE_MASTER.gapToTargetPercDetail = ((row.PRICE_MASTER.DPRSRSCM1ATD) /
					(row.PRICE_MASTER.DPRSRSGSATD - row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) * 100 - row.PRICE_MASTER.DPRSTMPRO);
				row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.DPRSCM1ATD;
				row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.gapToTargetPercDetail;

				row.PRICE_MASTER.cm1L12m = (row.PRICE_MASTER.DPRSRSCM1ATD) / (row.PRICE_MASTER.DPRSRSGSATD - row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER
					.DPRSVATD) * 100;
				row.PRICE_MASTER.cm1L12m = this._setInitialValueNeeded(row.PRICE_MASTER.cm1L12m);
				row.PRICE_MASTER.cm1L12m = parseFloat(row.PRICE_MASTER.cm1L12m.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVATD) > 0) {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				} else {
					row.PRICE_MASTER.NOTVALIDDPRSCM1PERC = "---";
					row.PRICE_MASTER.DPRSCM1PERC_EXCEL = row.PRICE_MASTER.NOTVALIDDPRSCM1PERC;
					row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail = "---";
					row.PRICE_MASTER.gapToTargetPercDetail_excel = row.PRICE_MASTER.NOTVALID_gapToTargetPercDetail;
				}

				row.PRICE_MASTER.gapToTargetPercDetail = this._setInitialValueNeeded(row.PRICE_MASTER.gapToTargetPercDetail);
				row.PRICE_MASTER.gapToTargetPercDetail = parseFloat(row.PRICE_MASTER.gapToTargetPercDetail.toFixed(2));

				/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>costChangeGross>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/

				CostChangeRaw = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				if (row.PRICE_MASTER.DPRDIVIS === 0) CostChangeRaw = 0;
				if (row.PRICE_MASTER.DCUKYCP === "USD" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 + 0.03;
				else if (row.PRICE_MASTER.DCUKYCP === "GBP" && row.PRICE_MASTER.DCUKYPC === "EUR")
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.05);
				else
					DRATE5_Buffered = row.PRICE_MASTER.DRATE5 * (1 + 0.02);

				OthCostInc_curr_kg = row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;
				FrgtInc_curr_kg = row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * DRATE5_Buffered;

				if (CostChangeRaw < 0) {
					costChangeGross = CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg) / (1 - (row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER
						.DPRSVATD +
						row.PRICE_MASTER
						.DPRSRSC3PATD) / (row.PRICE_MASTER.DPRSRSGSATD));
				} else {
					costChangeGross = (CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg)) / (1 - (row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER
						.DPRSVATD + row.PRICE_MASTER
						.DPRSRSC3PATD) / (row.PRICE_MASTER.DPRSRSGSATD));
				}

				if (isNaN(costChangeGross) || costChangeGross === Infinity || costChangeGross === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGross = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGross = "";

				costChangeGross = this._setInitialValueNeeded(costChangeGross);
				row.PRICE_MASTER.costChangeGross = parseFloat(costChangeGross.toFixed(2));

				costChangeGrossTotalEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				if (isNaN(costChangeGrossTotalEUR) || costChangeGrossTotalEUR === Infinity || costChangeGrossTotalEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossTotalEUR = "";
				costChangeGrossTotalEUR = this._setInitialValueNeeded(costChangeGrossTotalEUR);
				row.PRICE_MASTER.costChangeGrossTotalEUR = parseFloat(costChangeGrossTotalEUR.toFixed(2));

				row.PRICE_MASTER.costChangeGrossTotalEUR = costChangeGrossTotalEUR;

				if (row.PRICE_MASTER.costChangeGross < 0) {
					costChangeGrossDecrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossDecrEUR) || costChangeGrossDecrEUR === Infinity || costChangeGrossDecrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossDecrEUR = "";
				costChangeGrossDecrEUR = this._setInitialValueNeeded(costChangeGrossDecrEUR);
				row.PRICE_MASTER.costChangeGrossDecrEUR = parseFloat(costChangeGrossDecrEUR.toFixed(2));

				if (row.PRICE_MASTER.costChangeGross > 0) {
					costChangeGrossIncrEUR = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DPRSVFC;
				}
				if (isNaN(costChangeGrossIncrEUR) || costChangeGrossIncrEUR === Infinity || costChangeGrossIncrEUR === -Infinity)
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "---";
				else
					row.PRICE_MASTER.NOTVALID_costChangeGrossIncrEUR = "";
				costChangeGrossIncrEUR = this._setInitialValueNeeded(costChangeGrossIncrEUR);
				row.PRICE_MASTER.costChangeGrossIncrEUR = parseFloat(costChangeGrossIncrEUR.toFixed(2));

				//Cost change gross in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DCONDUNIT * row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross);
				row.PRICE_MASTER.sales_curr_perUnit_costChangeGross = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_costChangeGross.toFixed(2));
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossInSalesCurrency);
				row.PRICE_MASTER.costChangeGrossInSalesCurrency = parseFloat(row.PRICE_MASTER.costChangeGrossInSalesCurrency.toFixed(2));

				row.PRICE_MASTER.costChangeGrossEURKG = row.PRICE_MASTER.costChangeGross * row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.costChangeGrossEURKG = this._setInitialValueNeeded(row.PRICE_MASTER.costChangeGrossEURKG);
				row.PRICE_MASTER.costChangeGrossEURKG = parseFloat(row.PRICE_MASTER.costChangeGrossEURKG.toFixed(2));

				/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<costChangeGross<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
				var planMaterialCostChange = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange = this._setInitialValueNeeded(planMaterialCostChange);
				planMaterialCostChange = parseFloat(planMaterialCostChange.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange = 0;
				row.PRICE_MASTER.planMaterialCostChange = planMaterialCostChange;

				//Plan material cost changes in sales currency per unit
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER
					.DUNITQ;
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = this._setInitialValueNeeded(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange);
				row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange = parseFloat(row.PRICE_MASTER.sales_curr_perUnit_planMaterialCostChange
					.toFixed(
						2));

				var planMaterialCostChangeABS = row.PRICE_MASTER.planMaterialCostChange * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS = this._setInitialValueNeeded(planMaterialCostChangeABS);
				planMaterialCostChangeABS = parseFloat(planMaterialCostChangeABS.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS = planMaterialCostChangeABS;

				var planMaterialCostChange_EUR = row.PRICE_MASTER.DPRSCEXGC / row.PRICE_MASTER.DPRDIVIS;
				planMaterialCostChange_EUR = this._setInitialValueNeeded(planMaterialCostChange_EUR);
				planMaterialCostChange_EUR = parseFloat(planMaterialCostChange_EUR.toFixed(2));
				if (row.PRICE_MASTER.DPRDIVIS === 0) planMaterialCostChange_EUR = 0;
				row.PRICE_MASTER.planMaterialCostChange_EUR = planMaterialCostChange_EUR;

				var planMaterialCostChangeABS_EUR = row.PRICE_MASTER.planMaterialCostChange_EUR * row.PRICE_MASTER.DPRSVFC;
				planMaterialCostChangeABS_EUR = this._setInitialValueNeeded(planMaterialCostChangeABS_EUR);
				planMaterialCostChangeABS_EUR = parseFloat(planMaterialCostChangeABS_EUR.toFixed(2));
				row.PRICE_MASTER.planMaterialCostChangeABS_EUR = planMaterialCostChangeABS_EUR;

				if (row.NEW_PRICE.TARGET_EXW_PRICE_LIST === 0) {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
							.PRICE_MASTER
							.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 +
							(((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
									row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
								.DRATE5) + ((row.PRICE_MASTER.DVV093ATD + row.PRICE_MASTER.DVVTILATD) / (row.PRICE_MASTER.DPRSVATD) / row.PRICE_MASTER.DRATE1 *
								row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) /
							(row.PRICE_MASTER.DPRSVATD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER
							.DPRSVATD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
							.PRICE_MASTER
							.DRATE5 + row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 + (((
									row
									.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC / row
									.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
								.DRATE5) + ((row.PRICE_MASTER.DVV093ATD + row.PRICE_MASTER.DVVTILATD) / (row.PRICE_MASTER.DPRSVATD) / row.PRICE_MASTER.DRATE1 *
								row.PRICE_MASTER.DRATE5)) / (1 - row.PRICE_MASTER.DPRSTMPRO / 100)) + (((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) /
							(row.PRICE_MASTER.DPRSVATD)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) + ((row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER
							.DPRSVATD) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5)) * parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
					}

				} else {
					if (row.PRICE_MASTER.DISTR_CHAN === "60") {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP4 + row.PRICE_MASTER.DPROCSTZZP4 - row.PRICE_MASTER.DMBULKSTZZP4) * row
								.PRICE_MASTER
								.DRATE5 + (((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER.DUNITQ;

					} else {
						row.PRICE_MASTER.PRICE3 = ((((row.PRICE_MASTER.DMATCSTZZP7 + row.PRICE_MASTER.DPROCSTZZP7 - row.PRICE_MASTER.DMBULKSTZZP7) * row
								.PRICE_MASTER
								.DRATE5 + (((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
										row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 *
									row.PRICE_MASTER.DRATE5)) + (row.NEW_PRICE.TARGET_EXW_PRICE_LIST * row.PRICE_MASTER.DRATE5))) * parseFloat(row.PRICE_MASTER.DCONDUNIT) *
							row.PRICE_MASTER.DUNITQ;
					}
				}

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = 0;
				row.PRICE_MASTER.HIGHEST_PRICE = 0;

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = row.PRICE_MASTER.DPRSCPAIN / parseFloat(
						row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = row.PRICE_MASTER.PRICE3 / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
					row.PRICE_MASTER.DRATE3;

				row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE);
				row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE);

				row.PRICE_MASTER.GAPTOTARGET_PERC = ((row.PRICE_MASTER.DPRSRSCM1ATD) / (row.PRICE_MASTER.DPRSRSGSATD - row.PRICE_MASTER.DPRSSALDECAT *
					row.PRICE_MASTER.DPRSVATD) * 100 - row.PRICE_MASTER.DPRSTMPRO);

				row.PRICE_MASTER.GAPTOTARGET_VALUE = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;
				row.PRICE_MASTER.GAPTOTARGET_VALUE = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL = row.PRICE_MASTER.GAPTOTARGET_VALUE;
				row.PRICE_MASTER.GAPTOTARGET_PERC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC.toFixed(1));

				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE * row.PRICE_MASTER.DPRSVFC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL_IN_EUR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR;

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERKG.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = row.PRICE_MASTER.GAPTOTARGET_VALUE / row.PRICE_MASTER.DRATE3 * row.PRICE_MASTER.DCONDUNIT *
					row.PRICE_MASTER.DUNITQ;
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT);
				row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT = parseFloat(row.PRICE_MASTER.GAPTOTARGET_SALES_CURR_PERUNIT.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = row.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR / row.PRICE_MASTER.DRATE3;
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR);
				row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR = parseFloat(row.PRICE_MASTER.GAPTOTARGET_ABS_SALES_CURR.toFixed(2));

				row.PRICE_MASTER.HIGHEST_PRICE = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;

				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP7 - row.PRICE_MASTER
							.DMBULKSTZZP7) * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD)) -
						((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
							.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PATD) / (
							row
							.PRICE_MASTER
							.DPRSVATD);

				} else {
					row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE - ((row.PRICE_MASTER.DMATCSTZZP4 - row.PRICE_MASTER
							.DMBULKSTZZP4) * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD)) -
						((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER
							.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PATD) / (
							row
							.PRICE_MASTER
							.DPRSVATD);
				}

				row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER
					.DRATE4 -
					row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS - (row.PRICE_MASTER.DVV093ATD + row.PRICE_MASTER.DVVTILATD) / (row.PRICE_MASTER
						.DPRSVATD);

				row.PRICE_MASTER.PRICE3 = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE3);
				row.PRICE_MASTER.PRICE3 = parseFloat(row.PRICE_MASTER.PRICE3.toFixed(2));

				row.PRICE_MASTER.PRICE3_EXCEL = row.PRICE_MASTER.PRICE3;

				if (row.PRICE_MASTER.DPRDIVIS !== 0) {
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_EUR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_EUR_KG.toFixed(2));
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = row.PRICE_MASTER.DPRSCPATC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.CURR_PRICE_CURR_KG = parseFloat(row.PRICE_MASTER.CURR_PRICE_CURR_KG.toFixed(2));
					row.PRICE_MASTER.MAT_COST_CURR_KG = row.PRICE_MASTER.DPRSCEXTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.MAT_COST_CURR_KG = parseFloat(row.PRICE_MASTER.MAT_COST_CURR_KG.toFixed(2));
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = row.PRICE_MASTER.DCOSTINCTC / row.PRICE_MASTER.DPRDIVIS;
					row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = parseFloat(row.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG.toFixed(2));
				}

				if ((row.PRICE_MASTER.DPRSVATD) !== 0) {
					row.PRICE_MASTER.GM_EUR_KG = (row.PRICE_MASTER.DPRSGMATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD);

					row.PRICE_MASTER.CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1ATD) / (row.PRICE_MASTER.DPRSVATD);

					row.PRICE_MASTER.gapToTargetValDetail = row.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE;

					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "";
				} else {
					row.PRICE_MASTER.NOTVALIDGM_EUR_KG = "---";
					row.PRICE_MASTER.NOTVALIDCM1_EUR_KG = "---";
					row.PRICE_MASTER.gapToTargetValDetail = 0;
					row.PRICE_MASTER.GM_EUR_KG = 0;
					row.PRICE_MASTER.CM1_EUR_KG = 0;
				}

				row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
				row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				if ((row.PRICE_MASTER.DPRSVATD) !== 0) {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSATD) / (row.PRICE_MASTER.DPRSVATD);

					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;

					row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "";
					row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "";
				} else {
					row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = (row.PRICE_MASTER.DPRSRSGSATD) / (row.PRICE_MASTER.DPRSVATD);
					row.PRICE_MASTER.CURR_PRICE_AVERAGE = row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5 *
						parseFloat(row.PRICE_MASTER.DCONDUNIT) * row.PRICE_MASTER.DUNITQ;
					if (isNaN(row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE)) {
						row.PRICE_MASTER.EUR_KG_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_EUR_KG_PRICE_AVERAGE = "---";
					}
					if (isNaN(row.PRICE_MASTER.CURR_PRICE_AVERAGE)) {
						row.PRICE_MASTER.CURR_PRICE_AVERAGE = 0;
						row.PRICE_MASTER.NOTVALID_CURR_PRICE_AVERAGE = "---";
					}
				}

				row = this._calculatePricesWithNewPrice(row);
				//Red Position (forecast) and Forecasted CM1 (%) and  Forecasted Gap to Target (%)
				if ((row.PRICE_MASTER.CM1_EUR_KG_VALUE - row.PRICE_MASTER.costChangeGrossEURKG) < 0) {
					row.PRICE_MASTER.RED_POS_FC = "X";
				} else {
					row.PRICE_MASTER.RED_POS_FC = "X";
				}

				row.PRICE_MASTER.PERC_CM1_FC = ((row.PRICE_MASTER.CM1_EUR_KG_VALUE - row.PRICE_MASTER.costChangeGrossEURKG) / row.PRICE_MASTER.DPRSNEXATD) *
					100;

				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = row.PRICE_MASTER.PERC_CM1_FC - row.PRICE_MASTER.DPRSTMPRO;
				row.PRICE_MASTER.PERC_CM1_FC = this._setInitialValueNeeded(row.PRICE_MASTER.PERC_CM1_FC);
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = this._setInitialValueNeeded(row.PRICE_MASTER.GAPTOTARGET_PERC_FC);
				row.PRICE_MASTER.PERC_CM1_FC = parseFloat(row.PRICE_MASTER.PERC_CM1_FC.toFixed(2));
				row.PRICE_MASTER.GAPTOTARGET_PERC_FC = parseFloat(row.PRICE_MASTER.GAPTOTARGET_PERC_FC.toFixed(2));

				row.PRICE_MASTER.GAPTOTARGET_PERC_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_PERC;
				row.PRICE_MASTER.GAPTOTARGET_VALUE_EXCEL = row.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE === "---" ? 0 : row.PRICE_MASTER.GAPTOTARGET_VALUE;

			}
			return row;
		},

		_calculatePricesWithNewPrice: function (row) {
			//condition 4.2
			if (row.PRICE_MASTER.DPRSVVJ > 0) {
				/*************************************Price increase in % and  Price increase in abs.***************************************************/
				if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0) {
					row.PRICE_MASTER.PRICE_INCREASE_IN = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) / row.PRICE_MASTER.DPRSCPAIN -
						1) * 100;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) - row.PRICE_MASTER.DPRSCPAIN) /
						parseFloat(row
							.PRICE_MASTER
							.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
						row.NEW_PRICE.TARGET_VOLUME;
				} else {
					row.PRICE_MASTER.PRICE_INCREASE_IN = 0;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = 0;
				}

				row.PRICE_MASTER.PRICE_INCREASE_IN = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN);
				row.PRICE_MASTER.PRICE_INCREASE_IN = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN.toFixed(2));

				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS);
				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS.toFixed(2));

				var calculatedValue1 = (row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT)) / row.PRICE_MASTER.DUNITQ;
				if (calculatedValue1 === Infinity || isNaN(calculatedValue1)) {
					calculatedValue1 = 0;
				}

				var calculatedValue5 = calculatedValue1 - (((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ)) /
					row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) - (((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER
					.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) - (
					((row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER.DPRSVVJ)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5);
				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5);
				calculatedValue5 = parseFloat(calculatedValue5.toFixed(2));
				row.NEW_PRICE.NEW_PRICE_NET_EXW = calculatedValue5;

				/***********New Price planned €/kg*******************/
				var calculatedValue2 = calculatedValue1 * row.PRICE_MASTER.DRATE3;

				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				//***************eksik
				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP7 * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER
						.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ)) - ((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) /
						(row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PPY) / (
						row.PRICE_MASTER.DPRSVVJ);
				} else {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP4 * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER
						.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ)) - ((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) /
						(row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PPY) / (
						row.PRICE_MASTER.DPRSVVJ);
				}
				//**********

				row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST = calculatedValue2 - ((((row.PRICE_MASTER.DPRSNEXPY - row.PRICE_MASTER.DRPSFRTPY) * row.PRICE_MASTER
							.DPRSVVJ) / (row.PRICE_MASTER
							.DPRSVVJ)) - (row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER.DPRSVVJ) - (row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER.DPRSVVJ) /
						(row.PRICE_MASTER.DPRSVVJ)) - (((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ)) * (1 + (
						calculatedValue2 - (row.PRICE_MASTER.DPRSRSGSAPY) / (row.PRICE_MASTER.DPRSVVJ)) / (row.PRICE_MASTER.DPRSRSGSAPY) / (row.PRICE_MASTER
						.DPRSVVJ))) - ((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ) + (row.PRICE_MASTER.DFREIGHTINCGC /
						row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PPY) /
					(row.PRICE_MASTER.DPRSVVJ) - (row.PRICE_MASTER.DPRSCEXGC / row.PRICE_MASTER.DPRDIVIS) / (1 - (row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
						.DPRSVVJ + row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER.DPRSRSGSAPY));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST - ((row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER
						.DPRSVVJ) - (row.PRICE_MASTER.DPRSRSCM1PY)) / (row.PRICE_MASTER.DPRSVVJ) - (row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS) /
					(1 - (row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ + row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER.DPRSRSGSAPY));

				if (row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === -Infinity || row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === Infinity || isNaN(
						row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST)) {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = 0;
				} else {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "";
				}

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST.toFixed(2));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER.DRATE4 -
					row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS;

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE);
				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE.toFixed(2));

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
					.DRATE5;

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST);
				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVVJ) > 0) {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1PY) / (row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSSALDEPY *
						row.PRICE_MASTER.DPRSVVJ) * 100;
				} else {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = 0;
				}

				//2- CM1 % last cost
				row = this._setZeroForData(row);

				//3- CM1 % Target Price
				if (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE > 0) {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE / (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE -
						((row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ))) * 100;
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = 0;
					}
				}

				if (calculatedValue2 > 0 && row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE / (calculatedValue2 - ((row.PRICE_MASTER.DPRSSALDEPY *
						row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ))) * 100;
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "";
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "";
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = 0;
					}
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = 0;
				}

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = ((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ));

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.FREIGHT_AVG_PERKG);
				row.PRICE_MASTER.FREIGHT_AVG_PERKG = parseFloat(row.PRICE_MASTER.FREIGHT_AVG_PERKG.toFixed(2));

			}
			//condition 3.2
			if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) > 0) {
				/*************************************Price increase in % and  Price increase in abs.***************************************************/
				if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0) {
					row.PRICE_MASTER.PRICE_INCREASE_IN = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) / row.PRICE_MASTER.DPRSCPAIN -
						1) * 100;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) - row.PRICE_MASTER.DPRSCPAIN) /
						parseFloat(row
							.PRICE_MASTER
							.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
						row.NEW_PRICE.TARGET_VOLUME;
				} else {
					row.PRICE_MASTER.PRICE_INCREASE_IN = 0;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = 0;
				}

				row.PRICE_MASTER.PRICE_INCREASE_IN = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN);
				row.PRICE_MASTER.PRICE_INCREASE_IN = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN.toFixed(2));

				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS);
				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS.toFixed(2));

				var calculatedValue1 = (row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT)) / row.PRICE_MASTER.DUNITQ;
				if (calculatedValue1 === Infinity || isNaN(calculatedValue1)) {
					calculatedValue1 = 0;
				}

				var calculatedValue5 = calculatedValue1 - (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY *
						row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER
						.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY)) / row.PRICE_MASTER.DRATE1 *
					row.PRICE_MASTER
					.DRATE5) - (((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row
							.PRICE_MASTER
							.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) +
						(row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) /
					row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) - (((row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER
						.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY)) / row.PRICE_MASTER.DRATE1 *
					row.PRICE_MASTER.DRATE5);

				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5);
				calculatedValue5 = parseFloat(calculatedValue5.toFixed(2));
				row.NEW_PRICE.NEW_PRICE_NET_EXW = calculatedValue5;

				/***********New Price planned €/kg*******************/
				var calculatedValue2 = calculatedValue1 * row.PRICE_MASTER.DRATE3;

				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				//***************eksik
				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP7 * row.PRICE_MASTER
						.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
						.DPRSVYTDPY)) - ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DRPSFRTYPY *
						row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (row.PRICE_MASTER
						.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER
						.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER
						.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);
				} else {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP4 * row.PRICE_MASTER
						.DRATE4) - ((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
						.DPRSVYTDPY)) - ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ -
						row.PRICE_MASTER.DRPSFRTYPY *
						row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (row.PRICE_MASTER
						.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER
						.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD +
						row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);
				}
				//**********

				row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST = calculatedValue2 - ((((row.PRICE_MASTER.DPRSNEX - row.PRICE_MASTER.DRPSFRT) * row.PRICE_MASTER
							.DPRSVYTD + (row.PRICE_MASTER
								.DPRSNEXPY - row.PRICE_MASTER.DRPSFRTPY) * row.PRICE_MASTER.DPRSVVJ - (row.PRICE_MASTER.DPRSNEXYPY - row.PRICE_MASTER.DRPSFRTYPY) *
							row.PRICE_MASTER.DPRSVYTDPY) /
						(row.PRICE_MASTER.DPRSVYTD +
							row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY)) - (row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER
						.DPRSRSC3PYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) - (row.PRICE_MASTER.DPRSGM *
						row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSGMYPY * row.PRICE_MASTER
						.DPRSVYTDPY
					) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY)) - (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
						.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY
					) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY)) * (1 + (
						calculatedValue2 - (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY) / (row.PRICE_MASTER
							.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY)) / (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY -
						row.PRICE_MASTER.DPRSRSGSAYPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY))) - ((row.PRICE_MASTER
						.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DRPSFRTYPY *
						row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) + (row.PRICE_MASTER
						.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER
						.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER
						.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) - (row.PRICE_MASTER
						.DPRSCEXGC /
						row.PRICE_MASTER.DPRDIVIS) /
					(1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
						.DPRSVVJ + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY - row.PRICE_MASTER.DPRSRSC3PYPY
					) / (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST - ((row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER
							.DPRSVYTD + row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSGMYPY * row.PRICE_MASTER.DPRSVYTDPY) -
						(
							row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY)) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER
						.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) - (row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS) /
					(1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER
						.DPRSVVJ + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY - row.PRICE_MASTER.DPRSRSC3PYPY
					) / (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY));

				if (row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === -Infinity || row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === Infinity || isNaN(
						row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST)) {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = 0;
				} else {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "";
				}

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST.toFixed(2));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER.DRATE4 -
					row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS;

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE);
				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE.toFixed(2));

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
					.DRATE5;

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST);
				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) > 0) {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY) /
						(
							row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY - row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
							.DPRSVYTD - row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ + row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY
						) * 100;
				} else {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = 0;
				}

				//2- CM1 % last cost
				row = this._setZeroForData(row);

				//3- CM1 % Target Price
				if (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE > 0) {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE / (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE -
						((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER
							.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY))
					) * 100;
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = 0;
					}
				}

				if (calculatedValue2 > 0 && row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE / (calculatedValue2 - ((row.PRICE_MASTER.DPRSSALDEC *
							row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER
							.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY))) *
						100;
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "";
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "";
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = 0;
					}
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = 0;
				}

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER
					.DPRSVVJ - row.PRICE_MASTER.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ -
					row.PRICE_MASTER.DPRSVYTDPY));

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.FREIGHT_AVG_PERKG);
				row.PRICE_MASTER.FREIGHT_AVG_PERKG = parseFloat(row.PRICE_MASTER.FREIGHT_AVG_PERKG.toFixed(2));

			}
			//condition 2.2
			if (row.PRICE_MASTER.DPRSVYTD > 0) {
				/*************************************Price increase in % and  Price increase in abs.***************************************************/
				if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0) {
					row.PRICE_MASTER.PRICE_INCREASE_IN = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) / row.PRICE_MASTER.DPRSCPAIN -
						1) * 100;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) - row.PRICE_MASTER.DPRSCPAIN) /
						parseFloat(row
							.PRICE_MASTER
							.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
						row.NEW_PRICE.TARGET_VOLUME;
				} else {
					row.PRICE_MASTER.PRICE_INCREASE_IN = 0;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = 0;
				}

				row.PRICE_MASTER.PRICE_INCREASE_IN = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN);
				row.PRICE_MASTER.PRICE_INCREASE_IN = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN.toFixed(2));

				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS);
				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS.toFixed(2));

				var calculatedValue1 = (row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT)) / row.PRICE_MASTER.DUNITQ;
				if (calculatedValue1 === Infinity || isNaN(calculatedValue1)) {
					calculatedValue1 = 0;
				}

				var calculatedValue5 = calculatedValue1 - (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD)) /
					row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) - (((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER
						.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) /
					row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) - (((row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER.DPRSVYTD)) / row.PRICE_MASTER
					.DRATE1 * row.PRICE_MASTER.DRATE5);
				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5);
				calculatedValue5 = parseFloat(calculatedValue5.toFixed(2));
				row.NEW_PRICE.NEW_PRICE_NET_EXW = calculatedValue5;

				/***********New Price planned €/kg*******************/
				var calculatedValue2 = calculatedValue1 * row.PRICE_MASTER.DRATE3;

				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				//***************eksik
				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP7 * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER
						.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD)) - ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) /
						(row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3P) / (row
						.PRICE_MASTER.DPRSVYTD);
				} else {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP4 * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER
						.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD)) - ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) /
						(row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3P) / (row
						.PRICE_MASTER.DPRSVYTD);
				}
				//**********

				row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST = calculatedValue2 - ((((row.PRICE_MASTER.DPRSNEX - row.PRICE_MASTER.DRPSFRT) * row.PRICE_MASTER
							.DPRSVYTD) / (row.PRICE_MASTER
							.DPRSVYTD)) - (row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER.DPRSVYTD) - (row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER.DPRSVYTD) /
						(row.PRICE_MASTER.DPRSVYTD)) - (((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD)) * (1 +
						(
							calculatedValue2 - (row.PRICE_MASTER.DPRSRSGSA) / (row.PRICE_MASTER.DPRSVYTD)) / (row.PRICE_MASTER.DPRSRSGSA) / (row.PRICE_MASTER
							.DPRSVYTD))) - ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD) + (row.PRICE_MASTER.DFREIGHTINCGC /
						row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3P) /
					(row.PRICE_MASTER.DPRSVYTD) - (row.PRICE_MASTER.DPRSCEXGC / row.PRICE_MASTER.DPRDIVIS) / (1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER
						.DPRSVYTD + row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER.DPRSRSGSA));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST - ((row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER
						.DPRSVYTD) - (row.PRICE_MASTER.DPRSRSCM1)) / (row.PRICE_MASTER.DPRSVYTD) - (row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS) /
					(1 - (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER.DPRSRSGSA));

				if (row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === -Infinity || row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === Infinity || isNaN(
						row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST)) {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = 0;
				} else {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "";
				}

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST.toFixed(2));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER.DRATE4 -
					row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS;

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE);
				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE.toFixed(2));

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
					.DRATE5;

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST);
				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVYTD) > 0) {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1) / (row.PRICE_MASTER.DPRSRSGSA - row.PRICE_MASTER.DPRSSALDEC * row
						.PRICE_MASTER
						.DPRSVYTD) * 100;
				} else {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = 0;
				}

				//2- CM1 % last cost
				row = this._setZeroForData(row);

				//3- CM1 % Target Price
				if (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE > 0) {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE / (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE -
						((row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD))) * 100;
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = 0;
					}
				}

				if (calculatedValue2 > 0 && row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE / (calculatedValue2 - ((row.PRICE_MASTER.DPRSSALDEC *
						row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD))) * 100;
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "";
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "";
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = 0;
					}
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = 0;
				}

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD));

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.FREIGHT_AVG_PERKG);
				row.PRICE_MASTER.FREIGHT_AVG_PERKG = parseFloat(row.PRICE_MASTER.FREIGHT_AVG_PERKG.toFixed(2));

			}
			//condition 1.2
			if (row.PRICE_MASTER.DPRSVATD > 0) {
				/*************************************Price increase in % and  Price increase in abs.***************************************************/
				if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0) {
					row.PRICE_MASTER.PRICE_INCREASE_IN = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) / row.PRICE_MASTER.DPRSCPAIN -
						1) * 100;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = (parseFloat(row.NEW_PRICE.NEW_PRICE_PLAN) - row.PRICE_MASTER.DPRSCPAIN) /
						parseFloat(row
							.PRICE_MASTER
							.DCONDUNIT) / row.PRICE_MASTER.DUNITQ *
						row.NEW_PRICE.TARGET_VOLUME;
				} else {
					row.PRICE_MASTER.PRICE_INCREASE_IN = 0;
					row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = 0;
				}

				row.PRICE_MASTER.PRICE_INCREASE_IN = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN);
				row.PRICE_MASTER.PRICE_INCREASE_IN = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN.toFixed(2));

				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = this._setInitialValueNeeded(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS);
				row.PRICE_MASTER.PRICE_INCREASE_IN_ABS = parseFloat(row.PRICE_MASTER.PRICE_INCREASE_IN_ABS.toFixed(2));

				var calculatedValue1 = (row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT)) / row.PRICE_MASTER.DUNITQ;
				if (calculatedValue1 === Infinity || isNaN(calculatedValue1)) {
					calculatedValue1 = 0;
				}

				var calculatedValue5 = calculatedValue1 - (((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD)) /
					row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5) - (((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER
					.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5);
				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5) - (((row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER.DPRSVATD)) /
					row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER.DRATE5);
				calculatedValue5 = parseFloat(calculatedValue5.toFixed(2));
				row.NEW_PRICE.NEW_PRICE_NET_EXW = calculatedValue5;

				/***********New Price planned €/kg*******************/
				var calculatedValue2 = calculatedValue1 * row.PRICE_MASTER.DRATE3;

				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				//***************eksik
				if (row.PRICE_MASTER.DISTR_CHAN !== "60") {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP7 * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER
						.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD)) - ((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) /
						(row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PATD) / (
						row.PRICE_MASTER.DPRSVATD);
				} else {
					row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = calculatedValue2 - (row.PRICE_MASTER.DMATCSTZZP4 * row.PRICE_MASTER.DRATE4) - ((row.PRICE_MASTER
						.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD)) - ((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) /
						(row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC / row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PATD) / (
						row.PRICE_MASTER.DPRSVATD);
				}
				//**********

				row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST = calculatedValue2 - ((((row.PRICE_MASTER.DPRSNEXATD - row.PRICE_MASTER.DRPSFRTATD) * row.PRICE_MASTER
							.DPRSVATD) / (row.PRICE_MASTER
							.DPRSVATD)) - (row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER.DPRSVATD) - (row.PRICE_MASTER.DPRSGMATD * row.PRICE_MASTER.DPRSVATD) /
						(row.PRICE_MASTER.DPRSVATD)) - (((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD)) * (1 +
						(calculatedValue2 - (row.PRICE_MASTER.DPRSRSGSATD) / (row.PRICE_MASTER.DPRSVATD)) / (row.PRICE_MASTER.DPRSRSGSATD) / (row.PRICE_MASTER
							.DPRSVATD))) - ((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD) + (row.PRICE_MASTER.DFREIGHTINCGC /
						row.PRICE_MASTER.DPRDIVIS)) - (row.PRICE_MASTER.DPRSRSC3PATD) /
					(row.PRICE_MASTER.DPRSVATD) - (row.PRICE_MASTER.DPRSCEXGC / row.PRICE_MASTER.DPRDIVIS) / (1 - (row.PRICE_MASTER.DPRSSALDECAT *
						row.PRICE_MASTER
						.DPRSVATD + row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER.DPRSRSGSATD));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST - ((row.PRICE_MASTER.DPRSGMATD * row.PRICE_MASTER
						.DPRSVATD) - (row.PRICE_MASTER.DPRSRSCM1ATD)) / (row.PRICE_MASTER.DPRSVATD) - (row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS) /
					(1 - (row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD + row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER.DPRSRSGSATD));

				if (row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === -Infinity || row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST === Infinity || isNaN(
						row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST)) {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = 0;
				} else {
					row.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST = "";
				}

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST.toFixed(2));

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.GM_EUR_KG_NEW_PRICE - row.PRICE_MASTER.DPROCSTZZP7 * row.PRICE_MASTER.DRATE4 -
					row.PRICE_MASTER.DCOSTINCGC / row.PRICE_MASTER.DPRDIVIS;

				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE);
				row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE.toFixed(2));

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / row.PRICE_MASTER.DRATE1 * row.PRICE_MASTER
					.DRATE5;

				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST);
				row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = parseFloat(row.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST.toFixed(2));

				if ((row.PRICE_MASTER.DPRSVATD) > 0) {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = (row.PRICE_MASTER.DPRSRSCM1ATD) / (row.PRICE_MASTER.DPRSRSGSATD - row.PRICE_MASTER.DPRSSALDECAT *
						row.PRICE_MASTER.DPRSVATD) * 100;
				} else {
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG = 0;
				}

				//2- CM1 % last cost
				row = this._setZeroForData(row);

				//3- CM1 % Target Price
				if (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE > 0) {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE / (row.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE -
						((row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD))) * 100;
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = 0;
					}
				}

				if (calculatedValue2 > 0 && row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE / (calculatedValue2 - ((row.PRICE_MASTER.DPRSSALDECAT *
						row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD))) * 100;
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "";
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "";
				} else {
					if (row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE !== "---") {
						row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "---";
						row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE = 0;
					}
					row.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE_HIST = "---";
					row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = 0;
				}

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = ((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD));

				row.PRICE_MASTER.FREIGHT_AVG_PERKG = this._setInitialValueNeeded(row.PRICE_MASTER.FREIGHT_AVG_PERKG);
				row.PRICE_MASTER.FREIGHT_AVG_PERKG = parseFloat(row.PRICE_MASTER.FREIGHT_AVG_PERKG.toFixed(2));

			}

			return row;
		},

		_calculateNewPrices: function (formattedValue, activeLine) {
			var calculatedValue1 = 0, // New price planned curr/kg
				calculatedValue2 = 0, // New Price planned €/kg
				calculatedValue3 = 0, // CM1 curr kg planned
				calculatedValue4 = 0, // CM1 % planned
				calculatedValue5 = 0; //New price planned net exw curr/kg  
			//condition 4.3
			if (activeLine.PRICE_MASTER.DPRSVVJ > 0) {
				/***********New price planned curr/kg*******************/
				calculatedValue1 = (parseFloat(formattedValue) / parseFloat(activeLine.PRICE_MASTER.DCONDUNIT)) / activeLine.PRICE_MASTER.DUNITQ;

				calculatedValue1 = this._setInitialValueNeeded(calculatedValue1);

				calculatedValue5 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER
					.DPRSVVJ)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRTPY *
						activeLine.PRICE_MASTER
						.DPRSVVJ) / (activeLine.PRICE_MASTER.DPRSVVJ) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER.DPRSRSC3PPY / activeLine.PRICE_MASTER.DPRSVVJ /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5);
				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5);

				/***********New Price planned €/kg*******************/
				calculatedValue2 = calculatedValue1 * activeLine.PRICE_MASTER.DRATE3;
				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				if (activeLine.PRICE_MASTER.DISTR_CHAN === "60") {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER
							.DPRSVVJ)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRTPY *
								activeLine
								.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER.DPRSVVJ) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) /
							activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) -
						(activeLine.PRICE_MASTER.DPRSRSC3PPY / activeLine.PRICE_MASTER.DPRSVVJ / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER.DMATCSTZZP4 + activeLine.PRICE_MASTER.DPROCSTZZP4) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER
							.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				} else {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER
							.DPRSVVJ)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRTPY *
								activeLine
								.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER.DPRSVVJ) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) /
							activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) -
						(activeLine.PRICE_MASTER.DPRSRSC3PPY / activeLine.PRICE_MASTER.DPRSVVJ / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER.DMATCSTZZP7 + activeLine.PRICE_MASTER.DPROCSTZZP7) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER
							.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				}

				var div = 0;
				div = (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER.DPRSVVJ)) /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5)) * 100;
				if (div !== 0)
					calculatedValue4 = calculatedValue3 / (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ) /
						(activeLine.PRICE_MASTER.DPRSVVJ)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5)) * 100;

				calculatedValue3 = this._setInitialValueNeeded(calculatedValue3);
				calculatedValue4 = this._setInitialValueNeeded(calculatedValue4);

				activeLine.NEW_PRICE.NEW_PRICE_PLAN_K = parseFloat(calculatedValue1).toFixed(3);
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_K_EXCEL = parseFloat(calculatedValue1).toFixed(2);
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_E_KG = parseFloat(calculatedValue2).toFixed(3);

				activeLine = this._calculatePricesWithNewPrice(activeLine);

				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / (calculatedValue2 - (
					(
						activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER.DPRSVVJ))) * 100;

				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST);
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST;
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = parseFloat(activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST.toFixed(1));

				// if (activeLine.PRICE_MASTER.DPRSVVJ > 0 && activeLine.PRICE_MASTER.DPRSRSGSAPY > 0 &&
				// 	(activeLine.PRICE_MASTER.DPRSRSGSAPY - activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ > 0) && activeLine.PRICE_MASTER
				// 	.PERC_CM1_EUR_KG_NEW_PRICE_HIST !== 0) {
				// 	activeLine.NEW_PRICE.ZRENEWAL_PRICE =
				// 		(((activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / (activeLine.NEW_PRICE.ZAPPRCM1EXP / 100 - 1) +
				// 				(-
				// 					((((
				// 						activeLine.PRICE_MASTER.DPRSNEXPY - activeLine.PRICE_MASTER.DRPSFRTPY) * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER
				// 						.DPRSVVJ)) - (activeLine.PRICE_MASTER
				// 						.DPRSRSC3PPY) / (activeLine.PRICE_MASTER.DPRSVVJ) - (activeLine.PRICE_MASTER.DPRSGMPY * activeLine.PRICE_MASTER.DPRSVVJ) / (
				// 						activeLine.PRICE_MASTER.DPRSVVJ)) - ((activeLine.PRICE_MASTER.DRPSFRTPY * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER
				// 						.DPRSVVJ) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) - (activeLine.PRICE_MASTER.DPRSRSC3PPY) /
				// 					(activeLine.PRICE_MASTER.DPRSVVJ) - (activeLine.PRICE_MASTER.DPRSCEXGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - (activeLine.PRICE_MASTER
				// 						.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ + activeLine.PRICE_MASTER.DPRSRSC3PPY) / (activeLine.PRICE_MASTER.DPRSRSGSAPY)) -
				// 					((activeLine.PRICE_MASTER.DPRSGMPY * activeLine.PRICE_MASTER.DPRSVVJ) - (activeLine.PRICE_MASTER.DPRSRSCM1PY)) / (activeLine.PRICE_MASTER
				// 						.DPRSVVJ) - (activeLine.PRICE_MASTER.DCOSTINCGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - (activeLine.PRICE_MASTER.DPRSSALDEPY *
				// 						activeLine.PRICE_MASTER.DPRSVVJ + activeLine.PRICE_MASTER.DPRSRSC3PPY) / (activeLine.PRICE_MASTER.DPRSRSGSAPY))) / (activeLine
				// 					.NEW_PRICE.ZAPPRCM1EXP / 100 - 1) / activeLine.PRICE_MASTER.DRATE3) * parseFloat(activeLine.PRICE_MASTER.DCONDUNIT) *
				// 			activeLine.PRICE_MASTER
				// 			.DUNITQ);
				// }

				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = ((1 / ((activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST) +
				// 		((activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ) / (activeLine.PRICE_MASTER.DPRSVVJ))) / activeLine.PRICE_MASTER
				// 	.DRATE3) * (parseFloat(activeLine.PRICE_MASTER.DCONDUNIT) * activeLine.PRICE_MASTER.DUNITQ);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = this._setInitialValueNeeded(activeLine.NEW_PRICE.ZRENEWAL_PRICE);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = parseFloat(activeLine.NEW_PRICE.ZRENEWAL_PRICE.toFixed(2));

				if (activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN !== "---")
					activeLine.NEW_PRICE.CM1_CURR_KG_PLAN = parseFloat(calculatedValue3).toFixed(3);
				if (activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED !== "---")
					activeLine.NEW_PRICE.CM1_PER_PLANNED = parseFloat(calculatedValue4).toFixed(1);

			}
			//condition 3.3
			if ((activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) > 0) {
				/***********New price planned curr/kg*******************/
				calculatedValue1 = (parseFloat(formattedValue) / parseFloat(activeLine.PRICE_MASTER.DCONDUNIT)) / activeLine.PRICE_MASTER.DUNITQ;

				calculatedValue1 = this._setInitialValueNeeded(calculatedValue1);

				calculatedValue5 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER
							.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) /
						(
							activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) / activeLine.PRICE_MASTER
					.DRATE1 * activeLine.PRICE_MASTER
					.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRT * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DRPSFRTPY *
						activeLine.PRICE_MASTER
						.DPRSVVJ - activeLine.PRICE_MASTER.DRPSFRTYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD +
						activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER
						.DPRDIVIS)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
					.DRATE5) - (((activeLine.PRICE_MASTER.DPRSRSC3P + activeLine.PRICE_MASTER.DPRSRSC3PPY - activeLine.PRICE_MASTER
						.DPRSRSC3PYPY) / (activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5);

				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5);

				/***********New Price planned €/kg*******************/
				calculatedValue2 = calculatedValue1 * activeLine.PRICE_MASTER.DRATE3;
				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				if (activeLine.PRICE_MASTER.DISTR_CHAN === "60") {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER
									.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) /
								(
									activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) / activeLine.PRICE_MASTER
							.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRT * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DRPSFRTPY *
								activeLine
								.PRICE_MASTER
								.DPRSVVJ - activeLine.PRICE_MASTER.DRPSFRTYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD +
								activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER
								.DPRDIVIS)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER.DPRSRSC3P + activeLine.PRICE_MASTER.DPRSRSC3PPY - activeLine.PRICE_MASTER.DPRSRSC3PYPY) / (activeLine.PRICE_MASTER
								.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER
							.DMATCSTZZP4 + activeLine.PRICE_MASTER.DPROCSTZZP4) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				} else {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER
									.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) /
								(
									activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) / activeLine.PRICE_MASTER
							.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRT * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DRPSFRTPY *
								activeLine
								.PRICE_MASTER
								.DPRSVVJ - activeLine.PRICE_MASTER.DRPSFRTYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD +
								activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER
								.DPRDIVIS)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER.DPRSRSC3P + activeLine.PRICE_MASTER.DPRSRSC3PPY - activeLine.PRICE_MASTER.DPRSRSC3PYPY) / (activeLine.PRICE_MASTER
								.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER
							.DMATCSTZZP7 + activeLine.PRICE_MASTER.DPROCSTZZP7) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				}

				var div = 0;
				div = (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSSALDEPY *
						activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER
						.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) / activeLine.PRICE_MASTER.DRATE1 *
					activeLine.PRICE_MASTER.DRATE5)) * 100;
				if (div !== 0)
					calculatedValue4 = calculatedValue3 / (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD +
							activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER
							.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) /
						activeLine.PRICE_MASTER.DRATE1 *
						activeLine.PRICE_MASTER.DRATE5)) * 100;

				calculatedValue3 = this._setInitialValueNeeded(calculatedValue3);
				calculatedValue4 = this._setInitialValueNeeded(calculatedValue4);

				activeLine.NEW_PRICE.NEW_PRICE_PLAN_K = parseFloat(calculatedValue1).toFixed(3);
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_K_EXCEL = parseFloat(calculatedValue1).toFixed(2);
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_E_KG = parseFloat(calculatedValue2).toFixed(3);

				activeLine = this._calculatePricesWithNewPrice(activeLine);

				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / (calculatedValue2 -
					((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER
						.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD +
						activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY))) * 100;

				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST);
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST;
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = parseFloat(activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST.toFixed(1));

				// if (activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY > 0 &&
				// 	(activeLine.PRICE_MASTER.DPRSRSGSA + activeLine.PRICE_MASTER.DPRSRSGSAPY - activeLine.PRICE_MASTER.DPRSRSGSAYPY -
				// 		activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD - activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER
				// 		.DPRSVVJ +
				// 		activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY > 0) && (activeLine.PRICE_MASTER.DPRSRSGSA + activeLine
				// 		.PRICE_MASTER.DPRSRSGSAPY - activeLine.PRICE_MASTER.DPRSRSGSAYPY > 0) &&
				// 	activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST !== 0) {
				// 	activeLine.NEW_PRICE.ZRENEWAL_PRICE =
				// 		((((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER
				// 				.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD +
				// 				activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) * activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / (activeLine.NEW_PRICE
				// 				.ZAPPRCM1EXP / 100 - 1) + (-((((activeLine.PRICE_MASTER.DPRSNEX - activeLine.PRICE_MASTER.DRPSFRT) * activeLine.PRICE_MASTER.DPRSVYTD +
				// 					(activeLine.PRICE_MASTER.DPRSNEXPY - activeLine.PRICE_MASTER.DRPSFRTPY) *
				// 					activeLine.PRICE_MASTER.DPRSVVJ - (activeLine.PRICE_MASTER.DPRSNEXYPY - activeLine.PRICE_MASTER.DRPSFRTYPY) * activeLine.PRICE_MASTER
				// 					.DPRSVYTDPY) / (activeLine.PRICE_MASTER
				// 					.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) - (activeLine.PRICE_MASTER.DPRSRSC3P +
				// 					activeLine.PRICE_MASTER.DPRSRSC3PPY - activeLine.PRICE_MASTER.DPRSRSC3PYPY) / (activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER
				// 					.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) - (activeLine.PRICE_MASTER.DPRSGM * activeLine.PRICE_MASTER.DPRSVYTD +
				// 					activeLine.PRICE_MASTER.DPRSGMPY * activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSGMYPY * activeLine.PRICE_MASTER
				// 					.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY)) - ((
				// 					activeLine.PRICE_MASTER.DRPSFRT * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DRPSFRTPY * activeLine.PRICE_MASTER
				// 					.DPRSVVJ - activeLine.PRICE_MASTER.DRPSFRTYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD +
				// 					activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER
				// 					.DPRDIVIS)) - (activeLine.PRICE_MASTER.DPRSRSC3P + activeLine.PRICE_MASTER.DPRSRSC3PPY - activeLine.PRICE_MASTER.DPRSRSC3PYPY) /
				// 				(activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) - (activeLine.PRICE_MASTER
				// 					.DPRSCEXGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - ((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD +
				// 					activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER
				// 					.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) + (
				// 					activeLine.PRICE_MASTER.DPRSRSC3P + activeLine.PRICE_MASTER.DPRSRSC3PPY - activeLine.PRICE_MASTER.DPRSRSC3PYPY)) / (
				// 					activeLine.PRICE_MASTER.DPRSRSGSA + activeLine.PRICE_MASTER.DPRSRSGSAPY - activeLine.PRICE_MASTER.DPRSRSGSAYPY)) - ((
				// 						activeLine.PRICE_MASTER.DPRSGM * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSGMPY * activeLine.PRICE_MASTER
				// 						.DPRSVVJ - activeLine.PRICE_MASTER.DPRSGMYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) -
				// 					(activeLine.PRICE_MASTER.DPRSRSCM1 + activeLine.PRICE_MASTER.DPRSRSCM1PY - activeLine.PRICE_MASTER.DPRSRSCM1YPY)) / (
				// 					activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY) - (activeLine.PRICE_MASTER
				// 					.DCOSTINCGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - ((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD +
				// 						activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER
				// 						.DPRSVYTDPY) + (activeLine.PRICE_MASTER.DPRSRSC3P + activeLine.PRICE_MASTER.DPRSRSC3PPY - activeLine.PRICE_MASTER.DPRSRSC3PYPY)) /
				// 					(activeLine.PRICE_MASTER.DPRSRSGSA + activeLine.PRICE_MASTER.DPRSRSGSAPY - activeLine.PRICE_MASTER.DPRSRSGSAYPY))) / (
				// 				activeLine.NEW_PRICE.ZAPPRCM1EXP / 100 - 1) / activeLine.PRICE_MASTER.DRATE3) * parseFloat(activeLine.PRICE_MASTER.DCONDUNIT) *
				// 			activeLine.PRICE_MASTER
				// 			.DUNITQ);

				// }

				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = ((1 / ((activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST) +
				// 	((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSSALDEPY * activeLine.PRICE_MASTER
				// 		.DPRSVVJ - activeLine.PRICE_MASTER.DPRSSALDECYPY * activeLine.PRICE_MASTER.DPRSVYTDPY) / (activeLine.PRICE_MASTER.DPRSVYTD +
				// 		activeLine.PRICE_MASTER.DPRSVVJ - activeLine.PRICE_MASTER.DPRSVYTDPY))) / activeLine.PRICE_MASTER.DRATE3) * (parseFloat(
				// 	activeLine.PRICE_MASTER.DCONDUNIT) * activeLine.PRICE_MASTER.DUNITQ);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = this._setInitialValueNeeded(activeLine.NEW_PRICE.ZRENEWAL_PRICE);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = parseFloat(activeLine.NEW_PRICE.ZRENEWAL_PRICE.toFixed(2));

				if (activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN !== "---")
					activeLine.NEW_PRICE.CM1_CURR_KG_PLAN = parseFloat(calculatedValue3).toFixed(3);
				if (activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED !== "---")
					activeLine.NEW_PRICE.CM1_PER_PLANNED = parseFloat(calculatedValue4).toFixed(1);

			}
			//condition 2.3
			if (activeLine.PRICE_MASTER.DPRSVYTD > 0) {
				/***********New price planned curr/kg*******************/
				calculatedValue1 = (parseFloat(formattedValue) / parseFloat(activeLine.PRICE_MASTER.DCONDUNIT)) / activeLine.PRICE_MASTER.DUNITQ;

				calculatedValue1 = this._setInitialValueNeeded(calculatedValue1);

				calculatedValue5 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER
					.DPRSVYTD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRT * activeLine
						.PRICE_MASTER
						.DPRSVYTD) / (activeLine.PRICE_MASTER.DPRSVYTD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER.DPRSRSC3P / activeLine.PRICE_MASTER.DPRSVYTD /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5);
				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5);

				/***********New Price planned €/kg*******************/
				calculatedValue2 = calculatedValue1 * activeLine.PRICE_MASTER.DRATE3;
				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				if (activeLine.PRICE_MASTER.DISTR_CHAN === "60") {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER
							.DPRSVYTD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRT *
								activeLine.PRICE_MASTER
								.DPRSVYTD) / (activeLine.PRICE_MASTER.DPRSVYTD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) /
							activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (
							activeLine
							.PRICE_MASTER.DPRSRSC3P / activeLine.PRICE_MASTER.DPRSVYTD / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) -
						((activeLine.PRICE_MASTER.DMATCSTZZP4 + activeLine.PRICE_MASTER.DPROCSTZZP4) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER
							.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				} else {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER
							.DPRSVYTD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRT *
								activeLine.PRICE_MASTER
								.DPRSVYTD) / (activeLine.PRICE_MASTER.DPRSVYTD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) /
							activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) -
						(activeLine.PRICE_MASTER.DPRSRSC3P / activeLine.PRICE_MASTER.DPRSVYTD / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER.DMATCSTZZP7 + activeLine.PRICE_MASTER.DPROCSTZZP7) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER
							.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				}

				var div = 0;
				div = (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER.DPRSVYTD)) /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5)) * 100;
				if (div !== 0)
					calculatedValue4 = calculatedValue3 / (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD) /
						(activeLine.PRICE_MASTER.DPRSVYTD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5)) * 100;

				calculatedValue3 = this._setInitialValueNeeded(calculatedValue3);
				calculatedValue4 = this._setInitialValueNeeded(calculatedValue4);

				activeLine.NEW_PRICE.NEW_PRICE_PLAN_K = parseFloat(calculatedValue1).toFixed(3);
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_E_KG = parseFloat(calculatedValue2).toFixed(3);

				activeLine = this._calculatePricesWithNewPrice(activeLine);

				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / (calculatedValue2 - (
					(
						activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER.DPRSVYTD))) * 100;
				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST);
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST;
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = parseFloat(activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST.toFixed(1));

				// if (activeLine.PRICE_MASTER.DPRSVYTD > 0 && activeLine.PRICE_MASTER.DPRSRSGSA > 0 &&
				// 	(activeLine.PRICE_MASTER.DPRSRSGSA - activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD > 0) && activeLine.PRICE_MASTER
				// 	.PERC_CM1_EUR_KG_NEW_PRICE_HIST !== 0) {
				// 	activeLine.NEW_PRICE.ZRENEWAL_PRICE =
				// 		(((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / (activeLine.NEW_PRICE.ZAPPRCM1EXP / 100 - 1) + (-
				// 				((((
				// 						activeLine.PRICE_MASTER.DPRSNEX - activeLine.PRICE_MASTER.DRPSFRT) * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER
				// 						.DPRSVYTD)) -
				// 					(activeLine.PRICE_MASTER
				// 						.DPRSRSC3P) / (activeLine.PRICE_MASTER.DPRSVYTD) - (activeLine.PRICE_MASTER.DPRSGM * activeLine.PRICE_MASTER.DPRSVYTD) / (
				// 						activeLine.PRICE_MASTER.DPRSVYTD)) - ((activeLine.PRICE_MASTER.DRPSFRT * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER
				// 					.DPRSVYTD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) - (activeLine.PRICE_MASTER.DPRSRSC3P) /
				// 				(activeLine.PRICE_MASTER.DPRSVYTD) - (activeLine.PRICE_MASTER.DPRSCEXGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - (activeLine.PRICE_MASTER
				// 					.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSRSC3P) / (activeLine.PRICE_MASTER.DPRSRSGSA)) - (
				// 					(activeLine.PRICE_MASTER.DPRSGM * activeLine.PRICE_MASTER.DPRSVYTD) - (activeLine.PRICE_MASTER.DPRSRSCM1)) / (activeLine.PRICE_MASTER
				// 					.DPRSVYTD) - (activeLine.PRICE_MASTER.DCOSTINCGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - (activeLine.PRICE_MASTER.DPRSSALDEC *
				// 					activeLine.PRICE_MASTER.DPRSVYTD + activeLine.PRICE_MASTER.DPRSRSC3P) / (activeLine.PRICE_MASTER.DPRSRSGSA))) / (activeLine.NEW_PRICE
				// 				.ZAPPRCM1EXP / 100 - 1) / activeLine.PRICE_MASTER.DRATE3) * parseFloat(activeLine.PRICE_MASTER.DCONDUNIT) * activeLine.PRICE_MASTER
				// 			.DUNITQ);
				// }

				// // activeLine.NEW_PRICE.ZRENEWAL_PRICE = ((1 / ((activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST) +
				// // 		((activeLine.PRICE_MASTER.DPRSSALDEC * activeLine.PRICE_MASTER.DPRSVYTD) / (activeLine.PRICE_MASTER.DPRSVYTD))) / activeLine.PRICE_MASTER
				// // 	.DRATE3) * (parseFloat(
				// // 	activeLine.PRICE_MASTER.DCONDUNIT) * activeLine.PRICE_MASTER.DUNITQ);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = this._setInitialValueNeeded(activeLine.NEW_PRICE.ZRENEWAL_PRICE);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = parseFloat(activeLine.NEW_PRICE.ZRENEWAL_PRICE.toFixed(2));

				if (activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN !== "---")
					activeLine.NEW_PRICE.CM1_CURR_KG_PLAN = parseFloat(calculatedValue3).toFixed(3);
				if (activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED !== "---")
					activeLine.NEW_PRICE.CM1_PER_PLANNED = parseFloat(calculatedValue4).toFixed(1);

			}
			//condition 1.3
			if (activeLine.PRICE_MASTER.DPRSVATD > 0) {
				/***********New price planned curr/kg*******************/
				calculatedValue1 = (parseFloat(formattedValue) / parseFloat(activeLine.PRICE_MASTER.DCONDUNIT)) / activeLine.PRICE_MASTER.DUNITQ;

				calculatedValue1 = this._setInitialValueNeeded(calculatedValue1);

				calculatedValue5 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER
					.DPRSVATD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRTATD *
						activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER.DPRSVATD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER
						.DPRDIVIS)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
					.DRATE5) - (activeLine.PRICE_MASTER.DPRSRSC3PATD / activeLine.PRICE_MASTER.DPRSVATD / activeLine.PRICE_MASTER.DRATE1 *
					activeLine.PRICE_MASTER.DRATE5);

				calculatedValue5 = this._setInitialValueNeeded(calculatedValue5);

				/***********New Price planned €/kg*******************/
				calculatedValue2 = calculatedValue1 * activeLine.PRICE_MASTER.DRATE3;
				calculatedValue2 = this._setInitialValueNeeded(calculatedValue2);

				if (activeLine.PRICE_MASTER.DISTR_CHAN === "60") {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER
							.DPRSVATD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRTATD *
								activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER.DPRSVATD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER
								.DPRDIVIS)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) - (activeLine.PRICE_MASTER.DPRSRSC3PATD / activeLine.PRICE_MASTER.DPRSVATD / activeLine.PRICE_MASTER.DRATE1 *
							activeLine
							.PRICE_MASTER.DRATE5) -
						((activeLine.PRICE_MASTER.DMATCSTZZP4 + activeLine.PRICE_MASTER.DPROCSTZZP4) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER
							.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				} else {
					calculatedValue3 = calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER
							.DPRSVATD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5) - (((activeLine.PRICE_MASTER.DRPSFRTATD *
								activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER.DPRSVATD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER
								.DPRDIVIS)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						(activeLine.PRICE_MASTER.DPRSRSC3PATD / activeLine.PRICE_MASTER.DPRSVATD / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER
							.DRATE5) -
						((activeLine.PRICE_MASTER.DMATCSTZZP7 + activeLine.PRICE_MASTER.DPROCSTZZP7) * activeLine.PRICE_MASTER.DRATE5) - (activeLine.PRICE_MASTER
							.DCOSTINCGC /
							activeLine.PRICE_MASTER.DPRDIVIS) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5;
				}

				var div = 0;
				div = (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER.DPRSVATD)) /
					activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5)) * 100;
				if (div !== 0)
					calculatedValue4 = calculatedValue3 / (calculatedValue1 - (((activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD) /
						(activeLine.PRICE_MASTER.DPRSVATD)) / activeLine.PRICE_MASTER.DRATE1 * activeLine.PRICE_MASTER.DRATE5)) * 100;

				calculatedValue3 = this._setInitialValueNeeded(calculatedValue3);
				calculatedValue4 = this._setInitialValueNeeded(calculatedValue4);

				activeLine.NEW_PRICE.NEW_PRICE_PLAN_K = parseFloat(calculatedValue1).toFixed(3);
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_K_EXCEL = parseFloat(calculatedValue1).toFixed(2);
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_E_KG = parseFloat(calculatedValue2).toFixed(3);

				activeLine = this._calculatePricesWithNewPrice(activeLine);

				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST / (calculatedValue2 - (
					(
						activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER.DPRSVATD))) * 100;
				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = this._setInitialValueNeeded(activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST);
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST;
				activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST = parseFloat(activeLine.NEW_PRICE.CM1_PER_PLANNED_HIST.toFixed(1));

				// if (activeLine.PRICE_MASTER.DPRSVATD > 0 && activeLine.PRICE_MASTER.DPRSRSGSATD > 0 && (activeLine.PRICE_MASTER.DPRSRSGSATD -
				// 		activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD > 0) && activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST !==
				// 	0) {
				// 	activeLine.NEW_PRICE.ZRENEWAL_PRICE =
				// 		(((activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / (activeLine.NEW_PRICE.ZAPPRCM1EXP / 100 - 1) +
				// 				(-(((
				// 							(activeLine.PRICE_MASTER.DPRSNEXATD - activeLine.PRICE_MASTER.DRPSFRTATD) * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER
				// 							.DPRSVATD)) - (activeLine.PRICE_MASTER
				// 							.DPRSRSC3PATD) / (activeLine.PRICE_MASTER.DPRSVATD) - (activeLine.PRICE_MASTER.DPRSGMATD * activeLine.PRICE_MASTER.DPRSVATD) /
				// 						(activeLine.PRICE_MASTER.DPRSVATD)) - ((activeLine.PRICE_MASTER.DRPSFRTATD * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER
				// 						.DPRSVATD) + (activeLine.PRICE_MASTER.DFREIGHTINCGC / activeLine.PRICE_MASTER.DPRDIVIS)) - (activeLine.PRICE_MASTER.DPRSRSC3PATD) /
				// 					(activeLine.PRICE_MASTER.DPRSVATD) - (activeLine.PRICE_MASTER.DPRSCEXGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - (activeLine.PRICE_MASTER
				// 						.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD + activeLine.PRICE_MASTER.DPRSRSC3PATD) / (activeLine.PRICE_MASTER.DPRSRSGSATD)) -
				// 					((activeLine.PRICE_MASTER.DPRSGMATD * activeLine.PRICE_MASTER.DPRSVATD) - (activeLine.PRICE_MASTER.DPRSRSCM1ATD)) / (activeLine
				// 						.PRICE_MASTER.DPRSVATD) - (activeLine.PRICE_MASTER.DCOSTINCGC / activeLine.PRICE_MASTER.DPRDIVIS) / (1 - (activeLine.PRICE_MASTER
				// 						.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD + activeLine.PRICE_MASTER.DPRSRSC3PATD) / (activeLine.PRICE_MASTER.DPRSRSGSATD))
				// 				) / (activeLine.NEW_PRICE.ZAPPRCM1EXP / 100 - 1) / activeLine.PRICE_MASTER.DRATE3) * parseFloat(activeLine.PRICE_MASTER.DCONDUNIT) *
				// 			activeLine.PRICE_MASTER
				// 			.DUNITQ);
				// }

				// // activeLine.NEW_PRICE.ZRENEWAL_PRICE = ((1 / ((activeLine.NEW_PRICE.ZAPPRCM1EXP / 100) / activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST) +
				// // 		((activeLine.PRICE_MASTER.DPRSSALDECAT * activeLine.PRICE_MASTER.DPRSVATD) / (activeLine.PRICE_MASTER.DPRSVATD))) / activeLine.PRICE_MASTER
				// // 	.DRATE3) * (parseFloat(
				// // 	activeLine.PRICE_MASTER.DCONDUNIT) * activeLine.PRICE_MASTER.DUNITQ);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = this._setInitialValueNeeded(activeLine.NEW_PRICE.ZRENEWAL_PRICE);
				// activeLine.NEW_PRICE.ZRENEWAL_PRICE = parseFloat(activeLine.NEW_PRICE.ZRENEWAL_PRICE.toFixed(2));

				if (activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN !== "---")
					activeLine.NEW_PRICE.CM1_CURR_KG_PLAN = parseFloat(calculatedValue3).toFixed(3);
				if (activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED !== "---")
					activeLine.NEW_PRICE.CM1_PER_PLANNED = parseFloat(calculatedValue4).toFixed(1);
			}

			return activeLine;
		},

		_setZeroForData: function (activeLine) {

			if (activeLine.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE > 0) {
				activeLine.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE = "";
			} else {
				activeLine.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_TARGET_PRICE = "---";
				activeLine.PRICE_MASTER.PERC_CM1_EUR_KG_TARGET_PRICE = 0;
			}

			if (activeLine.NEW_PRICE.NEW_PRICE_PLAN === 0 || activeLine.NEW_PRICE.NEW_PRICE_PLAN === "0" || activeLine.NEW_PRICE.NEW_PRICE_PLAN ===
				"0.00") {
				activeLine.PRICE_MASTER.GM_EUR_KG_NEW_PRICE = 0;
				activeLine.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST = 0;
				activeLine.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = 0;
				activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE = 0;
				activeLine.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = 0;
				activeLine.NEW_PRICE.NEW_PRICE_PLAN_E_KG = 0;
				activeLine.NEW_PRICE.NEW_PRICE_NET_EXW = 0;
			}

			if (activeLine.PRICE_MASTER.DPRSVFC <= 0) {
				activeLine.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC = "---";
				activeLine.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE = "---";
				activeLine.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE_IN_EUR = "---";
				activeLine.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL = 0;
				activeLine.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL_IN_EUR = 0;
				activeLine.PRICE_MASTER.GAPTOTARGET_PERC_FOR_EXCEL = 0;

			} else {
				activeLine.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC = "";
				activeLine.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE = "";
				activeLine.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE_IN_EUR = "";
				activeLine.PRICE_MASTER.GAPTOTARGET_VALUE = this._setInitialValueNeeded(activeLine.PRICE_MASTER.GAPTOTARGET_VALUE);
				activeLine.PRICE_MASTER.GAPTOTARGET_PERC = this._setInitialValueNeeded(activeLine.PRICE_MASTER.GAPTOTARGET_PERC);
				activeLine.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL = activeLine.PRICE_MASTER.GAPTOTARGET_VALUE;
				activeLine.PRICE_MASTER.GAPTOTARGET_VALUE_FOR_EXCEL_IN_EUR = activeLine.PRICE_MASTER.GAPTOTARGET_VALUE_IN_EUR;
				activeLine.PRICE_MASTER.GAPTOTARGET_PERC_FOR_EXCEL = activeLine.PRICE_MASTER.GAPTOTARGET_PERC;
			}

			if (((activeLine.PRICE_MASTER.DPROCSTZZP8 + activeLine.PRICE_MASTER.DMATCSTZZP8) === 0)) {

			} else {

				activeLine.PRICE_MASTER.NOTVALIDPRICE_EURKG_CURRENT_PRICE = "";
			}

			if (((activeLine.PRICE_MASTER.DPROCSTZZP7 + activeLine.PRICE_MASTER.DMATCSTZZP7) === 0)) {
				activeLine.PRICE_MASTER.NOTVALIDPRICE_EURKG_TARGET_PRICE = "---";
				activeLine.PRICE_MASTER.NOTVALIDGM_EUR_KG_TARGET_PRICE = "---";
				activeLine.PRICE_MASTER.NOTVALIDCM1_EUR_KG_TARGET_PRICE = "---";
				activeLine.PRICE_MASTER.GM_EUR_KG_TARGET_PRICE = 0;
				activeLine.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE = 0;
				if (activeLine.PRICE_MASTER.DPRSVYTD <= 0)
					activeLine.PRICE_MASTER.gapToTargetValDetail = 0;
			} else {
				activeLine.PRICE_MASTER.NOTVALIDPRICE_EURKG_TARGET_PRICE = "";
				activeLine.PRICE_MASTER.NOTVALIDGM_EUR_KG_TARGET_PRICE = "";
				activeLine.PRICE_MASTER.NOTVALIDCM1_EUR_KG_TARGET_PRICE = "";
			}

			if ((activeLine.PRICE_MASTER.DPROCSTZZP4 + activeLine.PRICE_MASTER.DMATCSTZZP4) === 0 && activeLine.PRICE_MASTER.DISTR_CHAN ===
				"60") {
				activeLine.PRICE_MASTER.PRICE3 = 0;
				activeLine.PRICE_MASTER.NOTVALIDPRICE3 = "---";
				activeLine.PRICE_MASTER.PRICE3_EXCEL = 0;
			} else
				activeLine.PRICE_MASTER.NOTVALIDPRICE3 = "";

			if ((activeLine.PRICE_MASTER.DPROCSTZZP7 + activeLine.PRICE_MASTER.DMATCSTZZP7) === 0 && activeLine.PRICE_MASTER.DISTR_CHAN !==
				"60") {
				activeLine.PRICE_MASTER.PRICE3 = 0;
				activeLine.PRICE_MASTER.NOTVALIDPRICE3 = "---";
				activeLine.PRICE_MASTER.PRICE3_EXCEL = 0;
			} else
				activeLine.PRICE_MASTER.NOTVALIDPRICE3 = "";

			if ((activeLine.PRICE_MASTER.DPROCSTZZP4 + activeLine.PRICE_MASTER.DMATCSTZZP4) === 0 && activeLine.PRICE_MASTER.DISTR_CHAN ===
				"60") {
				activeLine.NEW_PRICE.CM1_CURR_KG_PLAN = 0;
				activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN = "---";
			} else
				activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN = "";

			if ((activeLine.PRICE_MASTER.DPROCSTZZP7 + activeLine.PRICE_MASTER.DMATCSTZZP7) === 0 && activeLine.PRICE_MASTER.DISTR_CHAN !==
				"60") {
				activeLine.NEW_PRICE.CM1_CURR_KG_PLAN = 0;
				activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN = "---";
			} else
				activeLine.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN = "";
			if ((activeLine.PRICE_MASTER.DPROCSTZZP4 + activeLine.PRICE_MASTER.DMATCSTZZP4) === 0 && activeLine.PRICE_MASTER.DISTR_CHAN ===
				"60") {
				activeLine.NEW_PRICE.CM1_PER_PLANNED = 0;
				activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED = "---";
			} else
				activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED = "";

			if ((activeLine.PRICE_MASTER.DPROCSTZZP7 + activeLine.PRICE_MASTER.DMATCSTZZP7) === 0 && activeLine.PRICE_MASTER.DISTR_CHAN !==
				"60") {
				activeLine.NEW_PRICE.CM1_PER_PLANNED = 0;
				activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED = "---";
			} else
				activeLine.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED = "";

			if (activeLine.PRICE_MASTER.DMATCSTZZP7 === 0) {
				activeLine.PRICE_MASTER.NOTVALID_CM1_EUR_KG_NEW_PRICE = "---";
				activeLine.PRICE_MASTER.NOTVALID_GM_EUR_KG_NEW_PRICE = "---";
				activeLine.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "---";

			} else {
				activeLine.PRICE_MASTER.NOTVALID_CM1_EUR_KG_NEW_PRICE = "";
				activeLine.PRICE_MASTER.NOTVALID_GM_EUR_KG_NEW_PRICE = "";
				activeLine.PRICE_MASTER.NOTVALID_PERC_CM1_EUR_KG_NEW_PRICE = "";
			}

			return activeLine;

		},

		_setInitialValueNeeded: function (value) {
			if (value === -Infinity || value === Infinity || isNaN(value)) value = 0;
			return value;
		},

		_afterReadBackendData: function (row) {
			var dprsvfc_val, key,
				dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "yyyy-MM-dd"
				}),
				from,
				to,
				timeDiff,
				diffDays,
				dateFormattedFrom,
				dateFormattedTo,
				priceValidFrom = "",
				priceValidTo = "",
				quotFrom = "",
				quotTo = "",
				today,
				date;

			if (row.NEW_PRICE.WORKLOW_STATUS !== "2") {
				row.NEW_PRICE.QUOTE_VALID_FROM = dateFormat.format(new Date());
			}

			row.PRICE_MASTER.CHANGED = false;
			//assign backend field to frontend field
			row.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = row.NEW_PRICE.CM1_PER_PLANNED_HIST;
			row.PRICE_MASTER.DFREIGHTINCTC = 0;
			if (row.PRICE_MASTER.DPRDIVIS !== 0)
				row.PRICE_MASTER.DFREIGHTINCTC = row.PRICE_MASTER.DFREIGHTINCTC / row.PRICE_MASTER.DPRDIVIS;

			row.PRICE_MASTER.DFREIGHTINCTC = parseFloat(row.PRICE_MASTER.DFREIGHTINCTC.toFixed(2));

			//MOQ old determination
			if (row.NEW_PRICE.CUSTOMER === "" || row.NEW_PRICE.MOQ_NEW === 0) {
				row.PRICE_MASTER.MOQ_NEW_OLD = row.PRICE_MASTER.DPRSMOQ;
			} else
				row.PRICE_MASTER.MOQ_NEW_OLD = row.NEW_PRICE.MOQ_NEW;
			//DCONDUNIT to integer
			row.PRICE_MASTER.DCONDUNIT = parseInt(row.PRICE_MASTER.DCONDUNIT).toString();
			//date value format 
			row.NEW_PRICE.PRICING_DEADLINE = row.NEW_PRICE.PRICING_DEADLINE === "    -  -  " ? "0000-00-00" : row.NEW_PRICE
				.PRICING_DEADLINE;
			row.NEW_PRICE.BU_DEADLINE = row.NEW_PRICE.BU_DEADLINE === "    -  -  " ? "0000-00-00" : row.NEW_PRICE
				.BU_DEADLINE;
			//Last Price Change 
			// row.PRICE_MASTER.DAPCHANGEDON = row.PRICE_MASTER.DPRSOURCE === "2" || row.PRICE_MASTER.DPRSOURCE === "6" ||
			// 	row.PRICE_MASTER.DPRSOURCE === "7" ? row.PRICE_MASTER.DCPCHANGEDON : row.PRICE_MASTER.DPRSOURCE === "1" ||
			// 	row.PRICE_MASTER.DPRSOURCE === "5" ? row.PRICE_MASTER.QUOT_FROM : row.PRICE_MASTER.DOPCHANGEDON;

			row.PRICE_MASTER.DAPCHANGEDON = row.PRICE_MASTER.DCHANGEON_ALL;
			if (row.PRICE_MASTER.DAPCHANGEDON === "" || row.PRICE_MASTER.DAPCHANGEDON === "00000000") row.PRICE_MASTER.DAPCHANGEDON_EXCEL = "";
			else row.PRICE_MASTER.DAPCHANGEDON_EXCEL = row.PRICE_MASTER.DAPCHANGEDON;
			row.PRICE_MASTER.DAPCHANGEDON = row.PRICE_MASTER.DAPCHANGEDON === "" ? "00000000" : row.PRICE_MASTER
				.DAPCHANGEDON;
			row.PRICE_MASTER.DAPCHANGEDON_FILTER = row.PRICE_MASTER.DAPCHANGEDON.substring(6, 8) + "." + row.PRICE_MASTER.DAPCHANGEDON.substring(
				4, 6) + "." + row.PRICE_MASTER.DAPCHANGEDON.substring(0, 4);
			row.PRICE_MASTER.QUOT_FROM_FILTER = row.PRICE_MASTER.QUOT_FROM.substring(6, 8) + "." + row.PRICE_MASTER.QUOT_FROM.substring(
				4, 6) + "." + row.PRICE_MASTER.QUOT_FROM.substring(0, 4);

			row.PRICE_MASTER.QUOT_TO_FILTER = row.PRICE_MASTER.QUOT_TO.substring(6, 8) + "." + row.PRICE_MASTER.QUOT_TO.substring(
				4, 6) + "." + row.PRICE_MASTER.QUOT_TO.substring(0, 4);

			//Write Reds
			row.PRICE_MASTER.RED_C = row.PRICE_MASTER.DPRSREDC === "X" ? row.PRICE_MASTER.CUSTOMER : "";
			row.PRICE_MASTER.RED_A = row.PRICE_MASTER.DPRSREDC === "X" ? row.PRICE_MASTER.MATERIAL : "";
			//target Volume
			dprsvfc_val = parseInt(parseFloat(row.PRICE_MASTER.DPRSVFC).toFixed(0));
			row.NEW_PRICE.TARGET_VOLUME = row.NEW_PRICE.TARGET_VOLUME === undefined || row.NEW_PRICE.TARGET_VOLUME ===
				0 || row.NEW_PRICE.TARGET_VOLUME === "" ?
				dprsvfc_val : row.NEW_PRICE.TARGET_VOLUME;

			if (row.NEW_PRICE.BU_ACTION !== "") {
				key = sap.ui.getCore().getModel("buActionDD").getData().filter(function (line) {
					return row.NEW_PRICE.BU_ACTION === line.DOMVALUE_L;
				});
				if (key.length)
					row.PRICE_MASTER.BU_ACTION_T = key[0].DDTEXT;
			} else
				row.PRICE_MASTER.BU_ACTION_T = "";

			if (row.NEW_PRICE.BU_MEASURE_STATUS !== "") {
				key = sap.ui.getCore().getModel("buStatusDD").getData().filter(function (line) {
					return row.NEW_PRICE.BU_MEASURE_STATUS === line.DOMVALUE_L;
				});
				if (key.length)
					row.PRICE_MASTER.BU_MEASURE_STATUS_T = key[0].DDTEXT;
			} else
				row.PRICE_MASTER.BU_MEASURE_STATUS_T = "";

			if (row.NEW_PRICE.PRICING_STATUS !== "") {
				key = sap.ui.getCore().getModel("priceStatusDD").getData().filter(function (line) {
					return row.NEW_PRICE.PRICING_STATUS === line.DOMVALUE_L;
				});
				if (key.length)
					row.PRICE_MASTER.PRICING_STATUS_T = key[0].DDTEXT;
			} else
				row.PRICE_MASTER.PRICING_STATUS_T = "";

			if (row.NEW_PRICE.WORKLOW_STATUS !== "") {
				key = sap.ui.getCore().getModel("workFlowStatusDD").getData().filter(function (line) {
					return row.NEW_PRICE.WORKLOW_STATUS === line.DOMVALUE_L;
				});
				if (key.length)
					row.PRICE_MASTER.WORKLOW_STATUS_T = key[0].DDTEXT;
				if (row.PRICE_MASTER.WORKLOW_STATUS_T === undefined) row.PRICE_MASTER.WORKLOW_STATUS_T = "";
				if (row.PRICE_MASTER.WORKLOW_STATUS_T !== "" && row.NEW_PRICE.WORKLOW_STATUS !== "1")
					row.PRICE_MASTER.WORKLOW_STATUS_TT = row.PRICE_MASTER.WORKLOW_STATUS_T + " by";
			} else
				row.PRICE_MASTER.WORKLOW_STATUS_T = "";

			if (row.NEW_PRICE.ROOT_CAUSE !== "") {
				key = sap.ui.getCore().getModel("rootCauseDD").getData().filter(function (line) {
					return row.NEW_PRICE.ROOT_CAUSE === line.DOMVALUE_L;
				});
				if (key.length)
					row.PRICE_MASTER.ROOT_CAUSE_T = key[0].DDTEXT;
			} else
				row.PRICE_MASTER.ROOT_CAUSE_T = "";

			if (row.NEW_PRICE.PRICE_ACTION !== "") {
				key = sap.ui.getCore().getModel("priceActionDD").getData().filter(function (line) {
					return row.NEW_PRICE.PRICE_ACTION === line.PRICE_ACTION;
				});
				if (key.length)
					row.PRICE_MASTER.PRICE_ACTION_T = key[0].PRICE_ACT_DESC;
			} else
				row.PRICE_MASTER.PRICE_ACTION_T = "";

			quotFrom = row.PRICE_MASTER.QUOT_FROM;
			quotTo = row.PRICE_MASTER.QUOT_TO;

			if (row.PRICE_MASTER.DPCHANGE_ALL > 0)
				row.PRICE_MASTER.PRICE_CHANGE_INDICATOR = "Down";
			if (row.PRICE_MASTER.DPCHANGE_ALL < 0)
				row.PRICE_MASTER.PRICE_CHANGE_INDICATOR = "Up";
			if (row.PRICE_MASTER.DPCHANGE_ALL === 0)
				row.PRICE_MASTER.PRICE_CHANGE_INDICATOR = "";
			if (row.PRICE_MASTER.DCHANGEON_ALL === "00000000" || row.PRICE_MASTER.DCHANGEON_ALL === "")
				row.PRICE_MASTER.PRICE_CHANGE_INDICATOR = "";

			if (quotFrom !== "" || quotFrom !== "00000000") {
				if (row.NEW_PRICE.PRICE_VALID_FROM === "    -  -  " || row.NEW_PRICE.PRICE_VALID_FROM === "0000-00-00") {
					date = new Date(quotTo.substring(0, 4), quotTo.substring(4, 6) - 1, quotTo.substring(6, 8));
					priceValidFrom = new Date(date);
					if (quotTo !== "99991231")
						priceValidFrom.setDate(date.getDate() + 1);
					dateFormattedFrom = dateFormat.format(priceValidFrom);
					row.NEW_PRICE.PRICE_VALID_FROM = dateFormattedFrom;

					date = new Date(dateFormattedFrom.split("-")[0], dateFormattedFrom.split("-")[1] - 1, dateFormattedFrom.split("-")[2]);
					priceValidFrom = new Date(date);
					today = new Date();
					if (date < today) {
						priceValidFrom = today;
						dateFormattedFrom = dateFormat.format(priceValidFrom);
						row.NEW_PRICE.PRICE_VALID_FROM = dateFormattedFrom;
					}
				} else {
					from = row.NEW_PRICE.PRICE_VALID_FROM;
					date = new Date(from.split("-")[0], from.split("-")[1] - 1, from.split("-")[2]);
					priceValidFrom = new Date(date);
					today = new Date();
					if (date < today) {
						priceValidFrom = today;
						dateFormattedFrom = dateFormat.format(priceValidFrom);
						row.NEW_PRICE.PRICE_VALID_FROM = dateFormattedFrom;
					}
				}

				if (row.NEW_PRICE.PRICE_VALID_TO === "    -  -  " || row.NEW_PRICE.PRICE_VALID_TO === "0000-00-00") {
					quotFrom = new Date(quotFrom.substring(0, 4), quotFrom.substring(4, 6) - 1, quotFrom.substring(6, 8));
					quotTo = new Date(quotTo.substring(0, 4), quotTo.substring(4, 6) - 1, quotTo.substring(6, 8));
					priceValidTo = new Date(priceValidFrom);
					if (dateFormat.format(quotTo) !== "9999-12-31") {
						timeDiff = Math.abs(quotTo.getTime() - quotFrom.getTime());
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						if (row.NEW_PRICE.PRICE_VALID_FROM === "2021-01-01" && diffDays === 365)
							diffDays = diffDays - 1;
						priceValidTo.setDate(priceValidTo.getDate() + diffDays);
					}
					var monthNum = priceValidTo.getMonth();
					var stackPriceTo = new Date(priceValidTo);
					var stackPriceTo2 = new Date(priceValidTo);
					stackPriceTo.setDate(stackPriceTo.getDate() + 2);
					stackPriceTo2.setDate(stackPriceTo2.getDate() + 1);
					var monthNum2 = stackPriceTo.getMonth();
					if (monthNum !== monthNum2) {
						if (monthNum !== stackPriceTo2.getMonth()) {
							stackPriceTo2.setDate(stackPriceTo2.getDate() - 1);
							priceValidTo = stackPriceTo2;
						} else {
							stackPriceTo2.setDate(stackPriceTo2.getDate());
							priceValidTo = stackPriceTo2;
						}
					} else {
						stackPriceTo = new Date(priceValidTo);
						stackPriceTo2 = new Date(priceValidTo);
						stackPriceTo.setDate(stackPriceTo.getDate() - 2);
						stackPriceTo2.setDate(stackPriceTo2.getDate() - 1);
						monthNum2 = stackPriceTo.getMonth();
						if (monthNum !== monthNum2) {
							if (monthNum !== stackPriceTo2.getMonth()) {
								priceValidTo = stackPriceTo2;
							} else {
								priceValidTo = stackPriceTo;
							}
						}
					}

					dateFormattedTo = dateFormat.format(priceValidTo);
					row.NEW_PRICE.PRICE_VALID_TO = dateFormattedTo;

					to = row.NEW_PRICE.PRICE_VALID_TO;
					date = new Date(to.split("-")[0], to.split("-")[1] - 1, to.split("-")[2]);
					priceValidTo = new Date(priceValidFrom);
					today = new Date();
					if (date < today) {
						timeDiff = Math.abs(quotTo.getTime() - quotFrom.getTime());
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						if (row.NEW_PRICE.PRICE_VALID_FROM === "2021-01-01" && diffDays === 365)
							diffDays = diffDays - 1;
						priceValidTo.setDate(priceValidTo.getDate() + diffDays);
						var monthNum = priceValidTo.getMonth();
						var stackPriceTo = new Date(priceValidTo);
						var stackPriceTo2 = new Date(priceValidTo);
						stackPriceTo.setDate(stackPriceTo.getDate() + 2);
						stackPriceTo2.setDate(stackPriceTo2.getDate() + 1);
						var monthNum2 = stackPriceTo.getMonth();
						if (monthNum !== monthNum2) {
							if (monthNum !== stackPriceTo2.getMonth()) {
								stackPriceTo2.setDate(stackPriceTo2.getDate() - 1);
								priceValidTo = stackPriceTo2;
							} else {
								stackPriceTo2.setDate(stackPriceTo2.getDate());
								priceValidTo = stackPriceTo2;
							}
						} else {
							stackPriceTo = new Date(priceValidTo);
							stackPriceTo2 = new Date(priceValidTo);
							stackPriceTo.setDate(stackPriceTo.getDate() - 2);
							stackPriceTo2.setDate(stackPriceTo2.getDate() - 1);
							monthNum2 = stackPriceTo.getMonth();
							if (monthNum !== monthNum2) {
								if (monthNum !== stackPriceTo2.getMonth()) {
									priceValidTo = stackPriceTo2;
								} else {
									priceValidTo = stackPriceTo;
								}
							}
						}
						dateFormattedTo = dateFormat.format(priceValidTo);
						row.NEW_PRICE.PRICE_VALID_TO = dateFormattedTo;
					}

				} else {
					quotFrom = new Date(quotFrom.substring(0, 4), quotFrom.substring(4, 6) - 1, quotFrom.substring(6, 8));
					quotTo = new Date(quotTo.substring(0, 4), quotTo.substring(4, 6) - 1, quotTo.substring(6, 8));
					to = row.NEW_PRICE.PRICE_VALID_TO;
					date = new Date(to.split("-")[0], to.split("-")[1] - 1, to.split("-")[2]);
					priceValidTo = new Date(priceValidFrom);
					today = new Date();
					if (date < today) {
						timeDiff = Math.abs(quotTo.getTime() - quotFrom.getTime());
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						if (row.NEW_PRICE.PRICE_VALID_FROM === "2021-01-01" && diffDays === 365)
							diffDays = diffDays - 1;
						priceValidTo.setDate(priceValidTo.getDate() + diffDays);
						dateFormattedTo = dateFormat.format(priceValidTo);
						row.NEW_PRICE.PRICE_VALID_TO = dateFormattedTo;
					}
				}
			}

			if (row.NEW_PRICE.PRICE_VALID_TO.split("-").length >= 3) {
				if (row.NEW_PRICE.PRICE_VALID_TO.split("-")[0].length > 4)
					row.NEW_PRICE.PRICE_VALID_TO = "9999-12-31";
			}

			row.NEW_PRICE.NEW_PRICE_PLAN_K_EXCEL = row.NEW_PRICE.NEW_PRICE_PLAN_K;

			return row;
		}
	};
});