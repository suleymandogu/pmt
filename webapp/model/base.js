/* Developer Anuj Harshe */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/resource/ResourceModel"
], function (JSONModel, ResourceModel) {
	"use strict";
	return {
		isFirst: false, // logic to check first item select specially for bom load
		currentPage: "", // current page
		previousPage: "",
		goToPMC: false, // check page is loading from PMC button
		//SOC: Ex_kenguvj Task:28 
		goToPP: false, // Check page is loading from Price Preparation Button
		//EOC: Ex_kenguvj Task:28
		isArticleChange: false, // check if article data change
		isBomChange: false, // check if BOM data change
		/** 
		 * Returns the Event Bus Object from Core.
		 * @public
		 * @returns {sap.ui.core.EventBus} EventBus object set in Core.
		 */
		getEventBus: function () {
			return sap.ui.getCore().getEventBus();
		},
		/*------------------------ EDIT MODE SETTER ---------------------------*/
		isEditMode: true,
		/** 
		 * Set's the this.isEditMode variable as True or False.
		 * @public
		 * @param {boolean} bol - Sets the current boolean value of Edit Mode. 
		 */
		setEditMode: function (bol) {
			this.isEditMode = bol;
		},
		/*---------------------- CURRENT TAB CLICKED --------------------------*/
		currentTabKey: "AHTab",
		previousTabKey: "AHTab",
		setCurrentTabKey: function (tabkey) {
			this.previousTabKey = this.currentTabKey;
			this.currentTabKey = tabkey;
		},

		//ex_dogus for bom tab selected automatically 06.05.2019
		setCurrentTabKeyForBom: function (tabkey) {
			this.previousTabKey = tabkey;
			this.currentTabKey = tabkey;
		},

		getCurrentTabKey: function () {
			return this.currentTabKey;
		},

		getPreviousTabKey: function () {
			return this.previousTabKey;
		},
		/*---------------------- OVERVIEW CURRENT TAB CLICKED --------------------------*/
		overviewCurrentTabKey: "PROverViewTab",
		overviewPreviousTabKey: "PROverViewTab",
		setOverviewCurrentTabKey: function (tabkey) {
			this.overviewPreviousTabKey = this.overviewCurrentTabKey;
			this.overviewCurrentTabKey = tabkey;
		},
		getOverviewCurrentTabKey: function () {
			return this.overviewCurrentTabKey;
		},
		getOverviewPreviousTabKey: function () {
			return this.overviewPreviousTabKey;
		},
		/*-------------------- GET DATE FORMAT FROM I18N -------------------------*/
		dateformat: null,
		/** 
		 * Gets the date display format from the i18n Resource Bundle Model.
		 * @public
		 * @returns {string} Date format from the Resource Bundle Model.
		 */
		getI18NDateFormat: function () {
			if (!this.dateformat) {
				var i18nModel = new ResourceModel({
					bundleName: "com.doehler.pr.i18n.i18n"
				});
				var oBundle = i18nModel.getResourceBundle();
				this.dateformat = oBundle.getText("dateformat");
			}
			return this.dateformat;
		},
		/////////////////////////// CONVERT FLAT TO TREE ///////////////////////////////////////
		sapDataToTreeData: function (arr, par, child) {
			var riskMarkupAllState = false;
			arr.forEach(function (item) {
				item.children = item[par];
				if (item.EDIT === "" && item.ZV_RPLPRC1_C !== "0,00" && sap.ui.getCore().getModel("globalModel").getProperty(
						"/BOMDetailsE/ZV_MARKUP1_C") === true) {
					riskMarkupAllState = true;
				}
			});
			sap.ui.getCore().getModel("globalModel").setProperty("/riskMarkupAllState", riskMarkupAllState);

			par = "children";
			var nodes = [];
			var nodeMap = {};
			if (arr) {
				var nodeOut;
				var parentId;
				for (var i = 0; i < arr.length; i++) {
					var nodeIn = arr[i];
					nodeOut = JSON.parse(JSON.stringify(nodeIn));
					nodeOut[par] = [];
					parentId = nodeIn[par];
					if (parentId && parentId.length > 0) {
						var parent = nodeMap[nodeIn[par]];
						if (parent) {
							parent[par].push(nodeOut);
						}
					} else {
						nodes.push(nodeOut);
					}
					nodeMap[nodeOut[child]] = nodeOut;
				}
			}
			return nodes;
		},
		/////////////////////////// END CONVERT FLAT TO TREE ///////////////////////////////////////
		/////////////////////////// CONVERT TREE TO FLAT ///////////////////////////////////////
		convertTreeToList: function (root) {
			var thiz = this;
			var stack = [],
				array = [],
				hashMap = {};
			stack.push(root);

			while (stack.length !== 0) {
				var node = stack.pop();
				if (node.children.length <= 0) {
					thiz.visitNode(node, hashMap, array);
				} else {
					thiz.visitNode(node, hashMap, array); //
					for (var i = node.children.length - 1; i >= 0; i--) {
						stack.push(node.children[i]);
					}
				}
			}
			//array.forEach(function(item){ delete item.children; });
			return array;
		},
		visitNode: function (node, hashMap, array) {
			if (!hashMap[node.KNUMH]) {
				hashMap[node.KNUMH] = true;
				array.push(node);
			}
		},
		/////////////////////////// CONVERT TREE TO FLAT ///////////////////////////////////////
		getData1: function () {
			return JSON.parse(
				'{"aColumns":[{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column0","order":0,"visible":true,"width":"3rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column1","order":1,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column2","order":2,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column3","order":3,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column4","order":4,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column5","order":5,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column6","order":6,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column7","order":7,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column8","order":8,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column9","order":9,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column10","order":10,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column11","order":11,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column12","order":12,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column13","order":13,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column14","order":14,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column15","order":15,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column16","order":16,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column17","order":17,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column18","order":18,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column19","order":19,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column20","order":20,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column21","order":21,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column22","order":22,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column23","order":23,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false}],"_persoSchemaVersion":"1.0"}'
			);
		},
		getData2: function () {
			return JSON.parse(
				'{"aColumns":[{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column1","order":0,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column0","order":1,"visible":true,"width":"3rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column2","order":2,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column3","order":3,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column4","order":4,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column5","order":5,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column6","order":6,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column7","order":7,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column8","order":8,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column9","order":9,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column10","order":10,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column11","order":11,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column12","order":12,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column13","order":13,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column14","order":14,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column15","order":15,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column16","order":16,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column17","order":17,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column18","order":18,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column19","order":19,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column20","order":20,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column21","order":21,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column22","order":22,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false},{"id":"application-zdoehlersempr-display-component---SearchView--searchRequestTableId-__column23","order":23,"visible":true,"width":"11rem","sorted":false,"sortOrder":"Ascending","grouped":false}],"_persoSchemaVersion":"1.0"}'
			);
		},
	};
});