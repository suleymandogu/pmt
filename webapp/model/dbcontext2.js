sap.ui.define([
		"sap/ui/model/json/JSONModel",
		"com/doehler/PricingMasterFile/controller/ErrorHandler"
	],
	function (JSONModel, ErrorHandler) {
		"use strict";
		return {
			/** ajax call to fetch backend data 
			 * @param {object} params - request data
			 * @param {function} fnSuccess - success callback (response data in jsonmodel, thiz)
			 * @param {object} controller - current context (this)
			 * @param {boolean} bShowErr - show error msg 
			 * @param {function} fnError - error callback (response data in jsonmodel, thiz)
			 * @param {boolean} bShowServerErr - to show or hide server error
			 * @param {boolean} bshowLoading - to show or hide loading
			 * */
			callServer: function (params, fnSuccess, controller, bShowErr, fnError, bShowServerErr, bshowLoading) {
				if (params == undefined)
					params = {};
				if (bShowErr == undefined)
					bShowErr = true;
				if (bShowServerErr == undefined)
					bShowServerErr = true;
				if (bshowLoading == undefined)
					bshowLoading = true;
				var serviceUrl = "";
				switch (location.hostname) {
				case "dafe1ci.da.doehler.com":
					serviceUrl = "/dhlr/proxies/pricing/proxy01/";
					break;
				case "dafe2ci.da.doehler.com":
					serviceUrl = "/dhlr/proxies/pricing/proxy01/";
					break;
				case "dafe3ci.da.doehler.com":
					serviceUrl = "/dhlr/proxies/pricing/proxy01/";
					break;
				case "webidetesting6874744-a610cbd7a.dispatcher.hana.ondemand.com":
					serviceUrl =
						"https://" + "dafe1ci.da.doehler.com:8111/dhlr/proxies/pricing/proxy01/?sap-client=001&sap-user=EX_DOGUS&sap-password=sdogus";
					break;
				default:
				}

				if (serviceUrl === "")
					serviceUrl = "/sap/vui/ZAPP/";
				if (bshowLoading) {
					sap.ui.core.BusyIndicator.show(0);
				}
				if (params.INPUTPARAMS !== undefined) {
					if (params.INPUTPARAMS.length) {
						if (params.INPUTPARAMS[0].PARIDN === "PACKAGING") {
							if (sap.ui.getCore().getModel("globalModel").getProperty(
									"/pressTableValueControl").getBindingContext("listM") !== undefined) {
								var plant = sap.ui.getCore().getModel("listM").getProperty(sap.ui.getCore().getModel("globalModel").getProperty(
									"/pressTableValueControl").getBindingContext("listM").getPath()).PLANT;
								if (plant === "") params.INPUTPARAMS[0].PARIDN = "PACKAGING_ALL";
							}
						}
					}
				}

				$.ajax({
					url: serviceUrl,
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					type: "POST",
					data: JSON.stringify(params),
					async: true,
					cache: false,
					/*beforeSend: function (xhr) {
						 xhr.setRequestHeader ("Authorization", "Basic " + btoa( "EX_INDUKUV"+ ":" + "Vistex123$"));
					 },*/
					success: function (oData, resp) {
						if (bshowLoading) {
							sap.ui.core.BusyIndicator.hide();
						}
						var oModel = new JSONModel();
						oModel.setData(oData);
						if (oData.MESSAGES) {
							if (oData.MESSAGES.STATUS === "E") {
								if (bShowErr) {
									controller.handleServerMessages(oModel, function () {});
								}
								if (typeof fnError == "function") {
									fnError(oModel, controller);
								}
							} else if (typeof fnSuccess == "function") {
								fnSuccess(oModel, controller);
							}
						} else {
							if (oData.ZHARBOUR !== undefined && oData.ZKZSHE !== undefined) {
								fnSuccess(oModel, controller);
							}
						}
					},
					error: function (err, textStatus, errorThrown) {
						debugger;
						sap.ui.core.BusyIndicator.hide();

						if (bShowServerErr) {
							ErrorHandler.handleMessages(controller, err, function () {}, true);
						} else {
							if (err.status === 400 && errorThrown !== "Session not found") {
								window.location.reload();
							}
						}
					},

					complete: function (xhr, status) {
						if (status === 'error' || !xhr.responseText) {} else {
							var data = xhr.responseText;
						}
					}
				});
			},

			/* ON F4 Select */
			getValueHelpData: function (controller, identifier, kotab, relFldModel, pagingModel, fnSuccess) {
				var oModel = {
					HANDLERPARAMS: {
						FUNC: "SEARCH_F4"
					},
					INPUTPARAMS: []
				};
				if (relFldModel === {}) {
					relFldModel = {
						RelFldValue1: "",
						RelFldValue2: "",
						RelFldValue3: "",
						RelFldValue4: "",
						RelFldValue5: "",
						RelFldValue6: "",
						RelFldValue7: "",
						RelFldValue8: "",
						RelFldValue9: "",
						RelFldValue10: "",
						RelFldValue11: "",
						RelFldValue12: "",
						RelFldValue13: ""
					};
				}
				if (pagingModel === {}) {
					pagingModel = {
						NoOfEntry: 0,
						SearchTerm: ""
					};
				}
				if (identifier === "WAERS") {
					kotab = "TCURC";
				}

				// if (identifier === "ZZ_CUST_SRV" && (pagingModel.SearchTerm === "" || pagingModel.SearchTerm === undefined)) {
				// 	pagingModel.SearchTerm = "XXXX";
				// }
				oModel.INPUTPARAMS.push({
					"PARIDN": identifier,
					"KOTAB": kotab,
					"RELFLDVAL1": relFldModel.RelFldValue1,
					"RELFLDVAL2": relFldModel.RelFldValue2,
					"RELFLDVAL3": relFldModel.RelFldValue3,
					"RELFLDVAL4": relFldModel.RelFldValue4,
					"RELFLDVAL5": relFldModel.RelFldValue5,
					"RELFLDVAL6": relFldModel.RelFldValue6,
					"RELFLDVAL7": relFldModel.RelFldValue7,
					"RELFLDVAL8": relFldModel.RelFldValue8,
					"RELFLDVAL9": relFldModel.RelFldValue9,
					"RELFLDVAL10": relFldModel.RelFldValue10,
					"RELFLDVAL11": relFldModel.RelFldValue11,
					"RELFLDVAL12": relFldModel.RelFldValue12,
					"RELFLDVAL13": relFldModel.RelFldValue13,
					"NOENTRY": pagingModel.NoOfEntry,
					"SEARCHT": pagingModel.SearchTerm
				});
				this.callServer(oModel, fnSuccess, controller);
			}
		}
	});