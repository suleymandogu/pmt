sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models",
	"com/doehler/PricingMasterFile/model/dbcontext",
	"com/doehler/PricingMasterFile/model/dbcontext2",
	"com/doehler/PricingMasterFile/excel/excel",
	"com/doehler/PricingMasterFile/model/formatter",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/Label",
	"sap/base/Log",
	"sap/m/TextArea",
	"sap/ui/core/Core",
	"sap/m/MessageToast",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, base, models, dbcontext, dbcontext2, excel, formatter, JSONModel, MessageBox, Dialog,
	DialogType, Button, ButtonType, Label, Log, TextArea, Core, MessageToast, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.List.List", {
		formatter: formatter,

		onChangeH: function (oEvent) {
			debugger;
		},

		selectionChangeHeaderSelection: function (oEvent) {
			var key = oEvent.getParameter("selectedItem").getProperty("key"),
				that = this;

			var listHeaderControls = this._globalM.getProperty("/listHeaderControls");
			Object.entries(listHeaderControls).forEach(function (m) {
				if (m[0].indexOf("_VISIBLE") < 0) {
					that._globalM.setProperty("/listHeaderControls/" + m[0], m[0] === "ZPAL2" || m[0] === "ZABS" ? false : "");
					that._globalM.setProperty("/listHeaderControls/" + m[0] + "_VISIBLE", m[0] === key);
				}
			});
		},

		onApplyToAll: function (oEvent) {
			var oView = this.getView(),
				prevData = [],
				tableData = this._listM.getData(),
				table = this._table,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				line,
				i = 0,
				aIndices = table.getBinding().aIndices,
				listHeaderControls = this._globalM.getProperty("/listHeaderControls"),
				columnHeaders = oView.byId("columnHeaders"),
				key = columnHeaders.getSelectedKey(),
				that = this;

			if (key === "" || key === undefined) {
				MessageToast.show("Please select a column");
				return;
			}
			if (aSelIndex.length === 0) {
				MessageToast.show("Please select at least one record");
				return;
			}

			aSelIndex.forEach(function (selIndex) {
				newSelIndex.push(aIndices[selIndex]);
			});

			aSelIndex = newSelIndex;
			var flag = false;
			for (var m = 0; m < tableData.length; m++) {
				flag = false;
				for (var mm = 0; mm < aSelIndex.length; mm++) {
					if (m === aSelIndex[mm])
						flag = true;
				}

				if (!flag)
					prevData.push(tableData[m]);
			}

			/*
			 */
			var jsonData = [];
			for (i = 0; i < aSelIndex.length; i++) {
				line = tableData[aSelIndex[i]];
				if (key === "ZPAL2" || key === "ZABS")
					line[key] = that._globalM.getProperty("/listHeaderControls/" + key) ? "X" : "";

				switch (key) {
				case "CUSTOMER":
					line["SHIP_TO"] = "";
					line["PAYER"] = "";
					line["SALESORG"] = "";
					line["DISTR_CHAN"] = "";
					line["READY"] = false;
					line["DATA_MATERIAL"] = false;
					line["DATA_CUSTOMER"] = false;
					line[key] = that._globalM.getProperty("/listHeaderControls/" + key);
					break;

				case "SALESORG":
					line["SHIP_TO"] = "";
					line["PAYER"] = "";
					line["DCUKYCP"] = "";
					line["READY"] = false;
					line[key] = that._globalM.getProperty("/listHeaderControls/" + key);
					break;

				case "DISTR_CHAN":
					line["SHIP_TO"] = "";
					line["PAYER"] = "";
					line["READY"] = false;
					line[key] = that._globalM.getProperty("/listHeaderControls/" + key);
					break;

				case "PLANT":
					var filterPlant = line.MATERIALDATA.filter(function (r) {
						return r.PLANT === that._globalM.getProperty("/listHeaderControls/" + key);
					});
					if (filterPlant.length) {
						line["MATERIALDATA_MAIN"] = filterPlant;
						line["PLANT"] = filterPlant[0].PLANT;
						line["UNIT"] = filterPlant[0].MEINS;
						line["MEINS"] = filterPlant[0].MEINS;
						line["ZZKALK03"] = filterPlant[0].ZZKALK03;
						line["ZZ_GEBART"] = filterPlant[0].ZZ_GEBART;
						line["ZZKALK03"] = filterPlant[0].ZZKALK03;
						line["ZQUANTITY2MIN"] = filterPlant[0].ZQUANTITY2MIN;
						line["ZQUANTITY3"] = line.ZQUANTITY2 * filterPlant[0].ZCONT1;
					}
					break;

				case "ZZKALK03": //microbiology 
					if (line["MAT_LENGTH"] === "6" && line["PLANT"] !== "") {
						line[key] = that._globalM.getProperty("/listHeaderControls/" + key);
						line["ZZ_GEBART"] = "";
						line["ZQUANTITY2"] = 0;
						line["SHIP_COND"] = "";
						line["ZCONT1"] = 0;
						line["ZQUANTITY3"] = 0;
						line["ZQUANTITY2MIN"] = 0;
						line["ZFREIGHT1"] = 0;
					}
					break;

				case "ZZ_GEBART": //packaging
					if (line["MAT_LENGTH"] === "6" && line["PLANT"] !== "")
						line[key] = that._globalM.getProperty("/listHeaderControls/" + key);
					break;

				case "ZQUANTITY2": //order size no of units
					var keyV = isNaN(that._globalM.getProperty("/listHeaderControls/" + key)) ? 0 : that._globalM.getProperty("/listHeaderControls/" +
						key);
					if (isNaN(line.ZCONT1)) line.ZCONT1 = 0;
					if (isNaN(line.ZQUANTITY2)) line.ZQUANTITY2 = 0;
					line.ZQUANTITY3 = keyV * line.ZCONT1;

					break;

				case "ZPAL2":
					line[key] = that._globalM.getProperty("/listHeaderControls/" + key) ? "X" : "";
					break;

				case "ZABS":
					line[key] = that._globalM.getProperty("/listHeaderControls/" + key) ? "X" : "";
					break;

				default:
					line[key] = that._globalM.getProperty("/listHeaderControls/" + key);
				}

				if (key === "SALESORG" || key === "DISTR_CHAN") {
					if (line["CUSTOMER"] !== "" && line["MATERIAL"] !== "" &&
						line["SALESORG"] !== "" && line["DISTR_CHAN"] !== "") {
						jsonData.push(line);
					}
				}
			}
			this._listM.refresh();
			if (jsonData.length)
				this._prepareListFromBackend(jsonData, undefined, prevData);

		},

		onSubmitOrChange: function (oEvent) {
			var bindValue = oEvent.getSource().getBindingInfo("value");
			var sName = "",
				sType = "";
			if (bindValue !== undefined || bindValue !== null) {
				sType = bindValue.type;
				if (sType !== undefined || sType !== null) {
					sName = sType.sName;
				}
			}

			if (sName === "Float" && oEvent.getParameter("newValue") === "")
				oEvent.getSource().setValue("0");

		},

		onInit: function () {
			this.getOwnerComponent().getService("ShellUIService").then( // promise is returned
				function (oService) {
					oService.setTitle("List");
				}
			);
			this._getControlsFromView();
			this._bindModels();
			this.getRouter().getRoute("List").attachPatternMatched(this._onRouteMatched, this);
			this._fillColumnNamesForList();
			this._tableId = this.getView().byId("listTableId");
			this._tableIdText = "listTableId";

		},

		_onRouteMatched: function (oEvent) {
			var arg = oEvent.getParameter("arguments"),
				tab = arg["?query"].tab,
				jsonData,
				table = this._table;

			var userId = sap.ushell.Container.getUser().getId();
			//	this.getView().byId("savePRBId").setVisible(userId === "EX_DOGUS" || userId === "EX_UZUNB" || userId === "DEFAULT_USER");

			sap.ui.getCore().getModel("globalModel").setProperty("/scenarios", []);

			jsonData = JSON.parse(decodeURI(tab));
			if (jsonData !== undefined) {
				if (!jsonData.length) {
					jsonData.push({});
				} else {
					if (jsonData.length === 1) {
						if (jsonData[0].SALESORG === "") {
							jsonData[0]["CUSTOMER_VALID_FROM"] = "0000-00-00";
							jsonData[0]["CUSTOMER_VALID_TO"] = "0000-00-00";
							jsonData[0]["CUSTOMER_TARGET_VOLUME"] = 0;
							this._listM.setData(jsonData);
						} else
							this._prepareListFromBackend(jsonData, undefined, []); //senddata,path,prevData
					} else {
						jsonData.forEach(function (m) {
							m["MANUAL"] = false;
						});
						this._prepareListFromBackend(jsonData, undefined, []); //senddata,path,prevData
					}
				}

			}
			//this._listM.setData(jsonData);
		},

		onExport: function () {
			var data = [];
			var userId = sap.ushell.Container.getUser().getId();
			var tableData = this._listM.getData();
			var copiedData = tableData;
			var aIndices = this._table.getBinding("rows").aIndices;

			aIndices.forEach(function (ind) {
				if (!copiedData[ind]["READY"])
					copiedData[ind]["READY_T"] = "Not ready";
				else
					copiedData[ind]["READY_T"] = "Ready";
				data.push(copiedData[ind]);
			});

			if (userId === "EX_DOGUS" || userId === "DEFAULT_USER")
				excel._downloadExcel("List", this._table, data);
			else
				excel._downloadExcel("List", this._table, data);

		},

		// check 6 digit or 9 digit
		set6or9digit: function (matnr) {
			if (matnr === undefined || matnr === "") return 0;
			var str = matnr;
			var patt1 = /[0-9A-Za-z]/g;
			var length = str.match(patt1).join("").length;
			return length;
		},

		onValueHelpInit2: function (oEvent) {
			oEvent.getSource().setController(this);
			oEvent.getSource().setDataFunction(dbcontext2.getValueHelpData.bind(dbcontext2));
			this._checkCustomerAndMaterial(oEvent);
			//sap.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM").getPath();
		},

		onSelectIncoterms: function (oEvent) {
			var that = this;
			var bindContext = oEvent.getSource().getBindingContext("listM");
			if (bindContext === undefined)
				bindContext = sap.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM");
			var path = bindContext.getPath();

			var oModel = sap.ui.getCore().getModel("priceRecM");
			var data = {};
			data.ZINCO1X = that._listM.getProperty(path + "/INCOTERMS");
			data.ZIC_BIZ1 = that._listM.getProperty(path + "/ZIC_BIZ1");
			data.ZWERKS = that._listM.getProperty(path + "/PLANT");
			data.ZKUNNR = that._listM.getProperty(path + "/CUSTOMER");
			data.ZVKORG = that._listM.getProperty(path + "/SALESORG");
			data.ZVTWEG = that._listM.getProperty(path + "/DISTR_CHAN");

			if (data.ZINCO1X === "FCA" || data.ZINCO1X === "EXW") {
				that._listM.setProperty(path + "/VISIBLE_ZFREIGHT1", false);
			} else {
				that._listM.setProperty(path + "/VISIBLE_ZFREIGHT1", true);
			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "FILL_FULFILLMENT_INPUT"
				},
				"INPUTPARAMS": [data]
			};
			dbcontext2.callServer(params, function (oModel1) {
				var value = oModel1.getProperty("/CITY");
				that._listM.setProperty(path + "/ZINCO2", value);
			}, this);
		},

		onSelectf4Packaging: function (oEvent) {
			var bindContext = oEvent.getSource().getBindingContext("listM");
			if (bindContext === undefined)
				bindContext = sap.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM");
			var path = bindContext.getPath();
			this._listM.setProperty(path + "/ZQUANTITY2", ""); //order size no
			this._listM.setProperty(path + "/ZIC_BIZ3", ""); //
			this._listM.setProperty(path + "/ZIC_BIZ5", ""); //
			this._listM.setProperty(path + "/ZFREIGHT1", ""); // Freight out
			this._listM.setProperty(path + "/SHIP_COND", ""); // shipping cond
			this._listM.setProperty(path + "/ZCONT1", ""); // Filling qty
			this._listM.setProperty(path + "/ZQUANTITY3", ""); //Order size
			this._listM.setProperty(path + "/ZQUANTITY2MIN", ""); // Minimum order size
			this._listM.setProperty(path + "/ZFRTWGHT", ""); // freight weight in pr details
			this.resetFillingQuantity(this._listM.getProperty(path), path);
		},

		resetFillingQuantity: function (line, path) {
			var that = this;
			if (line.MATERIALDATA_MAIN !== undefined)
				if (line.MATERIALDATA_MAIN.length) {
					// send data to reset value
					var data = {};
					data.ZMATNR = line.MATERIAL;
					data.WERKS = line.PLANT;
					data.ZZKALK01 = line.MATERIALDATA_MAIN[0].ZZKALK01;
					data.ZZKALK02 = line.MATERIALDATA_MAIN[0].ZZKALK02;
					data.ZZKALK03 = line.MATERIALDATA_MAIN[0].ZZKALK03;
					data.ZZKALK04 = line.MATERIALDATA_MAIN[0].ZZKALK04;
					data.ZZKALK05 = line.MATERIALDATA_MAIN[0].ZZKALK05;
					data.ZZ_GEBART = line.ZZ_GEBART;
					data.ZQUANTITY = 0;
					data.ZZEINR = "";
					data.DATAB = line.PRICE_VALID_FROM;
					data.DATBI = line.PRICE_VALID_TO;
					data.ZBRIX1 = "";
					data.MATNR = line.MATERIAL;
					data.PROFL = line.MATERIALDATA_MAIN[0].PROFL;
					data.PQNUM = line.PQNUM;

					var params = {
						"HANDLERPARAMS": {
							"FUNC": "UPDATE_FILLING_QTY"
						},
						"INPUTPARAMS": [data]
					};
					dbcontext2.callServer(params, function (oModel1, thiz) {
						// set filling qty with new value
						var value = oModel1.getProperty("/FILLING_QTY");
						var value1 = oModel1.getProperty("/DISPLAY_FL_QTY");
						that._listM.setProperty(path + "/ZCONT1", value);
						that._listM.setProperty(path + "/DISPLAY_FL_QTY", value1);
						//that.makeEditFillQty(path);

					}, this);
				}
		},

		// makeEditFillQty: function (path) {
		// 	var that = this;
		// 	var value = that._listM.getProperty(path + "/DISPLAY_FL_QTY");
		// 	var sValue = that._listM.getProperty(path + "/ZZ_XVFKZ");
		// 	if (value === "X" || sValue === "X") {
		// 		that._listM.setProperty(path + "/ZCONT1_EDIT", true);
		// 	} else {
		// 		that._listM.setProperty(path + "/ZCONT1_EDIT", false);
		// 	}
		// },

		onSelectRadioButton: function (oEvent) {
			var selectedIndex = oEvent.getParameter("selectedIndex"),
				oFilter;
			switch (selectedIndex) {
			case 0: //all
				oFilter = [];
				oFilter.push(new Filter("READY", FilterOperator.EQ, true));
				oFilter.push(new Filter("READY", FilterOperator.EQ, false));
				break;
			case 1: //green
				oFilter = new Filter("READY", FilterOperator.EQ, true);
				break;
			case 2: //red
				oFilter = new Filter("READY", FilterOperator.EQ, false);
				break;
			default:
			}

			var oBinding = this._tableId.getBinding("rows");
			oBinding.filter([oFilter]);
		},

		onSelectShipCond: function (oEvent) {
			var bindContext = oEvent.getSource().getBindingContext("listM");
			if (bindContext === undefined)
				bindContext = sap.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM");
			var path = bindContext.getPath();
			this._checkSeaHarbourVisibility(path);
			this._loadSeaHarbour(path);
		},

		onSelectMicrobiology: function (oEvent) {
			var bindContext = oEvent.getSource().getBindingContext("listM");
			if (bindContext === undefined)
				bindContext = sap.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM");
			var path = bindContext.getPath();

			this._listM.setProperty(path + "/ZZ_GEBART", "");
			this._listM.setProperty(path + "/ZQUANTITY2", 0);
			this._listM.setProperty(path + "/SHIP_COND", "");
			this._listM.setProperty(path + "/ZCONT1", 0);
			this._listM.setProperty(path + "/ZQUANTITY3", 0);
			this._listM.setProperty(path + "/ZQUANTITY2MIN", 0);
			this._listM.setProperty(path + "/ZFREIGHT1", 0);
		},

		onCreateWithRef: function (oEvent) {
			this.getView().byId("reqVH").setValue("");
			this.getView().byId("createWithRefDId").setVisible(true).open();
		},

		onChangePR: function (oEvent) {
			var value = oEvent.getParameter("newValue");
		},

		onCloseDialog: function (oEvent) {
			this.getView().byId("createWithRefDId").setVisible(false).close();
		},

		onAddFromRequest: function (oEvent) {
			var that = this;
			var requestId = this.getView().byId("reqVH").getValue();
			this.getView().byId("createWithRefDId").setVisible(false).close();
			var userId = sap.ushell.Container.getUser().getId();
			// if (userId === "DEFAULT_USER")
			// 	requestId = "P500004626";
			if (requestId === "") {
				MessageBox.warning("Please enter a price request number!");
				return;
			}
			var sPqnum = requestId;

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_REQUEST_DATA_IRPA"
				},
				"INPUTPARAMS": [{
					"PQNUM": sPqnum
				}]
			};
			dbcontext.callServer(params, function (oModel, thiz) {
				that._updateTableFromRequest(oModel.getData().ROWS);
			}, this);
		},

		_convertNumberToStringFormat: function (value) {
			if (isNaN(value) || value === Infinity || value === -Infinity) value = 0;
			value = value.toString().split(".").join(",");
			return value.toString();
		},

		_convertBotDate: function (value) {
			var output = "";
			if (value !== "")
				output = value.split("-")[2] + "." + value.split("-")[1] + "." + value.split("-")[0];
			return output;
		},

		checkItems: function (data) {
			if (data.length === 0) {
				return false;
			}

			var case1 = $.extend(true, [], data);
			var case1Data = [],
				case2Data = [],
				hasSameMat = false;

			var customer = case1[0]["CUSTOMER"];
			var title = case1[0]["TITLE"];
			var matnr = case1[0].MATERIAL.split(".").join('').replace(/ /g, '');

			case1Data = case1.filter(function (c1) {
				return customer === c1["CUSTOMER"] && title === c1["TITLE"];
			});

			if (case1Data.length !== data.length) {
				MessageBox.warning("Customer and Price req. title have to be the same for your items!");
				return false;
			} else {
				case1Data.forEach(function (c2) {
					c2["MATERIAL"] = c2["MATERIAL"].split(".").join('').replace(/ /g, '');
				});

				case1Data.forEach(function (c2) {
					case2Data = case1Data.filter(function (c3) {
						return c2["MATERIAL"] === c3["MATERIAL"];
					});
					if (case2Data.length > 1) {
						hasSameMat = true;
					}
				});

				if (hasSameMat === true) {
					MessageBox.warning("Material has to be different for your items!");
					return false;
				} else {
					return true;
				}
			}

			debugger;
		},

		onCallIRPA: function (oEvent) {
			var that = this;
			var tableData = [],
				apiArray = [],
				apiLine = {};
			var alldata = this._listM.getData();
			var selectedIndices = this._table.getSelectedIndices();

			var bindingContext = that._tableId.getBinding("rows").getContexts();
			var listData = [];

			if (selectedIndices.length === 0) {
				bindingContext.forEach(function (bc) {
					tableData.push(that._listM.getProperty(bc.getPath()));
				});
			} else {
				selectedIndices.forEach(function (m) {
					var path = that._tableId.getBinding("rows").getContexts()[m].getPath();
					tableData.push(that._listM.getProperty(path));
				});
			}

			var hasError = false;

			var isSame = this.checkItems(tableData);

			if (isSame === false)
				return;

			tableData.forEach(function (tableLine) {
				apiLine = {};
				apiLine["KUNNR"] = tableLine["CUSTOMER"]; //done
				if (tableLine["ZCONT1"] === "") tableLine["ZCONT1"] = 0;
				apiLine["ZCONT1"] = tableLine["ZCONT1"]; //done
				apiLine["SHIPTOPARTY"] = tableLine["SHIP_TO"]; //done
				apiLine["PAYER"] = tableLine["PAYER"]; //done
				apiLine["OPPURTUNITY"] = tableLine["OPP_ID"]; //done
				var materialValue = tableLine["MATERIAL"].split(".").join('');
				materialValue = materialValue.replace(/ /g, '');
				apiLine["MATNR"] = materialValue; //done
				apiLine["SALESORG"] = tableLine["SALESORG"]; //done
				apiLine["DUTY"] = tableLine["DISTR_CHAN"]; //done
				apiLine["PLANT"] = tableLine["PLANT"]; //done
				apiLine["RMSPOT"] = tableLine["ZPRICEVALIDITY"]; //done
				apiLine["ORIGINATOR"] = tableLine["ORIGINATOR"]; //done
				apiLine["TARGETVOLUME"] = that._convertNumberToStringFormat(tableLine["TARGET_VOLUME"]); //done
				apiLine["TARGETPRICE"] = that._convertNumberToStringFormat(tableLine["CUSTOMER_TARGET_PRICE"]); //done
				apiLine["ORDERSIZE"] = that._convertNumberToStringFormat(tableLine["ZQUANTITY2"]); //done //order size no of units
				apiLine["CONDITION"] = tableLine["SHIP_COND"]; //done
				apiLine["INCOTERS"] = tableLine["INCOTERMS"]; //done
				apiLine["PAYMENTTERMS"] = tableLine["PMNTTRMS"]; //done
				apiLine["QUOTCURR"] = tableLine["ZWAERS2Q"]; //done
				apiLine["ALTERUOM"] = tableLine["DOEKMEIN2"];
				apiLine["COMMISSION"] = that._convertNumberToStringFormat(tableLine["ZCOMM1"]); //Commission 3rd party %
				apiLine["COMMISSION3PARTY"] = that._convertNumberToStringFormat(tableLine["ZCOMM1A"]); //Commission 3rd party
				apiLine["FREIGHTOUT"] = that._convertNumberToStringFormat(tableLine["ZFREIGHT1"]);
				apiLine["SALESPRICE"] = that._convertNumberToStringFormat(tableLine["ZFINSALES"]);
				apiLine["VALIDFROM"] = that._convertBotDate(tableLine["PRICE_VALID_FROM"]);
				apiLine["VALIDTO"] = that._convertBotDate(tableLine["PRICE_VALID_TO"]);
				apiLine["REQTITLE"] = tableLine["TITLE"];
				apiLine["QUOTTRIGGER"] = "no";
				apiLine["EXCHANGERATE"] = that._convertNumberToStringFormat(tableLine["ZWAERS3AQ"]);
				apiLine["MICROBIO"] = tableLine["ZZKALK03"];
				apiLine["PACKAGING"] = tableLine["ZZ_GEBART"];
				apiLine["SEAHARBOUR"] = tableLine["SEA_HARBOUR"];
				apiLine["DOSAGE"] = that._convertNumberToStringFormat(tableLine["ZDOSAGE"]);
				apiLine["TARGETCOSTINUSED"] = that._convertNumberToStringFormat(tableLine["ZCOST2A"]);
				apiLine["PLACEFULFILL"] = tableLine["ZINCO2"];
				apiLine["FREIGHTWEIGHT"] = that._convertNumberToStringFormat(tableLine["ZFRTWGHT"]);
				apiLine["FIXEDTIMED"] = tableLine["ZFIXTIMDEL"];
				apiLine["CALCAVERAGEB"] = tableLine["ZABS"];
				//apiLine["PAYMENTTERMSCALC"] = that._convertNumberToStringFormat(tableLine["ZCOST8A"]);
				apiLine["PAYMENTTERMSDISCOUNT"] = that._convertNumberToStringFormat(tableLine["ZCOST8A"]);
				apiLine["DEBITDISCOUNT"] = that._convertNumberToStringFormat(tableLine["ZLAST"]);
				apiLine["NOOFREBATE"] = tableLine["ZBONUS1A"];
				apiLine["REBATES"] = that._convertNumberToStringFormat(tableLine["ZBONUS1"]);
				apiLine["NOOFREBATEPERCENT"] = tableLine["ZCUSTBONUS1"];
				apiLine["REBATESPERCENT"] = that._convertNumberToStringFormat(tableLine["ZCUSTBONUS"]);
				apiLine["OTHERCOST"] = that._convertNumberToStringFormat(tableLine["ZOTHEXP"]);
				apiLine["LOSTINTEREST"] = that._convertNumberToStringFormat(tableLine["ZLOSTCST"]);
				apiLine["FINANCECOST"] = that._convertNumberToStringFormat(tableLine["ZFNCECST"]);
				apiLine["DOCUMENTCOST"] = that._convertNumberToStringFormat(tableLine["ZPRICE_INT7"]);
				apiLine["IMPORTDUTY"] = that._convertNumberToStringFormat(tableLine["ZCOST7B_PRCT_MAN"]);
				apiLine["IMPORTDUTYKG"] = that._convertNumberToStringFormat(tableLine["ZCOST7B_KG_MAN"]);
				apiLine["OTHERIMPORTCOST"] = that._convertNumberToStringFormat(tableLine["ZCOST7F_MAN"]);
				apiLine["VAT"] = that._convertNumberToStringFormat(tableLine["ZCOST7D"]);

				apiLine["BUSINESSTYPE"] = tableLine["ZIC_BIZ1"];
				apiLine["ORDERSIZESUBS"] = that._convertNumberToStringFormat(tableLine["ZIC_BIZ5"]);
				apiLine["SHIPPINGCSUBS"] = tableLine["ZIC_BIZ7"];
				apiLine["INCOTERMSSUBS"] = tableLine["ZIC_BIZ2"];
				apiLine["ICFREIGHT"] = that._convertNumberToStringFormat(tableLine["ZIC_BIZ10"]);
				apiLine["THEREOFFREIGHTM"] = that._convertNumberToStringFormat(tableLine["ZIC_BIZ10_BORDER_MAN"]);
				apiLine["IMPORTDUTYM"] = that._convertNumberToStringFormat(tableLine["ZIC_BIZ12_MAN"]);
				apiLine["IMPORTDUTYPERKGM"] = that._convertNumberToStringFormat(tableLine["ZIC_BIZ13_MAN"]);
				apiLine["IMPORTCOSTPERKGM"] = that._convertNumberToStringFormat(tableLine["ZIC_BIZ14_MAN"]);
				apiLine["EXICETAXPERKGM"] = that._convertNumberToStringFormat(tableLine["ZCOST7C_KG_MAN"]);
				apiLine["FREIGHTFROMWAREH"] = that._convertNumberToStringFormat(tableLine["ZFREIGHT3"]);
				apiLine["PALLETYN"] = tableLine["ZPAL2"] === "X" ? "TRUE" : "FALSE";
				apiLine["PALLET"] = tableLine["PALLET"];
				if (tableLine["READY"] === false) hasError = true;
				apiArray.push(apiLine);
			});

			if (hasError === true) {
				MessageBox.warning("Please check validation for selected rows");
				return;
			}

			var inputLine = {
				irpa_data: []
			};
			inputLine.IRPA_DATA = apiArray;
			var listLine = {};
			var duplicatedControl = {};
			var duplicatedItems = {};
			var params = {};
			MessageBox.information("Would like to save your entries as an excel file?", {
				actions: ["Yes", "No"],
				onClose: function (sAction) {
					if (sAction === "Yes") {
						that.onExport();
						params = {
							"HANDLERPARAMS": {
								"FUNC": "RUN_IRPA_API"
							},
							"INPUTPARAMS": [inputLine]
						};
						dbcontext.callServer(params, function (oModel, thiz) {
							that.handleServerMessages(oModel, function (status) {});
						}, that);
					} else {
						params = {
							"HANDLERPARAMS": {
								"FUNC": "RUN_IRPA_API"
							},
							"INPUTPARAMS": [inputLine]
						};
						dbcontext.callServer(params, function (oModel, thiz) {
							that.handleServerMessages(oModel, function (status) {

							});
						}, that);
					}

				}
			});
			// var params = {
			// 	"HANDLERPARAMS": {
			// 		"FUNC": "RUN_IRPA_API"
			// 	},
			// 	"INPUTPARAMS": [inputLine]
			// };
			// dbcontext.callServer(params, function (oModel, thiz) {
			// 	that.handleServerMessages(oModel, function (status) {
			// 		debugger;
			// 	});
			// }, this);

		},

		_updateTableFromRequest: function (items) {
			var that = this,
				currentData = this._listM.getData(),
				newData = [],
				newLine = {},
				posnr = currentData.length ? currentData[currentData.length - 1]["POSNR"] : 0;
			if (items.length) {
				items.forEach(function (itm) {
					posnr++;
					newLine = that._getLine(posnr);
					newLine.FROM_PQ = true;
					newLine.MANUAL = false;
					newLine.CUSTOMER = itm.ZKUNNR;
					newLine.PQNUM = itm.ZPQNUM;
					newLine.CUSTOMER_TARGET_PRICE = itm.ZTARGET;
					newLine.DCUKYCP = itm.ZWAERS2;
					newLine.DISTR_CHAN = itm.ZVTWEG;
					newLine.DOEKMEIN2 = itm.ZUOM;
					newLine.INCOTERMS = itm.ZINCO1X;
					if (newLine.INCOTERMS === "FCA" || newLine.INCOTERMS === "EXW") {
						newLine.VISIBLE_ZFREIGHT1 = false;
					} else {
						newLine.VISIBLE_ZFREIGHT1 = true;
					}
					newLine.MATERIAL = itm.ZMATNR;
					newLine.MAT_LENGTH = that.set6or9digit(newLine.MATERIAL).toString();
					newLine.MEINS = itm.ZEXST_PUOM; //not
					newLine.OPP_ID = itm.ZC4C_NR;
					newLine.ORIGINATOR = itm.ZORIGINATOR;
					newLine.PALLET = itm.ZPALLET;
					newLine.PAYER = itm.ZREGU;
					newLine.PLANT = itm.ZWERKS;
					newLine.PMNTTRMS = itm.ZTERM3;
					newLine.POSNR = posnr;
					newLine.PRICE_VALID_FROM = itm.ZVALFRM_CONT;
					newLine.PRICE_VALID_TO = itm.ZVALTO_CONT;
					newLine.SALESORG = itm.ZVKORG;
					newLine.SEA_HARBOUR = itm.ZHARBOUR;
					newLine.SHIP_COND = itm.ZSHIP;
					newLine.SHIP_TO = itm.ZKUNWE;
					newLine.TARGET_VOLUME = itm.ZCONVOL;
					newLine.TITLE = itm.BOTEXT_REF;
					newLine.TRIGGER_A = "";
					newLine.TRIGGER_Q = "";
					newLine.UNIT = itm.ZEXST_PUOM;
					newLine.ZABS = itm.ZABS;
					newLine.ZBONUS1 = itm.ZBONUS1;
					newLine.ZBONUS1A = itm.ZBONUS1A;
					newLine.ZCOM1 = itm.ZCOM1;
					newLine.ZCOMM1A = itm.ZCOMM1A;
					newLine.ZCONT1 = itm.ZCONT1;
					newLine.ZCOST2A = itm.ZCOST2A;
					newLine.ZCOST7B_KG_MAN = itm.ZCOST7B_KG_MAN;
					newLine.ZCOST7B_PRCT_MAN = itm.ZCOST7B_PRCT_MAN;
					newLine.ZCOST7C_KG_MAN = itm.ZCOST7C_KG_MAN;
					newLine.ZCOST7D = itm.ZCOST7D;
					newLine.ZCOST7F_MAN = itm.ZCOST7F_MAN;
					newLine.ZCOST8A = itm.ZCOST8A;
					newLine.ZCUSTBONUS = itm.ZCUSTBONUS;
					newLine.ZCUSTBONUS1 = itm.ZCUSTBONUS1;
					newLine.ZDOSAGE = itm.ZDOSAGE;
					newLine.ZFINSALES = itm.ZFINSALES;
					newLine.ZFIXTIMDEL = itm.ZFIXTIMDEL;
					newLine.ZFNCECST = itm.ZFNCECST;
					newLine.ZFREIGHT1 = itm.ZFREIGHT1;
					newLine.ZFREIGHT3 = itm.ZFREIGHT3;
					newLine.ZFRTWGHT = itm.ZFRTWGHT;
					newLine.ZHARBOUR = itm.ZHARBOUR;
					newLine.ZIC_BIZ1 = itm.ZIC_BIZ1;
					newLine.ZIC_BIZ2 = itm.ZIC_BIZ2;
					newLine.ZIC_BIZ5 = itm.ZIC_BIZ5;
					newLine.ZIC_BIZ7 = itm.ZIC_BIZ7;
					newLine.ZIC_BIZ10 = itm.ZIC_BIZ10;
					newLine.ZIC_BIZ10_BORDER_MAN = itm.ZIC_BIZ10_BORDER_MAN;
					newLine.ZIC_BIZ12_MAN = itm.ZIC_BIZ12_MAN;
					newLine.ZIC_BIZ13_MAN = itm.ZIC_BIZ13_MAN;
					newLine.ZIC_BIZ14_MAN = itm.ZIC_BIZ14_MAN;
					newLine.ZINCO2 = itm.ZINCO2;
					newLine.ZKZSHE = itm.ZKZSHE;
					newLine.ZLAST = itm.ZLAST;
					newLine.ZOTHEXP = itm.ZOTHEXP;
					newLine.ZPAL2 = itm.ZPAL2;
					newLine.ZPRICEVALIDITY = itm.ZPRICEVALIDITY;
					newLine.ZPRICE_INT7 = itm.ZPRICE_INT7;
					newLine.ZQUANTITY2 = itm.ZQUANTITY2;
					newLine.ZQUANTITY2MIN = itm.ZQUANTITY2MIN;
					newLine.ZQUANTITY3 = itm.ZQUANTITY3;
					newLine.ZSUBCURR = itm.ZSUBCURR;
					newLine.ZWAERS2 = itm.ZWAERS2;
					newLine.ZWAERS2Q = itm.ZWAERS2Q;
					newLine.ZWAERS3AQ = itm.ZWAERS3AQ;
					newLine.ZZKALK03 = itm.ZZKALK03;
					newLine.ZZ_GEBART = itm.ZZ_GEBART;
					newLine.ZMATNR = itm.ZMATNR;
					newLine.ZMATNR_BULK = itm.ZMATNR_BULK;
					newLine.ZZEINR = itm.ZZEINR;
					newLine.ZBRIX1 = itm.ZBRIX1;
					newLine.ZZ_XVFKZ = itm.ZZ_XVFKZ;
					newLine.MAT_DESC = itm.MAKTX;
					newLine.PACK_DESC = itm.GEBART_DESC;
					newData.push(newLine);
				});

				//currentData = currentData.concat(newData);
				//that._listM.setData(currentData);
				//var path = "/" + (that._listM.getData().length - 1).toString();
				that._prepareListFromBackend(newData, undefined, currentData, "PR");
			}
		},

		_checkSeaHarbourVisibility: function (path) {
			var that = this;
			var line = this._listM.getProperty(path);
			var data = {};
			data.ZSHIP = line.SHIP_COND;
			data.ZLAND1 = line.CUSTOMERDATA_MAIN.length ? line.CUSTOMERDATA_MAIN[0].LAND1 : "";
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "UPDATE_VIS_HARBOUR"
				},
				"INPUTPARAMS": [data]
			};
			dbcontext2.callServer(params, function (oModel1, thiz) {
				that._listM.getProperty(path + "/ZHARBOUR", oModel1.oData.ZHARBOUR);
				that._listM.getProperty(path + "/ZKZSHE", oModel1.oData.ZKZSHE);
			}, this);
		},

		_loadSeaHarbour: function (path) {
			var that = this;
			var line = this._listM.getProperty(path);
			var data = {};
			data.ZLAND1 = line.CUSTOMERDATA_MAIN.length ? line.CUSTOMERDATA_MAIN[0].LAND1 : "";
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SEARCH_F4"
				},
				"INPUTPARAMS": [{
					"PARIDN": "HARBOUR",
					"RELFLDVAL1": data.ZLAND1,
					"NOENTRY": ""
				}]
			};
			dbcontext2.callServer(params, function (oModel1, thiz) {
				var result = oModel1.getData().F4_VALUE_DYN;
				that._listM.setProperty(path + "/SEAHARBOUR", result);

			}, this);
		},

		onSelectRebateAgree: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName(),
				jsonData = [],
				value = oEvent.getParameter("newValue"),
				bindingContext = oEvent.getSource().getBindingContext("listM") !== undefined ? oEvent.getSource().getBindingContext("listM") :
				sap
				.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM"),
				path = bindingContext.getPath(),
				row = this._listM.getProperty(path),
				that = this;

			var data = {};
			data.ZBONUS1A = row.ZBONUS1A;
			data.ZBONUS1 = row.ZBONUS1;
			data.ZKUNNR = row.CUSTOMER;
			data.ZVKORG = row.SALESORG;
			data.ZVTWEG = row.DISTR_CHAN;
			data.ZSPART = row.CUSTOMERDATA.length ? row.CUSTOMERDATA[0].SPART : "";
			data.DATAB = row.PRICE_VALID_FROM;
			data.DATBI = row.PRICE_VALID_TO;
			data.MATNR = row.MATERIAL;
			data.ZCONVOL = row.TARGET_VOLUME;
			data.ZQUANTITY3 = row.ZQUANTITY3;
			data.FIELD = "REB_AGMT_KG";
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "FILL_REBATE_INPUT"
				},
				"INPUTPARAMS": [data]
			};
			dbcontext2.callServer(params, function (oModel1) {
				var value = oModel1.getProperty("/AMOUNT");
				that._listM.setProperty(path + "/ZBONUS1", value);
			}, this);
		},

		onSelectRebatePre: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName(),
				jsonData = [],
				value = oEvent.getParameter("newValue"),
				bindingContext = oEvent.getSource().getBindingContext("listM") !== undefined ? oEvent.getSource().getBindingContext("listM") :
				sap
				.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM"),
				path = bindingContext.getPath(),
				row = this._listM.getProperty(path),
				that = this;

			var data = {};
			data.ZBONUS1A = row.ZBONUS1A;
			data.ZBONUS1 = row.ZBONUS1;
			data.ZKUNNR = row.CUSTOMER;
			data.ZVKORG = row.SALESORG;
			data.ZVTWEG = row.DISTR_CHAN;
			data.ZSPART = row.CUSTOMERDATA.length ? row.CUSTOMERDATA[0].SPART : "";
			data.DATAB = row.PRICE_VALID_FROM;
			data.DATBI = row.PRICE_VALID_TO;
			data.MATNR = row.MATERIAL;
			data.ZCONVOL = row.TARGET_VOLUME;
			data.ZQUANTITY3 = row.ZQUANTITY3;
			data.FIELD = "REB_AGMT_PER";
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "FILL_REBATE_INPUT"
				},
				"INPUTPARAMS": [data]
			};
			dbcontext2.callServer(params, function (oModel1) {
				var value = oModel1.getProperty("/AMOUNT");
				that._listM.setProperty(path + "/ZCUSTBONUS", value);
			}, this);
		},

		onSelectCurrency: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName(),
				jsonData = [],
				value = oEvent.getParameter("newValue"),
				bindingContext = oEvent.getSource().getBindingContext("listM") !== undefined ? oEvent.getSource().getBindingContext("listM") :
				sap
				.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM"),
				path = bindingContext.getPath(),
				row = this._listM.getProperty(path);

			var that = this;

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "UPDATE_EXCH_RATE"
				},
				"INPUTPARAMS": [{
					"DATAB": row.PRICE_VALID_FROM,
					"ZWAERS2": row.ZWAERS2,
					"ZWAERS2Q": row.ZWAERS2Q,
					"ZKUNNR": row.CUSTOMER,
					"ZVKORG": row.SALESORG,
					"ZVTWEG": row.DISTR_CHAN,
					"ZVALTO_CONT": row.PRICE_VALID_TO,
					"ZPRICEVALIDITY": row.ZPRICEVALIDITY,
				}]
			};
			dbcontext2.callServer(params, function (oModel1) {
				var exchangeRate = oModel1.getProperty("/EXCH_RATE");
				var subcurr = oModel1.getProperty("/E_SUBCURR");
				that._listM.setProperty(path + "/ZWAERS3AQ", exchangeRate);
				that._listM.setProperty(path + "/ZSUBCURR", subcurr);
			}, this);
		},

		onChangeOrderSize: function (oEvent) {
			var value = oEvent.getSource().getValue(),
				bindingContext = oEvent.getSource().getBindingContext("listM"),
				path = bindingContext.getPath(),
				line = this._listM.getProperty(path);

			if (value === "") value = 0;
			if (line.ZCONT1 === "") line.ZCONT1 = 0;

			this._listM.setProperty(path + "/ZQUANTITY3", value * line.ZCONT1);

		},

		onChange: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName(),
				jsonData = [],
				value = oEvent.getParameter("newValue"),
				bindingContext = oEvent.getSource().getBindingContext("listM") !== undefined ? oEvent.getSource().getBindingContext("listM") :
				sap
				.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM"),
				path = bindingContext.getPath(),
				plant;

			switch (fieldName) {
			case "CUSTOMER":
				this._listM.setProperty(path + "/SHIP_TO", "");
				this._listM.setProperty(path + "/PAYER", "");
				this._listM.setProperty(path + "/SALESORG", "");
				this._listM.setProperty(path + "/DISTR_CHAN", "");
				break;

			case "SALESORG":
				this._listM.setProperty(path + "/SHIP_TO", "");
				this._listM.setProperty(path + "/PAYER", "");
				this._listM.setProperty(path + "/DCUKYCP", "");
				break;

			case "VTWEG":
				this._listM.setProperty(path + "/SHIP_TO", "");
				this._listM.setProperty(path + "/PAYER", "");
				break;

			case "MATERIAL":
				this._listM.setProperty(path + "/PLANT", "");
				this._listM.setProperty(path + "/ZZ_GEBART", "");
				this._listM.setProperty(path + "/DOEKMEIN2", "");
				this._listM.setProperty(path + "/ZCUSTBONUS1", "");
				this._listM.setProperty(path + "/ZBONUS1A", "");
				this._listM.setProperty(path + "/MAT_DESC", "");
				this._listM.setProperty(path + "/PACK_DESC", "");
				break;

			case "PACKAGING":
				this._listM.setProperty(path + "/PACK_DESC", "");

			case "WERKS":
				plant = this._listM.getProperty(path).PLANT;
				var matData = this._listM.getProperty(path).MATERIALDATA;
				if (matData === undefined) {
					this._prepareListFromBackend(matData, path, []);
					return;
				}
				if (matData.length) {
					var plantResult = matData.filter(function (x) {
						return x.PLANT === plant;
					});
					if (plantResult.length) {
						this._listM.setProperty(path + "/MATERIALDATA_MAIN", plantResult);
						this._listM.setProperty(path + "/PLANT", plantResult[0].PLANT);
						this._listM.setProperty(path + "/UNIT", plantResult[0].MEINS);
						this._listM.setProperty(path + "/MEINS", plantResult[0].MEINS);
						this._listM.setProperty(path + "/ZZKALK03", plantResult[0].ZZKALK03);
						this._listM.setProperty(path + "/ZZ_GEBART", plantResult[0].ZZ_GEBART);
						this._listM.setProperty(path + "/ZCONT1", plantResult[0].ZCONT1);
						this._listM.setProperty(path + "/ZQUANTITY2MIN", plantResult[0].ZQUANTITY2MIN);
						this._listM.setProperty(path + "/ZQUANTITY3", this._listM.getProperty(path + "/ZQUANTITY2") * plantResult[0].ZCONT1);
					}

				}
				break;
			default:
			}

			if (fieldName === "CUSTOMER" || fieldName === "MATERIAL" || fieldName === "SALESORG" || fieldName === "VTWEG" || fieldName ===
				"PAYER" || fieldName === "SHIPTO") {
				if (this._listM.getProperty(path + "/CUSTOMER") !== "" && this._listM.getProperty(path + "/MATERIAL") !== "" &&
					this._listM.getProperty(path + "/SALESORG") !== "" && this._listM.getProperty(path + "/DISTR_CHAN") !== "") {
					jsonData.push(this._listM.getProperty(path));
					this._prepareListFromBackend(jsonData, path, []);
				}
			}
		},

		onLiveChange: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName(),
				value = oEvent.getParameter("newValue"),
				bindingContext = oEvent.getSource().getBindingContext("listM") !== undefined ? oEvent.getSource().getBindingContext("listM") :
				sap
				.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("listM"),
				path = bindingContext.getPath();

			switch (fieldName) {
			case "CUSTOMER":
				this._listM.setProperty(path + "/SHIP_TO", "");
				this._listM.setProperty(path + "/PAYER", "");
				this._listM.setProperty(path + "/SALESORG", "");
				this._listM.setProperty(path + "/DISTR_CHAN", "");
				break;

			case "SALESORG":
				this._listM.setProperty(path + "/SHIP_TO", "");
				this._listM.setProperty(path + "/PAYER", "");
				this._listM.setProperty(path + "/DCUKYCP", "");
				break;

			case "VTWEG":
				this._listM.setProperty(path + "/SHIP_TO", "");
				this._listM.setProperty(path + "/PAYER", "");
				break;
			default:
			}
		},

		_checkCustomerAndMaterial: function (oEvent) {
			var fieldName = oEvent.getSource().getFieldName();
			if (fieldName === "CUSTOMER" && fieldName === "MATERIAL") {

			}
		},

		_prepareListFromBackend: function (data, path, prevData, isPQ) {
			var that = this,
				material = [],
				customer = [],
				vkorg = [],
				vtweg = [],
				kunwe = [],
				regu = [],
				zwaers2q = [],
				ZWAERS2 = [],
				DATAB = [],
				ZVALTO_CONT = [],
				ZPRICEVALIDITY = [],
				ship_cond = [],
				zland1 = [],
				zbonus1a = [],
				price_valid_from = [],
				price_valid_to = [],
				zquantity3 = [],
				target_volume = [],
				POSNR = [],
				MAT_DESC = [],
				PACK_DESC = [],
				promise1,
				promise2,
				row = null,
				length,
				materialValue;
			debugger;

			var tableData = this._listM.getData();
			tableData.forEach(function (line) {
				line.MAT_LENGTH = that.set6or9digit(line.MATERIAL).toString();
				if (line.INCOTERMS === "FCA" || line.INCOTERMS == "EXW") {
					line.VISIBLE_ZFREIGHT1 = false;
				} else {
					line.VISIBLE_ZFREIGHT1 = true;
				}
			});
			this._listM.setData(tableData);

			data.forEach(function (line) {
				if (line.MATERIAL !== "" && line.MATERIAL)
					material.push(line.MATERIAL);
				if (line.CUSTOMER !== "" && line.CUSTOMER)
					customer.push(line.CUSTOMER);
				if (line.SALESORG !== "" && line.SALESORG)
					vkorg.push(line.SALESORG);
				if (line.DISTR_CHAN !== "" && line.DISTR_CHAN)
					vtweg.push(line.DISTR_CHAN);
				if (line.SHIP_TO !== "" && line.SHIP_TO)
					kunwe.push(line.SHIP_TO);
				if (line.PAYER !== "" && line.PAYER)
					regu.push(line.PAYER);

				zwaers2q.push(line.ZWAERS2Q);
				ZWAERS2.push(line.ZWAERS2);
				ZPRICEVALIDITY.push(line.ZPRICEVALIDITY);
				ZVALTO_CONT.push(line.PRICE_VALID_TO);
				DATAB.push(line.PRICE_VALID_FROM);
				ship_cond.push(line.SHIP_COND);
				zland1.push("");
				zbonus1a.push(line.ZBONUS1A);
				price_valid_from.push(line.PRICE_VALID_FROM);
				price_valid_to.push(line.PRICE_VALID_TO);
				zquantity3.push("");
				target_volume.push(line.TARGET_VOLUME);
				POSNR.push(line.POSNR);

			});

			if (data.length > 0) {
				if (material.length && customer.length && vkorg.length && vtweg.length) //when page opened with needed data
				{
					promise1 = new Promise(function (resolved, rejected) {
						that._getMaterialData(POSNR, material, customer, vkorg, vtweg, resolved);
					});
					promise2 = new Promise(function (resolved, rejected) {
						that._getCustomerData(material, customer, vkorg, vtweg, kunwe, regu, zwaers2q, ZWAERS2, ZPRICEVALIDITY, ZVALTO_CONT, DATAB,
							ship_cond,
							zland1, zbonus1a, price_valid_from, price_valid_to, zquantity3, target_volume, resolved);
					});

					Promise.all([promise1, promise2]).then(function (result) {
						that._updateTableData(result, data, path, prevData, isPQ);
					}, function () {}.bind(this));
				}
			}

		},

		checkValidationMessage: function (oEvent) {
			var that = this,
				oButton = oEvent.getSource(),
				oTable = this._table,
				newSelIndex = [],
				aIndices = oTable.getBinding().aIndices,
				aSelIndex = oTable.getSelectedIndices(),
				path = oButton.getBindingContext("listM").getPath(),
				line = this._listM.getProperty(path),
				data = [];
			data.push(line);
			this._getIRPAMessages(data);
		},

		onCheckForValidation: function (oEvent) {
			var that = this,
				oButton = oEvent.getSource(),
				oTable = this._table,
				newSelIndex = [],
				aIndices = oTable.getBinding().aIndices,
				aSelIndex = oTable.getSelectedIndices(),
				data = [];
			oButton.setPressed(true);
			if (!aSelIndex.length) {
				MessageToast.show("Select at least one record");
				return;
			} else {
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});
				aSelIndex = newSelIndex;
				aSelIndex.forEach(function (index, ind) {
					data.push(that._listM.getData()[index]);
				});
				this._getIRPAMessages(data);
			}
		},

		_getIRPAMessages: function (data) {
			var that = this,
				sendingDataArray = [],
				sendingData = {},
				finalData = {},
				MATERIAL_IRPA = [],
				CUSTOMER_IRPA = [],
				FIORI_IRPA = [],
				SCENARIO = [];

			data.forEach(function (line) {
				sendingData = {};
				MATERIAL_IRPA = [];
				CUSTOMER_IRPA = [];
				FIORI_IRPA = [];
				if (line.MATERIALDATA_MAIN === null || line.MATERIALDATA_MAIN === undefined) line.MATERIALDATA_MAIN = line.MATERIALDATA;
				var materialLine = line.MATERIALDATA_MAIN.filter(function (mat) {
					return line.PLANT === mat.PLANT;
				});
				var lineMat;
				if (materialLine === undefined) lineMat = [];
				else
					lineMat = materialLine.length ? materialLine[0] : [];
				MATERIAL_IRPA.push(lineMat);
				CUSTOMER_IRPA.push(line.CUSTOMERDATA_MAIN[0]);
				FIORI_IRPA.push(line);

				sendingData.MATERIAL_IRPA = MATERIAL_IRPA[0];
				sendingData.CUSTOMER_IRPA = CUSTOMER_IRPA[0];
				sendingData.FIORI_IRPA = FIORI_IRPA[0];
				sendingDataArray.push(sendingData);
			});

			SCENARIO = sap.ui.getCore().getModel("globalModel").getProperty("/scenarios");

			finalData.VALIDATION = sendingDataArray;
			finalData.SCENARIO = SCENARIO;

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "IRPA_MESSAGE"
				},
				"INPUTPARAMS": [finalData]
			};

			var finalItemNumbers = finalData.VALIDATION.map(function (oContext) {
				return parseInt(oContext.FIORI_IRPA.POSNR);
			});

			dbcontext.callServer(params, function (oModel) {
				that.handleServerMessages(oModel, function (status) {
					if (status === "" || status === "S") {
						var data = oModel.getData();
						var itemNos = data.ROWS.map(function (oContext) {
							return {
								POSNR: parseInt(oContext.ROWS),
								STATUS: "E"
							};
						});
						var array = [];
						finalItemNumbers.forEach(function (final) {
							var jLine = {};
							jLine.POSNR = final;
							var filtered = itemNos.filter(function (filter) {
								return filter.POSNR === final;
							});
							if (filtered.length)
								jLine.STATUS = "E";
							else
								jLine.STATUS = "S";

							array.push(jLine);
						});

						var tableData = that._listM.getData();

						for (var k = 0; k < tableData.length; k++) {
							for (var kk = 0; kk < array.length; kk++) {
								if (tableData[k].POSNR === array[kk].POSNR) {
									tableData[k].READY = array[kk].STATUS === "S";
								}
							}
						}
						that._listM.setData(tableData);

					}
				});
			}, this);
		},

		_checkGebart: function (line) {
			return line["ZZ_GEBART"] === "0001" || line["ZZ_GEBART"] === "0005" || line["ZZ_GEBART"] === "0170";
		},

		_updateTableData: function (result, data, path, prevData, isPQ) {
			var materialValue, row = null,
				filteredResult1,
				filteredResult1_M,
				//for customer data
				filteredResult1_Customer,
				filteredResult1_Customer_M,
				that = this,
				arrayFiltered = [],
				scenarios = sap.ui.getCore().getModel("globalModel").getProperty("/scenarios");

			if (path !== undefined) {
				row = this._listM.getProperty(path);
			}

			if (result[0].SCNERAIO.length) {
				scenarios = scenarios.concat(result[0].SCNERAIO);
			}

			if (row !== null) {
				materialValue = row.MATERIAL.split(".").join('');
				materialValue = materialValue.replace(/ /g, '');
				arrayFiltered = result[0].MATERIALDATA;
				if (row["MANUAL"] !== false) {

					arrayFiltered = result[0].MATERIALDATA.filter(function (mm) {
						return mm.LVORM_PLANT !== "X" && mm.LVORM !== "X" && mm.MSTAE !== "90" && mm.MMSTA !== "90" &&
							mm.MSTAV !== "03" && (mm.PT_ACTIVE !== "" || mm.PT_LIGHT_ACTIV !== "");
					});
				}

				filteredResult1 = arrayFiltered.filter(function (line) {
					line.MATNR = line.MATNR.replace(/^0+/, '');
					return materialValue === line.MATNR;
				});

				if (row.PLANT !== "" && row.PLANT !== undefined)
					var filteredResult1_new = arrayFiltered.filter(function (line3) {
						line3.MATNR = line3.MATNR.replace(/^0+/, '');
						return row.PLANT === line3.PLANT && materialValue === line3.MATNR;
					});

				var finalLine = null;

				if (filteredResult1_new !== undefined) {
					if (filteredResult1_new.length > 0) {
						finalLine = filteredResult1_new[0];
					} else {
						if (filteredResult1_new.length === 1) {
							finalLine = filteredResult1_new[0];
						}
					}
				} else {
					if (filteredResult1.length === 1) {
						finalLine = filteredResult1[0];
					}
				}

				if (finalLine !== null) {
					if (isPQ === "PR") {
						that._listM.setProperty(path + "/MATERIALDATA_MAIN", filteredResult1);
						that._listM.setProperty(path + "/DATA_MATERIAL", filteredResult1.length);
						that._listM.setProperty(path + "/GEBART_EDIT", that._checkGebart(that._listM.getProperty(path)));
					} else {
						that._listM.setProperty(path + "/ZMATNR", finalLine.ZMATNR);
						that._listM.setProperty(path + "/ZMATNR_BULK", finalLine.ZMATNR_BULK);
						that._listM.setProperty(path + "/ZZEINR", finalLine.ZZEINR);
						that._listM.setProperty(path + "/ZBRIX1", finalLine.ZBRIX1);
						that._listM.setProperty(path + "/ZZ_XVFKZ", finalLine.ZZ_XVFKZ);
						that._listM.setProperty(path + "/PLANT", finalLine.PLANT);
						that._listM.setProperty(path + "/MATERIAL_LENGTH", filteredResult1.length.toString());
						that._listM.setProperty(path + "/MEINS", finalLine.MEINS);
						that._listM.setProperty(path + "/UNIT", finalLine.MEINS);
						that._listM.setProperty(path + "/ZZKALK03", finalLine.ZZKALK03);
						that._listM.setProperty(path + "/ZZ_GEBART", finalLine.ZZ_GEBART);
						that._listM.setProperty(path + "/ZCONT1", finalLine.ZCONT1);
						that._listM.setProperty(path + "/ZQUANTITY2MIN", finalLine.ZQUANTITY2MIN);
						that._listM.setProperty(path + "/ZQUANTITY3", that._listM.getProperty(path + "/ZQUANTITY2") * finalLine.ZCONT1);
						that._listM.setProperty(path + "/MATERIALDATA_MAIN", filteredResult1);
						that._listM.setProperty(path + "/DATA_MATERIAL", filteredResult1.length);
						that._listM.setProperty(path + "/MAT_DESC", finalLine.MAKTX);
						that._listM.setProperty(path + "/PACK_DESC", finalLine.GEBART_DESC);
						that._listM.setProperty(path + "/DCUKYCP", finalLine.ZWAERS_PLANT);
						that._listM.setProperty(path + "/ZWAERS2", finalLine.ZWAERS_PLANT);
						that._listM.setProperty(path + "/GEBART_EDIT", that._checkGebart(finalLine));
					}
				}
				that._listM.setProperty(path + "/MATERIALDATA", filteredResult1);

				filteredResult1_Customer = result[1].CUSTOMERDATA.filter(function (line) {
					if (row.PAYER === "" && row.SHIP_TO === "")
						return row.CUSTOMER === line.KUNNR &&
							parseInt(row.SALESORG) === parseInt(line.VKORG) && row.DISTR_CHAN === line.VTWEG;

					if (row.PAYER !== "" && row.SHIP_TO === "")
						return row.CUSTOMER === line.KUNNR &&
							parseInt(row.SALESORG) === parseInt(line.VKORG) && row.DISTR_CHAN === line.VTWEG && row.PAYER === line.ZREGU;
					if (row.PAYER === "" && row.SHIP_TO !== "")
						return row.CUSTOMER === line.KUNNR &&
							parseInt(row.SALESORG) === parseInt(line.VKORG) && row.DISTR_CHAN === line.VTWEG && row.SHIP_TO === line.ZKUNWE;
					if (row.PAYER !== "" && row.SHIP_TO !== "")
						return row.CUSTOMER === line.KUNNR &&
							parseInt(row.SALESORG) === parseInt(line.VKORG) && row.DISTR_CHAN === line.VTWEG && row.PAYER === line.ZREGU &&
							row.SHIP_TO === line.ZKUNWE;
				});
				that._listM.setProperty(path + "/CUSTOMERDATA", filteredResult1_Customer);
				that._listM.setProperty(path + "/CUSTOMERDATA_MAIN", filteredResult1_Customer);
				that._listM.setProperty(path + "/DATA_CUSTOMER", filteredResult1_Customer.length);
				if (isPQ !== "PR")
					if (filteredResult1_Customer.length) {
						// that._listM.setProperty(path + "/DCUKYCP", filteredResult1_Customer[0].WAERS);
						// that._listM.setProperty(path + "/ZWAERS2", filteredResult1_Customer[0].WAERS);
						that._listM.setProperty(path + "/ZSUBCURR", filteredResult1_Customer[0].ZSUBCURR);
						that._listM.setProperty(path + "/ZWAERS3AQ", filteredResult1_Customer[0].ZWAERS3AQ);
						that._listM.setProperty(path + "/ZWAERS2Q", filteredResult1_Customer[0].ZWAERS2Q);
						that._listM.setProperty(path + "/ZCUSTBONUS", filteredResult1_Customer[0].ZCUSTBONUS);
						that._listM.setProperty(path + "/ZBONUS1", filteredResult1_Customer[0].ZBONUS1);
						that._listM.setProperty(path + "/ZKZSHE", filteredResult1_Customer[0].ZKZSHE);
						that._listM.setProperty(path + "/ZHARBOUR", filteredResult1_Customer[0].ZHARBOUR);
						that._listM.setProperty(path + "/PAYER", filteredResult1_Customer[0].ZREGU);
						that._listM.setProperty(path + "/SHIP_TO", filteredResult1_Customer[0].ZKUNWE);

					}

			} else {
				data.forEach(function (line, ind) {
					materialValue = line.MATERIAL.split(".").join('');
					materialValue = materialValue.replace(/ /g, '');
					arrayFiltered = result[0].MATERIALDATA;
					if (line["MANUAL"] !== false) {

						arrayFiltered = result[0].MATERIALDATA.filter(function (mm) {
							return mm.LVORM_PLANT !== "X" && mm.LVORM !== "X" && mm.MSTAE !== "90" && mm.MMSTA !== "90" &&
								mm.MSTAV !== "03" && (mm.PT_ACTIVE !== "" || mm.PT_LIGHT_ACTIV !== "");
						});
					}
					filteredResult1_M = arrayFiltered.filter(function (line2) {
						line2.MATNR = line2.MATNR.replace(/^0+/, '');
						return materialValue === line2.MATNR;
					});

					if (line.PLANT !== "" && line.PLANT !== undefined)
						var filteredResult1_M_new = arrayFiltered.filter(function (line3) {
							return line.PLANT === line3.PLANT && materialValue === line3.MATNR;
						});

					var finalLine = null;

					if (filteredResult1_M_new !== undefined) {
						if (filteredResult1_M_new.length > 0) {
							finalLine = filteredResult1_M_new[0];
						} else {
							if (filteredResult1_M.length === 1) {
								finalLine = filteredResult1_M[0];
							}
						}
					} else {
						if (filteredResult1_M.length === 1) {
							finalLine = filteredResult1_M[0];
						}
					}

					if (finalLine !== null) {
						if (isPQ === "PR") {
							line.MATERIALDATA_MAIN = filteredResult1_M;
							line.DATA_MATERIAL = filteredResult1_M.length ? true : false;
							line["GEBART_EDIT"] = that._checkGebart(line);
						} else {
							line.ZMATNR = finalLine.ZMATNR;
							line.ZMATNR_BULK = finalLine.ZMATNR_BULK;
							line.ZZEINR = finalLine.ZZEINR;
							line.ZBRIX1 = finalLine.ZBRIX1;
							line.ZZ_XVFKZ = finalLine.ZZ_XVFKZ;
							line.PLANT = finalLine.PLANT;
							line.MATERIAL_LENGTH = filteredResult1_M.length.toString();
							line.MEINS = finalLine.MEINS;
							line.UNIT = finalLine.MEINS;
							line.ZZKALK03 = finalLine.ZZKALK03;
							line.ZZ_GEBART = finalLine.ZZ_GEBART;
							line.ZCONT1 = finalLine.ZCONT1;
							line.ZQUANTITY2MIN = finalLine.ZQUANTITY2MIN;
							line.ZQUANTITY3 = line.ZQUANTITY2 * finalLine.ZCONT1;
							line.MATERIALDATA_MAIN = filteredResult1_M;
							line.DATA_MATERIAL = filteredResult1_M.length ? true : false;
							line.MAT_DESC = finalLine.MAKTX;
							line.PACK_DESC = finalLine.GEBART_DESC;
							line.ZWAERS2 = finalLine.ZWAERS_PLANT;
							line.DCUKYCP = finalLine.ZWAERS_PLANT;
							line["GEBART_EDIT"] = that._checkGebart(finalLine);
						}
					}
					line.MATERIALDATA = filteredResult1_M;

					filteredResult1_Customer = result[1].CUSTOMERDATA.filter(function (line2) {
						if (line.PAYER === "" && line.SHIP_TO === "")
							return line.CUSTOMER === line2.KUNNR &&
								parseInt(line.SALESORG) === parseInt(line2.VKORG) && line.DISTR_CHAN === line2.VTWEG;
						if (line.PAYER !== "" && line.SHIP_TO === "")
							return line.CUSTOMER === line2.KUNNR &&
								parseInt(line.SALESORG) === parseInt(line2.VKORG) && line.DISTR_CHAN === line2.VTWEG && line.PAYER === line2.ZREGU;
						if (line.PAYER === "" && line.SHIP_TO !== "")
							return line.CUSTOMER === line2.KUNNR &&
								parseInt(line.SALESORG) === parseInt(line2.VKORG) && line.DISTR_CHAN === line2.VTWEG && line.SHIP_TO === line2.ZKUNWE;
						if (line.PAYER !== "" && line.SHIP_TO !== "")
							return line.CUSTOMER === line2.KUNNR &&
								parseInt(line.SALESORG) === parseInt(line2.VKORG) && line.DISTR_CHAN === line2.VTWEG && line.PAYER === line2.ZREGU &&
								line.SHIP_TO === line2.ZKUNWE;
					});
					line.CUSTOMERDATA = filteredResult1_Customer;
					line.CUSTOMERDATA_MAIN = filteredResult1_Customer;
					line.DATA_CUSTOMER = filteredResult1_Customer.length ? true : false;
					if (isPQ !== "PR")
						if (filteredResult1_Customer.length) {
							// line.ZWAERS2 = filteredResult1_Customer[0].WAERS;
							// line.DCUKYCP = filteredResult1_Customer[0].WAERS;
							line.ZSUBCURR = filteredResult1_Customer[0].ZSUBCURR;
							line.ZWAERS3AQ = filteredResult1_Customer[0].ZWAERS3AQ;
							line.ZWAERS2Q = filteredResult1_Customer[0].ZWAERS2Q;
							line.ZCUSTBONUS = filteredResult1_Customer[0].ZCUSTBONUS;
							line.ZBONUS1 = filteredResult1_Customer[0].ZBONUS1;
							line.ZKZSHE = filteredResult1_Customer[0].ZKZSHE;
							line.ZHARBOUR = filteredResult1_Customer[0].ZHARBOUR;
							line.PAYER = filteredResult1_Customer[0].ZREGU;
							line.SHIP_TO = filteredResult1_Customer[0].ZKUNWE;

						}

						//that._listM.setProperty("/" + ind, line);
				});
				var last = prevData.concat(data);
				var last2 = [];
				if (that._listM.getData().length === last.length) {
					for (var a = 0; a < that._listM.getData().length; a++) {
						var posnr = that._listM.getData()[a]["POSNR"];
						for (var ab = 0; ab < last.length; ab++) {
							if (posnr === last[ab]["POSNR"])
								last2.push(last[ab]);

						}
					}
				} else last2 = last;
				last2.forEach(function (kk) {
					if (kk.INCOTERMS === "FCA" || kk.INCOTERMS === "EXW") {
						kk.VISIBLE_ZFREIGHT1 = false;
					} else {
						kk.VISIBLE_ZFREIGHT1 = true;
					}
				});
				that._listM.setData(last2);

			}
			sap.ui.getCore().getModel("globalModel").setProperty("/scenarios", scenarios);

		},

		_getMaterialData: function (POSNR, material, customer, vkorg, vtweg, resolved) {
			var sendData = {};
			var array = [];
			var that = this;
			sendData.POSNR = POSNR.join(",");
			sendData.CUSTOMER = customer.join(",");
			sendData.MATERIAL = material.join(",");
			sendData.VKORG = vkorg.join(",");
			sendData.VTWEG = vtweg.join(",");

			array.push(sendData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_MATERIAL_IRPA"
				},
				"INPUTPARAMS": array
			};

			dbcontext.callServer(params, function (oModel) {
				resolved(oModel.getData());
			}, this);
		},

		_getCustomerData: function (material, customer, vkorg, vtweg, kunwe, regu, zwaers2q, ZWAERS2, ZPRICEVALIDITY, ZVALTO_CONT, DATAB,
			ship_cond,
			zland1, zbonus1a, price_valid_from, price_valid_to, zquantity3, target_volume, resolved) {
			var sendData = {};
			var array = [];

			for (var i = 0; i < material.length; i++) {
				if (price_valid_from[i].indexOf(".") > -1) {
					if (price_valid_from[i].split(".").length === 3)
						price_valid_from[i] = price_valid_from[i].split(".")[2] + "-" + price_valid_from[i].split(".")[1] + "-" + price_valid_from[i]
						.split(
							".")[0];
				}
				if (price_valid_to[i].indexOf(".") > -1) {
					if (price_valid_to[i].split(".").length === 3)
						price_valid_to[i] = price_valid_to[i].split(".")[2] + "-" + price_valid_to[i].split(".")[1] + "-" + price_valid_to[i].split(
							".")[0];
				}
				if (DATAB[i].indexOf(".") > -1) {
					if (DATAB[i].split(".").length === 3)
						DATAB[i] = DATAB[i].split(".")[2] + "-" + DATAB[i].split(".")[1] + "-" + DATAB[i].split(
							".")[0];
				}
				if (ZVALTO_CONT[i].indexOf(".") > -1) {
					if (ZVALTO_CONT[i].split(".").length === 3)
						ZVALTO_CONT[i] = ZVALTO_CONT[i].split(".")[2] + "-" + ZVALTO_CONT[i].split(".")[1] + "-" + ZVALTO_CONT[i].split(
							".")[0];
				}
				sendData = {};
				sendData.CUSTOMER = customer[i];
				sendData.MATERIAL = material[i];
				sendData.VKORG = vkorg[i];
				sendData.VTWEG = vtweg[i];
				sendData.ZKUNWE = kunwe[i];
				sendData.ZREGU = regu[i];
				sendData.ZWAERS2Q = zwaers2q[i];
				sendData.ZWAERS2 = ZWAERS2[i];
				sendData.ZPRICEVALIDITY = ZPRICEVALIDITY[i];
				sendData.ZVALTO_CONT = ZVALTO_CONT[i];
				sendData.DATAB = DATAB[i];
				sendData.SHIP_COND = ship_cond[i];
				sendData.LAND1 = zland1[i];
				sendData.ZBONUS1A = zbonus1a[i];
				sendData.PRICE_VALID_FROM = price_valid_from[i];
				sendData.PRICE_VALID_TO = price_valid_to[i];
				sendData.ZQUANTITY3 = zquantity3[i];
				sendData.TARGET_VOLUME = target_volume[i];
				array.push(sendData);

			}

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_CUSTOMER_IRPA"
				},
				"INPUTPARAMS": array
			};

			dbcontext.callServer(params, function (oModel) {
				resolved(oModel.getData());
			}, this);

		},

		_getControlsFromView: function () {
			this._table = this.getView().byId("listTableId");
			this._globalM = sap.ui.getCore().getModel("globalModel");
			this._listM = sap.ui.getCore().getModel("listM");
		},

		_bindModels: function () {
			var that = this;
			var aModels = ["globalModel", "listM"];
			aModels.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});

			var aDropdown = ["priceValDD", "fixedTimeDelDD"];
			aDropdown.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
		},

		loadDropdown: function () {
			models.loadPriceValidity();
			models.loadFixedTimeDelivery();
		},

		onTableRowSelect: function (oEvent) {
			// var row = this._listM.getProperty(oEvent.getParameter("rowContext").getPath());
			// if (oEvent.getParameter("selectAll") !== true) {
			// 	if (row.READY === false) {
			// 		MessageToast.show("This line is not ready for BOT. Please use status button to check validaiton!");
			// 		oEvent.getSource().removeSelectionInterval(oEvent.getParameter("rowIndex"), 0);
			// 	}
			// }
		},

		_fillColumnNamesForList: function () {
			var list = [],
				aColumns = this._table.getColumns();

			aColumns.forEach(function (oColumn) {
				if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
					if (oColumn.getLabel().getText() !== "Packaging" && oColumn.getLabel().getText() !== "Sea Harbour" && oColumn.getLabel().getText() !==
						"Status" && oColumn.getLabel().getText() !==
						"Item No" && oColumn.getLabel().getText() !==
						"Customer Target volume" && oColumn.getLabel().getText() !== "Filling quantity" && oColumn.getLabel().getText() !==
						"Order size" && oColumn.getLabel().getText() !== "Minimum order size" && oColumn.getLabel().getText() !==
						"Customer Price valid from" && oColumn.getLabel().getText() !== "Customer Price valid to" && oColumn.getLabel().getText() !==
						"Shipping condition" && oColumn.getLabel().getText() !== "Incoterm" && oColumn.getLabel().getText() !==
						"No. of rebate agreement" && oColumn.getLabel().getText() !== "No. of rebate agreement (%)" && oColumn.getLabel().getText() !==
						"Pallet")

						list.push({
						key: oColumn.getSortProperty(),
						text: oColumn.getLabel().getText()
					});
				}
			});

			this._globalM.setProperty("/columnHeaderList", list);
			this._globalM.setProperty("/keyColumnHeader", "");
		},

		onSelectCheckBox: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("listM").getPath();
			this._listM.setProperty(path + "/ZPAL2", oEvent.getSource().getSelected() ? "X" : "");
		},

		onSelectCheckBox2: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("listM").getPath();
			this._listM.setProperty(path + "/ZABS", oEvent.getSource().getSelected() ? "X" : "");
		},

		selectionChange: function (oEvent) {
			var key = oEvent.getParameter("selectedItem").getKey(),
				contentRight = this.getView().byId("listBarId").getItems(),
				customData1,
				headerFilterControls = [];
			contentRight.forEach(function (contentR) {
				customData1 = contentR.getCustomData();
				if (customData1.length) {
					customData1 = customData1[0].getValue();
					headerFilterControls.push(contentR);
				}
			});

			headerFilterControls.forEach(function (control) {
				control.setVisible(control.getCustomData()[0].getValue() === key);
			});

		},

		_getLine: function (posnr) {
			return {
				POSNR: posnr,
				CUSTOMER: "", // Customer
				PQNUM: "",
				FROM_PQ: false,
				MATERIAL: "", // Material
				SHIP_TO: "", // Ship to
				PAYER: "", // Payer 
				SALESORG: "", // Sales Org
				DISTR_CHAN: "", // Disturbition Channel,
				ORIGINATOR: "", //Originator
				INCOTERMS: "",
				PMNTTRMS: "",
				DCUKYCP: "",
				ZWAERS2Q: "", //Quotation Currency,
				ZWAERS2: "",
				PLANT: "",
				TARGET_VOLUME: 0,
				CUSTOMER_TARGET_PRICE: 0,
				ZQUANTITY2: 0, // Order size in no. of units,
				SHIP_COND: "",
				DOEKMEIN2: "", // alternative uom
				ZCOMM1A: 0, // Commission 3rd party,
				ZCOM1: 0, // Commission 3rd party %,
				UNIT: "-",
				ZFREIGHT1: 0, //freight-out
				VISIBLE_ZFREIGHT1: false,
				ZFINSALES: 0, //final sales price,
				PRICE_VALID_FROM: "",
				PRICE_VALID_TO: "",
				TITLE: "",
				OPP_ID: "",
				ZWAERS3AQ: 0, //Exchange Rate
				ZPRICEVALIDITY: "", // RM SPOT,
				ZZKALK03: "", // Microbiology,
				ZZ_GEBART: "", // packaging,
				ZDOSAGE: 0, //Dosage
				ZCOST2A: 0, //Target Cost-in-Use Doehler
				ZSUBCURR: "",
				ZINCO2: "", // Place of fulfillment
				ZFRTWGHT: 0, //Freight weight
				ZFIXTIMDEL: "", //Fixed time delivery
				ZABS: "", //Calculate on average batch size,
				MEINS: "",
				ZCOST8A: 0, //Payment terms discount
				ZLAST: 0, // debit discount
				ZBONUS1A: "", // No. of rebate agreement
				ZBONUS1: 0, // Rebates
				ZCUSTBONUS1: "", //No. of rebate agreement, perc,
				ZCUSTBONUS: 0, // Rebates perc
				ZOTHEXP: 0, //other cost
				ZFNCECST: 0, //financial cost
				ZPRICE_INT7: 0, //document cost
				ZCOST7B_PRCT_MAN: 0, //Import duty in % (to customer) (manual)
				ZCOST7B_KG_MAN: 0, //Import duty per kg (to customer) (manual)
				ZCOST7F_MAN: 0, //Other import cost per kg (to customer) (manual)
				ZCOST7D: 0, // VAT,
				ZIC_BIZ1: "", //Business Type
				ZIC_BIZ5: 0, //Order size subsidiary in no. of units 
				ZIC_BIZ7: "", //Shipping Condition to subsidiary
				ZIC_BIZ2: "", //Incoterms with subsidiary
				ZIC_BIZ10: 0, //IC-Freight/Freight Drop shipment 
				ZIC_BIZ10_BORDER_MAN: 0, //Thereof freight till Customs Depar
				ZIC_BIZ12_MAN: 0, //Import duty in % (to subsidiary) (manual)
				ZIC_BIZ13_MAN: 0, //Import duty in per kg (to subsidiary) (manual)
				ZIC_BIZ14_MAN: 0, //Other import cost per kg (to subsidiary) (manual) 
				ZCOST7C_KG_MAN: 0, //Excise taxes per kg (to subsidiary) (manual) 
				ZFREIGHT3: 0, //Freight from warehouse to customer 
				ZKZSHE: "", // sea harbour visible if it is X,
				ZHARBOUR: "",
				MAT_LENGTH: "",
				SEA_HARBOUR: "",
				ZCONT1: "", // filling quantity,
				ZQUANTITY3: "", //order size,
				ZQUANTITY2MIN: "", //minumum order size
				ZPAL2: "", // Pallet Y / N\
				PALLET: "", // Pallet
				TRIGGER_Q: "",
				TRIGGER_A: "",
				ZMATNR: "",
				ZMATNR_BULK: "",
				ZZEINR: 0,
				ZBRIX1: 0,
				ZZ_XVFKZ: "",
				MAT_DESC: "", // Description	
				PACK_DESC: "", // Packagin Description
				GEBART_EDIT: false
			};
		},

		onAdd: function (oEvent) {
			var list = this._listM.getData();
			var posnr = list.length ? list[list.length - 1]["POSNR"] + 1 : 1;
			var newLine = this._getLine(posnr);

			list.push(newLine);
			this._listM.setData(list);
			MessageToast.show("Row has been added.");
		},

		onCopy: function (oEvent) {
			var selectedIndices = this._table.getSelectedIndices(),
				list = this._listM.getData(),
				copyData = [];
			if (!selectedIndices.length) {
				MessageToast.show("Please select at least one row!");
				return;
			}
			selectedIndices.forEach(function (index) {
				var currentData = $.extend(true, {}, list[index]);
				copyData.push(currentData);
			});

			var pos = list[list.length - 1]["POSNR"] + 1;
			for (var i = 0; i < copyData.length; i++) {
				copyData[i]["POSNR"] = pos++;
			}

			list = list.concat(copyData);
			this._listM.setData(list);
			this._table.setSelectedIndex(-1);
			MessageToast.show("Copied");
		},

		onDelete: function (oEvent) {
			var selectedIndices = this._table.getSelectedIndices(),
				list = this._listM.getData(),
				i = 0,
				allIndex = [],
				currentData = [];

			if (!selectedIndices.length) {
				MessageToast.show("Please select at least one row!");
				return;
			}

			list.forEach(function (row, m) {
				allIndex.push(m);
			});

			for (i = 0; i < allIndex.length; i++) {
				selectedIndices.forEach(function (m) {
					if (allIndex[i] === m) {
						allIndex.splice(i, 1);
					}
				});
			}

			allIndex.forEach(function (m) {
				currentData.push(list[m]);
			});
			this._listM.setData(currentData);
			this._table.setSelectedIndex(-1);
			MessageToast.show("Deleted");
		},

		onPaste: function (oEvent) {
			var list = this._listM.getData(),
				table = this._table,
				pasteData = oEvent.getParameter("data"),
				sortProperty,
				column,
				excelData = [],
				positionNumber = 1;

			if (list.length)
				positionNumber = list[list.length - 1].POSNR + 1;

			excelData = this._prepateExcelData(pasteData, "P");

			for (var m = 0; m < excelData.length; m++) {
				excelData[m]["POSNR"] = positionNumber++;
				Object.keys(excelData[m]).forEach(function (line) {
					if (line === "MATERIAL") {
						excelData[m][line] = excelData[m][line].split(".").join('');
						excelData[m][line] = excelData[m][line].replace(/ /g, '');
					} else
						excelData[m][line] = excelData[m][line];

					if (line === "ZPAL2" ||
						line === "ZABS") excelData[m][line] = excelData[m][line];

					if (line === "TARGET_VOLUME" ||
						line === "CUSTOMER_TARGET_PRICE" ||
						line === "ZQUANTITY2" ||
						line === "ZCOMM1A" ||
						line === "ZCOM1" ||
						line === "ZFREIGHT1" ||
						line === "ZFINSALES" ||
						line === "ZWAERS3AQ" ||
						line === "ZDOSAGE" ||
						line === "ZCOST2A" ||
						line === "ZCOST8A" ||
						line === "ZLAST" ||
						line === "ZBONUS1" ||
						line === "ZCUSTBONUS" ||
						line === "ZOTHEXP" ||
						line === "ZLOSTCST" ||
						line === "ZFNCECST" ||
						line === "ZPRICE_INT7" ||
						line === "ZCOST7B_PRCT_MAN" ||
						line === "ZCOST7B_KG_MAN" ||
						line === "ZCOST7F_MAN" ||
						line === "ZCOST7D" ||
						line === "ZIC_BIZ5" ||
						line === "ZIC_BIZ10" ||
						line === "ZIC_BIZ10_BORDER_MAN" ||
						line === "ZIC_BIZ12_MAN" ||
						line === "ZIC_BIZ13_MAN" ||
						line === "ZIC_BIZ14_MAN" ||
						line === "ZCOST7C_KG_MAN" ||
						line === "ZFREIGHT3"
					) {
						excelData[m][line] = excelData[m][line].replace(".", "");
						excelData[m][line] = excelData[m][line].replace(",", ".");
						if (isNaN(excelData[m][line])) excelData[m][line] = "0";
					}

				});
			}
			//list = list.concat(excelData);
			this._sendFirstValidation(excelData, undefined, list);

		},

		onUploadPress: function (oEvent) {
			var currentData = $.extend(true, [], this._listM.getData());

			var oFileUploader = this.getView().byId("fileUploader");
			if (!oFileUploader.getValue()) {
				MessageToast.show("Choose a file first");
				return;
			} else {
				this.typeXLSX();
			}
		},

		_sendFirstValidation: function (excelData, undefined, prevData) {

			this._prepareListFromBackend(excelData, undefined, prevData);
			//this._listM.setData(excelData);
		},

		typeXLSX: function () {
			var that = this;
			var oFileUploader = that.getView().byId("fileUploader");
			var file = oFileUploader.getFocusDomRef().files[0];
			var excelData = {};
			if (file && window.FileReader) {
				var reader = new FileReader();
				reader.onload = function (evt) {
					var data = evt.target.result;
					var workbook = XLSX.read(data, {
						type: 'binary'
					});
					workbook.SheetNames.forEach(function (sheetName) {
						excelData = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
						// excelData.shift();
						var list = that._listM.getData();
						var pos = list.length ? list[list.length - 1]["POSNR"] + 1 : 1;
						excelData = that._prepateExcelData(excelData, "U");
						for (var m = 0; m < excelData.length; m++) {
							excelData[m]["POSNR"] = pos++;
							excelData[m]["PLANT"] = "";
						}
						//	list = list.concat(excelData);
						that._sendFirstValidation(excelData, undefined, list);

					});
				};
				reader.onerror = function (ex) {
					console.log(ex);
				};
				reader.readAsBinaryString(file);

			}
		},

		_prepateExcelData: function (array, mod) {
			var excelData = [],
				excelLine = {},
				columnKeyData = [],
				columnText, type, property,
				that = this,
				table = this._table,
				aColumns = table.getColumns();

			aColumns.forEach(function (oColumn) {
				if (oColumn.mSkipPropagation.template) {
					columnText = oColumn.getLabel().getText();
					if (oColumn.getLabel().getCustomData().length) {
						type = oColumn.getLabel().getCustomData()[0].getValue();
						property = oColumn.getLabel().getCustomData()[1].getValue();
						columnKeyData.push({
							key: property,
							text: columnText
						});
					}
				}
			});

			if (mod === "U") {
				array.forEach(function (row) {
					excelLine = that._getLine(0);
					Object.keys(row).forEach(function (key) {
						var filter = columnKeyData.filter(function (ctx) {
							return key === ctx.text;
						});

						if (filter.length) {
							excelLine[filter[0].key] = row[filter[0].text];
							if (filter[0].key === "PRICE_VALID_FROM" || filter[0].key === "PRICE_VALID_TO" ||
								filter[0].key === "CUSTOMER_VALID_FROM" || filter[0].key === "CUSTOMER_VALID_TO") {
								var func = function ExcelDateToJSDate(serial) {
									var utc_days = Math.floor(serial - 25569);
									var utc_value = utc_days * 86400;
									var date_info = new Date(utc_value * 1000);

									var fractional_day = serial - Math.floor(serial) + 0.0000001;

									var total_seconds = Math.floor(86400 * fractional_day);

									var seconds = total_seconds % 60;

									total_seconds -= seconds;

									var hours = Math.floor(total_seconds / (60 * 60));
									var minutes = Math.floor(total_seconds / 60) % 60;

									return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
								};
								var oFormat = sap.ui.core.format.DateFormat.getDateInstance({
									pattern: "yyyy-MM-dd"
								});
								var date = func(row[filter[0].text]);
								date = oFormat.format(date);
								excelLine[filter[0].key] = date;
							}
						}

					});

					excelData.push(excelLine);
				});
			} else {
				for (var mm = 1; mm < array.length; mm++) {
					excelLine = that._getLine(0);
					for (var m = 0; m < array[0].length; m++) {
						var filter = columnKeyData.filter(function (ctx) {
							return array[0][m] === ctx.text;
						});
						if (filter.length) {
							excelLine[filter[0].key] = array[mm][m];
						}
					}
					excelData.push(excelLine);
				}

			}

			return excelData;

		},

		onSort: function (oEvent) {
			var data = this._listM.getData(),
				sortOrder = oEvent.getParameter("sortOrder"),
				sortFunction,
				sortColumnData = oEvent.getParameter("column").getSortProperty().split("/");
			if (sortColumnData.length > 0) {
				sortFunction = function sortByKey(array, key) {
					return array.sort(function (a, b) {
						var x = a[sortColumnData[0]];
						var y = b[sortColumnData[0]];
						return ((x > y) ? -1 : ((x < y) ? 1 : 0));
					});
				};
				data = sortFunction(data, sortColumnData[0]);
				if (sortOrder === "Ascending") {
					data = data.reverse();
				}
				this._listM.setData(data);
			}
		},

		/*------------------------------------------------------------------------ START OF VARIANT MANAGEMENT --------------------------------------------------------- */
		oCC: null,
		currTableData: null,
		oTPC: null,
		tableId: "listTableId",
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz.tableId);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var oVM = this.getView().byId("tableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(tableId, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(tableId);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oItem[oItem.defaultKey].rowCount === undefined ? 20 : oItem[oItem.defaultKey].rowCount);
				oTable.setVisibleRowCountMode(oItem[oItem.defaultKey].rowCount === undefined ? "Interactive" : "Fixed");
				//thiz._fillColumnNamesForList();

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			debugger;
			this.setPersoData(oTable, oItem[selKey].data); // apply data
			oTable.setVisibleRowCountMode("Fixed");
			oTable.setVisibleRowCount(oItem[selKey].rowCount === undefined ? 20 : oItem[selKey].rowCount);
			oTable.setVisibleRowCountMode(oItem[selKey].rowCount === undefined ? "Interactive" : "Fixed");
			//this._fillColumnNamesForList();

		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oTable.getVisibleRowCount());
				//thiz._fillColumnNamesForList();

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this.tableId;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},
		/*------------------------------------------------------------------------ END OF VARIANT MANAGEMENT --------------------------------------------------------- */

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.loadDropdown();
			this.afterRender = true;
			this.initVariant();
			//	this.initSearchVariant();
		}

		/*
		onPress: function(oEvent) {
		 
			// get a handle on the global XAppNav service
			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation"); 
			oCrossAppNavigator.isIntentSupported(["employeeId-display"])
				.done(function(aResponses) {

				})
				.fail(function() {
					new sap.m.MessageToast("Provide corresponding intent to navigate");
				});
			// generate the Hash to display a employee Id
			var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
				target: {
					semanticObject: "employeeId",
					action: "display"
				}
			})) || ""; 
			//Generate a  URL for the second application
			var url = window.location.href.split('#')[0] + hash; 
			//Navigate to second app
			sap.m.URLHelper.redirect(url, true); 
		}
		*/

	});

});