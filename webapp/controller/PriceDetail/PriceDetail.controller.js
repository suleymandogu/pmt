sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models",
	"com/doehler/PricingMasterFile/model/dbcontext",
	"sap/ui/model/json/JSONModel",
	"com/doehler/PricingMasterFile/model/formatter",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/Label",
	"sap/m/TextArea",
	"sap/ui/core/Core",
	"sap/m/MessageBox",
], function (BaseController, base, models, dbcontext, JSONModel, formatter, Dialog, DialogType, Button, ButtonType, Label, TextArea, Core,
	MessageBox) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.PriceDetail.PriceDetail", {
		formatter: formatter,
		onInit: function () {
			this._getControlsFromView();
			this._bindModels();
			this.getRouter().getRoute("PriceSetting").attachPatternMatched(this._onRouteMatched, this);
			var sPath = sap.ui.require.toUrl("com/doehler/PricingMasterFile/Data") + "/item.json";
			this.getView().setModel(new JSONModel(sPath), "Attachment");
			var globalModel = sap.ui.getCore().getModel("globalModel");
			var userId = sap.ushell.Container.getUser().getId();
			globalModel.setProperty("/USERID", userId);

		},

		_onRouteMatched: function (oEvent) {

			var arg = oEvent.getParameter("arguments");
			sap.ui.getCore().getModel("globalModel").setProperty("/arg", arg);
			this._globalM.setProperty("/Page", arg.Page);
			this._globalM.setProperty("/index", arg.index);

			this._globalM.setProperty("/savedDetailPage", false);

			this._globalM.setProperty("/changed", false);

			this._getPriceDetail(arg);
			this.arrangeArrows();
		},

		arrangeArrows: function () {
			var arg = this._globalM.getProperty("/arg"),
				index = this._globalM.getProperty("/index"),
				data;

			if (arg.Page === "PSI") {
				data = sap.ui.getCore().getModel("psresultM").getData();
				this._globalM.setProperty("/previousPage", 1);
			}
			if (arg.Page === "pM") {
				data = sap.ui.getCore().getModel("pMresultM").getData();
				this._globalM.setProperty("/previousPage", 2);
			}
			if (arg.Page === "PSTender") {
				data = sap.ui.getCore().getModel("tenderM").getData();
				this._globalM.setProperty("/previousPage", 3);
			}
			if (arg.Page === "BUM") {
				data = sap.ui.getCore().getModel("buMresultM").getData();
				this._globalM.setProperty("/previousPage", 4);
			}

			if (index !== undefined) index = parseInt(index);

			this._setVisibilityOfArrows(index !== 0 && index !== undefined && data.length > 1, data.length > 1 && index !== undefined && index <
				data.length - 1);

		},

		_getDataFromFrontend: function (arg, data) {
			var that = this,
				sendingData = {},
				params,
				resdata,
				costDrivers,
				copyData,
				resData2 = [];
			resdata = data[arg.index];
			resData2.push(resdata);
			resdata = resData2;
			resdata[0].NEW_PRICE.WORKLOW_STATUS_TEXT = resdata[0].NEW_PRICE.WORKLOW_STATUS;

			sendingData.CUSTOMER = resdata[0].PRICE_MASTER.CUSTOMER;
			sendingData.ARTICLE = resdata[0].PRICE_MASTER.MATERIAL;
			sendingData.SHIP_TO = resdata[0].PRICE_MASTER.SHIP_TO;
			sendingData.SALESORG = resdata[0].PRICE_MASTER.SALESORG;
			sendingData.DISTR_CHAN = resdata[0].PRICE_MASTER.DISTR_CHAN;
			params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_COST_DRIVERS"
				},
				"INPUTPARAMS": [sendingData]
			};

			setTimeout(function () {
				dbcontext.callServer(params, function (oModel) {
					var oldresdata = $.extend(true, {}, resdata[0]);
					costDrivers = oModel.getData().COSTDRIVERS;
					that._loadCostDriver(costDrivers, oldresdata);
					sap.ui.getCore().getModel("globalModel").setProperty("/selectedCustomer", oldresdata.PRICE_MASTER.CUSTOMER);
					resdata = that._prepareTableData(resdata);

					sap.ui.getCore().getModel("priceDetailM").setData(oldresdata);
					that._fillQuotationValidityDates();
					that._loadPriceScales(oldresdata);
					if (oldresdata["PRICE_MASTER"].DOEWERK !== oldresdata["PRICE_MASTER"].PLANT) {
						sap.ui.getCore().getModel("globalModel").setProperty("/pt_pmt", false);
						that._loadIcPriceBox(oldresdata);
					} else
						sap.ui.getCore().getModel("globalModel").setProperty("/pt_pmt", false);
					copyData = JSON.parse(JSON.stringify(oldresdata));
					sap.ui.getCore().getModel("globalModel").setProperty("/priceDetailData", copyData);
				}, this);
			}, 1000);

		},

		_getPriceDetail: function (arg, state) {
			sap.ui.getCore().getModel("globalModel").setProperty("/highestPrice", 0);
			sap.ui.getCore().getModel("globalModel").setProperty("/arg", arg);
			var model = "";

			if (arg.Page === "PSI") {
				if (this._psResultM.getData().length) {
					model = this._psResultM;
					if (state !== "S") {
						this._getDataFromFrontend(arg, this._psResultM.getData());
						return;
					}

				}

			} else {
				if (arg.Page === "BUM") {
					if (this._buResultM.getData().length) {
						model = this._buResultM;
						if (state !== "S") {
							this._getDataFromFrontend(arg, this._buResultM.getData());
							return;
						}
					}

				} else {
					if (arg.Page === "PSTender") {
						if (this._tenderM.getData().length) {
							model = this._tenderM;
							if (state !== "S") {
								this._getDataFromFrontend(arg, this._tenderM.getData());
								return;
							}
						}

					} else {
						if (arg.Page === "pM") {
							if (this._pMresultM.getData().length) {
								model = this._pMresultM;
								if (state !== "S") {
									this._getDataFromFrontend(arg, this._pMresultM.getData());
									return;
								}
							}

						}
					}
				}
			}

			var index, customer,
				material,
				ship_to,
				dist_chnl,
				sales_org,
				params, resdata, costDrivers, sendingData = {},
				highestPrice = 0,
				copyData,
				that = this;
			if (arg !== null) {
				index = arg.index;
				customer = arg.customer;
				material = arg.material;
				ship_to = arg.ship_to;
				dist_chnl = arg.dist_chnl;
				sales_org = arg.sales_org;
			}

			sendingData.CUSTOMER = customer;
			sendingData.ARTICLE = material;
			sendingData.SHIP_TO = ship_to;
			sendingData.SALESORG = sales_org;
			sendingData.DISTR_CHAN = dist_chnl;
			params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_DETAIL"
				},
				"INPUTPARAMS": [sendingData]
			};
			dbcontext.callServer(params, function (oModel) {
				resdata = oModel.getProperty("/PRICEREQHDR");
				resdata[0].NEW_PRICE.WORKLOW_STATUS_TEXT = resdata[0].NEW_PRICE.WORKLOW_STATUS;
				costDrivers = oModel.getData().COSTDRIVERS;
				that._loadCostDriver(costDrivers, resdata[0]);
				sap.ui.getCore().getModel("globalModel").setProperty("/selectedCustomer", resdata[0].PRICE_MASTER.CUSTOMER);
				resdata = that._prepareTableData(resdata);
				sap.ui.getCore().getModel("priceDetailM").setData(resdata[0]);
				that._fillQuotationValidityDates();
				that._loadPriceScales(resdata[0], "NEW");
				if (resdata[0]["PRICE_MASTER"].DOEWERK !== resdata[0]["PRICE_MASTER"].PLANT) {
					that._loadIcPriceBox(resdata[0], "NEW");
					sap.ui.getCore().getModel("globalModel").setProperty("/pt_pmt", false);
				} else
					sap.ui.getCore().getModel("globalModel").setProperty("/pt_pmt", false);
				copyData = JSON.parse(JSON.stringify(resdata[0]));
				sap.ui.getCore().getModel("globalModel").setProperty("/priceDetailData", copyData);
				var copyData2 = JSON.parse(JSON.stringify(copyData));
				if (model !== "")
					model.setProperty("/" + arg.index, copyData2);
			}, this);
		},

		_fillQuotationValidityDates: function (manual) {
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "yyyy-MM-dd"
			});
			var workFlowStatus = this._priceDetailM.getProperty("/NEW_PRICE/WORKLOW_STATUS");
			if (manual === undefined) {
				if (workFlowStatus !== "2") {
					this._priceDetailM.setProperty("/NEW_PRICE/QUOTE_VALID_FROM", dateFormat.format(new Date()));
					this.determineQuoteValidNewTo();
				}
			}

			if (manual === "X") {
				this._priceDetailM.setProperty("/NEW_PRICE/QUOTE_VALID_FROM", dateFormat.format(new Date()));
				this.determineQuoteValidNewTo();
			}

		},

		determineQuoteValidNewTo: function (oEvent) {
			var that = this;
			var params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_QUOTATION_VALIDITY"
				},
				"INPUTPARAMS": [{
					"SALESORG": this._priceDetailM.getData()["PRICE_MASTER"]["SALESORG"],
					"M_DZZB2B_L5": this._priceDetailM.getData()["PRICE_MASTER"]["M_DZZB2B_L5"],
					"M_DZZB2B_L1": this._priceDetailM.getData()["PRICE_MASTER"]["M_DZZB2B_L1"]
				}]
			};
			dbcontext.callServer(params, function (oModel, thiz) {
				var date = oModel.getData().DATE;
				that._priceDetailM.setProperty("/NEW_PRICE/QUOTE_VALID_TO", date);
			}, this);

		},

		onSubmitOrChange: function (oEvent) {
			var oSource = oEvent.getSource(),
				decFormat = sap.ui.getCore().getModel("globalModel").getProperty("/decFormat"),
				id = oSource.getId().split("__")[1].split("-")[2],
				value = oSource.getValue(),
				allCalculatedValues,
				saveBtnPressed = this.getView().byId("savePRBId")._buttonPressed,
				activeLine = sap.ui.getCore().getModel("priceDetailM").getData(),
				formattedValue = 0;
			if (oEvent.getId() === "submit") return;
			this._globalM.setProperty("/changed", true);
			if (decFormat !== "X") {
				formattedValue = value.replace(".", "");
			} else {
				formattedValue = value.replace(",", "");
			}

			if (id === "newPricePlanId" || id === "newPricePlanId2") {
				if (decFormat !== "X") {
					formattedValue = formattedValue.replace(",", ".");
				}

				allCalculatedValues = this.calculateNewPrices(value, activeLine);
				this._priceDetailM.setProperty("/", allCalculatedValues);
				this._updateScaleTable(formattedValue); //ZLSCALE_PRC_Q1
				this._checkApprovalIsNeeeded(activeLine, "", saveBtnPressed);
				// var that = this;
				// var promiseForAppNeeded = new Promise(function (myResolve, myReject) {
				// 	that._checkApprovalIsNeeeded(activeLine, "", saveBtnPressed, myResolve);
				// });
				// promiseForAppNeeded.then(
				// 	function (a) {
				// 		allCalculatedValues = that.calculateNewPrices(value, activeLine);
				// 		that._priceDetailM.setProperty("/", allCalculatedValues);
				// 		that._updateScaleTable(formattedValue); //ZLSCALE_PRC_Q1
				// 		if (saveBtnPressed === true)
				// 			that.onSavePR();
				// 	},
				// 	function (error) { /* code if some error */ }
				// );
			} else {
				if (id === "targetVolumeId" || id === "targetVolumeId2") {
					this._checkApprovalIsNeeeded(activeLine, "", saveBtnPressed);
				}
			}

		},

		changeMOQ: function (oEvent) {

			var oSource = oEvent.getSource(),
				tablePriceScales = this.getView().byId("tablePriceScales"),
				tModel = tablePriceScales.getModel(),
				data = tModel.getData().modelData,
				saveBtnPressed = this.getView().byId("savePRBId")._buttonPressed,
				id = oSource.getId().split("__")[1].split("-")[2],
				activeLine = sap.ui.getCore().getModel("priceDetailM").getData(),
				value = oEvent.getSource().getValue();
			if (id === "moq1" || id === "moq2") {
				this._checkApprovalIsNeeeded(activeLine, id, saveBtnPressed);
			}
			this._globalM.setProperty("/changed", true);
		},

		priceScalesChange: function (oEvent) {
			//priceDetailM>/NEW_PRICE/NEW_PRICE_PLAN
			var decFormat = sap.ui.getCore().getModel("globalModel").getProperty("/decFormat");
			var value = oEvent.getParameter("value");
			var saveBtnPressed = this.getView().byId("savePRBId")._buttonPressed;
			var activeLine = sap.ui.getCore().getModel("priceDetailM").getData();
			var formattedValue;

			if (decFormat !== "X") {
				formattedValue = value.replace(".", "");
				formattedValue = formattedValue.replace(",", ".");
			} else {
				formattedValue = value.replace(",", "");
			}
			formattedValue = formattedValue === "" ? "0" : formattedValue;
			var index = oEvent.getSource().getBindingContext(undefined).getPath().split("/")[2];
			if (index === "0") {
				sap.ui.getCore().getModel("priceDetailM").setProperty("/NEW_PRICE/NEW_PRICE_PLAN", parseFloat(formattedValue));
				var allCalculatedValues = this.calculateNewPrices(value, activeLine);
				this._priceDetailM.setProperty("/", allCalculatedValues);
				this._setPriceScales(parseFloat(value.replace(",", ".")));
				this._checkApprovalIsNeeeded(activeLine, "", saveBtnPressed);
			}
			this._globalM.setProperty("/changed", true);
		},

		priceScalesChangeMOQ: function (oEvent) {
			var decFormat = sap.ui.getCore().getModel("globalModel").getProperty("/decFormat");
			var value = oEvent.getParameter("value");
			var saveBtnPressed = this.getView().byId("savePRBId")._buttonPressed;
			var activeLine = sap.ui.getCore().getModel("priceDetailM").getData();
			var formattedValue;

			if (decFormat !== "X") {
				formattedValue = value.replace(".", "");
				formattedValue = formattedValue.replace(",", ".");
			} else {
				formattedValue = value.replace(",", "");
			}
			formattedValue = formattedValue === "" ? "0" : formattedValue;
			var index = oEvent.getSource().getBindingContext(undefined).getPath().split("/")[2];
			if (index === "0") {
				sap.ui.getCore().getModel("priceDetailM").setProperty("/NEW_PRICE/MOQ_NEW", parseFloat(formattedValue));
				this._checkApprovalIsNeeeded(activeLine, oEvent.getSource().getId().split("--")[1].split("-")[0], saveBtnPressed);
			}

			this._globalM.setProperty("/changed", true);
		},

		_updateScaleTable: function (formattedValue) {
			var activeLine = sap.ui.getCore().getModel("priceDetailM").getData();
			formattedValue = formattedValue === "" ? "0" : formattedValue;
			var priceScalesData = this.getView().byId("tablePriceScales").getModel().getData().modelData;
			if (activeLine.PRICE_MASTER.DOESTAFKZ === "X") {
				priceScalesData[0].PriceNew = parseFloat(formattedValue);
				this.getView().byId("tablePriceScales").getModel().setProperty("/modelData", priceScalesData);
				this._setPriceScales(priceScalesData[0].PriceNew);
			}
		},

		_setPriceScales: function (price) {
			var row = sap.ui.getCore().getModel("priceDetailM").getData();
			var value = 0;
			var priceScalesData = this.getView().byId("tablePriceScales").getModel().getData().modelData;
			if (priceScalesData[1].MOQOld !== 0) {
				priceScalesData[1].PriceNew = priceScalesData[0].PriceNew - priceScalesData[0].PriceOld + priceScalesData[1].PriceOld;
				value = priceScalesData[1].PriceNew;
				if (isNaN(value) || value === Infinity || value === -Infinity) {
					value = 0;
				}
				priceScalesData[1].PriceNew = parseFloat(value.toFixed(2));
			}
			if (priceScalesData[2].MOQOld !== 0) {
				priceScalesData[2].PriceNew = priceScalesData[0].PriceNew - priceScalesData[0].PriceOld + priceScalesData[2].PriceOld;
				value = priceScalesData[2].PriceNew;
				if (isNaN(value) || value === Infinity || value === -Infinity) {
					value = 0;
				}
				priceScalesData[2].PriceNew = parseFloat(value.toFixed(2));
			}
			if (priceScalesData[3].MOQOld !== 0) {
				priceScalesData[3].PriceNew = priceScalesData[0].PriceNew - priceScalesData[0].PriceOld + priceScalesData[3].PriceOld;
				value = priceScalesData[3].PriceNew;
				if (isNaN(value) || value === Infinity || value === -Infinity) {
					value = 0;
				}
				priceScalesData[3].PriceNew = parseFloat(value.toFixed(2));
			}
			if (priceScalesData[4].MOQOld !== 0) {
				priceScalesData[4].PriceNew = priceScalesData[0].PriceNew - priceScalesData[0].PriceOld + priceScalesData[4].PriceOld;
				value = priceScalesData[4].PriceNew;
				if (isNaN(value) || value === Infinity || value === -Infinity) {
					value = 0;
				}
				priceScalesData[4].PriceNew = parseFloat(value.toFixed(2));
			}
			if (priceScalesData[5].MOQOld !== 0) {
				priceScalesData[5].PriceNew = priceScalesData[0].PriceNew - priceScalesData[0].PriceOld + priceScalesData[5].PriceOld;
				value = priceScalesData[5].PriceNew;
				if (isNaN(value) || value === Infinity || value === -Infinity) {
					value = 0;
				}
				priceScalesData[5].PriceNew = parseFloat(value.toFixed(2));
			}
			this.getView().byId("tablePriceScales").getModel().setProperty("/modelData", priceScalesData);

		},

		_checkApprovalIsNeeeded: function (activeLine, id, saveBtnPressed, myResolve) {
			var that = this,
				sendingDataArray = [],
				sendingData = {},
				newPrice = [],
				line = activeLine,
				hasRecord = line.NEW_PRICE.CUSTOMER !== "",
				DOESTAFKZ = line.PRICE_MASTER.DOESTAFKZ,
				deact = line.NEW_PRICE.DEACTIVATE_SCALE,
				priceMaster = [],
				userEntry = false;

			if (id === "moq1" || id === "moq2" || id === "moqNewId" || id === "moqNewId2") userEntry = true;

			if (DOESTAFKZ !== "" && deact !== "X") {
				if (!userEntry) {
					if (!hasRecord)
						line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DSCALQTY1;
				}
			} else {
				if (hasRecord) {
					line.NEW_PRICE.MOQ_NEW = line.NEW_PRICE.MOQ_NEW;
				} else {
					if (!userEntry)
						line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DPRSMOQ;
				}
			}

			newPrice.push(line.NEW_PRICE);
			priceMaster.push(line.PRICE_MASTER);

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "APPROVEL_NEEDED"
				},
				"INPUTPARAMS": sendingDataArray
			};

			dbcontext.callServer(params, function (oModel) {
				activeLine.NEW_PRICE.APP_NEEDED = oModel.getData().APPRO_NEEDED;
				activeLine.NEW_PRICE.RFQ_NEEDED = oModel.getData().RFQ_NEEDED;
				activeLine.NEW_PRICE.THRESHOLD_CLASS = oModel.getData().THRESHOLD_CLASS;
				//	activeLine.NEW_PRICE.ZAPPRCM1EXP = oModel.getData().EXPECTED_PERC;

				that._priceDetailM.setProperty("/", activeLine);

				// if (myResolve !== undefined)
				// 	myResolve();
				// else {
				if (saveBtnPressed === true)
					that.onSavePR();
				// }
			}, this);
		},

		_loadPriceScales: function (row, isBackend) {
			var oModel = new JSONModel(),
				tablePriceScales = this.getView().byId("tablePriceScales"),
				oView = this.getView(),
				decFormat = this._globalM.getProperty("/decFormat"),
				priceDetailM = sap.ui.getCore().getModel("priceDetailM"),
				hasRecord = row.NEW_PRICE.CUSTOMER !== "",
				DOESTAFKZ = row.PRICE_MASTER.DOESTAFKZ;

			if (DOESTAFKZ !== "") {

				if (hasRecord) {

				} else {
					priceDetailM.setProperty("/NEW_PRICE/MOQ_NEW", row.PRICE_MASTER.DSCALQTY1);
					priceDetailM.setProperty("/NEW_PRICE/SCALE_FLG_NEW", "X");
				}
			} else {
				if (hasRecord) {
					priceDetailM.setProperty("/NEW_PRICE/MOQ_NEW", row.NEW_PRICE.MOQ_NEW);
				} else {
					priceDetailM.setProperty("/NEW_PRICE/MOQ_NEW", row.PRICE_MASTER.DPRSMOQ);
				}
			}

			if (DOESTAFKZ === "") {
				row.NEW_PRICE.ZLSCALE_Q1 = 0;
				row.NEW_PRICE.ZLSCALE_Q2 = 0;
				row.NEW_PRICE.ZLSCALE_Q3 = 0;
				row.NEW_PRICE.ZLSCALE_Q4 = 0;
				row.NEW_PRICE.ZLSCALE_Q5 = 0;
				row.NEW_PRICE.ZLSCALE_Q6 = 0;
				row.NEW_PRICE.ZLSCALE_Q1 = 0;
				row.NEW_PRICE.ZLSCALE_Q2 = 0;
				row.NEW_PRICE.ZLSCALE_Q3 = 0;
				row.NEW_PRICE.ZLSCALE_Q4 = 0;
				row.NEW_PRICE.ZLSCALE_Q5 = 0;
				row.NEW_PRICE.ZLSCALE_Q6 = 0;
			}

			if ((row.NEW_PRICE.ZLSCALE_Q1 === "" || row.NEW_PRICE.ZLSCALE_Q1 === 0) &&
				(row.NEW_PRICE.ZLSCALE_Q2 === "" || row.NEW_PRICE.ZLSCALE_Q2 === 0) &&
				(row.NEW_PRICE.ZLSCALE_Q3 === "" || row.NEW_PRICE.ZLSCALE_Q3 === 0) &&
				(row.NEW_PRICE.ZLSCALE_Q4 === "" || row.NEW_PRICE.ZLSCALE_Q4 === 0) &&
				(row.NEW_PRICE.ZLSCALE_Q5 === "" || row.NEW_PRICE.ZLSCALE_Q5 === 0) &&
				(row.NEW_PRICE.ZLSCALE_Q6 === "" || row.NEW_PRICE.ZLSCALE_Q6 === 0)) {
				priceDetailM.setProperty("/PRICE_MASTER/DOESTMEIN_V", row.PRICE_MASTER.DPRSMOQUOM !== "0" && row.PRICE_MASTER.DPRSMOQUOM !== "000" ?
					row.PRICE_MASTER.DPRSMOQUOM : row.DETAIL.MEINS);
				priceDetailM.setProperty("/PRICE_MASTER/MOQ_OLD_DOESTMEIN_V", row.PRICE_MASTER.DPRSMOQUOM !== "0" && row.PRICE_MASTER.DPRSMOQUOM !==
					"000" ?
					row.PRICE_MASTER.DPRSMOQUOM : row.DETAIL.MEINS);
			} else {
				priceDetailM.setProperty("/PRICE_MASTER/DOESTMEIN_V", row.PRICE_MASTER.DOESTMEIN);
				priceDetailM.setProperty("/PRICE_MASTER/MOQ_OLD_DOESTMEIN_V", row.PRICE_MASTER.DOESTMEIN);
			}

			var oData = {
				modelData: [{
					MOQOld: row.PRICE_MASTER.DSCALQTY1,
					UoMold: row.PRICE_MASTER.DOESTMEIN,
					PriceOld: row.PRICE_MASTER.DSCALPRC1,
					MOQNew: isBackend !== "X" ? row.NEW_PRICE.ZLSCALE_Q1 : DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q1 : DOESTAFKZ !==
						"" && !hasRecord ? row.PRICE_MASTER.DSCALQTY1 : DOESTAFKZ ===
						"" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q1 : 0,
					PriceNew: row.NEW_PRICE.ZLSCALE_PRC_Q1
				}, {
					MOQOld: row.PRICE_MASTER.DSCALQTY2,
					UoMold: row.PRICE_MASTER.DOESTMEIN,
					PriceOld: row.PRICE_MASTER.DSCALPRC2,
					MOQNew: isBackend !== "X" ? row.NEW_PRICE.ZLSCALE_Q2 : DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q2 : DOESTAFKZ !==
						"" && !hasRecord ? row.PRICE_MASTER.DSCALQTY2 : DOESTAFKZ ===
						"" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q2 : 0,
					PriceNew: row.NEW_PRICE.ZLSCALE_PRC_Q2
				}, {
					MOQOld: row.PRICE_MASTER.DSCALQTY3,
					UoMold: row.PRICE_MASTER.DOESTMEIN,
					PriceOld: row.PRICE_MASTER.DSCALPRC3,
					MOQNew: isBackend !== "X" ? row.NEW_PRICE.ZLSCALE_Q3 : DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q3 : DOESTAFKZ !==
						"" && !hasRecord ? row.PRICE_MASTER.DSCALQTY3 : DOESTAFKZ ===
						"" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q3 : 0,
					PriceNew: row.NEW_PRICE.ZLSCALE_PRC_Q3
				}, {
					MOQOld: row.PRICE_MASTER.DSCALQTY4,
					UoMold: row.PRICE_MASTER.DOESTMEIN,
					PriceOld: row.PRICE_MASTER.DSCALPRC4,
					MOQNew: isBackend !== "X" ? row.NEW_PRICE.ZLSCALE_Q4 : DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q4 : DOESTAFKZ !==
						"" && !hasRecord ? row.PRICE_MASTER.DSCALQTY4 : DOESTAFKZ ===
						"" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q4 : 0,
					PriceNew: row.NEW_PRICE.ZLSCALE_PRC_Q4
				}, {
					MOQOld: row.PRICE_MASTER.DSCALQTY5,
					UoMold: row.PRICE_MASTER.DOESTMEIN,
					PriceOld: row.PRICE_MASTER.DSCALPRC5,
					MOQNew: isBackend !== "X" ? row.NEW_PRICE.ZLSCALE_Q5 : DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q5 : DOESTAFKZ !==
						"" && !hasRecord ? row.PRICE_MASTER.DSCALQTY5 : DOESTAFKZ ===
						"" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q5 : 0,
					PriceNew: row.NEW_PRICE.ZLSCALE_PRC_Q5
				}, {
					MOQOld: row.PRICE_MASTER.DSCALQTY6,
					UoMold: row.PRICE_MASTER.DOESTMEIN,
					PriceOld: row.PRICE_MASTER.DSCALPRC6,
					MOQNew: isBackend !== "X" ? row.NEW_PRICE.ZLSCALE_Q6 : DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q6 : DOESTAFKZ !==
						"" && !hasRecord ? row.PRICE_MASTER.DSCALQTY6 : DOESTAFKZ ===
						"" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q6 : 0,
					PriceNew: row.NEW_PRICE.ZLSCALE_PRC_Q6
				}]
			};
			if (DOESTAFKZ === "") {
				oData.modelData.forEach(function (r) {
					r["MOQOld"] = 0;
					r["UoMold"] = 0;
					r["PriceOld"] = 0;
					r["MOQNew"] = 0;
					r["PriceNew"] = 0;
				});
			}

			// set the data for the model
			oModel.setData(oData);

			// set the model to the core
			tablePriceScales.setModel(oModel);

			//visibility Check
			/*When DOESTAFKZ is initial then new flag is empty and not editable*/
			if (DOESTAFKZ === "") {
				this.getView().byId("deactScaleChckBoxId").setEditable(false);
				//sap.ui.getCore().getModel("priceDetailM").setProperty("/NEW_PRICE/DEACTIVATE_SCALE", "");
			} else {
				/*When DOESTAFKZ is ‘X’ then new field must be editable by user; when user changes the field it must be saved to backend*/
				this.getView().byId("deactScaleChckBoxId").setEditable(true);
			}

			var deactScale = sap.ui.getCore().getModel("priceDetailM").getProperty("/NEW_PRICE/DEACTIVATE_SCALE");

			if ((DOESTAFKZ === "" && deactScale === "") || (DOESTAFKZ === "X" && deactScale === "X")) {
				this._globalM.setProperty("/scaleAreVisiblity", false);
			} else {
				this._globalM.setProperty("/scaleAreVisiblity", true);
			}

			if ((sap.ui.getCore().getModel("priceDetailM").getProperty("/PRICE_MASTER/DOESTMEIN_V") === "0" ||
					sap.ui.getCore().getModel("priceDetailM").getProperty("/PRICE_MASTER/DOESTMEIN_V") === "000" || sap.ui.getCore().getModel(
						"priceDetailM").getProperty("/PRICE_MASTER/DOESTMEIN_V") === "")) {
				sap.ui.getCore().getModel("priceDetailM").setProperty("/PRICE_MASTER/DOESTMEIN_V", sap.ui.getCore().getModel("priceDetailM").getProperty(
					"/DETAIL/MEINS"));
			}
			if (sap.ui.getCore().getModel("priceDetailM").getProperty("/NEW_PRICE/MOQ_NEW_UOM") !== "" &&
				sap.ui.getCore().getModel("priceDetailM").getProperty("/NEW_PRICE/MOQ_NEW_UOM") !== "0" &&
				sap.ui.getCore().getModel("priceDetailM").getProperty("/NEW_PRICE/MOQ_NEW_UOM") !== "000") {
				sap.ui.getCore().getModel("priceDetailM").setProperty("/PRICE_MASTER/DOESTMEIN_V", sap.ui.getCore().getModel("priceDetailM").getProperty(
					"/NEW_PRICE/MOQ_NEW_UOM"));
			}

			sap.ui.getCore().getModel("priceDetailM").setProperty("/NEW_PRICE/MOQ_NEW_UOM", sap.ui.getCore().getModel("priceDetailM").getProperty(
				"/PRICE_MASTER/DOESTMEIN_V"));

			var oTable = tablePriceScales;
			var columns = oTable.getColumns();
			var len = columns.length;
			for (var i = 0; i < len; i++) {
				columns[i].setFilterValue("").setFiltered(false).setSorted(false);
			}
			oTable.getBinding("rows").filter(null).sort(null);

		},

		_loadIcPriceBox: function (row, isBackend) {
			var oModel = new JSONModel(),
				tableIcPriceBox = this.getView().byId("tableIcPriceBox"),
				oView = this.getView(),
				decFormat = this._globalM.getProperty("/decFormat"),
				priceDetailM = sap.ui.getCore().getModel("priceDetailM"),
				hasRecord = row.NEW_PRICE.CUSTOMER !== "",
				DOESTAFKZ = row.PRICE_MASTER.DOESTAFKZ;

			var oData = {
				modelData: [{
					MOQ: 0,
					UoM: 0,
					IcPriceOld: 1,
					IcPriceNew: 2
				}, {
					MOQ: 0,
					UoM: 0,
					IcPriceOld: 1,
					IcPriceNew: 2
				}, {
					MOQ: 0,
					UoM: 0,
					IcPriceOld: 1,
					IcPriceNew: 2
				}, {
					MOQ: 0,
					UoM: 0,
					IcPriceOld: 1,
					IcPriceNew: 2
				}, {
					MOQ: 0,
					UoM: 0,
					IcPriceOld: 1,
					IcPriceNew: 2
				}, {
					MOQ: 0,
					UoM: 0,
					IcPriceOld: 1,
					IcPriceNew: 2
				}]
			};

			// set the data for the model
			oModel.setData(oData);

			// set the model to the core
			tableIcPriceBox.setModel(oModel);

			var oTable = tableIcPriceBox;
			var columns = oTable.getColumns();
			var len = columns.length;
			for (var i = 0; i < len; i++) {
				columns[i].setFilterValue("").setFiltered(false).setSorted(false);
			}
			oTable.getBinding("rows").filter(null).sort(null);

		},

		onSelectScaleCheckBox: function (oEvent) {
			var isSelected = oEvent.getParameter("selected"),
				DOESTAFKZ = sap.ui.getCore().getModel("priceDetailM").getData().PRICE_MASTER.DOESTAFKZ;

			sap.ui.getCore().getModel("priceDetailM").setProperty("/NEW_PRICE/DEACTIVATE_SCALE", isSelected ? "X" : "");

			if (isSelected && DOESTAFKZ === "X") {
				this._globalM.setProperty("/scaleAreVisiblity", false);
			} else {
				if (!isSelected && DOESTAFKZ === "X") {
					this._globalM.setProperty("/scaleAreVisiblity", true);
				}
			}
			this._globalM.setProperty("/changed", true);

		},

		gotoCreateRFQ: function () {
			var row = sap.ui.getCore().getModel("priceDetailM").getData(),
				createPriceReqItems = [],
				createRequestLink =
				"/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html#zdoehlersempr-display?tag=1&/CreateRequest",
				stringfyJSON;
			createRequestLink = location.host + createRequestLink;
			createPriceReqItems.push({
				Customer: row.PRICE_MASTER.CUSTOMER,
				ArticleNumber: row.PRICE_MASTER.MATERIAL,
				Title: "PMT – Price Renewal"
			});

			stringfyJSON = JSON.stringify(createPriceReqItems);
			createRequestLink = location.protocol + "//" + createRequestLink;
			createRequestLink += "?CustomerArticles=" + encodeURI(stringfyJSON);
			sap.m.URLHelper.redirect(createRequestLink, true);
		},

		_loadCostDriver: function (costDrivers, line) {
			var oModel = new JSONModel();

			// JSON sample data
			var oData = {
				modelData: [{
					CDT: costDrivers["ZEXST_TOP3_CD_T1"],
					CDV: costDrivers["ZEXST_TOP3_CD_V1"],
					CET: costDrivers["ZEXST_TOP3_CE_T1"],
					CEV: costDrivers["ZEXST_TOP3_CE_V1"] / line.PRICE_MASTER.DRATE3
				}, {
					CDT: costDrivers["ZEXST_TOP3_CD_T2"],
					CDV: costDrivers["ZEXST_TOP3_CD_V2"],
					CET: costDrivers["ZEXST_TOP3_CE_T2"],
					CEV: costDrivers["ZEXST_TOP3_CE_V2"] / line.PRICE_MASTER.DRATE3
				}, {
					CDT: costDrivers["ZEXST_TOP3_CD_T3"],
					CDV: costDrivers["ZEXST_TOP3_CD_V3"],
					CET: costDrivers["ZEXST_TOP3_CE_T3"],
					CEV: costDrivers["ZEXST_TOP3_CE_V3"] / line.PRICE_MASTER.DRATE3
				}]
			};

			// set the data for the model
			oModel.setData(oData);
			var tableCost = this.getView().byId("tableCostDriver"),
				oView = this.getView();
			// set the model to the core
			tableCost.setModel(oModel);

			oView.byId("multiheader").setHeaderSpan([2, 2, 2]);
			oView.byId("multiheader2").setHeaderSpan([2, 2, 2]);
		},

		_getControlsFromView: function () {
			// this._table = this.getView().byId("priceSettingInfromationTableId");
			this._buFilterM = sap.ui.getCore().getModel("buMfilterM");
			this._buResultM = sap.ui.getCore().getModel("buMresultM");
			this._globalM = sap.ui.getCore().getModel("globalModel");
			this._psFilterM = sap.ui.getCore().getModel("psfilterM");
			this._psResultM = sap.ui.getCore().getModel("psresultM");
			this._priceDetailM = sap.ui.getCore().getModel("priceDetailM");
			this._recipientM = sap.ui.getCore().getModel("recipientM");
			this._tenderM = sap.ui.getCore().getModel("tenderM");
			this._pMresultM = sap.ui.getCore().getModel("pMresultM");
		},

		_bindModels: function () {
			var that = this;
			var aModels = ["buMfilterM", "psresultM",
				"buMresultM", "globalModel", "priceDetailM", "pMresultM", "tenderM", "recipientM"
			];
			aModels.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});

			var aDropdown = ["priceActionDD", "buActionDD", "workFlowStatusDD"];
			aDropdown.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
		},

		onNavBackPS: function () {
			var page = this._globalM.getProperty("/previousPage"),
				oRouter = this.getRouter(),
				isChanged = this._isChanged(),
				that = this;

			if (isChanged) {
				MessageBox.confirm("Do you want to save your changes ?", {
					title: "Confirm", // default
					onClose: function (oAction) {
						if (oAction == "YES") {
							that.onSavePR("Navigate");
						} /* else if (oAction == "NO") {*/
						if (page === undefined || page === 1)
							oRouter.navTo("Main");
						else if (page === 2) oRouter.navTo("PriceMeasures", {
							requestId: "PM"
						});
						else if (page === 4) oRouter.navTo("BuMeasures", {
							requestId: "BU"
						});
						else if (page === 3) window.history.go(-1); //oRouter.navTo("Tender");

					},
					actions: [MessageBox.Action.YES, MessageBox.Action.NO], // default
				});
			} else {

				if (page === undefined || page === 1)
					oRouter.navTo("Main");
				else if (page === 2) oRouter.navTo("PriceMeasures", {
					requestId: "PM"
				});
				else if (page === 4) oRouter.navTo("BuMeasures", {
					requestId: "BU"
				});
				else if (page === 3) window.history.go(-1); //oRouter.navTo("Tender");
			}

		},

		onPressLeft: function () {
			var data = [],
				index = this._globalM.getProperty("/index"),
				page = this._globalM.getProperty("/Page"),
				oRouter = this.getRouter(),
				line;

			if (index !== undefined) index = parseInt(index);

			switch (page) {
			case "PSI":
				data = sap.ui.getCore().getModel("psresultM").getData();
				break;
			case "pM":
				data = sap.ui.getCore().getModel("pMresultM").getData();
				break;
			case "PSTender":
				data = sap.ui.getCore().getModel("tenderM").getData();
				break;
			case "BUM":
				data = sap.ui.getCore().getModel("buMresultM").getData();
				break;
			default:
			}
			index = index - 1;
			line = data[index];
			this._setVisibilityOfArrows(index > 0, true);
			var customer = line.PRICE_MASTER.CUSTOMER;
			var material = line.PRICE_MASTER.MATERIAL;
			var ship_to = line.PRICE_MASTER.SHIP_TO;
			var dist_chnl = line.PRICE_MASTER.DISTR_CHAN;
			var sales_org = line.PRICE_MASTER.SALESORG;

			oRouter.navTo("PriceSetting", {
				customer: customer,
				ship_to: ship_to,
				sales_org: sales_org,
				dist_chnl: dist_chnl,
				material: material,
				Page: page,
				index: index
			});

		},
		onPressRight: function () {
			var data = [],
				index = this._globalM.getProperty("/index"),
				page = this._globalM.getProperty("/Page"),
				oRouter = this.getRouter(),
				line;

			if (index !== undefined) index = parseInt(index);

			switch (page) {
			case "PSI":
				data = sap.ui.getCore().getModel("psresultM").getData();
				break;
			case "pM":
				data = sap.ui.getCore().getModel("pMresultM").getData();
			case "PSTender":
				data = sap.ui.getCore().getModel("tenderM").getData();
				break;
			case "BUM":
				data = sap.ui.getCore().getModel("buMresultM").getData();
			default:
			}

			index = index + 1;

			line = data[index];

			this._setVisibilityOfArrows(true, index < data.length - 1);

			var customer = line.PRICE_MASTER.CUSTOMER;
			var material = line.PRICE_MASTER.MATERIAL;
			var ship_to = line.PRICE_MASTER.SHIP_TO;
			var dist_chnl = line.PRICE_MASTER.DISTR_CHAN;
			var sales_org = line.PRICE_MASTER.SALESORG;

			oRouter.navTo("PriceSetting", {
				customer: customer,
				ship_to: ship_to,
				sales_org: sales_org,
				dist_chnl: dist_chnl,
				material: material,
				Page: page,
				index: index
			});

		},

		/* ----------------- Dropdown --------------------*/
		loadDropdown: function () {
			models.loadPricingAction();
			models.loadBUAction();
			models.loadWorkFlowStatus();
		},

		selectionChangeRowWorkFlow: function (oEvent) {
			this._globalM.setProperty("/changed", true);
			var key = oEvent.getParameter("selectedItem").getProperty("key");
			if (key === "2")
				this._fillQuotationValidityDates("X");
		},

		selectionChangeRow: function (oEvent) {
			this._globalM.setProperty("/changed", true);
		},

		onChangePriceValidty: function (oEvent) {
			this._globalM.setProperty("/changed", true);
		},

		onChangePricingAction: function (oEvent) {
			this._globalM.setProperty("/changed", true);
		},

		onChangeLAccount: function (oEvent) {
			this._globalM.setProperty("/changed", true);
		},

		onChangeCustomerTargetPrice: function (oEvent) {
			this._globalM.setProperty("/changed", true);
		},

		commentLiveChange: function (oEvent) {
			this._globalM.setProperty("/changed", true);
		},

		onSendQuotation: function (oEvent) {
			var data = [],
				sText, that = this,
				data = sap.ui.getCore().getModel("priceDetailM").getData(),
				oldData = sap.ui.getCore().getModel("globalModel").getProperty("/priceDetailData"),
				currentData = sap.ui.getCore().getModel("priceDetailM").getData(),
				decFormat = this._globalM.getProperty("/decFormat");
			if (decFormat === "X") {
				if (this.getView().byId("moq1").getValue() === "" || this.getView().byId("moq1").getValue() === "0") {
					sap.m.MessageToast.show("Please enter the MOQ field");
					return;
				}
			} else {
				if (this.getView().byId("moq2").getValue() === "" || this.getView().byId("moq2").getValue() === "0") {
					sap.m.MessageToast.show("Please enter the MOQ field");
					return;
				}
			}

			data = this._getSendingDataForQuotation(null, data);

			if (data === undefined) return;

			var promiseForSave = new Promise(function (myResolve, myReject) {
				that.onSavePR(undefined, myResolve, myReject);
			});

			var thiz = this,
				params;

			promiseForSave.then(
				function (value) {
					jQuery.sap.delayedCall(500, thiz, function () {
						params = {
							"HANDLERPARAMS": {
								"FUNC": "SEND_QUOT"
							},
							"INPUTPARAMS": data.data
						};
						dbcontext.callServer(params, function (oModel) {
							thiz.handleServerMessages(oModel, function (status) {
								if (status === "S") {
									thiz._globalM.setProperty("/savedDetailPage", true);
									thiz._getPriceDetail(sap.ui.getCore().getModel("globalModel").getProperty("/arg"), "S");
								}
							});
						}, thiz);
					});
				},
				function (error) { /* code if some error */ }
			);

		},

		//send approval
		onSendApproval: function (oEvent) {
			var data = [],
				sText,
				data = sap.ui.getCore().getModel("priceDetailM").getData(),
				oldData = sap.ui.getCore().getModel("globalModel").getProperty("/priceDetailData"),
				currentData = sap.ui.getCore().getModel("priceDetailM").getData();
			this._updataCurrentData(currentData);

			data = this._getSendingDataForApproval(null, data);
			if (data === undefined) return;
			if (data.isAppNeededInitial) {

				if (!this.oSubmitDialog) {
					this.oSubmitDialog = new Dialog({
						type: DialogType.Message,
						title: "Confirm",
						content: [
							new Label({
								text: "Why you want to start the workflow?",
								labelFor: "submissionNote"
							}),
							new TextArea("submissionNote", {
								maxLength: 200,
								width: "100%",
								placeholder: "Add note (required)",
								liveChange: function (oEvent) {
									sText = oEvent.getParameter("value");
									this.oSubmitDialog.getBeginButton().setEnabled(sText.length > 0);
								}.bind(this)
							})
						],
						beginButton: new Button({
							type: ButtonType.Emphasized,
							text: "Submit",
							enabled: false,
							press: function () {
								sText = Core.byId("submissionNote").getValue();
								data.data.forEach(function (line) {
									if (line.APP_NEEDED === "")
										line.APP_NEEDED_DESC = sText;
								});
								this.oSubmitDialog.close();
								this._callSendApprovalFunc(data);
							}.bind(this)
						}),
						endButton: new Button({
							text: "Cancel",
							press: function () {
								this.oSubmitDialog.close();
							}.bind(this)
						})
					});
				} else
					Core.byId("submissionNote").setValue("");

				this.oSubmitDialog.open();

			} else
				this._callSendApprovalFunc(data);

		},

		onEmptyButton: function (oEvent) {
			// var data = sap.ui.getCore().getModel("priceDetailM").getData();
			var data = sap.ui.getCore().getModel("globalModel").getData().priceDetailData;
			var priceScalesData = this.getView().byId("tablePriceScales").getModel().getData().modelData;
			var that = this;
			var volumeForecast,
				moqOld,
				validFrom,
				validTo;
			MessageBox.warning(
				"Do you really want to delete the entries?", {
					actions: ["Yes", "Cancel"],
					onClose: function (sAction) {
						if (sAction === "Cancel")
							return;
						else {
							volumeForecast = data.PRICE_MASTER.DPRSVFC;
							moqOld = data.PRICE_MASTER.DPRSMOQ;
							// var row = that._onPriceValidFromTo(data);
							validFrom = data.PRICE_MASTER.QUOT_FROM;
							validTo = data.PRICE_MASTER.QUOT_TO;
							if (validFrom.length === 8)
								validFrom = validFrom.substring(0, 4) + "-" + validFrom.substring(4, 6) + "-" + validFrom.substring(6, 8);
							if (validTo.length === 8)
								validTo = validTo.substring(0, 4) + "-" + validTo.substring(4, 6) + "-" + validTo.substring(6, 8);

							that._priceDetailM.setProperty("/NEW_PRICE/NEW_PRICE_PLAN", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/TARGET_VOLUME", parseInt(volumeForecast));
							that._priceDetailM.setProperty("/NEW_PRICE/PRICE_VALID_FROM", validFrom);
							that._priceDetailM.setProperty("/NEW_PRICE/PRICE_VALID_TO", validTo);
							that._priceDetailM.setProperty("/NEW_PRICE/SALES_COMMENT", "");
							that._priceDetailM.setProperty("/NEW_PRICE/PRICE_COMMENT", "");
							that._priceDetailM.setProperty("/NEW_PRICE/WORKLOW_STATUS", "");
							that._priceDetailM.setProperty("/NEW_PRICE/MOQ_NEW", moqOld);
							that._priceDetailM.setProperty("/NEW_PRICE/CUST_TARGET_PRICE", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/0" + "/PriceNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/1" + "/PriceNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/2" + "/PriceNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/3" + "/PriceNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/4" + "/PriceNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/5" + "/PriceNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/0" + "/MOQNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/1" + "/MOQNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/2" + "/MOQNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/3" + "/MOQNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/4" + "/MOQNew", 0);
							that.getView().byId("tablePriceScales").getModel().setProperty("/modelData/5" + "/MOQNew", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_PRC_Q1", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_PRC_Q2", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_PRC_Q3", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_PRC_Q4", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_PRC_Q5", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_PRC_Q6", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_Q1", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_Q2", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_Q3", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_Q4", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_Q5", 0);
							that._priceDetailM.setProperty("/NEW_PRICE/ZLSCALE_Q6", 0);
							that._globalM.setProperty("/changed", true);
							that.onSavePR();
						}
					}
				});
		},

		_updataCurrentData: function (currentData) {
			var priceScalesData = this.getView().byId("tablePriceScales").getModel().getData().modelData;
			if (priceScalesData.length === 6) {
				if (priceScalesData[0].PriceNew !== "" && priceScalesData[0].PriceNew !== 0)
					currentData.NEW_PRICE.SCALE_FLG_NEW = "X";
				currentData.NEW_PRICE.ZLSCALE_Q1 = priceScalesData[0].MOQNew;
				currentData.NEW_PRICE.ZLSCALE_PRC_Q1 = priceScalesData[0].PriceNew;
				currentData.NEW_PRICE.ZLSCALE_Q2 = priceScalesData[1].MOQNew;
				currentData.NEW_PRICE.ZLSCALE_PRC_Q2 = priceScalesData[1].PriceNew;
				currentData.NEW_PRICE.ZLSCALE_Q3 = priceScalesData[2].MOQNew;
				currentData.NEW_PRICE.ZLSCALE_PRC_Q3 = priceScalesData[2].PriceNew;
				currentData.NEW_PRICE.ZLSCALE_Q4 = priceScalesData[3].MOQNew;
				currentData.NEW_PRICE.ZLSCALE_PRC_Q4 = priceScalesData[3].PriceNew;
				currentData.NEW_PRICE.ZLSCALE_Q5 = priceScalesData[4].MOQNew;
				currentData.NEW_PRICE.ZLSCALE_PRC_Q5 = priceScalesData[4].PriceNew;
				currentData.NEW_PRICE.ZLSCALE_Q6 = priceScalesData[5].MOQNew;
				currentData.NEW_PRICE.ZLSCALE_PRC_Q6 = priceScalesData[5].PriceNew;
			}
		},

		onCancelPR: function () {
			var copieData = sap.ui.getCore().getModel("globalModel").getProperty("/priceDetailData");
			var copyData = JSON.parse(JSON.stringify(copieData));
			var resData = [];
			var that = this;
			resData.push(copyData);
			resData = that._prepareTableData(resData);
			sap.ui.getCore().getModel("priceDetailM").setData(resData[0]);
			that._loadPriceScales(resData[0]);
			that._globalM.setProperty("/changed", false);
		},

		_isChanged: function () {
			var oldData = sap.ui.getCore().getModel("globalModel").getProperty("/priceDetailData"),
				currentData = sap.ui.getCore().getModel("priceDetailM").getData();
			this._updataCurrentData(currentData);
			return (JSON.stringify(oldData) !== JSON.stringify(currentData) && this._globalM.getProperty("/changed") === true);
		},

		onSavePR: function (oName, myResolve, myReject) {
			var oldData = sap.ui.getCore().getModel("globalModel").getProperty("/priceDetailData"),
				currentData = sap.ui.getCore().getModel("priceDetailM").getData(),
				sendingDataArray = [],
				thiz = this,
				sendingData = {},
				newPrice = [],
				priceMaster = [];

			var userName = sap.ushell.Container.getUser().getFullName();
			var sap_user = sap.ushell.Container.getUser().getId();
			if (userName === "Default User") {
				userName = "Süleyman Dogu";
			}

			//this._updataCurrentData(currentData);
			if (oName !== "Navigate" && (myResolve === undefined || myResolve === null))
				if (this._isChanged() === false) {
					sap.m.MessageBox.warning("There is no change!");
					return;
				}
			if (oldData.NEW_PRICE.WORKLOW_STATUS !== currentData.NEW_PRICE.WORKLOW_STATUS) {
				if (currentData.NEW_PRICE.WORKLOW_STATUS !== "" && currentData.NEW_PRICE.WORKLOW_STATUS !== "1") {
					currentData.NEW_PRICE.APPROVAL_NAME = userName;
					currentData.NEW_PRICE.APP_USERNAME = sap_user;
				} else {
					currentData.NEW_PRICE.APPROVAL_NAME = "";
					currentData.NEW_PRICE.APP_USERNAME = "";
				}
			}

			newPrice.push(currentData.NEW_PRICE);
			priceMaster.push(currentData.PRICE_MASTER);

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingData.LV_ROL = this._globalM.getProperty("/ROLEINDEX");
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_PRC_MASTER"
				},
				"INPUTPARAMS": sendingDataArray
			};
			sap.ui.core.BusyIndicator.show(0);
			jQuery.sap.delayedCall(1500, this, function () {
				dbcontext.callServer(params, function (oModel) {
					thiz.handleServerMessages(oModel, function (status) {
						if (status === "S") {
							thiz._globalM.setProperty("/savedDetailPage", true);
							sap.ui.core.BusyIndicator.hide();
							if (myResolve === undefined || myResolve === null)
								thiz._getPriceDetail(sap.ui.getCore().getModel("globalModel").getProperty("/arg"), "S");
							else {
								myResolve();
							}
						}
					});
				}, this);
			});

		},

		_callSendApprovalFunc: function (data) {
			var thiz = this,
				params;
			if (data.dataForDacman.length) {
				if (!data.isInitialWorkflow) {
					sap.m.MessageBox.warning("You can not select the lines that have In progress, Approved or Rejected workflow status!");
				} else {

					params = {
						"HANDLERPARAMS": {
							"FUNC": "GET_DACCMAN_TO_USER"
						},
						"INPUTPARAMS": data.dataForDacman
					};
					dbcontext.callServer(params, function (oModel) {
						thiz.handleServerMessages(oModel, function (status) {
							if (status === "S") {
								var resultData = oModel.getData().OUTPUTDATA,
									lineResult = [];

								data.dataForNotApproval.forEach(function (itm) {
									lineResult = resultData.filter(function (resLine) {
										return resLine.C_DACCMAN === itm.PRICE_MASTER.C_DACCMAN;
									});
									if (lineResult.length)
										itm.PRICE_MASTER.RECIPIENT = lineResult[0].USRID;

								});

								thiz._createMailDialogForApproval(thiz, data.dataForNotApproval, "sendAppId");
							}
						});
					}, this);
				}

			}
			if (!data.data.length) return;
			jQuery.sap.delayedCall(500, this, function () {
				params = {
					"HANDLERPARAMS": {
						"FUNC": "SEND_APPROVAL"
					},
					"INPUTPARAMS": data.data
				};
				dbcontext.callServer(params, function (oModel) {
					thiz.handleServerMessages(oModel, function (status) {
						if (status === "S") {
							thiz._globalM.setProperty("/savedDetailPage", true);
							thiz._getPriceDetail(sap.ui.getCore().getModel("globalModel").getProperty("/arg"), "S");

						}
					});
				}, this);
			});
		},

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			this.loadDropdown();
		},

		onExit: function () {
			if (window.hasher.getHash() === "Shell-home") {
				this.onNavBackPS();
			}
		}
	});

});