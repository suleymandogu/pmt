sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models",
	"com/doehler/PricingMasterFile/model/formatter",
	"com/doehler/PricingMasterFile/model/dbcontext",
	"sap/ui/model/json/JSONModel",
	"com/doehler/PricingMasterFile/excel/excel",
	"sap/m/MessageBox"
], function (BaseController, base, models, formatter, dbcontext, JSONModel, excel, MessageBox) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.RedPosition.RedPosition", {
		formatter: formatter,
		onInit: function () {
			this._getControlsFromView();
			this._bindModels();
			this.getRouter().getRoute("RedPosition").attachPatternMatched(this._onRouteMatched, this);
			this.loadDropdown();
			// var oUser = new sap.ushell.services.UserInfo();
			// var userId = oUser.getId();
			// this.getView().byId("redPositionUserInputId").addToken(new sap.m.Token({
			// 	key: userId,
			// 	text: userId
			// }));
		},

		_onRouteMatched: function (oEvent) {
			//this.clearSelectionFields();
			this._viewUpdate("redPPage");
		},

		_bindModels: function () {
			var that = this;
			var aModels = ["buMfilterM", "buMresultM", "globalModel", "redPositionM", "redPositionfilterM"];
			aModels.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});

			var aDropdown = ["agreementTypeDD", "existingNewDD"];
			aDropdown.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
		},

		/* ----------------- Dropdown --------------------*/
		loadDropdown: function () {
			models.loadAgreementType();
			models.loadExistingNew();
		},

		handleSelectionChange: function (oEvent) {
			//oEvent.getSource().getSelectedItems()[0].getProperty("key")
		},

		_getControlsFromView: function () {
			//filter
			this._redPositionfilterM = sap.ui.getCore().getModel("redPositionfilterM");
			//table
			this._redPositionM = sap.ui.getCore().getModel("redPositionM");
			this._table = this.getView().byId("redPositionTableId");
			this._buFilterM = sap.ui.getCore().getModel("buMfilterM");
			this._buResultM = sap.ui.getCore().getModel("buMresultM");
			this._globalM = sap.ui.getCore().getModel("globalModel");

			this._psResultM = sap.ui.getCore().getModel("psresultM");

		},

		//ex_dogus: dowmloading excel
		onExport: function () {
			var data = [];
			var userId = sap.ushell.Container.getUser().getId();
			var tableData = this._redPositionM.getData();
			var copiedData = tableData;
			var aIndices = this._table.getBinding("rows").aIndices;

			if (copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			if (userId === "EX_DOGUS" || userId === "DEFAULT_USER")
				excel._downloadExcel("RP", this._table, data);
			else
				excel._downloadExcel("RP", this._table, data);

		},

		_setInitialValueNeeded: function (value) {
			if (value === -Infinity || value === Infinity || isNaN(value)) value = 0;
			return value;
		},

		onGo: function () {
			//this._fillCustomerTable();
			var copy = this._redPositionfilterM.getData(),
				isSearchPossible = true,
				params,
				agreementTypeText = "",
				newbizText = "";

			var data = $.extend(true, {}, copy);
			if (copy.AGGTYP !== "") {
				if (copy.AGGTYP.length > 1) {
					copy.AGGTYP.shift();
				}
				agreementTypeText = copy.AGGTYP.join(",");
			}
			if (copy.NEWBIZ !== "") {
				if (copy.NEWBIZ.length > 1) {
					copy.NEWBIZ.shift();
				}
				newbizText = copy.NEWBIZ.join(",");
			}
			data.AGGTYP = agreementTypeText;
			if (newbizText === "") {
				if (this.getView().byId("newBizCMBX").getSelectedItems().length === 1)
					newbizText = ",";
			}
			data.NEWBIZ = newbizText;
			isSearchPossible = this._isSearchPossible();
			params = {
				"HANDLERPARAMS": {
					"FUNC": "RED_POSITIONS"
				},
				"INPUTPARAMS": [data]
			};

			dbcontext.callServer(params, function (oModel) {
				var resdata = oModel.getProperty("/PRICEREQHDR");
				var that = this;
				resdata.forEach(function (row) {
					row.PRICE_MASTER.DCONDUNIT = parseInt(row.PRICE_MASTER.DCONDUNIT).toString();
					row.PRICE_MASTER.DPRSNEWBIZ_T = row.PRICE_MASTER.DPRSNEWBIZ === "" ? "New" : "Existing";
					//18.12.2020
					//volume
					row.PRICE_MASTER.VOLUME_VALUE = row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY;
					if (row.PRICE_MASTER.VOLUME_VALUE === -Infinity || row.PRICE_MASTER.VOLUME_VALUE === Infinity || isNaN(row.PRICE_MASTER.VOLUME_VALUE))
						row.PRICE_MASTER
						.VOLUME_VALUE = 0;
					row.PRICE_MASTER.VOLUME_VALUE = parseFloat(row.PRICE_MASTER.VOLUME_VALUE.toFixed(2));
					//GM
					row.PRICE_MASTER.GM_VALUE = row.PRICE_MASTER.DPRSGM * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSGMPY * row.PRICE_MASTER
						.DPRSVVJ -
						row.PRICE_MASTER.DPRSGMYPY * row.PRICE_MASTER.DPRSVYTDPY;
					if (row.PRICE_MASTER.GM_VALUE === -Infinity || row.PRICE_MASTER.GM_VALUE === Infinity || isNaN(row.PRICE_MASTER.GM_VALUE))
						row.PRICE_MASTER
						.GM_VALUE = 0;
					row.PRICE_MASTER.GM_VALUE = parseFloat(row.PRICE_MASTER.GM_VALUE.toFixed(2));

					//CM1
					row.PRICE_MASTER.CM1_VALUE = row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY;
					if (row.PRICE_MASTER.CM1_VALUE === -Infinity || row.PRICE_MASTER.CM1_VALUE === Infinity || isNaN(row.PRICE_MASTER.CM1_VALUE))
						row.PRICE_MASTER
						.CM1_VALUE = 0;
					row.PRICE_MASTER.CM1_VALUE = parseFloat(row.PRICE_MASTER.CM1_VALUE.toFixed(2));
					//CM1_EUR_KG	
					row.PRICE_MASTER.CM1_EUR_KG_VALUE = (row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY) /
						(
							row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);
					if (row.PRICE_MASTER.CM1_EUR_KG_VALUE === -Infinity || row.PRICE_MASTER.CM1_EUR_KG_VALUE === Infinity || isNaN(row.PRICE_MASTER
							.CM1_EUR_KG_VALUE)) row.PRICE_MASTER
						.CM1_EUR_KG_VALUE = 0;
					row.PRICE_MASTER.CM1_EUR_KG_VALUE = parseFloat(row.PRICE_MASTER.CM1_EUR_KG_VALUE.toFixed(2));

				});
				sap.ui.getCore().getModel("redPositionM").setData(resdata);
			}, this);
		},

		clearSelectionFields: function () {
			this._redPositionfilterM.setData({
				"C_DOEBKUNDE": "",
				"CUSTOMER": "",
				"ARTICLE": "",
				"REGION": "",
				"AGGTYP": "",
				"NEWBIZ": "",
				"C_DACCMAN": "",
				"AREA": "",
				"C_CUST_GRP2": ""
			});

			var controls = this.getView().byId("searchGridId").getContent();
			for (var i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}

			this.getView().byId("newBizCMBX").setSelectedKeys([]);
			// var oUser = new sap.ushell.services.UserInfo();
			// var userId = oUser.getId();
			// this.getView().byId("redPositionUserInputId").addToken(new sap.m.Token({
			// 	key: userId,
			// 	text: userId
			// }));
		},

		_isSearchPossible: function () {
			var filterData = this._redPositionfilterM.getData(),
				isReady = false;
			Object.keys(filterData).forEach(function (o) {
				if (filterData[o] !== "" && filterData[o].length !== 0 /*&& o !== "UNAME"*/ ) isReady = true;
			});

			return isReady;
		},

		_fillCustomerTable: function () {
			var tableData = [];
			for (var i = 0; i < 8; i++) {
				tableData.push({
					REP_ZKUNNR: "16847",
					REP_ZKUNNR_NAME: "Derhim Industrial Co",
					ZKUNNR: "16847",
					ZKUNNR_NAME: "Derhim Industrial Co",
					ZMATNR_BULK: "5.02482.881",
					DESC: "COLA-KOMPONENTE-TEIL-2/KAN/25 KG (XSA)",
					REGION: "GCC",
					Existing_New: "",
					AGREEMENT_TYPE: "Contract",
					PRICE_VALIDITY_S: "01.01.2019",
					PRICE_VALIDITY_E: "31.12.2019",
					Sales_Volume_PY: "102",
					Sales_Volume_CY: "120",
					Price_Gross_CY: "26",
					Price_Gross_PY: "3",
					GM_CY: "41",
					GM_PY: "52",
					CM1_CY: "6",
					CM1_PY: "92",
					CM1_CYY: "12",
					CM1_PYY: "11",
				});
			}

			sap.ui.getCore().getModel("redPositionM").setData(tableData);
		},
		/*-------------------- START OF VARIANT MANAGEMENT --------------------------- */
		oCC: null,
		currTableData: null,
		oTPC: null,
		tableId: "redPositionTableId",
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz.tableId);
				oCC.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var oVM = this.getView().byId("tableRedPosVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(tableId, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(tableId);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oItem[oItem.defaultKey].rowCount === undefined ? 8 : oItem[oItem.defaultKey].rowCount);
				oTable.setVisibleRowCountMode(oItem[oItem.defaultKey].rowCount === undefined ? "Interactive" : "Fixed");

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data

			oTable.setVisibleRowCountMode("Fixed");
			oTable.setVisibleRowCount(oItem[selKey].rowCount === undefined ? 8 : oItem[selKey].rowCount);
			oTable.setVisibleRowCountMode(oItem[selKey].rowCount === undefined ? "Interactive" : "Fixed");

		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oTable.getVisibleRowCount());

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this.tableId;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		/*************************************************Start Header Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdReds");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("redPositionfilterM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			// oVM.setInitialSelectionKey(ovar.defaultKey);
			// oVM.setDefaultVariantKey(ovar.defaultKey);
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("redPositionfilterM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantReds: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("redPositionfilterM").setData(ovar[selKey]);
			this.createToken();
		},

		/* on save variant */
		onSaveVariantReds: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("redPositionfilterM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMReds: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function () {
			var aControls = this.getView().byId("searchGridId").getContent();
			aControls.forEach(function (item) {
				if (item.getMetadata().getName() === "com.doehler.PricingMasterFile.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
					"sap.m.MultiInput") {
					if (item.getMetadata().getName() === "sap.m.MultiInput")
						item.destroyTokens();
					if (item.getId().indexOf("multiInputIssues") <= -1) {
						if (item.getMultiSelect() && item.getSelectedValues()) {
							var arr = item.getSelectedValues().split(",");
							arr.forEach(function (value) {
								if (value != "") {
									item.addToken(new sap.m.Token({
										key: value,
										text: value
									}));
								}
							});
						}
					} else {
						if (sap.ui.getCore().getModel("redPositionfilterM").getData().REDP === "X") {
							item.addToken(new sap.m.Token({
								key: 0,
								text: "Reds(position)"
							}));
						}
						if (sap.ui.getCore().getModel("redPositionfilterM").getData().REDC === "X") {
							item.addToken(new sap.m.Token({
								key: 1,
								text: "Reds(customer)"
							}));
						}
						if (sap.ui.getCore().getModel("redPositionfilterM").getData().LAST_PRICE_1 === "X") {
							item.addToken(new sap.m.Token({
								key: 2,
								text: "Last price change(last price change > 1 year"
							}));
						}
						if (sap.ui.getCore().getModel("redPositionfilterM").getData().LAST_PRICE_2 === "X") {
							item.addToken(new sap.m.Token({
								key: 3,
								text: "Last price change(last price change > 2 year"
							}));
						}
						if (sap.ui.getCore().getModel("redPositionfilterM").getData().GAP_TO_TARGET === "X") {
							item.addToken(new sap.m.Token({
								key: 4,
								text: "L & I-Account increase"
							}));
						}
						if (sap.ui.getCore().getModel("redPositionfilterM").getData().ACCOUNT_INCREASE === "X") {
							item.addToken(new sap.m.Token({
								key: 5,
								text: "GAP to Target"
							}));
						}

					}
				}
			});
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			this.initVariant();
			this.initSearchVariant();
		}

		/*-------------------------END OF VARIANT MANAGEMENT ------------------------*/
	});

});