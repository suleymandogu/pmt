sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models",
	"com/doehler/PricingMasterFile/model/dbcontext",
	"com/doehler/PricingMasterFile/model/formatter",
	"com/doehler/PricingMasterFile/excel/excel",
	"sap/ui/core/Fragment",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast",
	"sap/m/MessageBox"
], function (BaseController, base, models, dbcontext, formatter, excel, Fragment, Filter, FilterOperator, MessageToast, MessageBox) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.PricingMeasures.PricingMeasures", {
		formatter: formatter,
		onInit: function () {
			this._bindModels();
			this._getControlsFromView();
			//this._globalM.setProperty("/UIROLE", "PRICM");
			this._setButtonsPressed(this._globalM, "pricmBtnId");
			this.getRouter().getRoute("PriceMeasures").attachPatternMatched(this._onRouteMatched, this);
			this._getCmbxItems();
			this._getFilter();

			var table = this.getView().byId("pricMeasuresTableId");
			if (this._globalM.getData().UIROLE !== "ADMINM")
				this._columnAuthOnlyAdmin(table);

		},

		_onRouteMatched: function (oEvent) {
			var refresh = this._globalM.getProperty("/savedDetailPage");
			//this.clearSelectionFields();
			this._viewUpdate("measuresPage");
			var id = oEvent.getParameter("arguments").requestId;
			//this._globalM.setProperty("/UIROLE", id === "BU" ? "SALESM" : "PRICM");
			this._getCmbxItems();

			this._setButtonsPressed(this._globalM, id === "BU" ? "bumBtnId" : "pricmBtnId");

			if (refresh === true)
				if (this._resultM.getData().length > 0)
					if (base.previousPage !== "PriceSetting")
						this.onGo();
					else this._fillTable(this._resultM.getData());
		},

		onFilter: function (oEvent) {},

		_bindModels: function () {
			var that = this;
			var aModels = ["pMfilterM", "pMresultM", "globalModel"];
			aModels.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});

			var aDropdown = ["rootCauseDD", "priceActionDD", "buActionDD", "priceStatusDD"];
			aDropdown.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
		},

		_getFilter: function () {
			var thiz = this,
				params,
				data,
				globalRes = [],
				customerServ = [],
				accountManager = [],
				customerServiceInput = this.getView().byId("priceSettingUCSInputId"),
				accountManagerInput = this.getView().byId("priceSettingUAMInputId"),
				globalRespInput = this.getView().byId("priceSettingUGRInputId"),
				role = sap.ui.getCore().getModel("globalModel").getData().UIROLE;

			if (role !== "SALESM") return;

			params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_FILTER"
				}
			};
			dbcontext.callServer(params, function (oModel) {
				data = oModel.getData();
				globalRes = data.ZGLOBEL;
				accountManager = data.TVKGR;
				customerServ = data.T171;

				customerServ.forEach(function (line) {
					customerServiceInput.addToken(new sap.m.Token({
						key: line.BZIRK,
						text: line.BZIRK
					}));
				});
				accountManager.forEach(function (line) {
					accountManagerInput.addToken(new sap.m.Token({
						key: line.VKGRP,
						text: line.VKGRP
					}));
				});
				globalRes.forEach(function (line) {
					globalRespInput.addToken(new sap.m.Token({
						key: line.KVGR4,
						text: line.KVGR4
					}));
				});
				if (customerServ.length > 0 || accountManager.length > 0 || globalRes.length > 0)
					thiz.onGo();
			}, this);
		},

		/* ----------------- Dropdown --------------------*/
		loadDropdown: function () {
			models.loadRootCause();
			models.loadPricingAction();
			models.loadBUAction();
			models.loadPricingStatus();
			models.loadWorkFlowStatus();
		},

		_getControlsFromView: function () {
			this._table = this.getView().byId("pricMeasuresTableId");
			this._filterM = sap.ui.getCore().getModel("pMfilterM");
			this._resultM = sap.ui.getCore().getModel("pMresultM");
			this._globalM = sap.ui.getCore().getModel("globalModel");
		},

		onExport: function () {
			var data = [];
			var tableData = this._resultM.getData();
			var userId = sap.ushell.Container.getUser().getId();
			var copiedData = $.extend(true, [], tableData);
			var aIndices = this._table.getBinding("rows").aIndices;

			if (copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			if (userId === "MATTHEC" || userId === "EX_DOGUS" || userId === "DEFAULT_USER")
				excel._downloadExcel("PSM", this._table, data);
			else
				excel._downloadExcel("PSM", this._table, data);

		},

		_getCmbxItems: function () {
			var uiRole = this._globalM.getProperty("/UIROLE"),
				columnList = [],
				isManager = uiRole === "PRICM" || uiRole === "ADMINM";

			switch (isManager) {
			case false:
				columnList.push({
					key: "RouteCause",
					text: "Route Cause"
				});
				columnList.push({
					key: "Action",
					text: "Action"
				});
				columnList.push({
					key: "Who",
					text: "Who"
				});
				columnList.push({
					key: "Deadline",
					text: "Deadline"
				});
				columnList.push({
					key: "Comment",
					text: "Comment"
				});
				columnList.push({
					key: "Status",
					text: "Status"
				});
				break;
			case true:
				columnList.push({
					key: "PricingAction",
					text: "Price Campaign"
				});
				columnList.push({
					key: "PricingActionComment",
					text: "Pricing Action"
				});
				columnList.push({
					key: "Deadline",
					text: "Deadline"
				});
				columnList.push({
					key: "Who",
					text: "Who"
				});
				columnList.push({
					key: "Comment",
					text: "Comment"
				});
				columnList.push({
					key: "BU_PM_Comment",
					text: "Internal comment BU/Pricing"
				});
				columnList.push({
					key: "Status",
					text: "Status"
				});
				columnList.push({
					key: "LACC",
					text: "L-Account Price Increase"
				});
				break;
			default:
			}

			this._globalM.setProperty("/columnListCmbx", columnList);
			this._globalM.setProperty("/columnKeyCmx", columnList);
			this._globalM.setProperty("/columnListCmbx2", []);
		},

		clearSelectionFields: function () {
			this._filterM.setData({
				CUSTOMER: "", //"0000016847",
				ARTICLE: "",
				B2BL1: "",
				B2BL2: "",
				B2BL3: "",
				B2BL4: "",
				UNAME: "",
				STATUS: "",
				AREA: "",
				REGION: "",
				CUSTSEG: "",
				AGG_TYP: "",
				C_CUST_GRP4: "",
				C_DACCMAN: "",
				C_CUST_GRP2: "",
				C_DOEBKUNDE: "",
				REDC: "",
				REDP: "",
				LAST_PRICE_1: "",
				LAST_PRICE_2: "",
				GAP_TO_TARGET: "",
				ACCOUNT_INCREASE: "",
				TIER2_CUSTOMER_L1: "",
				GLOBAL_TEAM: ""
			});

			var controls = this.getView().byId("searchGridId").getContent();
			for (var i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}
		},

		onSubmitOrChange: function (oEvent) {
			var oSource = oEvent.getSource(),
				id = oSource.getId().split("__")[1].split("-")[2],
				path = oSource.getBindingContext("pMresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPM"),
				activeLine = this._resultM.getProperty(path),
				value = oSource.getValue(),
				allCalculatedValues,
				saveBtnPressed = this.getView().byId("savePRBId")._buttonPressed,
				line = this._resultM.getProperty(path);
			if (oEvent.getId() === "submit") return;
			var bindValue = oEvent.getSource().getBindingInfo("value");
			var sName = "",
				sType = "";
			if (bindValue !== undefined || bindValue !== null) {
				sType = bindValue.type;
				if (sType !== undefined || sType !== null) {
					sName = sType.sName;
				}
			}

			if (sName === "Float" && oEvent.getParameter("newValue") === "") oSource.setValue("0");
			if (this._resultM.getProperty(path + "/PRICE_MASTER/CHANGED") !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPM", savedData);
			if (id === "newPricePlannedId" || id === "newPricePlannedId2") {
				allCalculatedValues = this.calculateNewPrices(value, activeLine);
				this._resultM.setProperty(path, allCalculatedValues);
				this._checkApprovalIsNeeeded(path, saveBtnPressed);
				activeLine = this._updateScaleTableForMainViews(value, activeLine);
				this._resultM.setProperty(path, activeLine);

				// var that = this;
				// var promiseForAppNeeded = new Promise(function (myResolve, myReject) {
				// 	that._checkApprovalIsNeeeded(path, saveBtnPressed, myResolve);
				// });
				// promiseForAppNeeded.then(
				// 	function (a) {

				// 		allCalculatedValues = that.calculateNewPrices(value, activeLine);
				// 		that._resultM.setProperty(path, allCalculatedValues);
				// 		activeLine = that._updateScaleTableForMainViews(value, activeLine);
				// 		that._resultM.setProperty(path, activeLine);
				// 		if (saveBtnPressed === true)
				// 			that.onSavePR();
				// 	},
				// 	function (error) { /* code if some error */ }
				// );
			} else {
				if (id === "targetVolumeId" || id === "targetVolumeId2") {
					this._checkApprovalIsNeeeded(path, saveBtnPressed);
				}
			}
		},

		_checkApprovalIsNeeeded: function (path, saveBtnPressed, myResolve) {
			var that = this,
				sendingDataArray = [],
				sendingData = {},
				newPrice = [],
				line = this._resultM.getProperty(path),
				DOESTAFKZ = line.PRICE_MASTER.DOESTAFKZ,
				hasRecord = line.NEW_PRICE.CUSTOMER !== "",
				deact = line.NEW_PRICE.DEACTIVATE_SCALE,
				priceMaster = [];

			if (DOESTAFKZ !== "" && deact !== "X") {
				if (!hasRecord)
					line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DSCALQTY1;
			} else {
				if (hasRecord) {
					line.NEW_PRICE.MOQ_NEW = line.NEW_PRICE.MOQ_NEW;
				} else {
					line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DPRSMOQ;
				}
			}

			newPrice.push(line.NEW_PRICE);
			priceMaster.push(line.PRICE_MASTER);

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "APPROVEL_NEEDED"
				},
				"INPUTPARAMS": sendingDataArray
			};

			dbcontext.callServer(params, function (oModel) {
				that._resultM.setProperty(path + "/NEW_PRICE/APP_NEEDED", oModel.getData().APPRO_NEEDED);
				that._resultM.setProperty(path + "/NEW_PRICE/RFQ_NEEDED", oModel.getData().RFQ_NEEDED);
				that._resultM.setProperty(path + "/NEW_PRICE/THRESHOLD_CLASS", oModel.getData().THRESHOLD_CLASS);
				// that._resultM.setProperty(path + "/NEW_PRICE/ZAPPRCM1EXP", oModel.getData().EXPECTED_PERC);
				// if (myResolve !== undefined)
				// 	myResolve();
				// else {
				if (saveBtnPressed === true)
					that.onSavePR();
				// }
			}, this);
		},

		onChangeWho: function (oEvent) {
			var oSource = oEvent.getSource(),
				bindContext = oSource.getBindingContext("pMresultM"),
				savedData = this._globalM.getProperty("/savedPM");

			if (bindContext === undefined)
				bindContext = sap.ui.getCore().getModel("globalModel").getProperty("/pressTableValueControl").getBindingContext("pMresultM");
			var path = bindContext.getPath();
			var line = this._resultM.getProperty(path);
			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPM", savedData);

		},

		onChangeDeadline: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("pMresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPM"),
				line = this._resultM.getProperty(path);

			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPM", savedData);
		},

		onChangeWhen: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("pMresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPM"),
				line = this._resultM.getProperty(path);

			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPM", savedData);
		},

		onChangeComment: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("pMresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPM"),
				line = this._resultM.getProperty(path);

			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPM", savedData);
		},

		onChangeInput: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("pMresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPM"),
				line = this._resultM.getProperty(path);

			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPM", savedData);
		},

		selectionChangeRow: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("pMresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPM"),
				line = this._resultM.getProperty(path);

			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}

			if (oEvent.getSource().getCustomData().length) {
				if (oEvent.getSource().getCustomData()[0].getValue()) {
					this._resultM.setProperty(path + "/PRICE_MASTER/" + oEvent.getSource().getCustomData()[0].getValue(), oEvent.getParameter(
						"selectedItem").getText());
				}
			}

			this._globalM.setProperty("/savedPM", savedData);
		},

		onApplyToAll: function (oEvent) {
			var i = 0,
				line,
				decFormat = this._globalM.getProperty("/decFormat"),
				control = this.getView().byId("columnCmxId"),
				control2 = this.getView().byId("columnCmx2Id"),
				whoInput = this.getView().byId("whoInputHeaderId"),
				deadlineDate = this.getView().byId("datePickerHeaderId"),
				lACCPInputHeaderInput = this.getView().byId(decFormat === "X" ? "lACCPInputHeaderId" : "lACCPInputHeaderId2"),
				commentValue = this.getView().byId("textAreaHeader"),
				key = control.getSelectedKey(),
				selectedKey = control2.getSelectedKey(),
				tableData = this._resultM.getData(),
				table = this._table,
				aSelIndex = table.getSelectedIndices(),
				newSelIndex = [],
				aIndices = table.getBinding().aIndices;

			if (key === "" || key === undefined) {
				MessageToast.show("Please select a column");
				return;
			}
			if (aSelIndex.length === 0) {
				MessageToast.show("Please select at least one record");
				return;
			}

			aSelIndex.forEach(function (selIndex) {
				newSelIndex.push(aIndices[selIndex]);
			});

			aSelIndex = newSelIndex;

			var savedData = this._globalM.getProperty("/savedPM");
			for (i = 0; i < aSelIndex.length; i++) {
				if (tableData[aSelIndex[i]].PRICE_MASTER.CHANGED !== true) {
					savedData.push(tableData[aSelIndex[i]]);
					tableData[aSelIndex[i]].PRICE_MASTER.CHANGED = true;
					this._resultM.refresh();
				}
			}
			this._globalM.setProperty("/savedPM", savedData);

			switch (key) {
			case "Status":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.PRICING_STATUS = selectedKey;
				}
				break;
			case "RouteCause":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.ROOT_CAUSE = selectedKey;
				}
				break;
			case "Action":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.BU_ACTION = selectedKey;
				}
				break;
			case "Who":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.PRICING_WHO = whoInput.getValue();
				}
				break;
			case "Deadline":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.PRICING_DEADLINE = deadlineDate.getValue();
				}
				break;
			case "Comment":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.PRICE_COMMENT = commentValue.getValue();
				}
				break;
			case "BU_PM_Comment":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.BU_PM_COMMENT = commentValue.getValue();
				}
				break;
			case "PricingAction":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.PRICE_ACTION = selectedKey;
				}
				break;
			case "PricingActionComment":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.PRICE_ACTION_COMMENT = commentValue.getValue();
				}
				break;
			case "byWhen":
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.byWhen = deadlineDate.getValue();
				}
				break;
			case "LACC":
				var formattedValue = lACCPInputHeaderInput.getValue().replace(".", "");
				formattedValue = formattedValue.replace(",", ".");
				for (i = 0; i < aSelIndex.length; i++) {
					line = tableData[aSelIndex[i]];
					line.NEW_PRICE.L_ACC_PRICE_INC = formattedValue;
				}
				break;
			default:
			}

			this._resultM.refresh();

		},

		selectionChange: function (oEvent) {
			var id = oEvent.getSource().getId().split("--")[1],
				key = oEvent.getSource().getSelectedKey(),
				filteredData = [],
				t,
				columnListCmbx2 = [];

			this.getView().byId("whoInputHeaderId").setVisible(false);
			this.getView().byId("datePickerHeaderId").setVisible(false);
			this.getView().byId("textAreaHeader").setVisible(false);
			this.getView().byId("columnCmx2Id").setVisible(true);

			this.getView().byId("lACCPInputHeaderId").setVisible(false);
			this.getView().byId("lACCPInputHeaderId2").setVisible(false);

			this.getView().byId("whoInputHeaderId").setValue("");
			this.getView().byId("datePickerHeaderId").setValue("");
			this.getView().byId("textAreaHeader").setValue("");
			this.getView().byId("lACCPInputHeaderId").setValue("");
			this.getView().byId("lACCPInputHeaderId2").setValue("");

			switch (id) {
			case "columnCmxId":
				switch (key) {
				case "Status":
					filteredData = this.getView().getModel("priceStatusDD").getData();
					for (t = 0; t < filteredData.length; t++) {
						columnListCmbx2.push({
							key: filteredData[t].DOMVALUE_L,
							text: filteredData[t].DOMVALUE_L + " " + filteredData[t].DDTEXT
						});
					}
					break;
				case "RouteCause":
					filteredData = this.getView().getModel("rootCauseDD").getData();
					for (t = 0; t < filteredData.length; t++) {
						columnListCmbx2.push({
							key: filteredData[t].DOMVALUE_L,
							text: filteredData[t].DOMVALUE_L + " " + filteredData[t].DDTEXT
						});
					}
					break;
				case "Action":
					filteredData = this.getView().getModel("buActionDD").getData();
					for (t = 0; t < filteredData.length; t++) {
						columnListCmbx2.push({
							key: filteredData[t].DOMVALUE_L,
							text: filteredData[t].DOMVALUE_L + " " + filteredData[t].DDTEXT
						});
					}
					break;
				case "Who":
					this.getView().byId("whoInputHeaderId").setVisible(true);
					this.getView().byId("columnCmx2Id").setVisible(false);
					this.getView().byId("datePickerHeaderId").setVisible(false);
					this.getView().byId("textAreaHeader").setVisible(false);
					break;
				case "Deadline":
					this.getView().byId("whoInputHeaderId").setVisible(false);
					this.getView().byId("columnCmx2Id").setVisible(false);
					this.getView().byId("textAreaHeader").setVisible(false);
					this.getView().byId("datePickerHeaderId").setVisible(true);
					break;
				case "byWhen":
					this.getView().byId("whoInputHeaderId").setVisible(false);
					this.getView().byId("columnCmx2Id").setVisible(false);
					this.getView().byId("textAreaHeader").setVisible(false);
					this.getView().byId("datePickerHeaderId").setVisible(true);
					break;
				case "Comment":
					this.getView().byId("whoInputHeaderId").setVisible(false);
					this.getView().byId("columnCmx2Id").setVisible(false);
					this.getView().byId("textAreaHeader").setVisible(true);
					this.getView().byId("datePickerHeaderId").setVisible(false);
					break;
				case "BU_PM_Comment":
					this.getView().byId("whoInputHeaderId").setVisible(false);
					this.getView().byId("columnCmx2Id").setVisible(false);
					this.getView().byId("textAreaHeader").setVisible(true);
					this.getView().byId("datePickerHeaderId").setVisible(false);
					break;
				case "LACC":
					this.getView().byId("whoInputHeaderId").setVisible(false);
					this.getView().byId("columnCmx2Id").setVisible(false);
					this.getView().byId("textAreaHeader").setVisible(false);
					this.getView().byId("datePickerHeaderId").setVisible(false);
					var decFormat = this._globalM.getProperty("/decFormat");
					this.getView().byId("lACCPInputHeaderId").setVisible(decFormat === "X");
					this.getView().byId("lACCPInputHeaderId2").setVisible(decFormat !== "X");
					break;
				case "PricingActionComment":
					this.getView().byId("whoInputHeaderId").setVisible(false);
					this.getView().byId("columnCmx2Id").setVisible(false);
					this.getView().byId("textAreaHeader").setVisible(true);
					this.getView().byId("datePickerHeaderId").setVisible(false);
					break;
				case "PricingAction":
					filteredData = this.getView().getModel("priceActionDD").getData();
					for (var t = 0; t < filteredData.length; t++) {
						columnListCmbx2.push({
							key: filteredData[t].PRICE_ACTION,
							text: filteredData[t].PRICE_ACTION + " " + filteredData[t].PRICE_ACT_DESC,
						});
					}
					break;
				default:
				}

				this._globalM.setProperty("/columnListCmbx2", columnListCmbx2);

				break;
			case "columnCmx2Id":

				break;
			default:
			}

		},

		handleLoadItems: function (oControlEvent) {
			oControlEvent.getSource().getBinding("items").resume();
		},

		_isSearchPossible: function () {
			var filterData = this._filterM.getData(),
				isReady = false;
			Object.keys(filterData).forEach(function (o) {
				if (filterData[o] !== "" /*&& o !== "UNAME"*/ ) isReady = true;
			});
			return isReady;
		},

		_clearGenericTiles: function () {
			this._table.setSelectedIndex(-1);

			this._globalM.setProperty("/tile1Total_PM", 0);
			this._globalM.setProperty("/tile1Open_PM", 0);
			this._globalM.setProperty("/tile1Progress_PM", 0);
			this._globalM.setProperty("/tile1Closed_PM", 0);

			this._globalM.setProperty("/tile2Campaign1", 0);
			this._globalM.setProperty("/tile2Campaign2", 0);
			this._globalM.setProperty("/tile2Campaign3", 0);
			this._globalM.setProperty("/tile2Campaign4", 0);
		},

		//Price Setting Data çağırılacak yeni fonsiyon ile
		onGo: function () {
			var that = this,
				params,
				oldData;
			this._globalM.setProperty("/savedPM", []);
			this._clearGenericTiles();

			var multiIssues = this.getView().byId("multiInputIssues");
			this._checkIssueFilter(multiIssues, this._filterM);
			var data = this._filterM.getData();
			this._checkExistCustomerRole(data);

			if (data.CUSTOMER !== "") {
				data.CUSTOMER_LOW = data.CUSTOMER;
				data.CUSTOMER_HIGH = "";
				data.RUNTIME = "1";
				params = {
					"HANDLERPARAMS": {
						"FUNC": "SEARCH_PMF_NEW2"
					},
					"INPUTPARAMS": [data]
				};
				dbcontext.callServer(params, function (oModel) {
					var resdata = oModel.getProperty("/PRICEREQHDR");
					that._updateTimeJob(oModel.getData()["UPDATE_INFO"]);
					var table = that.getView().byId("pricMeasuresTableId");
					var sortedColumnData, sortProperty;

					if (resdata.length === 0) {
						MessageBox.warning("Results not found for provided selection");
						return;
					}

					that._fillTable(resdata);
				}, this, null, null, true, true);
			} else {
				that._callDataTwice(data);
			}
		},

		_callDataTwice: function (data) {
			var low_customer_1 = "1",
				low_customer_2 = "50000",
				high_customer_1 = "50000",
				high_customer_2 = "1000000000",
				params, name = "SEARCH_PMF_NEW2",
				promise1, promise2, result1, result2,
				resData = [],
				that = this;

			data.CUSTOMER_LOW = low_customer_1;
			data.CUSTOMER_HIGH = low_customer_2;

			data.RUNTIME = "1";

			params = {
				"HANDLERPARAMS": {
					"FUNC": name
				},
				"INPUTPARAMS": [data]
			};

			sap.ui.core.BusyIndicator.show(0);

			promise1 = new Promise(function (resolved, rejected) {
				dbcontext.callServer(params, function (oModel) {
					result1 = oModel.getProperty("/PRICEREQHDR");
					resolved(result1);
				}, this, null, null, true, false);
			});

			data.CUSTOMER_LOW = low_customer_1;
			data.CUSTOMER_HIGH = low_customer_2;
			data.RUNTIME = "2";

			promise2 = new Promise(function (resolved, rejected) {
				dbcontext.callServer(params, function (oModel) {
					result2 = oModel.getProperty("/PRICEREQHDR");
					that._updateTimeJob(oModel.getData()["UPDATE_INFO"]);
					resolved(result2);
				}, this, null, null, true, false);
			});

			Promise.all([promise1, promise2]).then(function (o) {
				o.forEach(function (lineData) {
					resData = resData.concat(lineData);
				});
				sap.ui.core.BusyIndicator.hide();

				if (resData.length === 0) {
					MessageBox.warning("Results not found for provided selection");
					return;
				}
				that._fillTable(resData);
			}, function () {}.bind(this));

		},
		//ex_dogus: open dialog t show cost data
		onPressCostChange: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("pMresultM").getPath(),
				line = sap.ui.getCore().getModel("pMresultM").getProperty(path);
			this._openCostDrivers(line, oEvent, this);
		},

		_fillTable: function (resdata) {
			var that = this,
				oldData;
			that._globalM.setProperty("/resData", resdata);
			resdata = that._prepareTableData(resdata);
			resdata = that._loadPriceScalesForViews(resdata);
			oldData = $.extend(true, [], resdata);
			that._globalM.setProperty("/oldDataPM", oldData);
			sap.ui.getCore().getModel("pMresultM").setData(resdata);
			that._globalM.setProperty("/savedBU", []);
			that._calculateHeaderTiles(oldData);
			that._globalM.setProperty("/savedPM", []);
		},

		onCancelPR: function () {
			var oldData = this._globalM.getProperty("/oldDataPM");
			var oldData2 = $.extend(true, [], oldData);
			sap.ui.getCore().getModel("pMresultM").setData(oldData2);
			this._globalM.setProperty("/savedPM", []);
			this._calculateHeaderTiles(oldData2);

		},

		onTableRowSelect: function (oEvent) {
			this._calculateHeaderTiles([]);
		},

		_calculateHeaderTiles: function (data) {
			var selectedIndexs = this._table.getSelectedIndices(),
				aIndices = this._table.getBinding().aIndices,
				newSelIndex = [],
				tableData = [],
				pmTotal = 0,
				pmOpen = 0, //"1"
				pmProgress = 0, //"2"
				pmClosed = 0, //"3"
				campaign1 = 0, //01
				campaign2 = 0, //02
				campaign3 = 0, // not yet
				campaign4 = 0, // no  campaign
				that = this;

			//gets lines that will be considered ----> tableData
			if (selectedIndexs.length === 0) {
				tableData = that._globalM.getProperty("/oldDataPM");
			} else {
				selectedIndexs.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});

				selectedIndexs = newSelIndex;
				selectedIndexs.forEach(function (line) {
					tableData.push(that._globalM.getProperty("/oldDataPM")[line]);
				});
			}

			tableData.forEach(function (line) {
				if (line.NEW_PRICE.PRICE_ACTION_COMMENT !== "" || line.NEW_PRICE.PRICE_ACTION !== "") pmTotal = pmTotal + 1;
				if (line.NEW_PRICE.PRICING_STATUS === "1") pmOpen = pmOpen + 1;
				if (line.NEW_PRICE.PRICING_STATUS === "2") pmProgress = pmProgress + 1;
				if (line.NEW_PRICE.PRICING_STATUS === "3") pmClosed = pmClosed + 1;
				if (line.NEW_PRICE.PRICE_ACTION === "01") campaign1 = campaign1 + 1;
				if (line.NEW_PRICE.PRICE_ACTION === "02") campaign2 = campaign2 + 1;
				if (line.NEW_PRICE.PRICE_ACTION === "") campaign4 = campaign4 + 1;
			});

			that._globalM.setProperty("/tile1Total_PM", pmTotal);
			that._globalM.setProperty("/tile1Open_PM", pmOpen);
			that._globalM.setProperty("/tile1Progress_PM", pmProgress);
			that._globalM.setProperty("/tile1Closed_PM", pmClosed);

			that._globalM.setProperty("/tile2Campaign1", campaign1);
			campaign2 = 0;
			that._globalM.setProperty("/tile2Campaign2", campaign2);
			that._globalM.setProperty("/tile2Campaign3", campaign3);
			that._globalM.setProperty("/tile2Campaign4", campaign4);
			that._globalM.refresh();

		},

		onSavePR: function () {
			var sendingDataArray = [],
				sendingData = {},
				newPrice = [],
				thiz = this,
				priceMaster = [],
				table = this._table,
				data = this._resultM.getData(),
				selectedIndexs = this._table.getSelectedIndices();

			var savedData = this._globalM.getProperty("/savedPM");

			if (savedData.length === 0) {
				MessageBox.warning("There is no changed line!");
				return;
			}

			for (var i = 0; i < savedData.length; i++) {
				newPrice.push(savedData[i].NEW_PRICE);
				priceMaster.push(savedData[i].PRICE_MASTER);
			}

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingData.LV_ROL = this._globalM.getProperty("/ROLEINDEX");
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_PRC_MASTER"
				},
				"INPUTPARAMS": sendingDataArray
			};

			dbcontext.callServer(params, function (oModel) {
				thiz.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						thiz.onGo();
					}
				});
			}, this);
		},

		handleValueHelp: function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();

			// create value help dialog
			if (!this._valueHelpDialog) {
				Fragment.load({
					id: "valueHelpDialog",
					name: "com.doehler.PricingMasterFile.fragments.Issues",
					controller: this
				}).then(function (oValueHelpDialog) {
					this._valueHelpDialog = oValueHelpDialog;
					this.getView().addDependent(this._valueHelpDialog);
					this._openValueHelpDialog(sInputValue);
				}.bind(this));
			} else {
				this._openValueHelpDialog(sInputValue);
			}
		},

		_openValueHelpDialog: function (sInputValue) {
			// create a filter for the binding
			this._valueHelpDialog.getBinding("items").filter([new Filter(
				"text",
				FilterOperator.Contains,
				sInputValue
			)]);

			// open value help dialog filtered by the input value
			this._valueHelpDialog.open(sInputValue);
		},

		_handleValueHelpSearch: function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"text",
				FilterOperator.Contains,
				sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpClose: function (evt) {
			var aSelectedItems = evt.getParameter("selectedItems"),
				oMultiInput = this.byId("multiInputIssues"),
				line,
				that = this;

			if (aSelectedItems && aSelectedItems.length > 0) {
				aSelectedItems.forEach(function (oItem) {
					line = sap.ui.getCore().getModel("globalModel").getProperty(oItem.getBindingContext("globalModel").getPath());

					switch (line.key) {
					case 0:
						that._filterM.setProperty("/REDP", "X");
						break;
					case 1:
						that._filterM.setProperty("/REDC", "X");
						break;
					case 2:
						that._filterM.setProperty("/LAST_PRICE_1", "X");
						break;
					case 3:
						that._filterM.setProperty("/LAST_PRICE_2", "X");
						break;
					case 4:
						that._filterM.setProperty("/ACCOUNT_INCREASE", "X");
						break;
					case 5:
						that._filterM.setProperty("/GAP_TO_TARGET", "X");

						break;
					default:
					}

					oMultiInput.addToken(new sap.m.Token({
						key: line.key,
						text: line.text
					}));
				});
			}
		},

		/*-------------------- START OF VARIANT MANAGEMENT --------------------------- */
		oCC: null,
		currTableData: null,
		oTPC: null,
		tableId: "pricMeasuresTableId",
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz.tableId);
				oCC.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var oVM = this.getView().byId("tablePricMVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(tableId, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(tableId);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oItem[oItem.defaultKey].rowCount === undefined ? 6 : oItem[oItem.defaultKey].rowCount);
				oTable.setVisibleRowCountMode(oItem[oItem.defaultKey].rowCount === undefined ? "Interactive" : "Fixed");

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data

			oTable.setVisibleRowCountMode("Fixed");
			oTable.setVisibleRowCount(oItem[selKey].rowCount === undefined ? 6 : oItem[selKey].rowCount);
			oTable.setVisibleRowCountMode(oItem[selKey].rowCount === undefined ? "Interactive" : "Fixed");

		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oTable.getVisibleRowCount());

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this.tableId;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		/*************************************************Start Header Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdPricingMeasure");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("pMfilterM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("pMfilterM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantPricingMeasure: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("pMfilterM").setData(ovar[selKey]);
			this.createToken();
		},

		/* on save variant */
		onSaveVariantPricingMeasure: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("pMfilterM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMPricingMeasure: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function () {
			var aControls = this.getView().byId("searchGridId").getContent();
			aControls.forEach(function (item) {
				if (item.getMetadata().getName() === "com.doehler.PricingMasterFile.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
					"sap.m.MultiInput") {
					if (item.getMetadata().getName() === "sap.m.MultiInput")
						item.destroyTokens();
					if (item.getId().indexOf("multiInputIssues") <= -1) {
						if (item.getMetadata().getName() === "sap.m.MultiComboBox") {
							// alert(item.getMetadata().getName());
						} else {
							if (item.getMultiSelect() && item.getSelectedValues()) {
								var arr = item.getSelectedValues().split(",");
								arr.forEach(function (value) {
									if (value != "") {
										item.addToken(new sap.m.Token({
											key: value,
											text: value
										}));
									}
								});
							}
						}
					} else {
						if (sap.ui.getCore().getModel("pMfilterM").getData().REDP === "X") {
							item.addToken(new sap.m.Token({
								key: 0,
								text: "Reds(position)"
							}));
						}
						if (sap.ui.getCore().getModel("pMfilterM").getData().REDC === "X") {
							item.addToken(new sap.m.Token({
								key: 1,
								text: "Reds(customer)"
							}));
						}
						if (sap.ui.getCore().getModel("pMfilterM").getData().LAST_PRICE_1 === "X") {
							item.addToken(new sap.m.Token({
								key: 2,
								text: "Last price change(last price change > 1 year"
							}));
						}
						if (sap.ui.getCore().getModel("pMfilterM").getData().LAST_PRICE_2 === "X") {
							item.addToken(new sap.m.Token({
								key: 3,
								text: "Last price change(last price change > 2 year"
							}));
						}
						if (sap.ui.getCore().getModel("pMfilterM").getData().GAP_TO_TARGET === "X") {
							item.addToken(new sap.m.Token({
								key: 4,
								text: "L & I-Account increase"
							}));
						}
						if (sap.ui.getCore().getModel("pMfilterM").getData().ACCOUNT_INCREASE === "X") {
							item.addToken(new sap.m.Token({
								key: 5,
								text: "GAP to Target"
							}));
						}

					}
				}
			});
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.loadDropdown();
			this.afterRender = true;
			this.initVariant();
			this.initSearchVariant();
		}

		/*-------------------------END OF VARIANT MANAGEMENT ------------------------*/
	});

});