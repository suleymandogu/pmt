/* Developer Anuj Harshe */
sap.ui.define([
		"sap/ui/core/mvc/Controller",
		"com/doehler/PricingMasterFile/model/dbcontext",
		"com/doehler/PricingMasterFile/model/dbcontext2",
		"com/doehler/PricingMasterFile/controller/ErrorHandler",
		"com/doehler/PricingMasterFile/model/models",
		"com/doehler/PricingMasterFile/model/base",
		"com/doehler/PricingMasterFile/model/formulas",
		"sap/m/MessageBox",
		"sap/ui/table/TablePersoController",
		"com/doehler/PricingMasterFile/model/formatter",
		"sap/m/Dialog",
		"sap/m/RadioButton",
		"sap/m/Button",
		"sap/m/PDFViewer",
		"sap/ui/core/routing/History",
		'sap/ui/core/Fragment',
		"sap/ui/model/json/JSONModel",
		"sap/ui/export/library",
		"sap/ui/export/Spreadsheet",
		"com/doehler/PricingMasterFile/excel/excel"
	],
	function (Controller, dbcontext, dbcontext2, ErrorHandler, models, base, formulas, MessageBox, TablePersoController, formatter, Dialog,
		RadioButton,
		Button,
		PDFViewer, History, Fragment, JSONModel, exportLibrary, Spreadsheet, excel) {
		"use strict";
		var EdmType = exportLibrary.EdmType;
		return Controller.extend("com.doehler.PricingMasterFile.controller.BaseController", {
			/** Main Router
			 * @return {object} - router object
			 * */

			_columnAuthOnlyAdmin: function (table) {
				var columns = [];
				columns = table.getColumns().filter(function (tCol) {
					var id = tCol.getId().split("--")[1];
					return id === "colPSI100" || id === "colPSI101" || id === "colPSI102" || id === "colPSI103" || id === "colPSI104";
				});
				columns.forEach(function (col) {
					table.removeColumn(col);
				});
			},

			_columnAuthOnlyAdminAdd: function (table) {
				table.addColumn(this.getView().byId("colPSI100"));
				table.addColumn(this.getView().byId("colPSI101"));
				table.addColumn(this.getView().byId("colPSI102"));
				table.addColumn(this.getView().byId("colPSI103"));
				table.addColumn(this.getView().byId("colPSI104"));
			},

			switchToRole: function (oEvent) {
				var gModel = sap.ui.getCore().getModel("globalModel"),
					value = oEvent.getParameter("state");
				gModel.setProperty("/UIROLE", value ? "ADMINM" : "SALESM");
				gModel.setProperty("/SWITCH", true);

				if (this.getView().byId("psiBtnId").getPressed()) {
					if (value) {
						if (this.getView().byId("priceSettingInfromationTableId") !== undefined)
							this._columnAuthOnlyAdminAdd(this.getView().byId("priceSettingInfromationTableId"));
					} else {
						if (this.getView().byId("priceSettingInfromationTableId") !== undefined)
							this._columnAuthOnlyAdmin(this.getView().byId("priceSettingInfromationTableId"));
					}
				}

			},

			_updateTimeJob: function (dateTime) {
				var gModel = sap.ui.getCore().getModel("globalModel");

				var eDate = dateTime["ENDDATE"],
					eTime = dateTime["ENDTIME"];
				// if (eDate === "0000-00-00")
				// 	gModel.setProperty("/jobDate", "");
				// else
				gModel.setProperty("/jobDate", eDate.split("-")[2] + "." + eDate.split("-")[1] + "." + eDate.split("-")[0] + " " +
					eTime.split(":")[0] + ":" + eTime.split(":")[1] + ":" + eTime.split(":")[2]);
			},

			_checkIssueFilter: function (multiIssues, filterM) {
				var that = this;
				this._filterM = filterM;
				this._filterM.setProperty("/REDP", "");
				this._filterM.setProperty("/REDC", "");
				this._filterM.setProperty("/LAST_PRICE_1", "");
				this._filterM.setProperty("/LAST_PRICE_2", "");
				this._filterM.setProperty("/ACCOUNT_INCREASE", "");
				this._filterM.setProperty("/GAP_TO_TARGET", "");
				var text;
				multiIssues.getTokens().forEach(function (token) {
					text = token.getText();
					switch (text) {
					case "Reds(position)":
						that._filterM.setProperty("/REDP", "X");
						break;
					case "Reds(customer)":
						that._filterM.setProperty("/REDC", "X");
						break;
					case "Last price change(last price change > 1 year":
						that._filterM.setProperty("/LAST_PRICE_1", "X");
						break;
					case "Last price change(last price change > 2 year":
						that._filterM.setProperty("/LAST_PRICE_2", "X");
						break;
					case "GAP to Target":
						that._filterM.setProperty("/GAP_TO_TARGET", "X");
						break;
					case "L & I-Account increase":
						that._filterM.setProperty("/ACCOUNT_INCREASE", "X");
						break;
					default:
					}
				});
			},

			calculateNewPrices: function (value, activeLine) {
				var decFormat = sap.ui.getCore().getModel("globalModel").getProperty("/decFormat");
				var formattedValue;

				if (decFormat !== "X") {
					formattedValue = value.replace(".", "");
					formattedValue = formattedValue.replace(",", ".");
				} else {
					formattedValue = value.replace(",", "");
				}

				formattedValue = formattedValue === "" ? "0" : formattedValue;

				activeLine = formulas._calculateNewPrices(formattedValue, activeLine);

				return activeLine;

			},

			_prepareTableData: function (data) {
				var index = 0;

				for (index = 0; index < data.length; index++) {
					/********************data adjustment********************/
					data[index] = formulas._afterReadBackendData(data[index]);
					/********************calculate Prices********************/
					data[index] = formulas._calculatePrices(data[index]);
				}
				return data;
			},

			_checkExistCustomerRole: function (data) {
				var gModel = sap.ui.getCore().getModel("globalModel");
				var INITALCUSTOMER = gModel.getProperty("/INITALCUSTOMER");
				var BU_GLOBAL_MANAGER = gModel.getProperty("/BU_GLOBAL_MANAGER");
				var BU_REGIONAL_MANAGER = gModel.getProperty("/BU_REGIONAL_MANAGER");
				var SALES_MANAGER = gModel.getProperty("/SALES_MANAGER");
				var MANAGER = gModel.getProperty("/MANAGER");

				var customersAuto = INITALCUSTOMER.map(function (oContext) {
					return oContext.KUNNR;
				}).join(",")
				data.CUSTOMER_AUTO = customersAuto;

				data.BU_GLOBAL_MANAGER = BU_GLOBAL_MANAGER;
				data.BU_REGIONAL_MANAGER = BU_REGIONAL_MANAGER;
				data.SALES_MANAGER = SALES_MANAGER;
				data.MANAGER = MANAGER;

				return data;
			},

			_checkSendQuotation: function (table, row) {
				var selectedIndexs, that = this,
					aIndices, newSelIndex = [],
					tableData = [],
					isActive = true;
				if (row === undefined) {
					selectedIndexs = table.getSelectedIndices();
					aIndices = table.getBinding().aIndices;
					selectedIndexs.forEach(function (selIndex) {
						newSelIndex.push(aIndices[selIndex]);
					});
					selectedIndexs = newSelIndex;

					selectedIndexs.forEach(function (line) {
						tableData.push(that._resultM.getData()[line]);
					});
				}

				tableData.forEach(function (line) {
					if (line.NEW_PRICE.APP_NEEDED === "X" && line.NEW_PRICE.WORKLOW_STATUS !== "2")
						isActive = false;

				});

				return isActive;
			},

			onReset: function (oEvent) {
				var tableId = oEvent.getSource().getAriaDescribedBy().toString();
				var oTable = sap.ui.getCore().byId(tableId);
				var columns = oTable.getColumns();
				var len = columns.length;
				for (var i = 0; i < len; i++) {
					columns[i].setFilterValue("").setFiltered(false).setSorted(false);
				}
				oTable.getBinding("rows").filter(null).sort(null);
			},

			_updateScaleTableForMainViews: function (value, activeLine) {
				var decFormat = sap.ui.getCore().getModel("globalModel").getProperty("/decFormat");
				var formattedValue;

				if (decFormat !== "X") {
					formattedValue = value.replace(".", "");
					formattedValue = formattedValue.replace(",", ".");
				} else {
					formattedValue = value.replace(",", "");
				}
				if (activeLine.PRICE_MASTER.DOESTAFKZ === "X") {
					activeLine.NEW_PRICE.ZLSCALE_PRC_Q1 = parseFloat(formattedValue);
					activeLine = this._setPriceScalesForMainViews(activeLine);
				}
				return activeLine;
			},

			_setPriceScalesForMainViews: function (activeLine) {
				var price = activeLine.NEW_PRICE.ZLSCALE_PRC_Q1;
				var row = activeLine;
				var value = 0;
				if (row.PRICE_MASTER.DSCALQTY2 !== 0) {
					row.NEW_PRICE.ZLSCALE_PRC_Q2 = row.NEW_PRICE.ZLSCALE_PRC_Q1 - row.PRICE_MASTER.DSCALPRC1 + row.PRICE_MASTER.DSCALPRC2;
					value = row.NEW_PRICE.ZLSCALE_PRC_Q2;
					if (isNaN(value) || value === Infinity || value === -Infinity) {
						value = 0;
					}
					row.NEW_PRICE.ZLSCALE_PRC_Q2 = parseFloat(value.toFixed(2));
				}

				if (row.PRICE_MASTER.DSCALQTY3 !== 0) {
					row.NEW_PRICE.ZLSCALE_PRC_Q3 = row.NEW_PRICE.ZLSCALE_PRC_Q1 - row.PRICE_MASTER.DSCALPRC1 + row.PRICE_MASTER.DSCALPRC3;
					value = row.NEW_PRICE.ZLSCALE_PRC_Q3;
					if (isNaN(value) || value === Infinity || value === -Infinity) {
						value = 0;
					}
					row.NEW_PRICE.ZLSCALE_PRC_Q3 = parseFloat(value.toFixed(2));
				}
				if (row.PRICE_MASTER.DSCALQTY4 !== 0) {
					row.NEW_PRICE.ZLSCALE_PRC_Q4 = row.NEW_PRICE.ZLSCALE_PRC_Q1 - row.PRICE_MASTER.DSCALPRC1 + row.PRICE_MASTER.DSCALPRC4;
					value = row.NEW_PRICE.ZLSCALE_PRC_Q4;
					if (isNaN(value) || value === Infinity || value === -Infinity) {
						value = 0;
					}
					row.NEW_PRICE.ZLSCALE_PRC_Q4 = parseFloat(value.toFixed(2));
				}
				if (row.PRICE_MASTER.DSCALQTY5 !== 0) {
					row.NEW_PRICE.ZLSCALE_PRC_Q5 = row.NEW_PRICE.ZLSCALE_PRC_Q1 - row.PRICE_MASTER.DSCALPRC1 + row.PRICE_MASTER.DSCALPRC5;
					value = row.NEW_PRICE.ZLSCALE_PRC_Q5;
					if (isNaN(value) || value === Infinity || value === -Infinity) {
						value = 0;
					}
					row.NEW_PRICE.ZLSCALE_PRC_Q5 = parseFloat(value.toFixed(2));
				}
				if (row.PRICE_MASTER.DSCALQTY6 !== 0) {
					row.NEW_PRICE.ZLSCALE_PRC_Q6 = row.NEW_PRICE.ZLSCALE_PRC_Q1 - row.PRICE_MASTER.DSCALPRC1 + row.PRICE_MASTER.DSCALPRC6;
					value = row.NEW_PRICE.ZLSCALE_PRC_Q6;
					if (isNaN(value) || value === Infinity || value === -Infinity) {
						value = 0;
					}
					row.NEW_PRICE.ZLSCALE_PRC_Q6 = parseFloat(value.toFixed(2));
				}
				return row;
			},

			_loadPriceScalesForViews: function (data) {
				data.forEach(function (row) {
					var DOESTAFKZ = row.PRICE_MASTER.DOESTAFKZ;
					var hasRecord = row.NEW_PRICE.CUSTOMER !== "";
					if (DOESTAFKZ !== "") {

						if (hasRecord) {

						} else {
							row.NEW_PRICE.SCALE_FLG_NEW = "X";
							row.NEW_PRICE.MOQ_NEW = row.PRICE_MASTER.DSCALQTY1;
						}
					} else {
						if (hasRecord) {
							row.NEW_PRICE.MOQ_NEW = row.NEW_PRICE.MOQ_NEW;
						} else {
							row.NEW_PRICE.MOQ_NEW = row.PRICE_MASTER.DPRSMOQ;
						}
					}

					if (DOESTAFKZ === "") {
						row.NEW_PRICE.ZLSCALE_Q1 = 0;
						row.NEW_PRICE.ZLSCALE_Q2 = 0;
						row.NEW_PRICE.ZLSCALE_Q3 = 0;
						row.NEW_PRICE.ZLSCALE_Q4 = 0;
						row.NEW_PRICE.ZLSCALE_Q5 = 0;
						row.NEW_PRICE.ZLSCALE_Q6 = 0;
						row.NEW_PRICE.ZLSCALE_Q1 = 0;
						row.NEW_PRICE.ZLSCALE_Q2 = 0;
						row.NEW_PRICE.ZLSCALE_Q3 = 0;
						row.NEW_PRICE.ZLSCALE_Q4 = 0;
						row.NEW_PRICE.ZLSCALE_Q5 = 0;
						row.NEW_PRICE.ZLSCALE_Q6 = 0;
					}

					if ((row.NEW_PRICE.ZLSCALE_Q1 === "" || row.NEW_PRICE.ZLSCALE_Q1 === 0) &&
						(row.NEW_PRICE.ZLSCALE_Q2 === "" || row.NEW_PRICE.ZLSCALE_Q2 === 0) &&
						(row.NEW_PRICE.ZLSCALE_Q3 === "" || row.NEW_PRICE.ZLSCALE_Q3 === 0) &&
						(row.NEW_PRICE.ZLSCALE_Q4 === "" || row.NEW_PRICE.ZLSCALE_Q4 === 0) &&
						(row.NEW_PRICE.ZLSCALE_Q5 === "" || row.NEW_PRICE.ZLSCALE_Q5 === 0) &&
						(row.NEW_PRICE.ZLSCALE_Q5 === "" || row.NEW_PRICE.ZLSCALE_Q5 === 0)) {
						row.PRICE_MASTER.DOESTMEIN_V = row.PRICE_MASTER.DPRSMOQUOM !== "0" && row.PRICE_MASTER.DPRSMOQUOM !==
							"000" ?
							row.PRICE_MASTER.DPRSMOQUOM : row.DETAIL.MEINS;
						row.PRICE_MASTER.MOQ_OLD_DOESTMEIN_V = row.PRICE_MASTER.DPRSMOQUOM !== "0" && row.PRICE_MASTER.DPRSMOQUOM !==
							"000" ?
							row.PRICE_MASTER.DPRSMOQUOM : row.DETAIL.MEINS;

					} else {
						row.PRICE_MASTER.DOESTMEIN_V = row.PRICE_MASTER.DOESTMEIN;
						row.PRICE_MASTER.MOQ_OLD_DOESTMEIN_V = row.PRICE_MASTER.DOESTMEIN;
					}

					//MOQ new fields -- scale
					row.NEW_PRICE.ZLSCALE_Q1 = DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q1 : DOESTAFKZ !== "" && !hasRecord ? row.PRICE_MASTER
						.DSCALQTY1 : DOESTAFKZ === "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q1 : 0;
					row.NEW_PRICE.ZLSCALE_Q2 = DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q2 : DOESTAFKZ !== "" && !hasRecord ? row.PRICE_MASTER
						.DSCALQTY2 : DOESTAFKZ === "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q2 : 0;
					row.NEW_PRICE.ZLSCALE_Q3 = DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q3 : DOESTAFKZ !== "" && !hasRecord ? row.PRICE_MASTER
						.DSCALQTY3 : DOESTAFKZ === "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q3 : 0;
					row.NEW_PRICE.ZLSCALE_Q4 = DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q4 : DOESTAFKZ !== "" && !hasRecord ? row.PRICE_MASTER
						.DSCALQTY4 : DOESTAFKZ === "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q4 : 0;
					row.NEW_PRICE.ZLSCALE_Q5 = DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q5 : DOESTAFKZ !== "" && !hasRecord ? row.PRICE_MASTER
						.DSCALQTY5 : DOESTAFKZ === "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q5 : 0;
					row.NEW_PRICE.ZLSCALE_Q6 = DOESTAFKZ !== "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q6 : DOESTAFKZ !== "" && !hasRecord ? row.PRICE_MASTER
						.DSCALQTY6 : DOESTAFKZ === "" && hasRecord ? row.NEW_PRICE.ZLSCALE_Q6 : 0;

					var deactScale = sap.ui.getCore().getModel("priceDetailM").getProperty("/NEW_PRICE/DEACTIVATE_SCALE");

					if ((DOESTAFKZ === "" && deactScale === "") || (DOESTAFKZ === "X" && deactScale === "X")) {
						row.PRICE_MASTER.MOQ_NEW_VISIBLE = false;
					} else {
						row.PRICE_MASTER.MOQ_NEW_VISIBLE = true;
					}

					if ((row.PRICE_MASTER.DOESTMEIN_V === "0" || row.PRICE_MASTER.DOESTMEIN_V === "000" || row.PRICE_MASTER.DOESTMEIN_V === "")) {
						row.PRICE_MASTER.DOESTMEIN_V = row.DETAIL.MEINS;

					}
					if (row.NEW_PRICE.MOQ_NEW_UOM !== "" && row.NEW_PRICE.MOQ_NEW_UOM !== "0" && row.NEW_PRICE.MOQ_NEW_UOM !== "000")
						row.PRICE_MASTER.DOESTMEIN_V = row.NEW_PRICE.MOQ_NEW_UOM;

					row.NEW_PRICE.MOQ_NEW_UOM = row.PRICE_MASTER.DOESTMEIN_V;
				});

				return data;

			},

			onGoPriceRequest: function (oEvent) {
				//https://dap11ci.da.doehler.com:8111/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html#zdoehlersempr-display?tag=41&/ArticleMasterPage/P500003939
				var priceReqNo = oEvent.getSource().getText(),
					host,
					link;

				host = location.protocol + "//" + location.host;

				switch (location.hostname) {
				case "dap11ci.da.doehler.com":
					host = "https://dafe1ci.da.doehler.com:8111";
					break;
				case "dat11ci.da.doehler.com":
					host = "https://dafe2ci.da.doehler.com:8111";
					break;
				case "dac11ci.da.doehler.com":
					host = "https://dafe3ci.da.doehler.com:8111";
					break;
				case "dac11d4.da.doehler.com":
					host = "https://dafe3ci.da.doehler.com:8111";
					break;
				case "dac11d3.da.doehler.com":
					host = "https://dafe3ci.da.doehler.com:8111";
					break;
				case "dac11d2.da.doehler.com":
					host = "https://dafe3ci.da.doehler.com:8111";
					break;
				case "dac11d1.da.doehler.com":
					host = "https://dafe3ci.da.doehler.com:8111";
					break;
				default:
				}

				link = host + "/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html#zdoehlersempr-display?tag=41&/ArticleMasterPage/" +
					priceReqNo;
				sap.m.URLHelper.redirect(link, true);

			},

			_openCostDrivers: function (line, oEvent, this2) {
				var that = this,
					customer,
					material,
					ship_to,
					dist_chnl,
					sales_org, sendingData = {},
					result;
				var oEvent2 = $.extend(true, {}, oEvent);
				customer = line.PRICE_MASTER.CUSTOMER;
				material = line.PRICE_MASTER.MATERIAL;
				ship_to = line.PRICE_MASTER.SHIP_TO;
				dist_chnl = line.PRICE_MASTER.DISTR_CHAN;
				sales_org = line.PRICE_MASTER.SALESORG;
				sendingData.CUSTOMER = customer;
				sendingData.ARTICLE = material;
				sendingData.SHIP_TO = ship_to;
				sendingData.SALESORG = sales_org;
				sendingData.DISTR_CHAN = dist_chnl;
				var data, params;
				var currency = line.PRICE_MASTER.DCUKYCP;
				params = {
					"HANDLERPARAMS": {
						"FUNC": "GET_COST_DRIVERS"
					},
					"INPUTPARAMS": [sendingData]
				};
				dbcontext.callServer(params, function (oModel) {
					result = oModel.getData().COSTDRIVERS;
					sap.ui.getCore().getModel("globalModel").setProperty("/COST_CURRENCY", currency);
					that._openCostDriver(result, oEvent2, this2, line);
				}, this);
			},

			handleCloseCostDriver: function (oEvent) {
				this._oPopoverCostDriver.close();
			},

			_openCostDriver: function (result, oEvent, this2, line) {

				var oControl = oEvent.getSource();
				var that = this;

				if (!this2._oPopoverCostDriver) {
					Fragment.load({
						id: this2.getView().getId(),
						name: "com.doehler.PricingMasterFile.fragments.CostDriver",
						controller: this2
					}).then(function (oPopover) {
						this2._oPopoverCostDriver = oPopover;
						this2.getView().addDependent(this2._oPopoverCostDriver);
						that._getCostDriver(result, oControl, this2, line);
						this2._oPopoverCostDriver.openBy(oControl);
					}.bind(this2));
				} else {
					that._getCostDriver(result, oControl, this2, line);
					that._oPopoverCostDriver.openBy(oControl);
				}
			},

			_getCostDriver: function (costDrivers, oControl, this2, line) {
				var oModel = new JSONModel();

				// JSON sample data
				var oData = {
					modelData: [{
						CDT: costDrivers["ZEXST_TOP3_CD_T1"],
						CDV: costDrivers["ZEXST_TOP3_CD_V1"],
						CET: costDrivers["ZEXST_TOP3_CE_T1"],
						CEV: costDrivers["ZEXST_TOP3_CE_V1"] / line.PRICE_MASTER.DRATE3
					}, {
						CDT: costDrivers["ZEXST_TOP3_CD_T2"],
						CDV: costDrivers["ZEXST_TOP3_CD_V2"],
						CET: costDrivers["ZEXST_TOP3_CE_T2"],
						CEV: costDrivers["ZEXST_TOP3_CE_V2"] / line.PRICE_MASTER.DRATE3
					}, {
						CDT: costDrivers["ZEXST_TOP3_CD_T3"],
						CDV: costDrivers["ZEXST_TOP3_CD_V3"],
						CET: costDrivers["ZEXST_TOP3_CE_T3"],
						CEV: costDrivers["ZEXST_TOP3_CE_V3"] / line.PRICE_MASTER.DRATE3
					}]
				};

				// set the data for the model
				oModel.setData(oData);
				var tableCost = sap.ui.getCore().byId("tableCostDriver");
				tableCost = this2.getView().byId("tableCostDriver");
				// set the model to the core
				tableCost.setModel(oModel);

				this2.getView().byId("multiHeaderCostDriver").setHeaderSpan([2, 2, 2]);
				// sap.ui.getCore().byId("multiheader2").setHeaderSpan([2, 2, 2]);
			},

			onGoQuotation: function (oEvent) {

				/* Quotation 0020102655 is created.
				 * https://dap11ci.da.doehler.com:8111/sap/bc/gui/sap/its/webgui?&~transaction=VA22+vbak-vbeln=20089342;&~OKCODE=ENT2#
				 * */
				var quotationNumber = oEvent.getSource().getText();
				var printOutSelected = false;
				var that = this;
				var dialogQuo = new Dialog({
					state: 'Information',
					title: 'Quotation',
					type: that.getOwnerComponent().getModel("i18n").getProperty("DLG_T_Message"),
					content: [
						new sap.m.ObjectIdentifier({
							title: "Quotation " + quotationNumber + " is created."
						}),
						new sap.m.Text(),
						new sap.m.RadioButtonGroup({
							columns: 1,
							selectedIndex: 0,
							buttons: [
								new RadioButton({
									//visible:false,    									
									id: 'printOutQuotationId',
									groupName: 'GroupA',
									text: that.getOwnerComponent().getModel("i18n").getProperty("quotationPrintOut"),
									selected: true
								}),
								new RadioButton({
									id: 'changeQuotationId',
									groupName: 'GroupA',
									text: that.getOwnerComponent().getModel("i18n").getProperty("quotationChangeQuotation"),
									selected: false
								})
							]
						})

					],

					beginButton: new Button({
						type: sap.m.ButtonType.Emphasized,
						text: that.getOwnerComponent().getModel("i18n").getProperty("DLG_T_OK"),
						press: function (oEvent) {
							printOutSelected = sap.ui.getCore().byId("printOutQuotationId").getSelected();
							dialogQuo.close();
							if (printOutSelected) {
								that._openQuotationInPDF(quotationNumber);
							} else {
								that._runLinkForQuo(quotationNumber);
							}

						}
					}),
					endButton: new Button({
						type: sap.m.ButtonType.Reject,
						text: that.getOwnerComponent().getModel("i18n").getProperty("DLG_T_Cancel"),
						press: function () {
							dialogQuo.close();
						}
					}),
					afterClose: function () {
						dialogQuo.destroy();
					}
				});

				dialogQuo.open();

			},

			_openQuotationInPDF: function (quotationNumber) {
				var that = this,
					oSample1Model = null,
					sSource,
					link = "";
				var sServiceUrl = this.getOwnerComponent().getModel("Pricing").sServiceUrl;

				that._pdfViewer = new PDFViewer({
					sourceValidationFailed: function (e) {
						e.preventDefault();
					},
					error: function (e) {
						e.preventDefault();
					}

				});
				that.getView().addDependent(that._pdfViewer);
				var url = sServiceUrl + "/pdfLinkSet(quotation='" + quotationNumber + "')/$value";
				that._pdfViewer.setSource(url);
				that._pdfViewer.open();

				/*	var params = {
						"HANDLERPARAMS": {
							"FUNC": "QUOTATION_PRINT"
						},
						"INPUTPARAMS": [{
							"VBELN": quotationNumber,
							"LINK": ""
						}]
					};

					dbcontext.callServer(params, function (oModel1) {
						link = oModel1.getData().LINK;

						var newLink = "",
							hostPort = sap.ui.getCore().getModel("globalModel").getProperty("/HOST_PORT");

						hostPort = hostPort.split("/sap/bc")[0];
						link = hostPort + "/sap/public/" + link.split("/sap/public/")[1];

						sSource = link;
						that._pdfViewer = new PDFViewer({
							sourceValidationFailed: function (e) {
								e.preventDefault();
							},
							error: function (e) {
								e.preventDefault();
							}

						});
						that.getView().addDependent(that._pdfViewer);
						that._pdfViewer.setSource(sSource);
						that._pdfViewer.open();
					}, this);*/
			},

			_runLinkForQuo: function (quotationNumber) {
				var quotationNumber = quotationNumber,
					gModel = sap.ui.getCore().getModel("globalModel"),
					link = gModel.getProperty("/tcodeHostName"),
					uiRole = gModel.getProperty("/UIROLE"),
					switchV = gModel.getProperty("/SWITCH"),
					tCode = uiRole === "SALESM" && switchV === false ? "VA22" : "VA23";

				link = link + "&~transaction=" + tCode + "+vbak-vbeln=" + quotationNumber + ";&~OKCODE=ENT2#";
				sap.m.URLHelper.redirect(link, true);
			},

			_getSendingDataForQuotation: function (table, dataParams) {
				var aSelIndex, aIndices, newSelIndex = [],
					isAppNeededInitial = false,
					isChanged = false,
					row, path, context,
					filteredCustomer = [],
					createPriceReqItems = [],
					rows = [],
					data = [],
					customerNumber,
					modelName = "";
				if (table !== null) {
					modelName = table.getBindingInfo("rows").model;
					aSelIndex = table.getSelectedIndices();
					if (!aSelIndex.length) {
						sap.m.MessageToast.show("Select at least one record");
						return;
					} else {
						aIndices = table.getBinding().aIndices;
						aSelIndex.forEach(function (selIndex) {
							newSelIndex.push(aIndices[selIndex]);
						});
						aSelIndex = newSelIndex;
						aSelIndex.forEach(function (index, ind) {
							//context = table.getContextByIndex(ind);
							//path = context.getPath();
							//row = context.getProperty(path);
							row = table.getBinding().oList[index];
							rows.push(row);
							createPriceReqItems.push({
								Customer: row.PRICE_MASTER.CUSTOMER,
								ArticleNumber: row.PRICE_MASTER.MATERIAL,
								Title: "PMT – Price Renewal",
								ZQUANTITY2: row.PRICE_MASTER.DUNITQ
							});
						});

						if (createPriceReqItems.length > 1) {
							customerNumber = createPriceReqItems[0].Customer;
							filteredCustomer = createPriceReqItems.filter(function (line) {
								return line.Customer === customerNumber;
							});
							if (filteredCustomer.length < createPriceReqItems.length) {
								MessageBox.warning("Please select only one customer to send for quotation");
								return;
							}
						}
					}
				} else {
					rows.push(dataParams);
				}

				var savedData = [];
				if (modelName === "tenderM")
					savedData = sap.ui.getCore().getModel("globalModel").getProperty("/savedTender");
				if (modelName === "psresultM")
					savedData = sap.ui.getCore().getModel("globalModel").getProperty("/savedPriceSetting");

				rows.forEach(function (line) {
					savedData.push(line);
					if (line.NEW_PRICE.APP_NEEDED === "") isAppNeededInitial = true;
					if (line.PRICE_MASTER.CHANGED === true) isChanged = true;

					// line.PRICE_MASTER.CHANGED = true;
					data.push({
						CUSTOMER: line.PRICE_MASTER.CUSTOMER,
						SHIP_TO: line.PRICE_MASTER.SHIP_TO,
						VKORG: line.PRICE_MASTER.SALESORG,
						VTWEG: line.PRICE_MASTER.DISTR_CHAN,
						MATERIAL: line.PRICE_MASTER.MATERIAL,
						PRICE_VALID_FROM: line.NEW_PRICE.PRICE_VALID_FROM,
						PRICE_VALID_TO: line.NEW_PRICE.PRICE_VALID_TO
					});
				});
				if (modelName === "tenderM")
					sap.ui.getCore().getModel("globalModel").setProperty("/savedTender", savedData);
				if (modelName === "psresultM")
					sap.ui.getCore().getModel("globalModel").setProperty("/savedPriceSetting", savedData);

				return {
					data: data,
					isAppNeededInitial: isAppNeededInitial,
					isChanged: isChanged
				};

			},

			_getSendingDataForApproval: function (table, dataParams) {
				var userId = sap.ushell.Container.getUser().getId(),
					aSelIndex, aIndices, newSelIndex = [],
					isAppNeededInitial = false,
					isInitialWorkflow = true,
					isChanged = false,
					row, path, context,
					filteredCustomer = [],
					createPriceReqItems = [],
					rows = [],
					data = [],
					dataForNotApproval = [],
					dataForDacman = [],
					customerNumber,
					newLogic = false;

				if (userId === "EX_DOGUS" || userId === "PAULA" || userId === "EX_SAYDAMZ" || userId === "EX_UZUNB" || userId ===
					"GRAULIM" ||
					userId === "BOCKS" || userId === "FISCHEA" || userId === "MATTHEC") newLogic = true;

				if (table !== null) {
					aSelIndex = table.getSelectedIndices();
					if (!aSelIndex.length) {
						sap.m.MessageToast.show("Select at least one record");
						return;
					} else {
						aIndices = table.getBinding().aIndices;
						aSelIndex.forEach(function (selIndex) {
							newSelIndex.push(aIndices[selIndex]);
						});
						aSelIndex = newSelIndex;
						aSelIndex.forEach(function (index, ind) {
							//context = table.getContextByIndex(ind);
							//path = context.getPath();
							//row = context.getProperty(path);
							row = table.getBinding().oList[index];
							rows.push(row);
							createPriceReqItems.push({
								Customer: row.PRICE_MASTER.CUSTOMER,
								ArticleNumber: row.PRICE_MASTER.MATERIAL,
								Title: "Pricing Master File - Price Renewal",
								ZQUANTITY2: row.PRICE_MASTER.DUNITQ
							});
						});

						if (createPriceReqItems.length > 1) {
							customerNumber = createPriceReqItems[0].Customer;
							filteredCustomer = createPriceReqItems.filter(function (line) {
								return line.Customer === customerNumber;
							});
							// if (filteredCustomer.length < createPriceReqItems.length) {
							// 	MessageBox.warning("Please select only one customer to send for approval");
							// 	return;
							// }
						}
					}
				} else {
					rows.push(dataParams);
				}

				rows.forEach(function (line) {
					if (newLogic) {
						if (line.NEW_PRICE.APP_NEEDED === "" || (line.NEW_PRICE.APP_NEEDED === "X" && (line.NEW_PRICE.THRESHOLD_CLASS === "4" ||
								line
								.NEW_PRICE
								.THRESHOLD_CLASS === "5"))) {
							if (line.NEW_PRICE.APP_NEEDED === "") isAppNeededInitial = true;
							if (line.PRICE_MASTER.CHANGED === true) isChanged = true;
							data.push({
								CUSTOMER: line.PRICE_MASTER.CUSTOMER,
								SHIP_TO: line.PRICE_MASTER.SHIP_TO,
								VKORG: line.PRICE_MASTER.SALESORG,
								VTWEG: line.PRICE_MASTER.DISTR_CHAN,
								MATERIAL: line.PRICE_MASTER.MATERIAL,
								APP_NEEDED_DESC: ""
							});
						} else {
							dataForNotApproval.push(line);
							if (line.NEW_PRICE.WORKLOW_STATUS !== "")
								isInitialWorkflow = false;
							dataForDacman.push({
								CUSTOMER: line.PRICE_MASTER.CUSTOMER,
								SHIP_TO: line.PRICE_MASTER.SHIP_TO,
								VKORG: line.PRICE_MASTER.SALESORG,
								VTWEG: line.PRICE_MASTER.DISTR_CHAN,
								MATERIAL: line.PRICE_MASTER.MATERIAL,
								C_DACCMAN: line.PRICE_MASTER.C_DACCMAN
							});
						}
					} else {
						if (line.NEW_PRICE.APP_NEEDED === "") isAppNeededInitial = true;
						if (line.PRICE_MASTER.CHANGED === true) isChanged = true;
						data.push({
							CUSTOMER: line.PRICE_MASTER.CUSTOMER,
							SHIP_TO: line.PRICE_MASTER.SHIP_TO,
							VKORG: line.PRICE_MASTER.SALESORG,
							VTWEG: line.PRICE_MASTER.DISTR_CHAN,
							MATERIAL: line.PRICE_MASTER.MATERIAL,
							APP_NEEDED_DESC: ""
						});
					}

				});

				return {
					data: data,
					dataForNotApproval: dataForNotApproval,
					dataForDacman: dataForDacman,
					isAppNeededInitial: isAppNeededInitial,
					isInitialWorkflow: isInitialWorkflow,
					isChanged: isChanged
				};

			},

			_createMailDialogForApproval: function (this2, data, id) {
				var footer = this2.getView().byId(id);
				var that = this;
				//if (!this2._oValueHelpDialogMail) {
				Fragment.load({
					name: "com.doehler.PricingMasterFile.fragments.Recipients",
					controller: this2
				}).then(function (oValueHelpDialog) {
					this2._oValueHelpDialogMail = oValueHelpDialog;
					this2.getView().addDependent(this2._oValueHelpDialogMail);
					this2._configValueHelpDialog(this2, data);
					that._oValueHelpDialogMail.openBy(footer);
				}.bind(this2));
				//	} else {
				//		that._configValueHelpDialog(this2, data);
				//		this2._oValueHelpDialogMail.openBy(footer);
				//	}
			},

			handleCloseButtonRecipients: function (oEvent) {
				this._oValueHelpDialogMail.close();
			},

			handleSendEmailToRecipient: function (oEvent) {
				var data = sap.ui.getCore().getModel("recipientM").getData().recipientsCollection,
					sendingData = [],
					that = this,
					params,
					line = {
						PRICE_MASTER: [],
						NEW_PRICE: []
					};
				data.forEach(function (itm) {
					line.PRICE_MASTER.push(itm.PRICE_MASTER);
					line.NEW_PRICE.push(itm.NEW_PRICE);
					if (itm.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST !== undefined)
						itm.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST = parseFloat(itm.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST.toFixed(2));
					else
						itm.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST = 0;

					if (itm.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST !== undefined)
						itm.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = parseFloat(itm.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST.toFixed(2));
					else
						itm.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = 0;

					if (itm.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST !== undefined)
						itm.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = parseFloat(itm.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST.toFixed(2));
					else
						itm.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = 0;

					line.GM_EUR_KG_NEW_PRICE_HIST = itm.PRICE_MASTER.GM_EUR_KG_NEW_PRICE_HIST;
					line.PERC_CM1_EUR_KG_NEW_PRICE_HIST = itm.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST;
					line.CM1_EUR_KG_NEW_PRICE_HIST = itm.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST;
					line.USRID = itm.PRICE_MASTER.RECIPIENT;
					sendingData.push(line);
				});

				params = {
					"HANDLERPARAMS": {
						"FUNC": "REQUEST_APPROVAL"
					},
					"INPUTPARAMS": sendingData
				};
				dbcontext.callServer(params, function (oModel) {
					that._oValueHelpDialogMail.close();
				}, this);

			},

			_configValueHelpDialog: function (this2, data) {

				this2._recipientM.setProperty("/recipientsCollection", data);
			},

			//navigation back
			onNavBack: function () {
				var oHistory, sPreviousHash;

				oHistory = History.getInstance();
				sPreviousHash = oHistory.getPreviousHash();

				if (sPreviousHash !== undefined) {
					window.history.go(-1);
				} else {
					this.getRouter().navTo("Main", {}, true /*no history*/ );
				}
			},
			getRouter: function () {
				return this.getOwnerComponent().getRouter();
			},
			/* Function Required for Custom F4 */
			onValueHelpInit: function (oEvent) {
				oEvent.getSource().setController(this);
				oEvent.getSource().setDataFunction(dbcontext.getValueHelpData.bind(dbcontext));
			},

			/* End of Function Required for Custom F4 */
			handleServerMessages: function (oModel, fnOnClose, showSuccessMessage) {
				ErrorHandler.handleMessages(this, oModel, fnOnClose, false, showSuccessMessage);
			},
			getText: function (key, args) {
				return sap.ui.getCore().getModel("i18n").getResourceBundle().getText(key, args);
			},

			//general Navigation
			onNavToPages: function (oEvent) {
				var oSource = oEvent.getSource(),
					oRouter = this.getRouter(),
					controlId = oSource.getId().split("--")[1];

				oSource.setPressed(true);

				switch (controlId) {
				case "psiBtnId":
					oRouter.navTo("Main");
					break;
				case "bumBtnId":
					oRouter.navTo("BuMeasures", {
						requestId: "BU"
					});
					break;
				case "pricmBtnId":
					oRouter.navTo("PriceMeasures", {
						requestId: "PM"
					});
					break;
					// replaced onNavToRedPosition	
					// case "redCusBtnId":
					// 	oRouter.navTo("RedPosition");
					// 	break;
				case "redArtBtnId":
					oRouter.navTo("RedCustomer");
					break;
				case "ppaBtnId":
					oRouter.navTo("PerformanceReview");
					break;
				case "kpisBtnId":
					oRouter.navTo("Kpis");
					break;
				default:
				}
			},

			onNavToRedPosition: function (oEvent) {

				window.open(
					"https://my336291-sso.crm.ondemand.com/sap/ap/ui/repository/SAP_UI/HTML5/newclient.html?app.component=/SAP_UI_CT/Main/root.uiccwoc&rootWindow=X&redirectUrl=/sap/byd/runtime&sap-ui-language=en_us#Nav/1/eyJiSXNTaG93bkFzV29jVmlldyI6dHJ1ZSwidGFyZ2V0IjoiL1NBUF9CWURfVUlfRkxFWC9XQ1ZJRVcvUkVEIFBPU0lUSU9OUy9GTEVYX1dDX1dDVl9UTVBMX1dDVklFVy5XQ1ZJRVcudWl3b2N2aWV3Iiwid2luSWQiOiI0YWZiYmZhYzIyYzE3NjIwNzZmYWY3ZmY3ODViNDBlNCJ9",
					'_blank').focus();

			},

			onNavToArticle: function (oEvent) {
				var oSource = oEvent.getSource(),
					oRouter = this.getRouter(),
					customData = oEvent.getSource().getCustomData()[0].getValue(),
					modelName = customData === "PSI" ? "psresultM" : customData === "PSTender" ? "tenderM" : customData === "pM" ? "pMresultM" :
					"buMresultM",
					oPath = oSource.getBindingContext(modelName).getPath(),
					index = oPath.split("/")[1],
					gModel = sap.ui.getCore().getModel("globalModel"),
					line = this.getView().getModel(modelName).getData()[index].PRICE_MASTER,
					customer = line.CUSTOMER,
					material = line.MATERIAL,
					ship_to = line.SHIP_TO,
					dist_chnl = line.DISTR_CHAN,
					sales_org = line.SALESORG;

				gModel.setProperty("/fromPage", customData === "PSI" ? "Main" : "Measures");
				gModel.setProperty("/toPage", "PriceSetting");
				oRouter.navTo("PriceSetting", {
					customer: customer,
					ship_to: ship_to,
					sales_org: sales_org,
					dist_chnl: dist_chnl,
					material: material,
					Page: customData,
					index: index
				});
				// PriceSetting({customer})({ship_to})({sales_org})({dist_chnl})({material})
			},

			_setVisibilityOfArrows: function (left, right) {
				this.getView().byId("leftId").setEnabled(left);
				this.getView().byId("rightId").setEnabled(right);
			},

			//Arrange pressed property of the main buttons
			_setButtonsPressed: function (globalM, id) {
				var oView = this.getView(),
					names = globalM.getData()["headerBtnNames"];

				names.forEach(function (btnName) {
					oView.byId(btnName).setPressed(false);
				});
				oView.byId(id).setPressed(true);
			},

			//downloading of excel

			/**********************************************EXCEL START*******************************************/

			_downloadExcelNew: function (name, table, tableData) {
				var aCols;
				aCols = tableData;
				excel._downloadExcelNew("PSI", this._table, aCols);
			},

			_downloadExcel: function (name, table, tableData) {
				var aCols, oSettings, oSheet,
					dial = new sap.m.BusyDialog();
				aCols = this.createColumnConfig(table, tableData);
				oSettings = {
					workbook: {
						columns: aCols.aCols,
						context: {
							title: 'Price Setting Information',
						}
					},
					dataSource: aCols.copiedData,
					fileName: name === "PSI" ? "priceSettingInformation.xlsx" : name === "BUM" ? "buMeasure.xlsx" : name === "PSM" ?
						"pricingMeasures.xslx" : "tenderView.xslx"
				};

				oSheet = new Spreadsheet(oSettings);
				oSheet.build()
					.then(function () {
						MessageToast.show('Spreadsheet export has finished');
					})
					.finally(oSheet.destroy);
			},

			_prepareRows: function (columnText, property, type, aCols) {
				var gModel = sap.ui.getCore().getModel("globalModel");
				var role = gModel.getProperty("/UIROLE");

				switch (type) {
				case "String":

					if (property === "PRICE_MASTER/DPRSCM1PERC") {
						aCols.push({
							property: "PRICE_MASTER/DPRSCM1PERC_EXCEL",
							label: columnText + "(% CM1/ytd)",
							width: 20,
							type: EdmType.String,
							wrap: true,
							textAlign: "Center"
						});
						aCols.push({
							property: "PRICE_MASTER/DPRSTMPRO",
							label: columnText + "% target",
							width: 20,
							type: EdmType.String,
							wrap: true,
							textAlign: "Center"
						});
						aCols.push({
							property: "PRICE_MASTER/gapToTargetPercDetail_excel",
							label: columnText + "(% delta)",
							width: 20,
							type: EdmType.String,
							wrap: true,
							textAlign: "Center"
						});
					} else if (property === "PRICE_MASTER/DPRSVFC") {
						aCols.push({
							label: columnText,
							property: property,
							type: EdmType.String,
							width: 20,
							wrap: true,
							textAlign: "Center"
						});
						aCols.push({
							label: "Unit",
							property: "PRICE_MASTER/UNIT",
							type: EdmType.String,
							width: 20,
							wrap: true,
							textAlign: "Center"
						});

					} else if (property === "PRICE_MASTER/DPRSCPAIN") {
						aCols.push({
							property: "PRICE_MASTER/DPRSCPAIN",
							label: columnText + "(curr. price)",
							width: 20,
							type: EdmType.String,
							wrap: true,
							textAlign: "Center"
						});

						aCols.push({
							property: "PRICE_MASTER/PRICE3",
							label: columnText + "(target)",
							width: 20,
							type: EdmType.String,
							wrap: true,
							textAlign: "Center"
						});
						aCols.push({
							label: "Unit",
							property: ['PRICE_MASTER/DCUKYCP', 'PRICE_MASTER/DCONDUNIT', 'PRICE_MASTER/DOEKMEIN2'],
							type: EdmType.String,
							width: 20,
							wrap: true,
							textAlign: "Center",
							template: '{0} /{1} {2}'
						});

					} else if (property === "NEW_PRICE/NEW_PRICE_PLAN" || property === "NEW_PRICE/NEW_PRICE_NET_EXW") {
						aCols.push({
							property: property,
							label: columnText,
							width: 20,
							type: EdmType.String,
							wrap: true,
							textAlign: "Center"
						});
						aCols.push({
							label: "Unit",
							property: ['PRICE_MASTER/DCUKYCP', 'PRICE_MASTER/DCONDUNIT', 'PRICE_MASTER/DOEKMEIN2'],
							type: EdmType.String,
							width: 20,
							wrap: true,
							textAlign: "Center",
							template: '{0} /{1} {2}'
						});
					} else {
						aCols.push({
							label: columnText,
							property: property,
							type: EdmType.String,
							width: 20,
							wrap: true,
							textAlign: "Center"
						});
					}

					break;
				case "Date":
					//Price Validty
					if (property === "PRICE_MASTER/QUOT_FROM") {
						aCols.push({
							label: columnText + " From",
							type: EdmType.Date,
							property: "PRICE_MASTER/QUOT_FROM",
							wrap: true,
							textAlign: "Center",
							inputFormat: 'yyyymmdd',
							width: 25
						});
						aCols.push({
							label: columnText + " To",
							type: EdmType.Date,
							property: "PRICE_MASTER/QUOT_TO",
							wrap: true,
							textAlign: "Center",
							inputFormat: 'yyyymmdd',
							width: 25
						});
					} else {
						aCols.push({
							label: columnText,
							type: EdmType.Date,
							property: property,
							width: 25,
							wrap: true,
							textAlign: "Center",
							inputFormat: 'yyyymmdd'
						});
					}
					break;

				case "Enumeration":
					aCols.push({
						label: columnText,
						type: EdmType.Enumeration,
						property: property,
						valueMap: property === "NEW_PRICE/PRICE_ACTION" ? {
							'01': 'Price Campaign Q4 2020',
							'02': 'Price Campaign – Mango20'
						} : property === "NEW_PRICE/ROOT_CAUSE" ? {
							'01': "Overall customer profitable",
							'02': "SC cost issue",
							'03': "Raw material increase",
							'04': "Captive use",
							'05': "Price too low",
							'06': "Lifecycle stage"
						} : property === "NEW_PRICE/BU_MEASURE_STATUS" ? {
							"1": "Open",
							"2": "In Progress",
							"3": "Closed"
						} : property === "NEW_PRICE/PRICING_STATUS" ? {
							"1": "Open",
							"2": "In Progress",
							"3": "Closed"
						} : property === "NEW_PRICE/WORKLOW_STATUS" ? {
							"1": "In Progress",
							"2": "Approved",
							"3": "Rejected"
						} : {
							'01': 'Recipe',
							'02': 'Swap/Stop',
							'03': 'Production',
							'04': 'Price'
						},
						width: 15,
						textAlign: 'center'
					});
					break;

				default:
				}
				return aCols;
			},

			createColumnConfig: function (oTable, tableData) {
				var that = this,
					aCols = [],
					aColumns = oTable.getColumns(),
					columnText = "",
					type = "",
					property = "",
					buActionList = sap.ui.getCore().getModel("buActionDD"),
					copiedData = $.extend(true, [], tableData);

				copiedData.forEach(function (line) {
					var val = that.getFormattedValue(line.NEW_PRICE.L_ACC_PRICE_INC,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.L_ACC_PRICE_INC = val;

					//cm1

					val = that.getFormattedValue(line.PRICE_MASTER.DPRSCM1PERC,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);

					line.PRICE_MASTER.DPRSCM1PERC_EXCEL = line.PRICE_MASTER.NOTVALIDDPRSCM1PERC === "" ? val : line.PRICE_MASTER.NOTVALIDDPRSCM1PERC;

					val = that.getFormattedValue(line.PRICE_MASTER.gapToTargetPercDetail,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);

					line.PRICE_MASTER.gapToTargetPercDetail_excel = line.PRICE_MASTER.NOTVALID_gapToTargetPercDetail === "" ? val : line.PRICE_MASTER
						.NOTVALID_gapToTargetPercDetail;

					val = that.getFormattedValue(line.PRICE_MASTER.DPRSTMPRO,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSTMPRO = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSCM1PERCD,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSCM1PERCD = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSVFC,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:0, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSVFC = val;

					//prices
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSCPAIN,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSCPAIN = val;
					val = that.getFormattedValue(line.PRICE_MASTER.PRICE_INCREASE_IN,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:1, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.PRICE_INCREASE_IN = val;

					val = that.getFormattedValue(line.PRICE_MASTER.PRICE_INCREASE_IN_ABS,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.PRICE_INCREASE_IN_ABS = val;

					val = that.getFormattedValue(line.PRICE_MASTER.PRICE3,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.PRICE3_EXCEL = line.PRICE_MASTER.NOTVALIDPRICE3 === "" ? val : line.PRICE_MASTER
						.NOTVALIDPRICE3;
					//new price
					val = that.getFormattedValue(line.NEW_PRICE.NEW_PRICE_PLAN,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.NEW_PRICE_PLAN = val;
					val = that.getFormattedValue(line.NEW_PRICE.NEW_PRICE_NET_EXW,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.NEW_PRICE_NET_EXW = val;
					val = that.getFormattedValue(line.NEW_PRICE.NEW_PRICE_PLAN_K,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.NEW_PRICE_PLAN_K = val;
					val = that.getFormattedValue(line.NEW_PRICE.CM1_CURR_KG_PLAN,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.CM1_CURR_KG_PLAN = line.PRICE_MASTER.NOTVALIDCM1_CURR_KG_PLAN === "" ? val : line.PRICE_MASTER
						.NOTVALIDCM1_CURR_KG_PLAN;
					val = that.getFormattedValue(line.NEW_PRICE.CM1_PER_PLANNED,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.CM1_PER_PLANNED = line.PRICE_MASTER.NOTVALIDCM1_PER_PLANNED === "" ? val : line.PRICE_MASTER
						.NOTVALIDCM1_PER_PLANNED;

					val = that.getFormattedValue(line.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.PERC_CM1_EUR_KG_NEW_PRICE_HIST = line.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST === "" ? val : line
						.PRICE_MASTER
						.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST;

					val = that.getFormattedValue(line.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.CM1_CURR_KG_NEW_PRICE_HIST = val;
					val = that.getFormattedValue(line.NEW_PRICE.TARGET_VOLUME,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:0, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.TARGET_VOLUME = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSVYTD,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:0, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSVYTD = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSRSGSA,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:0, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSRSGSA = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRRGM,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:0, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRRGM = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSRSCM1,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:0, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSRSCM1 = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSGM,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSGM = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSCM1R,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSCM1R = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DPRSMOQ,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:0, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DPRSMOQ = val;
					val = that.getFormattedValue(line.PRICE_MASTER.DCCHGRCURKG,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.DCCHGRCURKG = val;
					val = that.getFormattedValue(line.PRICE_MASTER.costChangeGross,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.costChangeGross = val;
					val = that.getFormattedValue(line.PRICE_MASTER.planMaterialCostChange,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.planMaterialCostChange = val;
					val = that.getFormattedValue(line.NEW_PRICE.NEW_PRICE_PLAN_E_KG,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.NEW_PRICE.NEW_PRICE_PLAN_E_KG = val;
					val = that.getFormattedValue(line.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST = line.PRICE_MASTER.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST === "" ? val : line.PRICE_MASTER
						.NOT_VALID_CM1_EUR_KG_NEW_PRICE_HIST;
					val = that.getFormattedValue(line.PRICE_MASTER.GAPTOTARGET_PERC,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.GAPTOTARGET_PERC_EXCEL = line.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC === "" ? val : line.PRICE_MASTER.NOTVALIDGAPTOTARGET_PERC;
					val = that.getFormattedValue(line.PRICE_MASTER.GAPTOTARGET_VALUE,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.GAPTOTARGET_VALUE_EXCEL = line.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE === "" ? val : line.PRICE_MASTER.NOTVALIDGAPTOTARGET_VALUE;
					line.PRICE_MASTER.GAPTOTARGET_VALUE = val;
					val = that.getFormattedValue(line.PRICE_MASTER.CURR_PRICE_EUR_KG,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.CURR_PRICE_EUR_KG = val;
					val = that.getFormattedValue(line.PRICE_MASTER.CURR_PRICE_CURR_KG,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.CURR_PRICE_CURR_KG = val;
					val = that.getFormattedValue(line.PRICE_MASTER.MAT_COST_CURR_KG,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.MAT_COST_CURR_KG = val;
					val = that.getFormattedValue(line.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG,
						",type: 'sap.ui.model.type.Float', formatOptions: {decimals:2, groupingEnabled: true, groupingSeparator : '.', decimalSeparator : ','}"
					);
					line.PRICE_MASTER.FREIGHT_PROCESS_COST_CURR_KG = val;
				});

				aColumns.forEach(function (oColumn) {
					if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
						if (oColumn.mSkipPropagation.template) {
							columnText = oColumn.getLabel().getText();
							if (oColumn.getLabel().getCustomData().length) {

								type = oColumn.getLabel().getCustomData()[0].getValue();
								property = oColumn.getLabel().getCustomData()[1].getValue();

								aCols = that._prepareRows(columnText, property, type, aCols);

							}
						}
					}
				});

				return {
					aCols: aCols,
					copiedData: copiedData
				};
			},

			/**********************************************EXCEL END*******************************************/

			prepareExcelData: function (oTable, tableData) {
				var aRow = [],
					aExcelData = [],
					aColumns,
					thiz = this;
				if (oTable) {
					aColumns = oTable.getColumns();
					if (aColumns) {
						aRow = this.prepareColumnsArray(aColumns);
					}
					if (aRow.length) {
						aExcelData.push(aRow);
						tableData.forEach(function (oRecord) {
							aRow = [];
							aRow = thiz.prepareRowsArray(aColumns, oRecord);
							if (aRow.length) {
								aExcelData.push(aRow);
							}
						})
					}
				}
				return aExcelData;
			},

			prepareColumnsArray: function (aColumns) {
				var aRow = [];
				aColumns.forEach(function (oColumn) {
					if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
						if (oColumn.mSkipPropagation.template) {
							var oTemplate = oColumn.getAggregation("template");
							if (oTemplate.mBindingInfos.icon || oTemplate.mBindingInfos.text || oTemplate.mBindingInfos.value || oTemplate.mBindingInfos
								.selectedKey ||
								oTemplate.getMetadata().getName() === "sap.m.VBox") {
								if (oColumn.getAggregation("label").getText()) {
									aRow.push(oColumn.getAggregation("label").getText());
								}
							}
						}
					}
				});
				return aRow;
			},

			prepareRowsArray: function (aColumns, oRecord) {
				var aRow = [],
					thiz = this,
					aParts = [],
					nLength,
					sValue,
					oTemplate,
					i;

				oRecord = Object.assign(oRecord.PRICE_MASTER, oRecord.NEW_PRICE);

				aColumns.forEach(function (oColumn) {
					if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
						if (oColumn.mSkipPropagation.template) {
							oTemplate = oColumn.getAggregation("template");
							nLength = 0;
							sValue = "";
							if (oTemplate.mBindingInfos.icon) {
								if (oTemplate.mBindingInfos.icon.parts.length) {
									aRow.push(oRecord[oTemplate.mBindingInfos.icon.parts[0].path]);
								};
							} else if (oTemplate.mBindingInfos.text || oTemplate.getMetadata().getName() === "sap.m.VBox") {
								if (oTemplate.getMetadata().getName() === "sap.m.VBox") {
									for (i = 0; i < oTemplate.getItems().length; i++) {
										if (oTemplate.getItems()[i].getMetadata().getName() === "sap.m.Text") {
											aParts = oTemplate.getItems()[i].mBindingInfos.text.parts || [];
											nLength = aParts.length;
											if (nLength === 1) {
												if (oRecord[aParts[0].path.split("/")[1]] !== undefined) {
													if (sValue === "") {
														sValue = oRecord[aParts[0].path.split("/")[1]];
													} else {
														sValue = sValue + "\n" + sValue;
													}
												}
											} else if (nLength === 2) {
												if (oRecord[aParts[0].path.split("/")[1]] !== undefined) {
													sValue = oRecord[aParts[0].path.split("/")[1]];
												}
												if (oRecord[aParts[1].path.split("/")[1]] !== undefined) {
													sValue = sValue + " " + oRecord[aParts[1].path.split("/")[1]];
												}
											} else if (nLength === 3) {
												if (oRecord[aParts[0].path.split("/")[1]] !== undefined) {
													sValue = oRecord[aParts[0].path.split("/")[1]];
												}
												if (oRecord[aParts[1].path.split("/")[1]] !== undefined) {
													sValue = sValue + " " + oRecord[aParts[1].path.split("/")[1]];
												}
												if (oRecord[aParts[2].path.split("/")[1]] !== undefined) {
													sValue = sValue + "/" + oRecord[aParts[2].path.split("/")[1]];
												}
											} else {
												sValue = ""
												aParts.forEach(function (oPart) {
													if (oRecord[oPart.path.split("/")[1]] !== undefined) {
														sValue = sValue + " " + oPart.path.split("/")[1];
													}
												});
											}

										}
									}
									aRow.push(sValue);
									aParts = [];
								} else {
									aParts = oTemplate.mBindingInfos.text.parts || [];
								}

								nLength = aParts.length;

								if (nLength) {
									if (nLength === 1) {
										if (oRecord[aParts[0].path.split("/")[1]] !== undefined) {
											sValue = oRecord[aParts[0].path.split("/")[1]];
										}
									} else if (nLength === 2) {
										if (oRecord[aParts[0].path.split("/")[1]] !== undefined) {
											sValue = oRecord[aParts[0].path.split("/")[1]];
										}
										if (oRecord[aParts[1].path.split("/")[1]] !== undefined) {
											sValue = sValue + " " + oRecord[aParts[1].path.split("/")[1]];
										}
									} else if (nLength === 3) {
										if (oRecord[aParts[0].path.split("/")[1]] !== undefined) {
											sValue = oRecord[aParts[0].path.split("/")[1]];
										}
										if (oRecord[aParts[1].path.split("/")[1]] !== undefined) {
											sValue = sValue + " " + oRecord[aParts[1].path.split("/")[1]];
										}
										if (oRecord[aParts[2].path.split("/")[1]] !== undefined) {
											sValue = sValue + "/" + oRecord[aParts[2].path.split("/")[1]];
										}
									} else {
										sValue = ""
										aParts.forEach(function (oPart) {
											if (oRecord[oPart.path.split("/")[1]] !== undefined) {
												sValue = sValue + " " + oPart.path.split("/")[1];
											}
										});
									}
									aRow.push(sValue);
								}
							}
						}
					}
				});

				return aRow;
			},

			dataConversion: function (sData) {
				if (typeof ArrayBuffer !== "undefined") {
					let oBuf = new ArrayBuffer(sData.length);
					let aView = new Uint8Array(oBuf);
					for (let i = 0; i !== sData.length; ++i) {
						aView[i] = sData.charCodeAt(i) & 0xFF;
					}
					return oBuf;
				} else {
					let oBuf = new Array(sData.length);
					for (let i = 0; i !== sData.length; ++i) {
						oBuf[i] = sData.charCodeAt(i) & 0xFF;
					}
					return oBuf;
				}
			},

			downloadBlob: function (sFileName, oBlob) {
				if (typeof window.navigator.msSaveBlob !== "undefined") {
					window.navigator.msSaveBlob(oBlob, sFileName);
				} else {
					const oURL = window.URL || window.webkitURL;
					const oTempURL = oURL.createObjectURL(oBlob);

					let oLinkObject = document.createElement("a");
					oLinkObject.style = "display: none";

					if (typeof oLinkObject.download !== "undefined") {
						oLinkObject.href = oTempURL;
						oLinkObject.download = sFileName;
						document.body.appendChild(oLinkObject);
						oLinkObject.click();
					} else {
						window.open(oTempURL, "_blank");
					}
				}
			},

			_viewUpdate: function (id) {
				this.getView().byId(id).setVisible(false);
				this.getView().byId(id).setVisible(true);
			},

			/****************************VARIANT MANAGEMENT************************/
			/* get default perso data of table */
			getDefaultPersoData: function (oTable, callback) {
				var data = null;
				var oPersonalizer = {
					getPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					},
					setPersData: function (oBundle) {
						var oDeferred = jQuery.Deferred();
						data = oBundle;
						oDeferred.resolve();
						return oDeferred.promise();
					},
					delPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					}
				};
				var oTPC = new TablePersoController({
					table: oTable,
					persoService: oPersonalizer
				});
				oTPC.savePersonalizations().done(function () {
					callback(data);
				}).fail(function (err) {});
			},
			/* set perso data to table */
			setPersoData: function (oTable, data, callback) {
				var viewId = this.getView().getId();
				if (data.hasOwnProperty("aColumns")) {
					data.aColumns.forEach(function (item) {
						item.id = item.id.replace(/__xmlview[0-9]+/g, viewId);
					});
				}
				var oPersonalizer = {
					getPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve(data);
						return oDeferred.promise();
					},
					setPersData: function (oBundle) {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					},
					delPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					}
				}
				var oTPC = new TablePersoController({
					table: oTable,
					persoService: oPersonalizer,
					autoSave: false
				});
				oTPC.savePersonalizations().done(function () {
					if (callback) {
						callback();
						oTPC.refresh();
					}
				}).fail(function (err) {
					console.log(err);
				});
			},
			// // set standard variant with data
			setStandardVariant: function (tableId, callback) {
				var thiz = this;
				var oTable = this.getView().byId(tableId);
				sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").then(function (oCC) {
					thiz.oCC = oCC; //oContextContainer
					// 1 check if item is available or not if not then create
					if (!oCC.containsItem(tableId)) {
						oCC.setItemValue(tableId, {
							items: []
						});
					}
					// 2 check if standard is available or not if not then create
					var ovar = oCC.getItemValue(tableId);
					//if (!ovar.hasOwnProperty("*standard*")) {
					thiz.getDefaultPersoData(oTable, function (defaultSet) {
						ovar["*standard*"] = {
							key: "*standard*",
							text: "Standard",
							data: defaultSet
						}; //set default data
						if (!ovar.hasOwnProperty("defaultKey")) {
							ovar["defaultKey"] = "*standard*";
						}
						// quotation new layout
						var oModel = new sap.ui.model.json.JSONModel();
						if (tableId === "priceSettingInfromationTableId") {
							oModel.loadData(jQuery.sap.getModulePath("com.doehler.PricingMasterFile", "/model/settings.json"), "", false);
							var oBom = oModel.getProperty("/psresultM");
							for (var lay in oBom) {
								ovar[lay] = {
									key: lay,
									text: oBom[lay].text,
									data: oBom[lay].settings
								}
								ovar.items = ovar.items.filter(function (key) {
									return key.key !== "layout2"
								});
								ovar.items.push({
									key: lay,
									text: oBom[lay].text
								});
							}
							oModel.loadData(jQuery.sap.getModulePath("com.doehler.PricingMasterFile", "/model/settingsCampaign.json"), "", false);
							var oCampaign = oModel.getProperty("/psresultCampaignM");
							var notVisible = [];
							oTable.getColumns().forEach(function (clmn) {
								var line = oCampaign.layout4.settings.aColumns.filter(function (aCol) {
									return clmn.getId().split("--")[1] === aCol.id.split("priceSettingInfromationTableId")[1].split("--")[1];
								});

								if (line.length === 0) {
									notVisible.push({
										"id": "__xmlview2--priceSettingInfromationTableId-" + clmn.getId(),
										"order": 10000,
										"visible": false,
										"sorted": false,
										"sortOrder": "Ascending",
										"grouped": false,
										"name": "-"
									});
								}

							});
							if (notVisible.length > 0)
								oCampaign.layout4.settings.aColumns = oCampaign.layout4.settings.aColumns.concat(notVisible);
							for (var lay2 in oCampaign) {
								ovar[lay2] = {
									key: lay2,
									text: oCampaign[lay2].text,
									data: oCampaign[lay2].settings
								}
								ovar.items = ovar.items.filter(function (key) {
									return key.key !== "layout3" && key.key !== "layout4";
								});
								ovar.items.push({
									key: lay2,
									text: oCampaign[lay2].text
								});
							}
						}

						// list view new layout
						if (tableId === "listTableId") {
							oModel.loadData(jQuery.sap.getModulePath("com.doehler.PricingMasterFile", "/model/settingsList.json"), "", false);
							var oBom = oModel.getProperty("/listM");
							for (var lay in oBom) {
								ovar[lay] = {
									key: lay,
									text: oBom[lay].text,
									data: oBom[lay].settings
								}
								ovar.items = ovar.items.filter(function (key) {
									return key.key !== "layout2"
								});
								ovar.items.push({
									key: lay,
									text: oBom[lay].text
								});
							}
						}
						oCC.setItemValue(tableId, ovar);
						oCC.save().done(function () {
							if (callback) {
								callback(oCC, defaultSet);
							}
						}).fail(function (err) {
							console.log(err);
						});
					});
				});
			},

			/* Get Perso Service with new data it is use to set new layout to table */
			getPersoService: function (oTable, callback) {
				var oPersonalizer = {
					getPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					},
					setPersData: function (oBundle) {
						var oDeferred = jQuery.Deferred();
						if (callback) {
							callback(oBundle);
						}
						oDeferred.resolve();
						return oDeferred.promise();
					},
					delPersData: function () {
						var oDeferred = jQuery.Deferred();
						oDeferred.resolve();
						return oDeferred.promise();
					}
				}
				var oTPC = new TablePersoController({
					table: oTable,
					persoService: oPersonalizer
				});
				return oTPC;
			},

			getFormattedValue: function (value, type) {
				var oModel = new sap.ui.model.json.JSONModel({
					val: value
				});
				var oText = new sap.m.Text({
					text: "{path:'/val' " + type + "}",
					visible: false
				});
				oText.setModel(oModel);
				this.getView().addDependent(oText);
				var text = oText.getText();
				oText.destroy();
				oModel.destroy();
				return text;
			},
			//price valid from and price valid to empty button (current aggrement)
			_onPriceValidFromTo: function (row) {
				var dprsvfc_val, key,
					dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
						pattern: "yyyy-MM-dd"
					}),
					from,
					to,
					timeDiff,
					diffDays,
					dateFormattedFrom,
					dateFormattedTo,
					priceValidFrom = "",
					priceValidTo = "",
					quotFrom = "",
					quotTo = "",
					today,
					date;

				quotFrom = row.PRICE_MASTER.QUOT_FROM;
				quotTo = row.PRICE_MASTER.QUOT_TO;

				if (quotFrom !== "" || quotFrom !== "00000000") {
					if (row.NEW_PRICE.PRICE_VALID_FROM === "    -  -  " || row.NEW_PRICE.PRICE_VALID_FROM === "0000-00-00") {
						date = new Date(quotTo.substring(0, 4), quotTo.substring(4, 6) - 1, quotTo.substring(6, 8));
						priceValidFrom = new Date(date);
						if (quotTo !== "99991231")
							priceValidFrom.setDate(date.getDate() + 1);
						dateFormattedFrom = dateFormat.format(priceValidFrom);
						row.NEW_PRICE.PRICE_VALID_FROM = dateFormattedFrom;

						date = new Date(dateFormattedFrom.split("-")[0], dateFormattedFrom.split("-")[1] - 1, dateFormattedFrom.split("-")[2]);
						priceValidFrom = new Date(date);
						today = new Date();
						if (date < today) {
							priceValidFrom = today;
							dateFormattedFrom = dateFormat.format(priceValidFrom);
							row.NEW_PRICE.PRICE_VALID_FROM = dateFormattedFrom;
						}
					} else {
						from = row.NEW_PRICE.PRICE_VALID_FROM;
						date = new Date(from.split("-")[0], from.split("-")[1] - 1, from.split("-")[2]);
						priceValidFrom = new Date(date);
						today = new Date();
						if (date < today) {
							priceValidFrom = today;
							dateFormattedFrom = dateFormat.format(priceValidFrom);
							row.NEW_PRICE.PRICE_VALID_FROM = dateFormattedFrom;
						}
					}
				}

				if (row.NEW_PRICE.PRICE_VALID_TO === "    -  -  " || row.NEW_PRICE.PRICE_VALID_TO === "0000-00-00") {
					quotFrom = new Date(quotFrom.substring(0, 4), quotFrom.substring(4, 6) - 1, quotFrom.substring(6, 8));
					quotTo = new Date(quotTo.substring(0, 4), quotTo.substring(4, 6) - 1, quotTo.substring(6, 8));
					priceValidTo = new Date(priceValidFrom);
					if (dateFormat.format(quotTo) !== "9999-12-31") {
						timeDiff = Math.abs(quotTo.getTime() - quotFrom.getTime());
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						if (row.NEW_PRICE.PRICE_VALID_FROM === "2021-01-01" && diffDays === 365)
							diffDays = diffDays - 1;
						priceValidTo.setDate(priceValidTo.getDate() + diffDays);
					}
					var monthNum = priceValidTo.getMonth();
					var stackPriceTo = new Date(priceValidTo);
					var stackPriceTo2 = new Date(priceValidTo);
					stackPriceTo.setDate(stackPriceTo.getDate() + 2);
					stackPriceTo2.setDate(stackPriceTo2.getDate() + 1);
					var monthNum2 = stackPriceTo.getMonth();
					if (monthNum !== monthNum2) {
						if (monthNum !== stackPriceTo2.getMonth()) {
							stackPriceTo2.setDate(stackPriceTo2.getDate() - 1);
							priceValidTo = stackPriceTo2;
						} else {
							stackPriceTo2.setDate(stackPriceTo2.getDate());
							priceValidTo = stackPriceTo2;
						}
					} else {
						stackPriceTo = new Date(priceValidTo);
						stackPriceTo2 = new Date(priceValidTo);
						stackPriceTo.setDate(stackPriceTo.getDate() - 2);
						stackPriceTo2.setDate(stackPriceTo2.getDate() - 1);
						monthNum2 = stackPriceTo.getMonth();
						if (monthNum !== monthNum2) {
							if (monthNum !== stackPriceTo2.getMonth()) {
								priceValidTo = stackPriceTo2;
							} else {
								priceValidTo = stackPriceTo;
							}
						}
					}
					dateFormattedTo = dateFormat.format(priceValidTo);
					row.NEW_PRICE.PRICE_VALID_TO = dateFormattedTo;

				} else {
					quotFrom = new Date(quotFrom.substring(0, 4), quotFrom.substring(4, 6) - 1, quotFrom.substring(6, 8));
					quotTo = new Date(quotTo.substring(0, 4), quotTo.substring(4, 6) - 1, quotTo.substring(6, 8));
					to = row.NEW_PRICE.PRICE_VALID_TO;
					date = new Date(to.split("-")[0], to.split("-")[1] - 1, to.split("-")[2]);
					priceValidTo = new Date(priceValidFrom);
					today = new Date();
					if (date < today) {
						timeDiff = Math.abs(quotTo.getTime() - quotFrom.getTime());
						diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
						if (row.NEW_PRICE.PRICE_VALID_FROM === "2021-01-01" && diffDays === 365)
							diffDays = diffDays - 1;
						priceValidTo.setDate(priceValidTo.getDate() + diffDays);
						dateFormattedTo = dateFormat.format(priceValidTo);
						row.NEW_PRICE.PRICE_VALID_TO = dateFormattedTo;
					}
				}

				if (row.NEW_PRICE.PRICE_VALID_TO.split("-").length >= 3) {
					if (row.NEW_PRICE.PRICE_VALID_TO.split("-")[0].length > 4)
						row.NEW_PRICE.PRICE_VALID_TO = "9999-12-31";
				}

				row.NEW_PRICE.NEW_PRICE_PLAN_K_EXCEL = row.NEW_PRICE.NEW_PRICE_PLAN_K;

				return row;
			},

			//********************************************New excel***************************************************

			_getExcel: function (name, table, tableData) {
				var excelTitle = "",
					aExcelData;

				switch (name) {
				case "PSI":
					excelTitle = "PriceSettingInformation";
					break;
				default:
				}

				if (tableData && tableData.length) {
					aExcelData = this._getExcelData(table, tableData);
					if (aExcelData.length) {
						var oWorkbook = {
							"Sheets": {
								"Sheet1": ""
							},
							"Props": {},
							"SSF": {},
							"SheetNames": []
						};
						var oWorkSheet = {};
						var oRange = {
							s: {
								c: 0,
								r: 0
							},
							e: {
								c: 0,
								r: 0
							}
						};

						for (var nRowNum = 0; nRowNum !== aExcelData.length; ++nRowNum) {
							if (oRange.s.r > nRowNum) {
								oRange.s.r = nRowNum;
							}
							if (oRange.e.r < nRowNum) {
								oRange.e.r = nRowNum
							}
							for (var nColNum = 0; nColNum !== aExcelData[nRowNum].length; ++nColNum) {
								if (oRange.e.r < nRowNum) {
									oRange.e.r = nRowNum
								}
								if (oRange.e.c < nColNum) {
									oRange.e.c = nColNum
								}
								var oCell = {
									v: aExcelData[nRowNum][nColNum],
									s: {
										font: {
											name: "Algerian",
											sz: 10,
											bold: true,
											color: "3FFF00FF"
										}
									}
								};
								if (oCell.v === null) {
									continue;
								}
								var oCellRef = XLSX.utils.encode_cell({
									c: nColNum,
									r: nRowNum
								});
								if (typeof oCell.v === "number") {
									oCell.t = "n";

								} else if (typeof oCell.v === "boolean") {
									oCell.t = "b";
								} else {
									oCell.t = "s";
								}
								oWorkSheet[oCellRef] = oCell;
							}
						}
						oWorkSheet["!ref"] = XLSX.utils.encode_range(oRange);

						// add worksheet to workbook
						oWorkbook.SheetNames.push("Sheet1");
						oWorkbook.Sheets["Sheet1"] = oWorkSheet;
						oWorkbook.Props = {
							Title: excelTitle,
							LastAuthor: sap.ushell.Container.getUser().getFullName(),
							CreatedDate: new Date()
						};

						var oWB = XLSX.write(oWorkbook, {
							type: "binary",
							bookType: "xlsx",
							WTF: 1
						});
						var oWData = this.dataConversion(oWB);
						this.downloadBlob("Document", new Blob([oWData], {
							type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64"
						}));
					}
				}
			},

			_getExcelData: function (table, tableData) {
				var aColumns = table.getColumns(),
					aRow,
					aExcelData = [],
					that = this;
				if (table) {
					if (aColumns) {
						aRow = this._getColumnsArray(aColumns);
					}
					if (aRow.length > 0) {
						aExcelData.push(aRow);
						tableData.forEach(function (oRecord) {
							aRow = [];
							aRow = that._getRowsArray(aColumns, oRecord);
							if (aRow.length) {
								aExcelData.push(aRow);
							}
						})
					}
				}

				return aExcelData;
			},

			_getColumnsArray: function (aColumns) {
				var aRow = [];

				aColumns.forEach(function (oColumn) {
					if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
						if (oColumn.mSkipPropagation.template) {
							var oTemplate = oColumn.getAggregation("template");
							if (oTemplate.mBindingInfos.icon || oTemplate.mBindingInfos.text) {
								if (oColumn.getAggregation("label").getText()) {
									aRow.push(oColumn.getAggregation("label").getText());
								}
							}
						}
					}
				});
				return aRow;
			},

			_getRowsArray: function (aColumns, oRecord) {
				var aRow = [],
					aParts, sValue,
					oTemplate, nLength, path1,
					that = this;
				aColumns.forEach(function (oColumn) {
					if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
						if (oColumn.mSkipPropagation.template) {
							oTemplate = oColumn.getAggregation("template");
							if (oTemplate.mBindingInfos.icon) {
								if (oTemplate.mBindingInfos.icon.parts.length) {
									aRow.push(oRecord[oTemplate.mBindingInfos.icon.parts[0].path]);
								};
							} else {
								if (oTemplate.mBindingInfos.text) {
									aParts = oTemplate.mBindingInfos.text.parts || [];
									nLength = aParts.length;
									if (nLength === 1) {
										path1 = aParts[0].path;
										if (path1.indexOf("PRICE_MASTER/") > -1) {
											path1 = path1.split("PRICE_MASTER/")[1];
										} else {
											path1 = path1.split("NEW_PRICE/")[1];
										}

										if (oRecord.PRICE_MASTER[path1] !== undefined) {
											sValue = oRecord.PRICE_MASTER[path1];
										} else if (oRecord.NEW_PRICE[path1] !== undefined) {
											sValue = oRecord.NEW_PRICE[path1];
										}
										aRow.push(sValue);
									}
								}
							}
						}
					}
				});
				return aRow;
			},

			/************************************Header search variant*******************************/
			setManageVM: function (oEvent, oCC, itemName, callback) {
				var ovar = oCC.getItemValue(itemName);
				// Rename
				var renameKeys = oEvent.getParameters().renamed;
				if (renameKeys.length > 0) {
					ovar.items.forEach(function (item) {
						renameKeys.forEach(function (reitem) {
							if (reitem.key === item.key) {
								item.text = reitem.name;
								ovar[item.key].text = reitem.name;
							}
						});
					});
				}
				// Delete
				var deletedKeys = oEvent.getParameters().deleted;
				if (deletedKeys.length > 0) {
					for (var i = ovar.items.length - 1; i >= 0; i--) {
						for (var j = 0; j < deletedKeys.length; j++) {
							if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
								ovar.items.splice(i, 1);
								delete ovar[deletedKeys[j]];
							}
						}
					}
				}
				ovar["defaultKey"] = oEvent.getParameters().def; // Default
				oCC.setItemValue(itemName, ovar);
				oCC.save().done(function () {
					if (callback) {
						callback(oCC);
					}
				}); // save all
			},

			/* remove default and make standard */
			fixVariant: function (oVM) {
				oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({ // set default
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(this.getVariantByKey(oVM, "default")); // remove default
			},

			setFilterVariant: function (itemName, key, text, data, bDefault, callback, fnError) {
				sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").done(function (oCC) {

					if (!oCC.containsItem(itemName)) {
						oCC.setItemValue(itemName, {
							items: []
						});
					}
					var ovar = oCC.getItemValue(itemName);
					if (!ovar.hasOwnProperty("defaultKey")) {
						ovar["defaultKey"] = "*standard*";
					}
					if (bDefault) {
						ovar["defaultKey"] = key;
					}
					if (key != "*standard*") {
						ovar.items.push({
							key: key,
							text: text
						});
					} // if not standard then push into items
					if (data) {
						ovar[key] = JSON.parse(JSON.stringify(data));
					} // if data is available then add data
					oCC.setItemValue(itemName, ovar);
					oCC.save().done(function () {
						if (callback) {
							callback(oCC);
						}
					}).fail(function (err) {
						if (fnError) {
							fnError();
						}
						console.log(err);
					});
				});
			}

		});
	});