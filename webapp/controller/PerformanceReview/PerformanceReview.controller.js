sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models",
	"com/doehler/PricingMasterFile/model/formatter",
	"com/doehler/PricingMasterFile/model/dbcontext",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (BaseController, base, models, formatter, dbcontext, JSONModel, MessageBox, Fragment, Filter, FilterOperator) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.PerformanceReview.PerformanceReview", {
		formatter: formatter,
		onInit: function () {
			this._tenderRegionTable = this.getView().byId("tenderRegionTableId");
			this._getControlsFromView();
			this._bindModels();
			this._setButtonsPressed(this._globalM, "ppaBtnId");
			this.getRouter().getRoute("PerformanceReview").attachPatternMatched(this._onRouteMatched, this);
			this._clearHeaderTiles();
		},

		_onRouteMatched: function (oEvent) {
			this._setButtonsPressed(this._globalM, "ppaBtnId");
			this.clearSelectionFields();
			this._viewUpdate("redPPage");
		},

		_bindModels: function () {
			var that = this;
			var aModels = ["buMfilterM", "buMresultM", "globalModel", "performanceM", "performancefilterM"];
			aModels.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});

			var aDropdown = ["agreementTypeDD", "existingNewDD", "SELECT1DD", "SELECT2DD", "SELECT3DD"];
			aDropdown.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
		},

		/* ----------------- Dropdown --------------------*/
		loadDropdown: function () {
			models.loadSELECT1();
			models.loadSELECT2();
			models.loadSELECT3();
			// models.loadAgreementType();
			// models.loadExistingNew();
		},

		clearSelectionFields: function () {
			this._performancefilterM.setData({
				C_DOEBKUNDE: "",
				CUSTOMER: "",
				B2BL1: "",
				B2BL2: "",
				REGION: "",
				AREA: "",
				ARTICLE: "",
				CUSTSEG: "",
				C_DACCMAN: "",
				REDC: "",
				LAST_PRICE_1: "",
				LAST_PRICE_2: "",
				GAP_TO_TARGET: "",
				ACCOUNT_INCREASE: "",
				SELECT1: "",
				SELECT2: "",
				SELECT3: "", 
				GLOBAL_TEAM:""
			});

			var controls = this.getView().byId("searchGridId").getContent();
			for (var i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}

			// this.getView().byId("newBizCMBX").setSelectedKeys([]);
		},

		handleSelectionChange: function (oEvent) {
			//oEvent.getSource().getSelectedItems()[0].getProperty("key")
		},

		_getControlsFromView: function () {
			//filter
			this._performancefilterM = sap.ui.getCore().getModel("performancefilterM");
			//table
			this._performanceM = sap.ui.getCore().getModel("performanceM");
			// this._table = this.getView().byId("priceSettingInfromationTableId");
			this._buFilterM = sap.ui.getCore().getModel("buMfilterM");
			this._buResultM = sap.ui.getCore().getModel("buMresultM");
			this._globalM = sap.ui.getCore().getModel("globalModel");

			this._psResultM = sap.ui.getCore().getModel("psresultM");

		},

		onGo: function () {
			var data = [];
			this._clearHeaderTiles();
			this._readData(null);
		},

		_clearHeaderTiles: function () {
			this._globalM.setProperty("/cm1Last12Tile", 0);
			this._globalM.setProperty("/cm1ExpectedTile", 0);
			this._globalM.setProperty("/renewalRateTile", 0);
		},

		_readData: function (param) {
			var that = this,
				data,
				params,
				newData = {},
				functionName = "",
				userId = sap.ushell.Container.getUser().getId();

			functionName = "PRICE_PERFORMANS_REVIEW_NEW";

			data = this._performancefilterM.getData();

			var multiIssues = this.getView().byId("multiInputIssues");
			this._performancefilterM.setProperty("/REDP", "");
			this._performancefilterM.setProperty("/REDC", "");
			this._performancefilterM.setProperty("/LAST_PRICE_1", "");
			this._performancefilterM.setProperty("/LAST_PRICE_2", "");
			this._performancefilterM.setProperty("/GAP_TO_TARGET", "");
			this._performancefilterM.setProperty("/ACCOUNT_INCREASE", "");
			var text;
			multiIssues.getTokens().forEach(function (token) {
				text = token.getText();
				switch (text) {
				case "Reds(position)":
					that._performancefilterM.setProperty("/REDP", "X");
					break;
				case "Reds(customer)":
					that._performancefilterM.setProperty("/REDC", "X");
					break;
				case "Last price change(last price change > 1 year":
					that._performancefilterM.setProperty("/LAST_PRICE_1", "X");
					break;
				case "Last price change(last price change > 2 year":
					that._performancefilterM.setProperty("/LAST_PRICE_2", "X");
					break;
				case "GAP to Target":
					that._performancefilterM.setProperty("/GAP_TO_TARGET", "X");
					break;
				case "L & I-Account increase":
					that._performancefilterM.setProperty("/ACCOUNT_INCREASE", "X");
					break;
				default:
				}
			});
			newData = data;

			if (data.SELECT1 === "") {
				data.SELECT1 = this.getView().byId("S1").getSelectedKey();
				data.SELECT2 = this.getView().byId("S2").getSelectedKey();
				data.SELECT3 = this.getView().byId("S3").getSelectedKey();
			}

			this._checkExistCustomerRole(data);

			if (param !== null) {
				newData = $.extend(true, {}, data);
				newData.B2BL2 = param.B2BL2;
				newData.REGION = param.region;
			}
			params = {
				"HANDLERPARAMS": {
					"FUNC": functionName
				},
				"INPUTPARAMS": [newData]
			};
			dbcontext.callServer(params, function (oModel) {
				if (functionName === "PRICE_PERFORMANS_REVIEW_NEW")
					that._createRegionTable(oModel);
				else
					that._createRegionTable2(oModel);
			}, this);
		},

		cellClick: function (oEvent) {
			var table = this._tenderRegionTable,
				tModel = table.getModel(),
				data = tModel.getData(),
				line = data.rows[oEvent.getParameter("rowIndex")],
				clmn = data.columns[oEvent.getParameter("columnIndex")],
				B2BL2 = line.B2BL2_HTXTID,
				region = clmn.ColumnKey;
			this._readData({
				B2BL2: B2BL2,
				region: region
			});
		},

		calculateHeaderTiles: function () {
			var tableData = this._performanceM.getData(),
				totalCM1_L12M = 0,
				totalVolume_L12M = 0,
				totalSalesGross_L12M = 0,
				totalTargetCM1_L12M = 0,
				totalSalesDeduction_L12M = 0,
				//for second tile
				totalCM1_Quoted = 0,
				totalNetSales_Quoted = 0,
				totalCM1_L12M_NoQuote = 0,
				totalNetSales_L12M_NoQuote = 0,
				//for third Tile
				totalForecast = 0,
				totalForecast_quoted = 0,
				//tiles
				cm1Last12Tile = 0,
				cm1ExpectedTile = 0,
				renewalRateTile = 0;

			tableData.forEach(function (line) {
				totalCM1_L12M = totalCM1_L12M + (line.PRICE_MASTER.DPRSRSCM1 + line.PRICE_MASTER.DPRSRSCM1PY - line.PRICE_MASTER.DPRSRSCM1YPY);
				totalVolume_L12M = totalVolume_L12M + (line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ - line.PRICE_MASTER.DPRSVYTDPY);
				totalSalesGross_L12M = totalSalesGross_L12M + (line.PRICE_MASTER.DPRSRSGSA + line.PRICE_MASTER.DPRSRSGSAPY - line.PRICE_MASTER.DPRSRSGSAYPY);
				totalTargetCM1_L12M = totalTargetCM1_L12M + (line.PRICE_MASTER.DPRSPLCM1 + line.PRICE_MASTER.DPRSPLCM1PY - line.PRICE_MASTER.DPRSPLCM1YPY);
				totalSalesDeduction_L12M = totalSalesDeduction_L12M + ((line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER.DPRSVYTD) + (line.PRICE_MASTER
					.DPRSSALDEPY * line.PRICE_MASTER.DPRSVVJ) - (line.PRICE_MASTER.DPRSSALDECYPY * line.PRICE_MASTER.DPRSVYTDPY));

				totalForecast = totalForecast + line.PRICE_MASTER.DPRSVFC;

				//second tile
				var changePRI = line.NEW_PRICE.CHANGED_PRI.replace(/-/g, "");
				var lastPRI = line.PRICE_MASTER.DAPCHANGEDON;
				var erdat = line.SALES_DOC.ERDAT;
				var calculatedValue2 = 0;
				var calculatedValue1 = (line.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(line.PRICE_MASTER.DCONDUNIT)) / line.PRICE_MASTER.DUNITQ;
				if (calculatedValue1 === Infinity || isNaN(calculatedValue1)) {
					calculatedValue1 = 0;
				}
				calculatedValue2 = calculatedValue1 * line.PRICE_MASTER.DRATE3;
				if (calculatedValue2 === Infinity || isNaN(calculatedValue2)) {
					calculatedValue2 = 0;
				}
				changePRI = new Date(changePRI.substring(0, 4), changePRI.substring(4, 6) - 1, changePRI.substring(6, 8));
				lastPRI = new Date(lastPRI.substring(0, 4), lastPRI.substring(4, 6) - 1, lastPRI.substring(6, 8));
				erdat = new Date(erdat.substring(0, 4), erdat.substring(4, 6) - 1, erdat.substring(6, 8));
				if (((lastPRI > changePRI) && (line.NEW_PRICE.PRICE_ACTION !== "")) ||
					(line.SALES_DOC.VBELN !== "") && (line.SALES_DOC.ZZ_OVR_QUOT_ST === "X") && (lastPRI < erdat)) {
					totalCM1_Quoted = totalCM1_Quoted + (line.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST * line.PRICE_MASTER.TARGET_VOLUME);
					totalNetSales_Quoted = totalNetSales_Quoted + ((calculatedValue2 - line.PRICE_MASTER.DPRSSALDEC - line.PRICE_MASTER.DRPSFRT -
						line
						.PRICE_MASTER.DPRSRSC3P / line.PRICE_MASTER.DPRSVYTD) * line.PRICE_MASTER.TARGET_VOLUME);

					totalForecast_quoted = totalForecast_quoted + line.PRICE_MASTER.DPRSVFC;

				} else {
					totalCM1_L12M_NoQuote = totalCM1_L12M_NoQuote + (line.PRICE_MASTER.DPRSRSCM1 + line.PRICE_MASTER.DPRSRSCM1PY - line.PRICE_MASTER
						.DPRSRSCM1YPY);
					totalNetSales_L12M_NoQuote = totalNetSales_L12M_NoQuote + ((line.PRICE_MASTER.DPRSRSGSA + line.PRICE_MASTER.DPRSRSGSAPY - line.PRICE_MASTER
						.DPRSRSGSAYPY) - ((line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER.DPRSVYTD) + (line.PRICE_MASTER.DPRSSALDEPY * line.PRICE_MASTER
						.DPRSVVJ) - (line.PRICE_MASTER.DPRSSALDECYPY * line.PRICE_MASTER.DPRSVYTDPY)));
				}

			});

			if (totalSalesGross_L12M - totalSalesDeduction_L12M !== 0)
				cm1Last12Tile = totalCM1_L12M / (totalSalesGross_L12M - totalSalesDeduction_L12M) * 100;
			else
				cm1Last12Tile = 0;

			cm1ExpectedTile = (totalCM1_Quoted + totalCM1_L12M_NoQuote) / (totalNetSales_Quoted + totalNetSales_L12M_NoQuote) * 100;

			renewalRateTile = totalForecast_quoted / totalForecast * 100;

			if (isNaN(cm1Last12Tile)) cm1Last12Tile = 0;
			if (isNaN(cm1ExpectedTile)) cm1ExpectedTile = 0;
			if (isNaN(renewalRateTile)) renewalRateTile = 0;

			this._globalM.setProperty("/cm1Last12Tile", cm1Last12Tile);
			this._globalM.setProperty("/cm1ExpectedTile", cm1ExpectedTile);
			this._globalM.setProperty("/renewalRateTile", renewalRateTile);

		},

		_createRegionTable2: function (oModel) {
			var data = oModel.getData(),
				bwData = data.BWDATA,
				BW_COL_B2B = data.BW_COL_B2B,
				BW_COL_REGION = data.BW_COL_REGION,
				BW_COL_TOTAL = data.BW_COL_TOTAL,
				mainPSettingTable = data.PRDATA_MAIN,
				linesDescr = data.LINE_DESCR,
				listRegion = data.REGION,
				totalRow = data.BW_REGION_COL,
				totalColumn = data.BWDATA_COL,
				columnData = [],
				rowData = [],
				Column,
				oTable = this._tenderRegionTable,
				i = 0;

			debugger;

			this.getView().byId("priceSettingInfromationTableId").setVisible(false);
			this.getView().byId("priceSettingInfromationTableId").setVisible(true);

			mainPSettingTable = this._prepareTableData(mainPSettingTable);
			sap.ui.getCore().getModel("performanceM").setData(mainPSettingTable);
			this.calculateHeaderTiles();
			listRegion.forEach(function (item) {
				if (item.HDESCR === "TOTAL") {
					item.ATXTID = "TOTAL";
					item.HTXTID = "TOTAL";
				}

				if (item.ATXTID === "" && item.HDESCR !== "TOTAL") {

				} else {
					columnData.push({
						ColumnKey: item.HTXTID,
						columnName: "LKPI_" + item.ATXTID,
						columnLabel: item.ATXTID + " Based on selection",
						columnTooltip: item.HDESCR,
						columnLabel2: "",
						columnLabel3: "Based on selection"
					});
					columnData.push({
						ColumnKey: item.HTXTID,
						columnName: "RKPI_" + item.ATXTID,
						columnLabel: item.ATXTID + " Based on selection",
						columnTooltip: item.HDESCR,
						columnLabel2: "",
						columnLabel3: "Based on selection"
					});
				}

			});

			columnData.unshift({
				columnName: "B2BL2",
				columnLabel: "B2B LEVEL 2",
				columnTooltip: "B2B LEVEL 2"
			});
			columnData.unshift({
				columnName: "B2BL1",
				columnLabel: "B2B LEVEL 1",
				columnTooltip: "B2B LEVEL 1"
			});
			var kpis;
			var row = {},
				kpiName,
				columnKey,
				columnName;
			linesDescr.forEach(function (line) {
				row = {};
				row = line;
				row.B2BL1 = line.B2BL1;
				row.B2BL2 = line.B2BL2;
				rowData.push(row);
			});

			for (var k = 0; k < rowData.length; k++) {
				var line = rowData[k].B2BL1;
				k = k + 1;
				while (k < rowData.length && line === rowData[k].B2BL1) {
					rowData[k].B2BL1 = "";
					k++;
				}
				k--;
			}

			var LKPI_TOTAL_REG = 0,
				RKPI_TOTAL_REG = 0;

			var lastline = {};
			lastline.B2BL1 = "TOTAL";
			lastline["LKPI_TOTAL"] = 0;
			lastline["RKPI_TOTAL"] = 0;
			rowData.forEach(function (line) {
				if (line.B2BL1 !== "TOTAL") {
					for (i = 2; i < columnData.length - 2; i++) {
						LKPI_TOTAL_REG = 0;
						RKPI_TOTAL_REG = 0;
						columnName = columnData[i].columnName;
						columnKey = columnData[i].ColumnKey;
						kpis = bwData.filter(function (bw) {
							return line.B2BL1_NODEID === bw.B2BL1_NODEID && line.B2BL1_NODEPR === bw.B2BL1_NODEPR && line.B2BL2_NODEID === bw.B2BL2_NODEID &&
								line.B2BL2_NODEPR === bw.B2BL2_NODEPR && columnKey === bw.REGION;
						});

						kpiName = columnName.split("_")[0];
						if (kpis.length > 0)
							line[columnName] = kpis[0][kpiName];
						// if (lastline[columnName] === undefined)
						// 	lastline[columnName] = 0;
						if (kpiName === "LKPI") {
							lastline[columnName] = kpis[0].LKPI_TOTAL_REG;
							lastline["LKPI_TOTAL"] = lastline["LKPI_TOTAL"] + kpis[0].LKPI_B2B_TOTAL;
							line["LKPI_TOTAL"] = kpis[0].LKPI_B2B_TOTAL;
						} else {
							lastline[columnName] = kpis[0].RKPI_TOTAL_REG;
							lastline["RKPI_TOTAL"] = lastline["RKPI_TOTAL"] + kpis[0].RKPI_B2B_TOTAL;
							line["RKPI_TOTAL"] = kpis[0].RKPI_B2B_TOTAL;
						}

					}
					// for (i = columnData.length - 2; i < columnData.length; i++) {
					// 	columnName = columnData[i].columnName;
					// 	columnKey = columnData[i].ColumnKey;
					// 	kpis = totalColumn.filter(function (bw) {
					// 		return line.B2BL1_NODEID === bw.B2BL1_NODEID && line.B2BL1_NODEPR === bw.B2BL1_NODEPR && line.B2BL2_NODEID === bw.B2BL2_NODEID &&
					// 			line.B2BL2_NODEPR === bw.B2BL2_NODEPR;
					// 	});

					// 	kpiName = columnName.split("_")[0];
					// 	if (kpis.length > 0)
					// 		line[columnName] = kpis[0][kpiName];

					// }
				} else {
					// for (i = 2; i < columnData.length - 2; i++) {
					// 	columnName = columnData[i].columnName;
					// 	columnKey = columnData[i].ColumnKey;
					// 	kpis = totalRow.filter(function (bw) {
					// 		return columnKey === bw.REGION;
					// 	});

					// 	kpiName = columnName.split("_")[0];
					// 	if (kpis.length > 0)
					// 		line[columnName] = kpis[0][kpiName];
					// }
					// for (i = columnData.length - 2; i < columnData.length; i++) {
					// 	columnName = columnData[i].columnName;
					// 	columnKey = columnData[i].ColumnKey;
					// 	var left = 0,
					// 		right = 0;
					// 	if (columnName === "LKPI_TOTAL") {
					// 		totalColumn.forEach(function (o) {
					// 			left = left + o.LKPI;
					// 		});

					// 		line[columnName] = left;

					// 	}
					// 	if (columnName === "RKPI_TOTAL") {
					// 		totalColumn.forEach(function (o) {
					// 			right = right + o.RKPI;
					// 		});
					// 		line[columnName] = right;
					// 	}
					// }
				}
			});
			rowData.push(lastline);
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setData({
				rows: rowData,
				columns: columnData
			});
			oTable.setModel(oModel);
			oTable.unbindColumns();

			var that = this;

			oTable.bindColumns("/columns", function (sId, oContext) {

				var columnName = oContext.getObject().columnName,
					columnLabel = oContext.getObject().columnLabel,
					columnTooltip = oContext.getObject().columnTooltip,
					columnLabel2 = oContext.getObject().columnLabel2,
					columnLabel3 = oContext.getObject().columnLabel3;
				if (columnLabel === "B2B LEVEL 1" || columnLabel === "B2B LEVEL 2") {
					Column = new sap.ui.table.Column({
						width: "10rem",
						id: columnLabel === "B2B LEVEL 1" ? 'B2BL1Id' : 'B2BL2Id',
						label: new sap.m.Label({
							text: columnLabel,
							tooltip: columnTooltip, // tooltip for Regions long text
							textAlign: "Center",
							width: "100%",
							design: "Bold",
							wrapping: true
						}),
						template: new sap.m.Text({
							textAlign: "Center",
							text: {
								path: columnName
							},
							tooltip: {
								path: columnName
							},
							wrapping: false
						})
					});
					return Column;
				} else {
					Column = new sap.ui.table.Column({
						multiLabels: [
							new sap.m.Label({
								text: columnLabel,
								tooltip: columnTooltip, // tooltip for Regions long text
								textAlign: "Center",
								width: "100%",
								design: "Bold",
								wrapping: true
							}), new sap.m.Label({
								text: columnLabel2,
								tooltip: columnLabel2,
								wrapping: true
							})
						],

						// width: "5rem",
						template: new sap.m.ObjectIdentifier({
							textAlign: "Right",
							titleActive: false,
							title: {
								path: columnName,
								type: "sap.ui.model.odata.type.Decimal",
								formatOptions: {
									decimals: 2,
									groupingEnabled: true,
									groupingSeparator: ".",
									decimalSeparator: ","
								}
							}
						})
					});
					Column.setHeaderSpan([2, 2, 1]);
					return Column;
				}

			});

			oTable.bindRows("/rows");
			var oColumn = this.getView().byId("B2BL1Id");
			this.getView().byId("tenderRegionTableId").setGroupBy(oColumn);
		},

		_createRegionTable: function (oModel) {
			var data = oModel.getData(),
				bwData = data.BWDATA,
				BW_COL_B2B = data.BW_COL_B2B,
				BW_COL_REGION = data.BW_COL_REGION,
				BW_COL_TOTAL = data.BW_COL_TOTAL,
				mainPSettingTable = data.PRDATA_MAIN,
				linesDescr = data.LINE_DESCR,
				listRegion = data.REGION,
				totalRow = data.BW_REGION_COL,
				totalColumn = data.BWDATA_COL,
				columnData = [],
				rowData = [],
				Column,
				oTable = this._tenderRegionTable,
				i = 0;

			this.getView().byId("priceSettingInfromationTableId").setVisible(false);
			this.getView().byId("priceSettingInfromationTableId").setVisible(true);

			mainPSettingTable = this._prepareTableData(mainPSettingTable);
			sap.ui.getCore().getModel("performanceM").setData(mainPSettingTable);
			this.calculateHeaderTiles();
			listRegion.forEach(function (item) {
				if (item.HDESCR === "TOTAL") {
					item.ATXTID = "TOTAL";
					item.HTXTID = "TOTAL";
				}

				if (item.ATXTID === "" && item.HDESCR !== "TOTAL") {

				} else {
					columnData.push({
						ColumnKey: item.HTXTID,
						columnName: "LKPI_" + item.ATXTID,
						columnLabel: item.ATXTID + " Based on selection",
						columnTooltip: item.HDESCR,
						columnLabel2: "",
						columnLabel3: "Based on selection"
					});
					columnData.push({
						ColumnKey: item.HTXTID,
						columnName: "RKPI_" + item.ATXTID,
						columnLabel: item.ATXTID + " Based on selection",
						columnTooltip: item.HDESCR,
						columnLabel2: "",
						columnLabel3: "Based on selection"
					});
				}

			});

			columnData.unshift({
				columnName: "B2BL2",
				columnLabel: "B2B LEVEL 2",
				columnTooltip: "B2B LEVEL 2"
			});
			columnData.unshift({
				columnName: "B2BL1",
				columnLabel: "B2B LEVEL 1",
				columnTooltip: "B2B LEVEL 1"
			});
			var kpis,
				b2bkpis,
				row = {},
				lastline = {},
				kpiName,
				columnKey,
				columnName;
			linesDescr.forEach(function (line) {
				row = {};
				row = line;
				row.B2BL1 = line.B2BL1;
				row.B2BL2 = line.B2BL2;
				rowData.push(row);
			});

			for (var k = 0; k < rowData.length; k++) {
				var line = rowData[k].B2BL1;
				k = k + 1;
				while (k < rowData.length && line === rowData[k].B2BL1) {
					rowData[k].B2BL1 = "";
					k++;
				}
				k--;
			}

			lastline.B2BL1 = "TOTAL";
			lastline["LKPI_TOTAL"] = 0;
			lastline["RKPI_TOTAL"] = 0;
			// rowData.push(lastline);
			rowData.forEach(function (line) {
				//filling table main data
				for (i = 2; i < columnData.length - 2; i++) {
					columnName = columnData[i].columnName;
					columnKey = columnData[i].ColumnKey;
					kpis = bwData.filter(function (bw) {
						return line.B2BL1_NODEID === bw.B2BL1_NODEID && line.B2BL1_NODEPR === bw.B2BL1_NODEPR && line.B2BL2_NODEID === bw.B2BL2_NODEID &&
							line.B2BL2_NODEPR === bw.B2BL2_NODEPR && columnKey === bw.REGION;
					});

					kpiName = columnName.split("_")[0];
					if (kpis.length > 0)
						line[columnName] = kpis[0][kpiName];
					else
						line[columnName] = 0;

					var regionTotalKPIS = BW_COL_REGION.filter(function (regTotalLine) {
						return regTotalLine.REGION === columnKey;
					});
					if (regionTotalKPIS.length > 0)
						lastline[columnName] = regionTotalKPIS[0][kpiName];
					else
						lastline[columnName] = 0;
				}
				//filling right totals
				for (var j = columnData.length - 2; j < columnData.length; j++) {
					columnName = columnData[j].columnName;
					columnKey = columnData[j].ColumnKey;
					b2bkpis = BW_COL_B2B.filter(function (line_b2b) {
						return line.B2BL2_HTXTID === line_b2b.M_DZZB2B_L2;
					});
					kpiName = columnName.split("_")[0];
					if (b2bkpis.length > 0)
						line[columnName] = b2bkpis[0][kpiName];
					else
						line[columnName] = 0;
				}

			});
			if (BW_COL_TOTAL.length === 1) {
				lastline["LKPI_TOTAL"] = BW_COL_TOTAL[0].LKPI;
				lastline["RKPI_TOTAL"] = BW_COL_TOTAL[0].RKPI;
			}
			rowData.push(lastline);
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setData({
				rows: rowData,
				columns: columnData
			});
			oTable.setModel(oModel);
			oTable.unbindColumns();

			var that = this;

			oTable.bindColumns("/columns", function (sId, oContext) {

				var columnName = oContext.getObject().columnName,
					columnLabel = oContext.getObject().columnLabel,
					columnTooltip = oContext.getObject().columnTooltip,
					columnLabel2 = oContext.getObject().columnLabel2,
					columnLabel3 = oContext.getObject().columnLabel3;
				if (columnLabel === "B2B LEVEL 1" || columnLabel === "B2B LEVEL 2") {
					Column = new sap.ui.table.Column({
						width: "10rem",
						id: columnLabel === "B2B LEVEL 1" ? 'B2BL1Id' : 'B2BL2Id',
						label: new sap.m.Label({
							text: columnLabel,
							tooltip: columnTooltip, // tooltip for Regions long text
							textAlign: "Center",
							width: "100%",
							design: "Bold",
							wrapping: true
						}),
						template: new sap.m.Text({
							textAlign: "Center",
							text: {
								path: columnName
							},
							tooltip: {
								path: columnName
							},
							wrapping: false
						})
					});
					return Column;
				} else {
					Column = new sap.ui.table.Column({
						multiLabels: [
							new sap.m.Label({
								text: columnLabel,
								tooltip: columnTooltip, // tooltip for Regions long text
								textAlign: "Center",
								width: "100%",
								design: "Bold",
								wrapping: true
							}), new sap.m.Label({
								text: columnLabel2,
								tooltip: columnLabel2,
								wrapping: true
							})
						],

						// width: "5rem",
						template: new sap.m.ObjectIdentifier({
							textAlign: "Right",
							titleActive: false,
							title: {
								path: columnName,
								type: "sap.ui.model.odata.type.Decimal",
								formatOptions: {
									decimals: 2,
									groupingEnabled: true,
									groupingSeparator: ".",
									decimalSeparator: ","
								}
							}
						})
					});
					Column.setHeaderSpan([2, 2, 1]);
					return Column;
				}

			});

			oTable.bindRows("/rows");
			var oColumn = this.getView().byId("B2BL1Id");
			this.getView().byId("tenderRegionTableId").setGroupBy(oColumn);
		},

		handleValueHelp: function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();

			// create value help dialog
			if (!this._valueHelpDialog) {
				Fragment.load({
					id: "valueHelpDialog",
					name: "com.doehler.PricingMasterFile.fragments.Issues",
					controller: this
				}).then(function (oValueHelpDialog) {
					this._valueHelpDialog = oValueHelpDialog;
					this.getView().addDependent(this._valueHelpDialog);
					this._openValueHelpDialog(sInputValue);
				}.bind(this));
			} else {
				this._openValueHelpDialog(sInputValue);
			}
		},

		_openValueHelpDialog: function (sInputValue) {
			// create a filter for the binding
			this._valueHelpDialog.getBinding("items").filter([new Filter(
				"text",
				FilterOperator.Contains,
				sInputValue
			)]);

			// open value help dialog filtered by the input value
			this._valueHelpDialog.open(sInputValue);
		},

		_handleValueHelpSearch: function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"text",
				FilterOperator.Contains,
				sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpClose: function (evt) {
			var aSelectedItems = evt.getParameter("selectedItems"),
				oMultiInput = this.byId("multiInputIssues"),
				line,
				that = this;

			if (aSelectedItems && aSelectedItems.length > 0) {
				aSelectedItems.forEach(function (oItem) {
					line = sap.ui.getCore().getModel("globalModel").getProperty(oItem.getBindingContext("globalModel").getPath());

					switch (line.key) {
					case 0:
						that._performancefilterM.setProperty("/REDP", "X");
						break;
					case 1:
						that._performancefilterM.setProperty("/REDC", "X");
						break;
					case 2:
						that._performancefilterM.setProperty("/LAST_PRICE_1", "X");
						break;
					case 3:
						that._performancefilterM.setProperty("/LAST_PRICE_2", "X");
						break;
					case 4:
						that._performancefilterM.setProperty("/ACCOUNT_INCREASE", "X");
						break;
					case 5:
						that._performancefilterM.setProperty("/GAP_TO_TARGET", "X");
						break;
					default:
					}

					oMultiInput.addToken(new sap.m.Token({
						key: line.key,
						text: line.text
					}));
				});
			}
		},

		/*-------------------- START OF VARIANT MANAGEMENT --------------------------- */
		oCC: null,
		currTableData: null,
		oTPC: null,
		tableId: "performanceReviewTableId",
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz.tableId);
				oCC.save().then(function () {
					// Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var oVM = this.getView().byId("tableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(tableId, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(tableId);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// oTable.setVisibleRowCountMode("Fixed");
				// oTable.setVisibleRowCount(oItem[oItem.defaultKey].rowCount === undefined ? 10 : oItem[oItem.defaultKey].rowCount);
				// oTable.setVisibleRowCountMode(oItem[oItem.defaultKey].rowCount === undefined ? "Interactive" : "Fixed");

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data

			// oTable.setVisibleRowCountMode("Fixed");
			// oTable.setVisibleRowCount(oItem[selKey].rowCount === undefined ? 10 : oItem[selKey].rowCount);
			// oTable.setVisibleRowCountMode(oItem[selKey].rowCount === undefined ? "Interactive" : "Fixed");
			//oTable.setVisibleRowCountMode("Interactive"); 

		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				//oTable.setVisibleRowCount(oItem[oItem.defaultKey].rowCount===undefined?10:oItem[oItem.defaultKey].rowCount);
				// oTable.setVisibleRowCountMode("Fixed");
				// oTable.setVisibleRowCount(oTable.getVisibleRowCount());
				// oTable.setVisibleRowCountMode("Interactive");
				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this.tableId;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		/*************************************************Start Header Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdReview");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("performancefilterM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			// oVM.setInitialSelectionKey(ovar.defaultKey);
			// oVM.setDefaultVariantKey(ovar.defaultKey);
			oVM.setInitialSelectionKey(ovar.defaultKey);
			oVM.setDefaultVariantKey(ovar.defaultKey);
			sap.ui.getCore().getModel("performancefilterM").setData(ovar[ovar.defaultKey]);
			this.createToken();
		},

		/* on select variant */
		onSelectVariantReview: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("performancefilterM").setData(ovar[selKey]);
			this.createToken();
		},

		/* on save variant */
		onSaveVariantReview: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("performancefilterM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMReview: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function () {
			var aControls = this.getView().byId("searchGridId").getContent();
			aControls.forEach(function (item) {
				if (item.getMetadata().getName() === "com.doehler.PricingMasterFile.customControls.MultiValueHelpControl" || item.getMetadata().getName() ===
					"sap.m.MultiInput") {
					if (item.getMetadata().getName() === "sap.m.MultiInput")
						item.destroyTokens();
					if (item.getId().indexOf("multiInputIssues") <= -1) {
						if (item.getMultiSelect() && item.getSelectedValues()) {
							var arr = item.getSelectedValues().split(",");
							arr.forEach(function (value) {
								if (value != "") {
									item.addToken(new sap.m.Token({
										key: value,
										text: value
									}));
								}
							});
						}
					} else {
						if (sap.ui.getCore().getModel("performancefilterM").getData().REDP === "X") {
							item.addToken(new sap.m.Token({
								key: 0,
								text: "Reds(position)"
							}));
						}
						if (sap.ui.getCore().getModel("performancefilterM").getData().REDC === "X") {
							item.addToken(new sap.m.Token({
								key: 1,
								text: "Reds(customer)"
							}));
						}
						if (sap.ui.getCore().getModel("performancefilterM").getData().LAST_PRICE_1 === "X") {
							item.addToken(new sap.m.Token({
								key: 2,
								text: "Last price change(last price change > 1 year"
							}));
						}
						if (sap.ui.getCore().getModel("performancefilterM").getData().LAST_PRICE_2 === "X") {
							item.addToken(new sap.m.Token({
								key: 3,
								text: "Last price change(last price change > 2 year"
							}));
						}
						if (sap.ui.getCore().getModel("performancefilterM").getData().GAP_TO_TARGET === "X") {
							item.addToken(new sap.m.Token({
								key: 4,
								text: "L & I-Account increase"
							}));
						}
						if (sap.ui.getCore().getModel("performancefilterM").getData().ACCOUNT_INCREASE === "X") {
							item.addToken(new sap.m.Token({
								key: 5,
								text: "GAP to Target"
							}));
						}

					}
				}
			});
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.afterRender = true;
			this.loadDropdown();
			// this.initVariant();
			this.initSearchVariant();
		}

		/*-------------------------END OF VARIANT MANAGEMENT ------------------------*/
	});

});