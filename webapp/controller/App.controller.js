jQuery.sap.require("com.doehler.PricingMasterFile.model.xlsx-full-min");
sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models"
], function (BaseController, base, models) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.App", {
		onInit: function () {
			sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(this.handleRouteMatched, this);
			var app = this.getView().byId("app");
			app.attachNavigate(function (evt) {});
		},

		/* event trigger when route change */
		handleRouteMatched: function (oEvent) {
			var currPage = oEvent.getParameter("name");
			if (base.previousPage == "") {
				base.previousPage = currPage;
			}
			base.previousPage = base.currentPage;
			base.currentPage = currPage;
		}
		//////şinasi
	});
});