sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models",
	"com/doehler/PricingMasterFile/model/dbcontext",
	"com/doehler/PricingMasterFile/excel/excel",
	"com/doehler/PricingMasterFile/model/formatter",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/Label",
	"sap/m/TextArea",
	"sap/ui/core/Core",
	"sap/m/MessageToast"
], function (BaseController, base, models, dbcontext, excel, formatter, JSONModel, MessageBox, Dialog,
	DialogType, Button, ButtonType, Label, TextArea, Core, MessageToast) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.Tender.Tender", {
		formatter: formatter,
		onInit: function () {
			this._getControlsFromView();
			this._bindModels();
			this.getRouter().getRoute("Tender").attachPatternMatched(this._onRouteMatched, this);
			var sPath = sap.ui.require.toUrl("com/doehler/PricingMasterFile/Data") + "/item.json";
			this.getView().setModel(new JSONModel(sPath), "Attachment");
			var oView = this.getView();
			// set the model to the core
			oView.byId("multiheader").setHeaderSpan([7, 1, 2]);

			var table = this.getView().byId("tenderTableId");
			if (this._globalM.getData().UIROLE !== "ADMINM")
				this._columnAuthOnlyAdmin(table);
		},

		onFilter: function (oEvent) {},

		onNavBackPS: function () {
			this._globalM.setProperty("/previousPage", 1);
			this.getRouter().navTo("Main");
		},

		_onRouteMatched: function (oEvent) {
			this._viewUpdate("tenderPage");
			var arg = oEvent.getParameter("arguments"),
				tab = arg["?query"].tab,
				jsonData;
			var table = this._table;
			if (this._globalM.getProperty("/previousPage") === 3) // if comes from tender via back button
				return;
			table.setSelectedIndex(-1);

			this._globalM.setProperty("/savedDetailPage", false);
			this._globalM.setProperty("/firstTenderData", JSON.parse(tab));

			jsonData = JSON.parse(tab);
			this._globalM.setProperty("/urlData", jsonData);
			this.initVariant_item();
			this._initKPIs();
			this._getDetail(jsonData, "", "R"); //done

		},

		onExport: function () {
			var data = [];
			var userId = sap.ushell.Container.getUser().getId();
			var tableData = this._tenderM.getData();
			var copiedData = $.extend(true, [], tableData);
			var aIndices = this._table.getBinding("rows").aIndices;

			if (copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			if (userId === "MATTHEC" || userId === "EX_DOGUS" || userId === "DEFAULT_USER")
				excel._downloadExcel("TENDER", this._table, data);
			else
				excel._downloadExcel("TENDER", this._table, data);
		},

		_getDetail: function (jsonData, key, func) {
			this._globalM.setProperty("/savedTender", []);
			if (func === "R") {
				if (this._psresultM.getData().length) {
					var result = [];
					var dataPSI = this._psresultM.getData();
					var data2 = [];
					jsonData.forEach(function (m) {
						data2.push({
							MATERIAL: m.Material,
							SALESORG: m.SalesOrg
						});
					});

					var params2 = {
						"HANDLERPARAMS": {
							"FUNC": "GET_CUSTOMS_TARIFFCODE"
						},
						"INPUTPARAMS": data2
					};
					var thiz = this;
					dbcontext.callServer(params2, function (oModel) {
						var resdata = oModel.getProperty("/PRICEREQHDR");
						jsonData.forEach(function (m, m2) {
							dataPSI[m.aSelIndex].CCNGN = {
								CCNGN: resdata[m2].CCNGN.CCNGN
							};
							result.push(dataPSI[m.aSelIndex]);
						});
						thiz._applyData(result, key);

					}, this);

					return;
				}
			}

			var sendingData = {},
				params,
				that = this;

			sendingData.CUSTOMER = jsonData.map(function (oContext) {
				return oContext.Customer;
			}).join(",");
			sendingData.ARTICLE = jsonData.map(function (oContext) {
				return oContext.Material;
			}).join(",");
			sendingData.SHIP_TO = jsonData.map(function (oContext) {
				return oContext.Ship_To;
			}).join(",");
			sendingData.SALESORG = jsonData.map(function (oContext) {
				return oContext.SalesOrg;
			}).join(",");
			sendingData.DISTR_CHAN = jsonData.map(function (oContext) {
				return oContext.Dis_Chnl;
			}).join(",");

			params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_DETAIL"
				},
				"INPUTPARAMS": [sendingData]
			};
			dbcontext.callServer(params, function (oModel) {
				var resdata = oModel.getProperty("/PRICEREQHDR");
				that._applyData(resdata, key);

			}, this);

		},

		_applyData: function (resdata, key) {
			var that = this,
				oldData,
				resdata = that._prepareTableData(resdata);

			resdata = that._loadPriceScalesForViews(resdata);

			if (key !== "X") {
				oldData = $.extend(true, [], resdata);
				that._globalM.setProperty("/oldData", oldData);
			}
			that._getKPIsValues(resdata);
			sap.ui.getCore().getModel("tenderM").setData(resdata);
		},

		onTableRowSelect: function (oEvent) {
			var selectedInds = oEvent.getSource().getSelectedIndices(),
				aIndices = oEvent.getSource().getBinding().aIndices,
				newSelIndex = [],
				data = [],
				tableData = sap.ui.getCore().getModel("tenderM").getData();

			selectedInds.forEach(function (selIndex) {
				newSelIndex.push(aIndices[selIndex]);
			});

			selectedInds = newSelIndex;
			selectedInds.forEach(function (ind) {
				data.push(tableData[ind]);
			});

			this._getKPIsValues(data.length > 0 ? data : tableData);
		},

		_initKPIs: function () {

			this._tenderHeaderM.setData({
				forecastTableH: 0,
				currentPriceTableH: 0,
				priceLastCostTableH: 0,
				targetPriceTableH: 0,
				newPriceTableH: 0,
				priceChangeAbsTableH: 0,
				priceChangePercTableH: 0
			});
		},

		_getKPIsValues: function (resdata) {
			var totalCM1 = 0,
				totalSalesGross = 0,
				totalSalesDeduction = 0,

				totalDPRSTMPRO = 0,
				totalVolume = 0,
				totalForecast = 0,
				totalCM1_CurrentPrice = 0,
				totalCM1_TargetPrice = 0,
				totalNewPrice = 0,
				totalTargetCM1 = 0,
				CM1PageHeaderPerc = 0,
				deltaTargetCM1 = 0,

				priceChangeAbsTableH = 0,
				priceChangePercTableH = 0,
				gross_sales_new = 0,
				gross_sales_exist = 0,
				gross_sales_exist_full = 0;

			resdata.forEach(function (row) {
				totalDPRSTMPRO = totalDPRSTMPRO + (row.PRICE_MASTER.DPRSTMPRO * row.NEW_PRICE.TARGET_VOLUME);
				totalForecast = totalForecast + row.NEW_PRICE.TARGET_VOLUME;
				//condition 1.1
				if (row.PRICE_MASTER.DPRSVATD > 0) {
					totalVolume = totalVolume + (row.PRICE_MASTER.DPRSVATD);

					totalCM1_CurrentPrice = totalCM1_CurrentPrice + ((row.PRICE_MASTER.DPRSRSCM1ATD) /
						(row.PRICE_MASTER.DPRSVATD)) * row.NEW_PRICE.TARGET_VOLUME;
					totalCM1_TargetPrice = totalCM1_TargetPrice + row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE * row.NEW_PRICE.TARGET_VOLUME;
					totalNewPrice = totalNewPrice + row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST * row.NEW_PRICE.TARGET_VOLUME;
					//Page Header
					totalTargetCM1 = totalTargetCM1 + (row.PRICE_MASTER.DPRSPLCM1ATD);

					totalCM1 = totalCM1 + (row.PRICE_MASTER.DPRSRSCM1ATD);
					totalSalesGross = totalSalesGross + (row.PRICE_MASTER.DPRSRSGSATD);
					totalSalesDeduction = totalSalesDeduction + (row.PRICE_MASTER.DPRSSALDECAT * row.PRICE_MASTER.DPRSVATD);

					gross_sales_exist_full = gross_sales_exist_full + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
						.DUNITQ *
						row.NEW_PRICE.TARGET_VOLUME;

					if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0 && row.PRICE_MASTER.DPRSCPAIN !== 0) {
						priceChangeAbsTableH = priceChangeAbsTableH + (row.NEW_PRICE.NEW_PRICE_PLAN - row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER
							.DCONDUNIT) / row.PRICE_MASTER.DUNITQ * row.NEW_PRICE.TARGET_VOLUME;
						gross_sales_new = gross_sales_new + (row.NEW_PRICE.NEW_PRICE_PLAN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
							.DUNITQ *
							row.NEW_PRICE.TARGET_VOLUME;

						gross_sales_exist = gross_sales_exist + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
							.DUNITQ *
							row.NEW_PRICE.TARGET_VOLUME;
					}

				} else {
					//condition 2.1
					if (row.PRICE_MASTER.DPRSVYTD > 0) {
						totalVolume = totalVolume + (row.PRICE_MASTER.DPRSVYTD);
						// totalDPRSTMPRO = (row.PRICE_MASTER.DPRSTMPRO * row.NEW_PRICE.TARGET_VOLUME);
						// totalForecast = totalForecast + row.NEW_PRICE.TARGET_VOLUME;
						totalCM1_CurrentPrice = totalCM1_CurrentPrice + ((row.PRICE_MASTER.DPRSRSCM1) /
							(row.PRICE_MASTER.DPRSVYTD)) * row.NEW_PRICE.TARGET_VOLUME;
						totalCM1_TargetPrice = totalCM1_TargetPrice + row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE * row.NEW_PRICE.TARGET_VOLUME;
						totalNewPrice = totalNewPrice + row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST * row.NEW_PRICE.TARGET_VOLUME;
						//Page Header
						totalTargetCM1 = totalTargetCM1 + (row.PRICE_MASTER.DPRSPLCM1);

						totalCM1 = totalCM1 + (row.PRICE_MASTER.DPRSRSCM1);
						totalSalesGross = totalSalesGross + (row.PRICE_MASTER.DPRSRSGSA);
						totalSalesDeduction = totalSalesDeduction + (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD);

						gross_sales_exist_full = gross_sales_exist_full + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
							.DUNITQ *
							row.NEW_PRICE.TARGET_VOLUME;

						if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0 && row.PRICE_MASTER.DPRSCPAIN !== 0) {
							priceChangeAbsTableH = priceChangeAbsTableH + (row.NEW_PRICE.NEW_PRICE_PLAN - row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER
								.DCONDUNIT) / row.PRICE_MASTER.DUNITQ * row.NEW_PRICE.TARGET_VOLUME;
							gross_sales_new = gross_sales_new + (row.NEW_PRICE.NEW_PRICE_PLAN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
								.DUNITQ *
								row.NEW_PRICE.TARGET_VOLUME;

							gross_sales_exist = gross_sales_exist + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
								.DUNITQ *
								row.NEW_PRICE.TARGET_VOLUME;
						}
					} else {
						//condition 3.1
						if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) > 0) {
							totalVolume = totalVolume + (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY);
							// totalDPRSTMPRO = (row.PRICE_MASTER.DPRSTMPRO * row.NEW_PRICE.TARGET_VOLUME);
							// totalForecast = totalForecast + row.NEW_PRICE.TARGET_VOLUME;
							totalCM1_CurrentPrice = totalCM1_CurrentPrice + ((row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER
									.DPRSRSCM1YPY) /
								(row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY)) * row.NEW_PRICE.TARGET_VOLUME;
							totalCM1_TargetPrice = totalCM1_TargetPrice + row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE * row.NEW_PRICE.TARGET_VOLUME;
							totalNewPrice = totalNewPrice + row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST * row.NEW_PRICE.TARGET_VOLUME;
							//Page Header
							totalTargetCM1 = totalTargetCM1 + (row.PRICE_MASTER.DPRSPLCM1 + row.PRICE_MASTER.DPRSPLCM1PY - row.PRICE_MASTER.DPRSPLCM1YPY);

							totalCM1 = totalCM1 + (row.PRICE_MASTER.DPRSRSCM1 + row.PRICE_MASTER.DPRSRSCM1PY - row.PRICE_MASTER.DPRSRSCM1YPY);
							totalSalesGross = totalSalesGross + (row.PRICE_MASTER.DPRSRSGSA + row.PRICE_MASTER.DPRSRSGSAPY - row.PRICE_MASTER.DPRSRSGSAYPY);
							totalSalesDeduction = totalSalesDeduction + (row.PRICE_MASTER.DPRSSALDEC * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSSALDEPY *
								row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSSALDECYPY * row.PRICE_MASTER.DPRSVYTDPY);

							gross_sales_exist_full = gross_sales_exist_full + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row
								.PRICE_MASTER
								.DUNITQ *
								row.NEW_PRICE.TARGET_VOLUME;

							if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0 && row.PRICE_MASTER.DPRSCPAIN !== 0) {
								priceChangeAbsTableH = priceChangeAbsTableH + (row.NEW_PRICE.NEW_PRICE_PLAN - row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER
									.DCONDUNIT) / row.PRICE_MASTER.DUNITQ * row.NEW_PRICE.TARGET_VOLUME;
								gross_sales_new = gross_sales_new + (row.NEW_PRICE.NEW_PRICE_PLAN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
									.DUNITQ *
									row.NEW_PRICE.TARGET_VOLUME;

								gross_sales_exist = gross_sales_exist + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
									.DUNITQ *
									row.NEW_PRICE.TARGET_VOLUME;
							}

						} else {
							//condition 4.1
							if (row.PRICE_MASTER.DPRSVVJ > 0) {
								totalVolume = totalVolume + (row.PRICE_MASTER.DPRSVVJ);
								// totalDPRSTMPRO = (row.PRICE_MASTER.DPRSTMPRO * row.NEW_PRICE.TARGET_VOLUME);
								// totalForecast = totalForecast + row.NEW_PRICE.TARGET_VOLUME;
								totalCM1_CurrentPrice = totalCM1_CurrentPrice + ((row.PRICE_MASTER.DPRSRSCM1PY) /
									(row.PRICE_MASTER.DPRSVVJ)) * row.NEW_PRICE.TARGET_VOLUME;
								totalCM1_TargetPrice = totalCM1_TargetPrice + row.PRICE_MASTER.CM1_EUR_KG_TARGET_PRICE * row.NEW_PRICE.TARGET_VOLUME;
								totalNewPrice = totalNewPrice + row.PRICE_MASTER.CM1_EUR_KG_NEW_PRICE_HIST * row.NEW_PRICE.TARGET_VOLUME;
								//Page Header
								totalTargetCM1 = totalTargetCM1 + (row.PRICE_MASTER.DPRSPLCM1PY);

								totalCM1 = totalCM1 + (row.PRICE_MASTER.DPRSRSCM1PY);
								totalSalesGross = totalSalesGross + (row.PRICE_MASTER.DPRSRSGSAPY);
								totalSalesDeduction = totalSalesDeduction + (row.PRICE_MASTER.DPRSSALDEPY * row.PRICE_MASTER.DPRSVVJ);

								gross_sales_exist_full = gross_sales_exist_full + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) /
									row.PRICE_MASTER
									.DUNITQ *
									row.NEW_PRICE.TARGET_VOLUME;

								if (row.NEW_PRICE.NEW_PRICE_PLAN !== 0 && row.PRICE_MASTER.DPRSCPAIN !== 0) {
									priceChangeAbsTableH = priceChangeAbsTableH + (row.NEW_PRICE.NEW_PRICE_PLAN - row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER
										.DCONDUNIT) / row.PRICE_MASTER.DUNITQ * row.NEW_PRICE.TARGET_VOLUME;
									gross_sales_new = gross_sales_new + (row.NEW_PRICE.NEW_PRICE_PLAN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
										.DUNITQ *
										row.NEW_PRICE.TARGET_VOLUME;

									gross_sales_exist = gross_sales_exist + (row.PRICE_MASTER.DPRSCPAIN) / parseFloat(row.PRICE_MASTER.DCONDUNIT) / row.PRICE_MASTER
										.DUNITQ *
										row.NEW_PRICE.TARGET_VOLUME;
								}

							}
						}
					}

				}

			});

			priceChangePercTableH = (gross_sales_new - gross_sales_exist) / gross_sales_exist * 100;
			if (isNaN(priceChangePercTableH)) priceChangePercTableH = 0;

			totalDPRSTMPRO = totalDPRSTMPRO / totalForecast;

			if (resdata.length === 1) {
				//condition 1.2
				if (resdata[0].PRICE_MASTER.DPRSVATD > 0) {
					CM1PageHeaderPerc = (resdata[0].PRICE_MASTER.DPRSRSCM1ATD) /
						((resdata[0].PRICE_MASTER.DPRSRSGSATD) - (((resdata[0]
							.PRICE_MASTER.DPRSSALDECAT * resdata[0].PRICE_MASTER.DPRSVATD) / (resdata[0].PRICE_MASTER.DPRSVATD)) * (resdata[0].PRICE_MASTER
							.DPRSVATD))) * 100;
				} else {
					//condition 2.2
					if (resdata[0].PRICE_MASTER.DPRSVYTD > 0) {
						CM1PageHeaderPerc = (resdata[0].PRICE_MASTER.DPRSRSCM1) /
							((resdata[0].PRICE_MASTER.DPRSRSGSA) - (((resdata[0]
								.PRICE_MASTER.DPRSSALDEC * resdata[0].PRICE_MASTER.DPRSVYTD) / (resdata[0].PRICE_MASTER.DPRSVYTD)) * (resdata[0].PRICE_MASTER
								.DPRSVYTD))) * 100;

					} else {
						//condition 3.2
						if ((resdata[0].PRICE_MASTER.DPRSVYTD + resdata[0].PRICE_MASTER.DPRSVVJ - resdata[0].PRICE_MASTER.DPRSVYTDPY) > 0) {
							CM1PageHeaderPerc = (resdata[0].PRICE_MASTER.DPRSRSCM1 + resdata[0].PRICE_MASTER.DPRSRSCM1PY - resdata[0].PRICE_MASTER.DPRSRSCM1YPY) /
								((resdata[0].PRICE_MASTER.DPRSRSGSA + resdata[0].PRICE_MASTER.DPRSRSGSAPY - resdata[0].PRICE_MASTER.DPRSRSGSAYPY) - (((resdata[
										0]
									.PRICE_MASTER.DPRSSALDEC * resdata[0].PRICE_MASTER.DPRSVYTD + resdata[0].PRICE_MASTER.DPRSSALDEPY * resdata[0].PRICE_MASTER
									.DPRSVVJ -
									resdata[0].PRICE_MASTER.DPRSSALDECYPY * resdata[0].PRICE_MASTER.DPRSVYTDPY) / (resdata[0].PRICE_MASTER.DPRSVYTD + resdata[
										0]
									.PRICE_MASTER
									.DPRSVVJ - resdata[0].PRICE_MASTER.DPRSVYTDPY)) * (resdata[0].PRICE_MASTER.DPRSVYTD + resdata[0].PRICE_MASTER.DPRSVVJ -
									resdata[0].PRICE_MASTER.DPRSVYTDPY))) * 100;

						} else {
							//condition 4.2
							if (resdata[0].PRICE_MASTER.DPRSVVJ > 0) {
								CM1PageHeaderPerc = (resdata[0].PRICE_MASTER.DPRSRSCM1PY) /
									((resdata[0].PRICE_MASTER.DPRSRSGSAPY) - (((resdata[0]
										.PRICE_MASTER.DPRSSALDEPY * resdata[0].PRICE_MASTER.DPRSVVJ) / (resdata[0].PRICE_MASTER.DPRSVVJ)) * (resdata[0].PRICE_MASTER
										.DPRSVVJ))) *
									100;

							}
						}
					}

				}

			} else {
				CM1PageHeaderPerc = totalCM1 / (totalSalesGross - totalSalesDeduction) * 100;
			}

			deltaTargetCM1 = (totalCM1 - totalTargetCM1) / (totalSalesGross - totalSalesDeduction) * 100;

			// If totalForecast = 0 then show ‘---‘

			totalForecast = isNaN(totalForecast) ? 0 : totalForecast;
			totalCM1_CurrentPrice = isNaN(totalCM1_CurrentPrice) ? 0 :
				totalCM1_CurrentPrice;
			totalCM1_TargetPrice =
				isNaN(totalCM1_TargetPrice) ? 0 : totalCM1_TargetPrice;
			totalNewPrice = isNaN(totalNewPrice) ? 0 : totalNewPrice;
			//header
			CM1PageHeaderPerc = isNaN(CM1PageHeaderPerc) ? 0 : CM1PageHeaderPerc;
			deltaTargetCM1 = isNaN(deltaTargetCM1) ? 0 : deltaTargetCM1;

			var totalCM1_CurrentPriceNotValid = "",
				totalCM1_TargetPriceNotValid = "",
				totalNewPriceNotValid = "",
				CM1PageHeaderPercNotValid = "",
				totalTargetCM1PageHNotValid = "",
				deltaTargetCM1PageHNotValid = "",
				priceChangeAbsTableHNotValid = "",
				priceChangePercTableHNotValid = "";
			if (totalForecast === 0) {
				totalCM1_CurrentPriceNotValid = "---";
				totalCM1_TargetPriceNotValid = "---";
				totalNewPriceNotValid = "---";
				totalTargetCM1PageHNotValid = "---";
				deltaTargetCM1PageHNotValid = "---";
				priceChangeAbsTableHNotValid = "---";
				priceChangePercTableHNotValid = "---";

			}

			if (totalVolume === 0) {
				CM1PageHeaderPercNotValid = "---";
				deltaTargetCM1PageHNotValid = "---";
			}

			this._tenderHeaderM.setProperty("/forecastTableH", totalForecast);

			this._tenderHeaderM.setProperty("/targetPriceTableH", totalCM1_TargetPrice);
			this
				._tenderHeaderM.setProperty("/newPriceTableH", totalNewPrice);
			this._tenderHeaderM.setProperty("/priceChangeAbsTableH",
				priceChangeAbsTableH);
			this._tenderHeaderM.setProperty("/priceChangePercTableH", priceChangePercTableH);

			this._tenderHeaderM.setProperty("/totalCM1_CurrentPriceNotValid", totalCM1_CurrentPriceNotValid);
			this._tenderHeaderM.setProperty(
				"/totalCM1_TargetPriceNotValid", totalCM1_TargetPriceNotValid);
			this._tenderHeaderM.setProperty("/totalNewPriceNotValid",
				totalNewPriceNotValid);
			this._tenderHeaderM.setProperty("/priceChangeAbsTableHNotValid", priceChangeAbsTableHNotValid);
			this._tenderHeaderM
				.setProperty("/priceChangePercTableHNotValid", priceChangePercTableHNotValid);

			this._tenderHeaderM.setProperty("/CM1PageHeaderPercPageH", CM1PageHeaderPerc);
			this._tenderHeaderM.setProperty(
				"/CM1PageHeaderPercNotValid", CM1PageHeaderPercNotValid);
			this._tenderHeaderM.setProperty("/totalTargetCM1PageH", totalDPRSTMPRO);
			this
				._tenderHeaderM.setProperty("/totalTargetCM1PageHNotValid", totalTargetCM1PageHNotValid);
			this._tenderHeaderM.setProperty(
				"/deltaTargetCM1PageH", deltaTargetCM1);
			this._tenderHeaderM.setProperty("/deltaTargetCM1PageHNotValid",
				deltaTargetCM1PageHNotValid);
		},

		onSubmitOrChange: function (oEvent) {
			var oSource = oEvent.getSource(),
				id = oSource.getId().split("__")[1].split("-")[2],
				path = oSource.getBindingContext("tenderM").getPath(),
				savedData = this._globalM.getProperty("/savedTender"),
				activeLine = this._tenderM.getProperty(path),
				value = oSource.getValue(),
				saveBtnPressed = this.getView().byId("savePRBId")._buttonPressed,
				allCalculatedValues;
			if (oEvent.getId() === "submit") return;
			var bindValue = oEvent.getSource().getBindingInfo("value");
			var sName = "",
				sType = "";
			if (bindValue !== undefined || bindValue !== null) {
				sType = bindValue.type;
				if (sType !== undefined || sType !== null) {
					sName = sType.sName;
				}
			}

			if (sName === "Float" && oEvent.getParameter("newValue") === "") oSource.setValue("0");
			if (this._tenderM.getProperty(path + "/PRICE_MASTER/CHANGED") !== true) {
				savedData.push(this._tenderM.getProperty(path));
				this._tenderM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedTender", savedData);

			if (id === "newPricePlannedId" || id === "newPricePlannedId2") {
				allCalculatedValues = this.calculateNewPrices(value, activeLine);
				this._tenderM.setProperty(path, allCalculatedValues);
				this._checkApprovalIsNeeeded(path, saveBtnPressed);
				activeLine = this._updateScaleTableForMainViews(value, activeLine);
				this._tenderM.setProperty(path, activeLine);
				// var that = this;
				// var promiseForAppNeeded = new Promise(function (myResolve, myReject) {
				// 	that._checkApprovalIsNeeeded(path, saveBtnPressed, myResolve);
				// });
				// promiseForAppNeeded.then(
				// 	function (a) {

				// 		allCalculatedValues = that.calculateNewPrices(value, activeLine);
				// 		that._tenderM.setProperty(path, allCalculatedValues);
				// 		activeLine = that._updateScaleTableForMainViews(value, activeLine);
				// 		that._tenderM.setProperty(path, activeLine);
				// 		if (saveBtnPressed === true)
				// 			that.onSavePR();
				// 	},
				// 	function (error) { /* code if some error */ }
				// );

			} else {
				if (id === "targetVolumeId" || id === "targetVolumeId2") {
					this._checkApprovalIsNeeeded(path, saveBtnPressed);
				}
			}
		},

		onChangePriceValidty: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("tenderM").getPath(),
				savedData = this._globalM.getProperty("/savedTender"),
				line = this._tenderM.getProperty(path);
			if (this._tenderM.getProperty(path + "/PRICE_MASTER/CHANGED") !== true) {
				savedData.push(this._tenderM.getProperty(path));
				this._tenderM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedTender", savedData);
		},

		onChangeInput: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("tenderM").getPath(),
				savedData = this._globalM.getProperty("/savedTender"),
				line = this._tenderM.getProperty(path);
			if (this._tenderM.getProperty(path + "/PRICE_MASTER/CHANGED") !== true) {
				savedData.push(this._tenderM.getProperty(path));
				this._tenderM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedTender", savedData);
		},

		_checkApprovalIsNeeeded: function (path, saveBtnPressed, myResolve) {
			var that = this,
				sendingDataArray = [],
				sendingData = {},
				newPrice = [],
				thiz = this,
				line = this._tenderM.getProperty(path),
				DOESTAFKZ = line.PRICE_MASTER.DOESTAFKZ,
				hasRecord = line.NEW_PRICE.CUSTOMER !== "",
				deact = line.NEW_PRICE.DEACTIVATE_SCALE,
				priceMaster = [];
			var data = this._tenderM.getData();

			if (DOESTAFKZ !== "" && deact !== "X") {
				if (!hasRecord)
					line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DSCALQTY1;
			} else {
				if (hasRecord) {
					line.NEW_PRICE.MOQ_NEW = line.NEW_PRICE.MOQ_NEW;
				} else {
					line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DPRSMOQ;
				}
			}

			newPrice.push(line.NEW_PRICE);
			priceMaster.push(line.PRICE_MASTER);

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "APPROVEL_NEEDED"
				},
				"INPUTPARAMS": sendingDataArray
			};

			dbcontext.callServer(params, function (oModel) {
				that._tenderM.setProperty(path + "/NEW_PRICE/APP_NEEDED", oModel.getData().APPRO_NEEDED);
				that._tenderM.setProperty(path + "/NEW_PRICE/RFQ_NEEDED", oModel.getData().RFQ_NEEDED);
				that._tenderM.setProperty(path + "/NEW_PRICE/THRESHOLD_CLASS", oModel.getData().THRESHOLD_CLASS);
				// that._tenderM.setProperty(path + "/NEW_PRICE/ZAPPRCM1EXP", oModel.getData().EXPECTED_PERC);
				// if (myResolve !== undefined)
				// 	myResolve();
				// else {
				if (saveBtnPressed === true)
					that.onSavePR();
				// }
			}, this);
		},

		onCancelPR: function () {
			var table = this._table;
			var oldData = this._globalM.getProperty("/oldData");
			var oldData2 = $.extend(true, [], oldData);
			sap.ui.getCore().getModel("tenderM").setData(oldData2);
			this._globalM.setProperty("/savedTender", []);
			this._getKPIsValues(oldData2);
			table.setSelectedIndex(-1);
		},

		onSavePR: function (name, myResolve, myReject) {
			var sendingDataArray = [],
				sendingData = {},
				newPrice = [],
				thiz = this,
				that = this,
				priceMaster = [],
				variantMng = this.oCC_item.getItemValue(this.tableId2)[this.getView().byId("tableVMId_item").getSelectionKey()].tableData;
			var data = this._tenderM.getData();
			var savedData = this._globalM.getProperty("/savedTender");

			if (savedData.length === 0) {
				MessageBox.warning("There is no changed line!");
				return;
			}

			for (var i = 0; i < savedData.length; i++) {
				newPrice.push(savedData[i].NEW_PRICE);
				priceMaster.push(savedData[i].PRICE_MASTER);
			}

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingData.LV_ROL = this._globalM.getProperty("/ROLEINDEX");
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_PRC_MASTER"
				},
				"INPUTPARAMS": sendingDataArray
			};

			dbcontext.callServer(params, function (oModel) {
				thiz.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						thiz._globalM.setProperty("/savedDetailPage", true);
						if (myResolve === undefined || myResolve === null)
							if (variantMng === undefined)
								thiz._getDetail(thiz._globalM.getProperty("/urlData"), "", "S"); //done
							else
								thiz._getDetail(variantMng, "X", "S"); //done
						else {
							myResolve();
						}

					}
				});
			}, this);

		},

		onPressOverview: function () {
			this._globalM.setProperty("/previousPage", 1);
			this.getRouter().navTo("Main");
		},

		_getControlsFromView: function () {
			this._table = this.getView().byId("tenderTableId");
			this._globalM = sap.ui.getCore().getModel("globalModel");
			this._tenderM = sap.ui.getCore().getModel("tenderM");
			this._tenderCMM = sap.ui.getCore().getModel("tenderCMM");
			this._tenderHeaderM = sap.ui.getCore().getModel("tenderHeaderM");
			this._recipientM = sap.ui.getCore().getModel("recipientM");
			this._psresultM = sap.ui.getCore().getModel("psresultM");
		},

		_bindModels: function () {
			var that = this;
			var aModels = ["tenderM", "tenderHeaderM", "psresultM",
				"tenderCMM", "globalModel", "recipientM", "buActionDD", "priceActionDD"
			];
			aModels.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
		},

		onCustomerPrintOut: function (oEvent) {
			var table = this._table,
				sServiceUrl = this.getOwnerComponent().getModel("Pricing").sServiceUrl,
				that = this,
				newLink, link, hostPort, sSource,
				context, params,
				path, row,
				sendingData = [],
				aSelIndex = table.getSelectedIndices(),
				aIndices = table.getBinding().aIndices,
				newSelIndex = [];
			var userId = sap.ushell.Container.getUser().getId();
			oEvent.getSource().setPressed(true);
			if (!aSelIndex.length) {
				sap.m.MessageToast.show("Select at least one record");
				return;
			} else {
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});

				aSelIndex = newSelIndex;
				aSelIndex.forEach(function (index, ind) {
					row = that._tenderM.getData()[index];
					//path = context.getPath();
					//row = context.getProperty(path);
					row.PRICE_MASTER.DEACTIVATE_SCALE = row.NEW_PRICE.DEACTIVATE_SCALE;

					sendingData.push(row.PRICE_MASTER);

				});
			}

			if (userId === "EX_YILMAZO") {
				params = {
					"HANDLERPARAMS": {
						"FUNC": "PRINT"
					},
					"INPUTPARAMS": sendingData
				};
				dbcontext.callServer(params, function (oModel) {
					link = oModel.getData().LINK;
					hostPort = oModel.getData().HOST_PORT;
					customer = oModel.getData().PDF;

					hostPort = hostPort.split("/sap/bc")[0];
					link = hostPort + "/sap/public/" + link.split("/sap/public/")[1];

					sSource = link;
					that._pdfViewer = new sap.m.PDFViewer({
						sourceValidationFailed: function (e) {
							e.preventDefault();
						},
						error: function (e) {
							e.preventDefault();
						}

					});
					that.getView().addDependent(that._pdfViewer);
					var url = sServiceUrl + "/TenderPDFSet(CUSTOMER='" + customer + "')/$value";
					that._pdfViewer.setSource(url);
					that._pdfViewer.open();

				}, this);
			} else {
				that._pdfViewer = new sap.m.PDFViewer({
					sourceValidationFailed: function (e) {
						e.preventDefault();
					},
					error: function (e) {
						e.preventDefault();
					}

				});
				var customer = sendingData.map(function (m) {
					return m.CUSTOMER;
				}).join(",");
				var ship_to = sendingData.map(function (m) {
					return m.SHIP_TO;
				}).join(",");
				var comp_code = sendingData.map(function (m) {
					return m.COMP_CODE;
				}).join(",");
				var salesorg = sendingData.map(function (m) {
					return m.SALESORG;
				}).join(",");
				var material = sendingData.map(function (m) {
					return m.MATERIAL;
				}).join(",");
				var dist_chnl = sendingData.map(function (m) {
					return m.DISTR_CHAN;
				}).join(",");

				that.getView().addDependent(that._pdfViewer);
				var url = sServiceUrl + "/TenderPDFSet(CUSTOMER='" + customer + "',SHIP_TO='" + ship_to + "',COMP_CODE='" + comp_code +
					"',SALESORG='" + salesorg + "',DISTR_CHAN='" + dist_chnl + "',MATERIAL='" + material + "')/$value";
				that._pdfViewer.setSource(url);
				that._pdfViewer.open();
			}

		},

		goToCreatePriceRequestPage: function (oEvent) {
			var table = this._table,
				createRequestLink =
				"/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html#zdoehlersempr-display?tag=1&/CreateRequest",
				stringfyJSON,
				aSelIndex = table.getSelectedIndices(),
				aIndices = table.getBinding().aIndices,
				newSelIndex = [],
				context,
				path,
				customerNumber,
				filteredCustomer = [],
				row,
				that = this,
				createPriceReqItems = [];

			createRequestLink = location.host + createRequestLink;

			if (!aSelIndex.length) {
				MessageToast.show("Select at least one record");
			} else {
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});

				aSelIndex = newSelIndex;
				aSelIndex.forEach(function (index, ind) {
					row = that._tenderM.getData()[index];
					//path = context.getPath();
					//row = context.getProperty(path);
					createPriceReqItems.push({
						Customer: row.PRICE_MASTER.CUSTOMER,
						ArticleNumber: row.PRICE_MASTER.MATERIAL,
						Title: "Pricing Master File - Price Renewal"
					});
				});

				if (createPriceReqItems.length > 1) {
					customerNumber = createPriceReqItems[0].Customer;
					filteredCustomer = createPriceReqItems.filter(function (line) {
						return line.Customer === customerNumber;
					});
					if (filteredCustomer.length < createPriceReqItems.length) {
						MessageBox.warning("Please select only one customer to create a price request");
						return;
					}
				}

				table.setSelectedIndex(-1);
				stringfyJSON = JSON.stringify(createPriceReqItems);
				createRequestLink = location.protocol + "//" + createRequestLink;
				createRequestLink += "?CustomerArticles=" + encodeURI(stringfyJSON);
				sap.m.URLHelper.redirect(createRequestLink, true);
			}
		},

		//send quotation
		onSendQuotation: function (oEvent) {
			var table = this._table,
				data = [],
				that = this;
			data = this._getSendingDataForQuotation(table, null);

			if (data === undefined) return;
			var thiz = this,
				params;

			var promiseForSave = new Promise(function (myResolve, myReject) {
				that.onSavePR(undefined, myResolve, myReject);
			});
			var thiz = this,
				params;

			promiseForSave.then(
				function (value) {
					jQuery.sap.delayedCall(500, thiz, function () {
						params = {
							"HANDLERPARAMS": {
								"FUNC": "SEND_QUOT"
							},
							"INPUTPARAMS": data.data
						};
						dbcontext.callServer(params, function (oModel) {
							thiz.handleServerMessages(oModel, function (status) {
								if (status === "S") {
									thiz._globalM.setProperty("/savedDetailPage", true);
								}
							});
						}, thiz);
					});
				},
				function (error) { /* code if some error */ }
			);

		},

		//send approval
		onSendApproval: function (oEvent) {
			var data = [],
				sText,
				table = this._table;

			data = this._getSendingDataForApproval(table, null);
			if (data === undefined) return;
			if (data.isAppNeededInitial) {

				if (!this.oSubmitDialog) {
					this.oSubmitDialog = new Dialog({
						type: DialogType.Message,
						title: "Confirm",
						content: [
							new Label({
								text: "Why you want to start the workflow?",
								labelFor: "submissionNoteTenderTender"
							}),
							new TextArea("submissionNoteTender", {
								maxLength: 200,
								width: "100%",
								placeholder: "Add note (required)",
								liveChange: function (oEvent) {
									sText = oEvent.getParameter("value");
									this.oSubmitDialog.getBeginButton().setEnabled(sText.length > 0);
								}.bind(this)
							})
						],
						beginButton: new Button({
							type: ButtonType.Emphasized,
							text: "Submit",
							enabled: false,
							press: function () {
								sText = Core.byId("submissionNoteTender").getValue();
								data.data.forEach(function (line) {
									if (line.APP_NEEDED === "")
										line.APP_NEEDED_DESC = sText;
								});
								this.oSubmitDialog.close();
								this._callSendApprovalFunc(data);
							}.bind(this)
						}),
						endButton: new Button({
							text: "Cancel",
							press: function () {
								this.oSubmitDialog.close();
							}.bind(this)
						})
					});
				} else
					Core.byId("submissionNoteTender").setValue("");

				this.oSubmitDialog.open();

			} else
				this._callSendApprovalFunc(data);

		},

		_callSendApprovalFunc: function (data) {
			var thiz = this,
				params,
				variantMng = this.oCC_item.getItemValue(this.tableId2)[this.getView().byId("tableVMId_item").getSelectionKey()].tableData;

			if (data.dataForDacman.length) {
				if (!data.isInitialWorkflow) {
					MessageBox.warning("You can not select the lines that have In progress, Approved or Rejected workflow status!");
				} else {
					params = {
						"HANDLERPARAMS": {
							"FUNC": "GET_DACCMAN_TO_USER"
						},
						"INPUTPARAMS": data.dataForDacman
					};
					dbcontext.callServer(params, function (oModel) {
						thiz.handleServerMessages(oModel, function (status) {
							if (status === "S") {
								var resultData = oModel.getData().OUTPUTDATA,
									lineResult = [];

								data.dataForNotApproval.forEach(function (itm) {
									lineResult = resultData.filter(function (resLine) {
										return resLine.C_DACCMAN === itm.PRICE_MASTER.C_DACCMAN;
									});
									if (lineResult.length)
										itm.PRICE_MASTER.RECIPIENT = lineResult[0].USRID;

								});

								thiz._createMailDialogForApproval(thiz, data.dataForNotApproval, "sendAppId");
							}
						});
					}, this);
				}
			}
			if (!data.data.length) return;
			jQuery.sap.delayedCall(500, this, function () {
				params = {
					"HANDLERPARAMS": {
						"FUNC": "SEND_APPROVAL"
					},
					"INPUTPARAMS": data.data
				};
				dbcontext.callServer(params, function (oModel) {
					thiz.handleServerMessages(oModel, function (status) {
						if (status === "S") {
							thiz._globalM.setProperty("/savedDetailPage", true);
							if (variantMng === undefined)
								thiz._getDetail(thiz._globalM.getProperty("/urlData"), "", "S"); //done
							else
								thiz._getDetail(variantMng, "X", "S"); //done
						}
					});
				}, this);
			});
		},
		//ex_dogus: open dialog t show cost data
		onPressCostChange: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("tenderM").getPath(),
				line = sap.ui.getCore().getModel("tenderM").getProperty(path);
			this._openCostDrivers(line, oEvent, this);
		},

		/*-------------------- START OF VARIANT MANAGEMENT --------------------------- */
		oCC: null,
		oCC_item: null,
		currTableData: null,
		currTableData_item: null,
		oTPC: null,
		oTPC_item: null,
		tableId: "tenderTableId",
		tableId2: "tenderTableId2",
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz.tableId);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var oVM = this.getView().byId("tableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(tableId, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(tableId);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		initVariant_item: function () {
			var thiz = this;
			var tableId = this.tableId2;
			var oTable = this.getView().byId(this.tableId2);
			var oVM = this.getView().byId("tableVMId_item");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(tableId, function (oCC_item) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC_item = oCC_item;
				var oItem = oCC_item.getItemValue(tableId);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData_item = oItem[oItem.defaultKey].data; // apply first data to currTableData_item
				if (oItem[oItem.defaultKey].tableData !== undefined) {

				}

				// oTable.setVisibleRowCountMode("Fixed");
				// oTable.setVisibleRowCount(oItem[oItem.defaultKey].rowCount === undefined ? 5 : oItem[oItem.defaultKey].rowCount);
				// oTable.setVisibleRowCountMode(oItem[oItem.defaultKey].rowCount === undefined ? "Interactive" : "Fixed");

				// attach perso to get current change data
				thiz.oTPC_item = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData_item = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data
		},

		onSelectVariant_item: function (oEvent) {
			var tableId = this.tableId2;
			var oTable = this.getView().byId(this.tableId2);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC_item.getItemValue(tableId);
			var data;
			this.setPersoData(oTable, oItem[selKey].data); // apply data

			if (oItem[selKey].tableData && oItem[selKey].tableData.length && oItem[selKey].tableData.length > 0) {
				this._getDetail(oItem[selKey].tableData, "X", "S"); //done
			} else {
				data = this._globalM.getProperty("/oldData");
				data.forEach(function (m) {
					m.PRICE_MASTER.CHANGED = false;
				});
				this._tenderM.setData(data);
				this._globalM.setProperty("/savedTender", []);
			}

			// oTable.setVisibleRowCountMode("Fixed");
			// oTable.setVisibleRowCount(oItem[selKey].rowCount === undefined ? 6 : oItem[selKey].rowCount);
			// oTable.setVisibleRowCountMode(oItem[selKey].rowCount === undefined ? "Interactive" : "Fixed");

		},

		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};
				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},

		/* on save new variant */
		onSaveVariant_item: function (oEvent) {
			var thiz = this;
			var oCC_item = this.oCC_item;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this.tableId2;
			var oTable = this.getView().byId(this.tableId2);

			// 2 check if varName is available or not if not then create
			var ovar = oCC_item.getItemValue(tableId);
			if (this.currTableData_item) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData_item,
					tableData: thiz._globalM.getProperty("/urlData")
				};
				oCC_item.setItemValue(tableId, ovar); // set updated obj 
				oCC_item.save();
			}
		},

		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this.tableId;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},

		onManageVM_item: function (oEvent) {
			var oCC_item = this.oCC_item;
			var tableId = this.tableId2;
			var ovar = oCC_item.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC_item.setItemValue(tableId, ovar);
			oCC_item.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},

		loadDropdown: function () {
			models.loadRootCause();
			models.loadPricingAction();
			models.loadBUAction();
			models.loadWorkFlowStatus();
		},

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			this.loadDropdown();
			this.afterRender = true;
			this.initVariant();

		}
	});

});