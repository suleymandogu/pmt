sap.ui.define([
	"sap/m/MessageBox",
	"sap/ui/model/json/JSONModel",
	"sap/m/Dialog",
	"sap/m/MessageToast"
], function (MessageBox, JSONModel, Dialog, MessageToast) {
	"use strict";

	return {
		/*
		 * Handle all type of messages comes from server - Exception or Normal Messages 
		 */
		handleMessages: function (oController, oMessageData, fnOnClose, isServerException, showSuccessMessage) {
			this._oController = oController;
			this._oMessageData = oMessageData;
			this._isServerException = isServerException;
			this._fnOnClose = fnOnClose;

			if (showSuccessMessage === undefined) {
				showSuccessMessage = true;
			}
			this._showSuccessMessage = showSuccessMessage;

			if (this._isServerException) {
				this._handleExceptionMessages(this._oMessageData);
			} else
				this._handleNormalMessages(this._oMessageData);
		},

		/*
		 * Handle exceptions from error event of ajax call to the server
		 */
		_getTimeOutMsg: function (oData, st) {
			var isTimeOut = false;
			if (oData !== undefined && oData !== "" && oData !== null) {
				if (oData.responseText !== undefined && oData.responseText !== "" && oData.responseText !== null) {
					if (oData.responseText.indexOf('Session Timed Out') > -1 || oData.responseText.indexOf('Connection Timed Out') > -1)
						isTimeOut = true;
				}
			}

			return isTimeOut;
		},

		/*
		 * Handle exceptions from error event of ajax call to the server
		 */
		_handleExceptionMessages: function (oData, st) {
			var thiz = this;
			var errorMForTimeOut = this._getTimeOutMsg(oData);
			MessageBox.error(errorMForTimeOut ? "Timed Out" : "There is an error contacting server", {
				id: "serverExceptionMessageBox",
				title: "Error " + oData.status,
				details: oData.statusText + (oData.status === 0 ? "- Please check your internet connection..." : ""),
				actions: [MessageBox.Action.CLOSE],
				onClose: function () {
					if (thiz._fnOnClose)
						thiz._fnOnClose();
				}
			});
		},

		/*
		 * Handle response messages from the normal ajax call
		 */
		_handleNormalMessages: function (oData) {
			var messageModel = this._getMessageModel(oData);
			//
			var temp1 = oData.getData();
			if (temp1.hasOwnProperty("MESSAGES") && temp1.MESSAGES.STATUS === "S" && temp1.MESSAGES.TEXTS.length === 1 && temp1.MESSAGES.TEXTS[0]
				.MSGTYP === "S") {

				if (temp1.MESSAGES.TEXTS[0].MSG === "RPA Bot has been started") {
					MessageBox.information("BOT has been started. You will receive an email when BOT is finished");
				} else
					MessageToast.show(temp1.MESSAGES.TEXTS[0].MSG);
				this._fnOnClose(messageModel.getProperty("/Status"));
				return;
			}
			//
			if (!this._showSuccessMessage && messageModel.getProperty("/Status") === "S") {
				this._fnOnClose(messageModel.getProperty("/Status"));
				return;
			}

			var thiz = this;

			if (temp1.ROWS !== undefined && temp1.ROWS.length) {

				temp1.MESSAGES.TEXTS.forEach(function (msg) {
					var itemNo = parseInt(msg.ITEM_NO);
					msg.MSG = "Item " + itemNo.toString() + " : " + msg.MSG;
				});
				oData.getData().MESSAGES.TEXTS = temp1.MESSAGES.TEXTS;
				var messageModel = this._getMessageModel(oData);

			}

			//this._oController.getComponent().setModel(messageModel, "serverMessageModel");
			this._oController.getOwnerComponent().setModel(messageModel, "serverMessageModel");

			var dialog = new Dialog({
				title: "Log Display",
				content: sap.ui.xmlview({
					viewName: "com.doehler.PricingMasterFile.view.ServerMessages"
				}),
				endButton: new sap.m.Button({
					text: "Close",
					press: function () {
						dialog.close();
						if (thiz._fnOnClose)
							thiz._fnOnClose(messageModel.getProperty("/Status"));
					}
				}),

				afterClose: function () {
					dialog.destroy();
				}
			}).addStyleClass("sapUiSizeCompact");

			this._oController.getView().addDependent(dialog);
			dialog.open();
		},

		/*
		 * Prepare the message model being used by Server Message Dialog Box
		 */
		_getMessageModel: function (oModel) {
			var oResult = {
				Status: oModel.getProperty("/MESSAGES/STATUS"),
				Count: {
					All: 0,
					Success: 0,
					Warning: 0,
					Error: 0,
					Info: 0
				},
				Messages: []
			};

			var oMessages = oModel.getProperty("/MESSAGES/TEXTS");

			oMessages.forEach(function (item) {
				oResult.Count.All++;

				function getTypeText(oType) {
					var res = "";

					switch (oType) {
					case "E":
						{
							res = "error";oResult.Count.Error++;
						}
						break;
					case "W":
						{
							res = "warning";oResult.Count.Warning++;
						}
						break;
					case "I":
						{
							res = "information";oResult.Count.Info++;
						}
						break;
					case "S":
						{
							res = "success";oResult.Count.Success++;
						}
						break;
					default:
						break;
					}
					return res;
				}
				oResult.Messages.push({
					Icon: "sap-icon://message-" + getTypeText(item.MSGTYP),
					Code: item.MSGCODE,
					Type: item.MSGTYP,
					Desc: item.MSG
				});
			});
			return new JSONModel(oResult);
		}
	}
});