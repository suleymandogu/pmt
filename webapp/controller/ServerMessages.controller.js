/* Developer Süleyman Doğu */
sap.ui.define([
		"com/doehler/PricingMasterFile/controller/BaseController",
		"sap/ui/model/Filter",
		"sap/ui/model/FilterOperator"
	],
	function (BaseController, Filter, FilterOperator) {
		"use strict";
		return BaseController.extend("com.doehler.PricingMasterFile.controller.ServerMessages", {
			onInit: function () {
				//BaseController.prototype.onInit.apply(this, arguments);
			},
			onFilterServerMessageClick: function (oEvent) {
				var filterType = oEvent.getParameter("id").slice(-1);
				if (filterType) this._filterServerMessageDialog(filterType);
			},
			_filterServerMessageDialog: function (filterType) {
				var aFilter = [];
				if (filterType !== "A") {
					aFilter.push(new Filter("Type", FilterOperator.EQ, filterType));
				}
				var filters = [];
				if (aFilter.length > 0)
					filters = new Filter(aFilter, false);
				//var oBinding = this.getObj("tblServerMessages").getBinding("items");
				var oBinding = this.getView().byId("tblServerMessages").getBinding("items");
				oBinding.filter(filters);
			}
		});
	});