sap.ui.define([
	"com/doehler/PricingMasterFile/controller/BaseController",
	"com/doehler/PricingMasterFile/model/base",
	"com/doehler/PricingMasterFile/model/models",
	"com/doehler/PricingMasterFile/model/formulas",
	"com/doehler/PricingMasterFile/model/formatter",
	"com/doehler/PricingMasterFile/excel/excel",
	"com/doehler/PricingMasterFile/model/dbcontext",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment",
	"sap/base/Log",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/Dialog",
	"sap/m/DialogType",
	"sap/m/Button",
	"sap/m/ButtonType",
	"sap/m/Label",
	"sap/m/TextArea",
	"sap/ui/core/Core",
	"sap/m/Text"
], function (BaseController, base, models, formulas, formatter, excel, dbcontext, MessageToast, MessageBox, Fragment, Log, Filter,
	FilterOperator,
	Dialog,
	DialogType, Button, ButtonType, Label, TextArea, Core, Text) {
	"use strict";

	return BaseController.extend("com.doehler.PricingMasterFile.controller.Kpis.Kpis", {
		formatter: formatter,

		onInit: function () {
			this._bindModels();
			this._defineModelsAndControls();
			this._setButtonsPressed(this._globalM, "kpisBtnId");
			this.getRouter().getRoute("Kpis").attachPatternMatched(this._onRouteMatched, this);
			this._setFieldsFromLink();
			this._getFilter();
			sap.ui.getCore().getModel("globalModel").setProperty("/periodSelectedIndex", 0);
		},

		//ex_dogus: route matched
		_onRouteMatched: function (oEvent) {
			if (location.hash.indexOf("&/List") > -1) {
				this.getRouter().navTo("List", {
					query: {
						tab: location.hash.split("/List?tab=")[1]
					}
				}, true);
				return;
			}

			var refresh = this._globalM.getProperty("/savedDetailPage");

			this._setButtonsPressed(this._globalM, "kpisBtnId");
			this._viewUpdate("mainPage");

			if (refresh === true) {
				if (this._resultM.getData().length > 0)
					if (base.previousPage !== "PriceSetting") {
						if (base.previousPage === "Tender") {
							// var tenderData = sap.ui.getCore().getModel("tenderM").getData();
							// var that = this;
							// tenderData.forEach(function (tenderLine) {
							// 	var line = that._resultM.getData().filter(function (resultLine, ind) {
							// 		return resultLine.PRICE_MASTER.CUSTOMER === tenderLine.PRICE_MASTER.CUSTOMER &&
							// 			resultLine.PRICE_MASTER.SHIP_TO === tenderLine.PRICE_MASTER.SHIP_TO &&
							// 			resultLine.PRICE_MASTER.SALESORG === tenderLine.PRICE_MASTER.SALESORG &&
							// 			resultLine.PRICE_MASTER.DISTR_CHAN === tenderLine.PRICE_MASTER.DISTR_CHAN &&
							// 			resultLine.PRICE_MASTER.MATERIAL === tenderLine.PRICE_MASTER.MATERIAL;
							// 	});

							// });
							this.onGo();
						} else
							this.onGo();
					} else this._fillTable(this._resultM.getData());

			}
		},

		//ex_dogus: create variables for table and models
		_defineModelsAndControls: function () {
			this._filterM = sap.ui.getCore().getModel("psfilterM");
			this._resultM = sap.ui.getCore().getModel("psresultM");
			this._recipientM = sap.ui.getCore().getModel("recipientM");
			this._globalM = sap.ui.getCore().getModel("globalModel");
			this._filterMKpis = sap.ui.getCore().getModel("psfilterMKpis");

		},

		//ex_dogus:bind models to this controller
		_bindModels: function () {
			var that = this;
			var aModels = ["psfilterM", "psresultM", "globalModel", "tenderM", "recipientM", "psfilterMKpis"];
			aModels.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
			var aDropdown = ["rootCauseDD", "priceActionDD", "buActionDD", "workFlowStatusDD"];
			aDropdown.forEach(function (item) {
				that.getView().setModel(sap.ui.getCore().getModel(item), item);
			});
		},

		onSelectPeriod: function (oEvent) {
			var that = this;
			var resdata = that._globalM.getProperty("/resDataKpis");
			var text = oEvent.getSource().getSelectedButton().getText();
			//sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnText", text);

			var cm1Text = "";
			switch (text) {
			case "Since last price validity":
				cm1Text = "Since last PA";
				break;
			case "YTD":
				cm1Text = "YTD";
				break;
			case "Last 12 months":
				cm1Text = "L12M";
				break;
			case "Full last year":
				cm1Text = "PY";
				break;
			default:
			}

			//sap.ui.getCore().getModel("globalModel").setProperty("/rdBtnTextCM1", cm1Text);
			//that._fillTable(resdata);
			that._calculateValues(resdata);
		},

		//ex_dogus: get parameters from the url to set the field
		_setFieldsFromLink: function () {
			var search = window.location.search,
				oView = this.getView(),
				customer = "",
				customerInput,
				material;
			if (window.location.host === "webidetesting0128379-a610cbd7a.dispatcher.hana.ondemand.com") return;
			if (search === "") {
				var hash = window.location.hash;
				if (hash === "" || hash === "#zpriceplanning-display") return;
				else search = hash;
			}
			if (search.split("Customer=").length > 1) {
				customer = search.split("Customer=")[1].split("&")[0].split(";");
				customerInput = oView.byId("custMIdPSI");
				customer.forEach(function (line) {
					customerInput.addToken(new sap.m.Token({
						key: line,
						text: line
					}));
				});
			}

			if (search.split("OpportunityID=").length > 1)
				material = search.split("OpportunityID=")[1].split("&")[0].split(";");
			if (customer !== "")
				this.onGo();
		},

		//ex_dogus: get some needed filters below to fill table once the page is opened.
		_getFilter: function () {
			var thiz = this,
				userId = sap.ushell.Container.getUser().getId(),
				params,
				data,
				globalRes = [],
				customerServ = [],
				accountManager = [],
				customerServiceInput = this.getView().byId("priceSettingUCSInputKpisId"),
				accountManagerInput = this.getView().byId("priceSettingUAMInputKpisId"),
				globalRespInput = this.getView().byId("priceSettingUGRInputKpisId"),
				role = sap.ui.getCore().getModel("globalModel").getData().UIROLE;

			if (role !== "SALESM" || userId === "MICHALB") return;

			params = {
				"HANDLERPARAMS": {
					"FUNC": "GET_FILTER"
				}
			};
			dbcontext.callServer(params, function (oModel) {

				data = oModel.getData();
				globalRes = data.ZGLOBEL;
				accountManager = data.TVKGR;
				customerServ = data.T171;

				customerServ.forEach(function (line) {
					customerServiceInput.addToken(new sap.m.Token({
						key: line.BZIRK,
						text: line.BZIRK
					}));
				});
				accountManager.forEach(function (line) {
					accountManagerInput.addToken(new sap.m.Token({
						key: line.VKGRP,
						text: line.VKGRP
					}));
				});
				globalRes.forEach(function (line) {
					globalRespInput.addToken(new sap.m.Token({
						key: line.KVGR4,
						text: line.KVGR4
					}));
				});
				if (customerServ.length > 0 || accountManager.length > 0 || globalRes.length > 0) {
					thiz._globalM.setProperty("/AutoS", true);
					thiz.onGo();
				}

			}, this);
		},

		//ex_dogus: loading dropdown list from the backend
		loadDropdown: function () {
			models.loadRootCause();
			models.loadPricingAction();
			models.loadBUAction();
			models.loadWorkFlowStatus();
		},

		//ex_dogus: the function which is triggered when any textArea comment fields are changed
		onChangeComment: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("psresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPriceSetting"),
				line = this._resultM.getProperty(path);
			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPriceSetting", savedData);
		},

		//ex_dogus: the function which is triggered when any combobox fields are changed
		selectionChangeRow: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("psresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPriceSetting"),
				line = this._resultM.getProperty(path);
			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}

			if (oEvent.getSource().getCustomData().length) {
				if (oEvent.getSource().getCustomData()[0].getValue()) {
					this._resultM.setProperty(path + "/PRICE_MASTER/" + oEvent.getSource().getCustomData()[0].getValue(), oEvent.getParameter(
						"selectedItem").getText());
				}
			}

			this._globalM.setProperty("/savedPriceSetting", savedData);
		},

		//ex_dogus: the function which is triggered when any date fields are changed
		onChangePriceValidty: function (oEvent) {
			var oSource = oEvent.getSource(),
				path = oSource.getBindingContext("psresultM").getPath(),
				savedData = this._globalM.getProperty("/savedPriceSetting"),
				line = this._resultM.getProperty(path);
			if (line.PRICE_MASTER.CHANGED !== true) {
				savedData.push(line);
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPriceSetting", savedData);
		},

		//ex_dogus: the function which is triggered when any input fields are changed
		onChangeInput: function (oEvent) {
			var savedData = this._globalM.getProperty("/savedPriceSetting"),
				oSource = oEvent.getSource(),
				path = oSource.getBindingContext("psresultM").getPath();
			if (this._resultM.getProperty(path + "/PRICE_MASTER/CHANGED") !== true) {
				savedData.push(this._resultM.getProperty(path));
				this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
			}
			this._globalM.setProperty("/savedPriceSetting", savedData);
		},

		//ex_dogus: the function which is triggered when user enters a value for nex price or target volume
		// onSubmitOrChange: function (oEvent) {
		// 	var oSource = oEvent.getSource(),
		// 		id = oSource.getId().split("__")[1].split("-")[2],
		// 		path = oSource.getBindingContext("psresultM").getPath(),
		// 		savedData = this._globalM.getProperty("/savedPriceSetting"),
		// 		activeLine = this._resultM.getProperty(path),
		// 		value = oSource.getValue(),
		// 		allCalculatedValues,
		// 		saveBtnPressed = this.getView().byId("savePRBId")._buttonPressed,
		// 		line = this._resultM.getProperty(path);

		// 	var bindValue = oEvent.getSource().getBindingInfo("value");
		// 	var sName = "",
		// 		sType = "";
		// 	if (bindValue !== undefined || bindValue !== null) {
		// 		sType = bindValue.type;
		// 		if (sType !== undefined || sType !== null) {
		// 			sName = sType.sName;
		// 		}
		// 	}

		// 	if (sName === "Float" && oEvent.getParameter("newValue") === "") oSource.setValue("0");

		// 	if (this._resultM.getProperty(path + "/PRICE_MASTER/CHANGED") !== true) {
		// 		savedData.push(line);
		// 		this._resultM.setProperty(path + "/PRICE_MASTER/CHANGED", true);
		// 	}
		// 	this._globalM.setProperty("/savedPriceSetting", savedData);
		// 	if (id === "newPricePlannedId" || id === "newPricePlannedId2") {
		// 		allCalculatedValues = this.calculateNewPrices(value, activeLine);
		// 		this._resultM.setProperty(path, allCalculatedValues);
		// 		this._checkApprovalIsNeeeded(path, saveBtnPressed);
		// 		activeLine = this._updateScaleTableForMainViews(value, activeLine);
		// 		this._resultM.setProperty(path, activeLine);
		// 	} else {
		// 		if (id === "targetVolumeId" || id === "targetVolumeId2" || id === "moq1" || id === "moq2") {
		// 			this._checkApprovalIsNeeeded(path, saveBtnPressed);
		// 		}
		// 	}
		// },

		//ex_dogus: call backend function to update 'approval needed' field
		_checkApprovalIsNeeeded: function (path, saveBtnPressed) {
			var that = this,
				sendingDataArray = [],
				sendingData = {},
				newPrice = [],
				line = this._resultM.getProperty(path),
				DOESTAFKZ = line.PRICE_MASTER.DOESTAFKZ,
				hasRecord = line.NEW_PRICE.CUSTOMER !== "",
				deact = line.NEW_PRICE.DEACTIVATE_SCALE,
				priceMaster = [];

			if (DOESTAFKZ !== "" && deact !== "X") {
				if (!hasRecord)
					line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DSCALQTY1;
			} else {
				if (hasRecord) {
					line.NEW_PRICE.MOQ_NEW = line.NEW_PRICE.MOQ_NEW;
				} else {
					line.NEW_PRICE.MOQ_NEW = line.PRICE_MASTER.DPRSMOQ;
				}
			}

			newPrice.push(line.NEW_PRICE);
			priceMaster.push(line.PRICE_MASTER);

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "APPROVEL_NEEDED"
				},
				"INPUTPARAMS": sendingDataArray
			};

			dbcontext.callServer(params, function (oModel) {
				that._resultM.setProperty(path + "/NEW_PRICE/APP_NEEDED", oModel.getData().APPRO_NEEDED);
				that._resultM.setProperty(path + "/NEW_PRICE/RFQ_NEEDED", oModel.getData().RFQ_NEEDED);
				that._resultM.setProperty(path + "/NEW_PRICE/THRESHOLD_CLASS", oModel.getData().THRESHOLD_CLASS);
				that._resultM.setProperty(path + "/NEW_PRICE/ZAPPRCM1EXP", oModel.getData().EXPECTED_PERC);
				if (saveBtnPressed === true)
					that.onSavePR();
			}, this);
		},

		//ex_dogus: when user selected line(s)
		onTableRowSelect: function (oEvent) {
			this._calculateValues([]);
		},

		_setInitialValueNeeded: function (value) {
			if (value === -Infinity || value === Infinity || isNaN(value)) value = 0;
			return value;
		},

		//ex_dogus: calculation of tiles in header area
		_calculateValues: function (param) {
			var
				tableData = param,

				totalCM1 = 0, // tile CM1 -> total CM1 Value
				totalGM = 0, // tile GM ->total Value
				totalVolume = 0, // tile General Information ->total value
				totalSalesGross = 0, // tile General Information->total value

				totalCM1EURKG = 0, // tile CM1 -> total EURKG Value
				totalGMEURKG = 0, // tile GM -> total EURKG Value
				totalSALESGROSSKG = 0,
				gapToTargetEURKG = 0, // tile GToTarget -> total EURKG Value

				totalTargetCM1 = 0, // DPRSPLCM1
				totalSalesDeduction = 0, // (line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER.DPRSVYTD)
				CM1Percentage = 0, // cm1 percentage
				gapToTarget_PERC = 0,
				GapToTarget_weighed = 0,
				totalGapToTarget = 0,
				totalGapToTargetLast12M = 0,
				totalForecast = 0,
				that = this;

			//gets lines that will be considered ----> tableData
			// if (selectedIndexs.length === 0) {
			// 	tableData = that._resultM.getData();
			// } else {
			// 	selectedIndexs.forEach(function (selIndex) {
			// 		newSelIndex.push(aIndices[selIndex]);
			// 	});

			// 	selectedIndexs = newSelIndex;
			// 	selectedIndexs.forEach(function (line) {
			// 		tableData.push(that._resultM.getData()[line]);
			// 	});
			// }

			var DRATE5_Buffered = 0,
				OthCostInc_curr_kg = 0,
				FrgtInc_curr_kg = 0,
				CostChangeRaw = 0,
				costChangeGross = 0,
				totalCostChange = 0,
				totalCostChangeLast12M = 0,
				totalCostChange_Decr = 0,
				totalCostChange_DecrLast12M = 0,
				totalCostChange_Incr = 0,
				totalCostChange_IncrLast12M = 0;

			tableData.forEach(function (line) {
				//need only once 
				costChangeGross = 0;
				CostChangeRaw = line.PRICE_MASTER.DPRSCEXGC / line.PRICE_MASTER.DPRDIVIS;

				OthCostInc_curr_kg = line.PRICE_MASTER.DCOSTINCGC / line.PRICE_MASTER.DPRDIVIS;
				FrgtInc_curr_kg = line.PRICE_MASTER.DFREIGHTINCGC / line.PRICE_MASTER.DPRDIVIS;
				var cost2 = 0;
				if (CostChangeRaw < 0) {
					cost2 = CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg) / (1 - (line.PRICE_MASTER.DPRSSALDECAT * line.PRICE_MASTER
						.DPRSVATD +
						line
						.PRICE_MASTER
						.DPRSRSC3PATD) / (line.PRICE_MASTER.DPRSRSGSATD));
					if (!isNaN(cost2))
						costChangeGross = cost2;
				} else {
					cost2 = (CostChangeRaw + (OthCostInc_curr_kg + FrgtInc_curr_kg)) / (1 - (line.PRICE_MASTER.DPRSSALDECAT * line.PRICE_MASTER
						.DPRSVATD +
						line.PRICE_MASTER
						.DPRSRSC3PATD) / (line.PRICE_MASTER.DPRSRSGSATD));
					if (!isNaN(cost2))
						costChangeGross = cost2;
				}

				if (isNaN(costChangeGross) || costChangeGross === Infinity || costChangeGross === -Infinity)
					line.PRICE_MASTER.NOTVALID_costChangeGross = "---";
				else
					line.PRICE_MASTER.NOTVALID_costChangeGross = "";

				costChangeGross = that._setInitialValueNeeded(costChangeGross);

				totalCostChange = totalCostChange + costChangeGross * line.PRICE_MASTER.DPRSVFC;
				totalCostChangeLast12M = totalCostChangeLast12M + costChangeGross * (line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ -
					line.PRICE_MASTER.DPRSVYTDPY);
				if (costChangeGross < 0) {
					totalCostChange_Decr = totalCostChange_Decr + costChangeGross * line.PRICE_MASTER.DPRSVFC;
					totalCostChange_DecrLast12M = totalCostChange_DecrLast12M + costChangeGross * (line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ -
						line.PRICE_MASTER.DPRSVYTDPY);
				}

				if (costChangeGross > 0) {
					totalCostChange_Incr = totalCostChange_Incr + costChangeGross * line.PRICE_MASTER.DPRSVFC;
					totalCostChange_IncrLast12M = totalCostChange_IncrLast12M + costChangeGross * (line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ -
						line.PRICE_MASTER.DPRSVYTDPY);
				}
				if (line.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE === undefined) line.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE = 0;
				if (line.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE === undefined) line.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE = 0;
				totalGapToTarget = totalGapToTarget + (line.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - line.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE) *
					(line.PRICE_MASTER.DPRSVFC);
				totalGapToTargetLast12M = totalGapToTargetLast12M + (line.PRICE_MASTER.PRICE_EURKG_CURRENT_PRICE - line.PRICE_MASTER.PRICE_EURKG_TARGET_PRICE) *
					(line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ - line.PRICE_MASTER.DPRSVYTDPY);
				totalForecast = totalForecast + (line.PRICE_MASTER.DPRSVFC);

				if (line.PRICE_MASTER.DPRSVATD > 0) {
					GapToTarget_weighed = GapToTarget_weighed + ((line.PRICE_MASTER.DPRSVFC) * (line.PRICE_MASTER.DPRSCM1ATD - line.PRICE_MASTER.DPRSTMPRO));
				} else {
					if (line.PRICE_MASTER.DPRSVYTD > 0) {
						GapToTarget_weighed = GapToTarget_weighed + ((line.PRICE_MASTER.DPRSVFC) *
							(line.PRICE_MASTER.DPRSCM1PERC - line.PRICE_MASTER.DPRSTMPRO));
					} else {
						if (line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ - line.PRICE_MASTER.DPRSVYTDPY > 0) {
							GapToTarget_weighed = GapToTarget_weighed + ((line.PRICE_MASTER.DPRSVFC) *
								(((line.PRICE_MASTER.DPRSRSCM1 + line.PRICE_MASTER.DPRSRSCM1PY - line.PRICE_MASTER.DPRSRSCM1YPY) / (line.PRICE_MASTER.DPRSRSGSA +
									line.PRICE_MASTER.DPRSRSGSAPY - line.PRICE_MASTER.DPRSRSGSAYPY - line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER.DPRSVYTD -
									line.PRICE_MASTER.DPRSSALDEPY * line.PRICE_MASTER.DPRSVVJ + line.PRICE_MASTER.DPRSSALDECYPY * line.PRICE_MASTER.DPRSVYTDPY
								)) * 100) - line.PRICE_MASTER.DPRSTMPRO);
						} else {
							if (line.PRICE_MASTER.DPRSVVJ > 0) {
								GapToTarget_weighed = GapToTarget_weighed + ((line.PRICE_MASTER.DPRSVFC) *
									(line.PRICE_MASTER.DPRSCM1PERCP - line.PRICE_MASTER.DPRSTMPRO));
							}
						}
					}
				}
				if (isNaN(GapToTarget_weighed)) GapToTarget_weighed = 0;

				var rbn = that.getView().byId("rbgPeriodId");
				var LV_PERIOD_TRIGGER = rbn.getSelectedIndex();

				if (LV_PERIOD_TRIGGER === 0 && (line.PRICE_MASTER.DPRSVATD) > 0) {
					totalCM1 = totalCM1 + (line.PRICE_MASTER.DPRSRSCM1ATD);
					totalGM = totalGM + (line.PRICE_MASTER.DPRSGMATD * line.PRICE_MASTER.DPRSVATD);
					totalVolume = totalVolume + (line.PRICE_MASTER.DPRSVATD);
					totalSalesGross = totalSalesGross + (line.PRICE_MASTER.DPRSRSGSATD);
					totalTargetCM1 = totalTargetCM1 + (line.PRICE_MASTER.DPRSPLCM1ATD);
					totalSalesDeduction = totalSalesDeduction + (line.PRICE_MASTER.DPRSSALDECAT * line.PRICE_MASTER.DPRSVATD);

					if (tableData.length === 1) {
						CM1Percentage = (line.PRICE_MASTER.DPRSRSCM1ATD) / ((line.PRICE_MASTER.DPRSRSGSATD) - (line.PRICE_MASTER.DPRSSALDECAT * line.PRICE_MASTER
							.DPRSVATD));
					}
				}

				if (LV_PERIOD_TRIGGER === 1 && line.PRICE_MASTER.DPRSVYTD > 0) {
					totalCM1 = totalCM1 + (line.PRICE_MASTER.DPRSRSCM1);
					totalGM = totalGM + (line.PRICE_MASTER.DPRSGM * line.PRICE_MASTER.DPRSVYTD);
					totalVolume = totalVolume + (line.PRICE_MASTER.DPRSVYTD);
					totalSalesGross = totalSalesGross + (line.PRICE_MASTER.DPRSRSGSA);
					totalTargetCM1 = totalTargetCM1 + (line.PRICE_MASTER.DPRSPLCM1);
					totalSalesDeduction = totalSalesDeduction + (line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER.DPRSVYTD);

					if (tableData.length === 1) {
						CM1Percentage = (line.PRICE_MASTER.DPRSRSCM1) / ((line.PRICE_MASTER.DPRSRSGSA) - (line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER
							.DPRSVYTD));
					}
				}

				if (LV_PERIOD_TRIGGER === 2 && (line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ - line.PRICE_MASTER.DPRSVYTDPY) > 0) {
					totalCM1 = totalCM1 + (line.PRICE_MASTER.DPRSRSCM1 + line.PRICE_MASTER.DPRSRSCM1PY - line.PRICE_MASTER.DPRSRSCM1YPY);
					totalGM = totalGM + (line.PRICE_MASTER.DPRSGM * line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSGMPY * line.PRICE_MASTER.DPRSVVJ -
						line.PRICE_MASTER.DPRSGMYPY * line.PRICE_MASTER.DPRSVYTDPY);
					totalVolume = totalVolume + (line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSVVJ - line.PRICE_MASTER.DPRSVYTDPY);
					totalSalesGross = totalSalesGross + (line.PRICE_MASTER.DPRSRSGSA + line.PRICE_MASTER.DPRSRSGSAPY - line.PRICE_MASTER.DPRSRSGSAYPY);
					totalTargetCM1 = totalTargetCM1 + (line.PRICE_MASTER.DPRSPLCM1 + line.PRICE_MASTER.DPRSPLCM1PY - line.PRICE_MASTER.DPRSPLCM1YPY);
					totalSalesDeduction = totalSalesDeduction + (line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER.DPRSVYTD + line.PRICE_MASTER.DPRSSALDEPY *
						line.PRICE_MASTER.DPRSVVJ - line.PRICE_MASTER.DPRSSALDECYPY * line.PRICE_MASTER.DPRSVYTDPY);

					if (tableData.length === 1) {
						CM1Percentage = (line.PRICE_MASTER.DPRSRSCM1 + line.PRICE_MASTER.DPRSRSCM1PY - line.PRICE_MASTER.DPRSRSCM1YPY) / ((line.PRICE_MASTER
							.DPRSRSGSA + line.PRICE_MASTER.DPRSRSGSAPY - line.PRICE_MASTER.DPRSRSGSAYPY) - (line.PRICE_MASTER.DPRSSALDEC * line.PRICE_MASTER
							.DPRSVYTD + line.PRICE_MASTER.DPRSSALDEPY * line.PRICE_MASTER.DPRSVVJ - line.PRICE_MASTER.DPRSSALDECYPY * line.PRICE_MASTER
							.DPRSVYTDPY
						));
					}
				}

				if (LV_PERIOD_TRIGGER === 3 && line.PRICE_MASTER.DPRSVVJ > 0) {
					totalCM1 = totalCM1 + (line.PRICE_MASTER.DPRSRSCM1PY);
					totalGM = totalGM + (line.PRICE_MASTER.DPRSGMPY * line.PRICE_MASTER.DPRSVVJ);
					totalVolume = totalVolume + (line.PRICE_MASTER.DPRSVVJ);
					totalSalesGross = totalSalesGross + (line.PRICE_MASTER.DPRSRSGSAPY);
					totalTargetCM1 = totalTargetCM1 + (line.PRICE_MASTER.DPRSPLCM1PY);
					totalSalesDeduction = totalSalesDeduction + (line.PRICE_MASTER.DPRSSALDEPY * line.PRICE_MASTER.DPRSVVJ);

					if (tableData.length === 1) {
						CM1Percentage = (line.PRICE_MASTER.DPRSRSCM1PY) / ((line.PRICE_MASTER.DPRSRSGSAPY) - (line.PRICE_MASTER.DPRSSALDEPY * line.PRICE_MASTER
							.DPRSVVJ));
					}

				}

			});

			if (tableData.length > 1) {
				CM1Percentage = totalCM1 / (totalSalesGross - totalSalesDeduction);
			}

			if (totalVolume !== 0) {
				totalCM1EURKG = totalCM1 / totalVolume;
				totalGMEURKG = totalGM / totalVolume;
				totalSALESGROSSKG = totalSalesGross / totalVolume;
			}

			if (totalForecast !== 0) {
				gapToTargetEURKG = totalGapToTarget / totalForecast;
				gapToTarget_PERC = GapToTarget_weighed / totalForecast;
			}

			/*********************************CM1 Tile*********************************************/
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResultsKpis", totalCM1);
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResults_EURKGKpis", totalCM1EURKG);
			this._globalM.setProperty("/CM1PercentageKpis", CM1Percentage * 100);
			/*********************************GM Tile***********************************************/
			this._globalM.setProperty("/gmTileValueBasedOnSalesResultsKpis", totalGM);
			this._globalM.setProperty("/gmTileValueBasedOnSalesResults_EURKGKpis", totalGMEURKG);
			/*********************************General Information Tile******************************/
			this._globalM.setProperty("/volumeValueKpis", totalVolume);
			this._globalM.setProperty("/salesGrossValueKpis", totalSalesGross);
			/*********************************Gap to Target Tile************************************/
			this._globalM.setProperty("/gapToTarget_EURKGKpis", gapToTargetEURKG);
			this._globalM.setProperty("/gapToTarget_PERCKpis", gapToTarget_PERC);

			//Sales Results Tile
			this._globalM.setProperty("/salesGrossValueKpis", totalSalesGross);
			this._globalM.setProperty("/gmTileValueBasedOnSalesResultsKpis", totalGM);
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResultsKpis", totalCM1);
			this._globalM.setProperty("/CM1PercentageKpis", CM1Percentage * 100);

			//General Information Tile
			this._globalM.setProperty("/volumeValueKpis", totalVolume);
			this._globalM.setProperty("/salesGrossKGValueKpis", totalSALESGROSSKG);
			this._globalM.setProperty("/gmTileValueBasedOnSalesResults_EURKGKpis", totalGMEURKG);
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResults_EURKGKpis", totalCM1EURKG);

			//Gap to target tile
			this._globalM.setProperty("/totalGapToTargetKpis", totalGapToTarget);
			this._globalM.setProperty("/gapToTarget_EURKGKpis", gapToTargetEURKG);
			this._globalM.setProperty("/gapToTarget_PERCKpis", gapToTarget_PERC);

			//cost change gross tile
			this._globalM.setProperty("/costChangeGrossTotalKpis", totalCostChange);
			this._globalM.setProperty("/totalCostChange_DecrKpis", totalCostChange_Decr);
			this._globalM.setProperty("/totalCostChange_IncrKpis", totalCostChange_Incr);

			//Gap to target tile last 12m
			this._globalM.setProperty("/totalGapToTargetLast12MKpis", totalGapToTargetLast12M);
			this._globalM.setProperty("/gapToTarget_EURKGKpis", gapToTargetEURKG);
			this._globalM.setProperty("/gapToTarget_PERCKpis", gapToTarget_PERC);

			//cost change gross tile last 12m
			this._globalM.setProperty("/costChangeGrossTotalLast12MKpis", totalCostChangeLast12M);
			this._globalM.setProperty("/totalCostChange_DecrLast12MKpis", totalCostChange_DecrLast12M);
			this._globalM.setProperty("/totalCostChange_IncrLast12MKpis", totalCostChange_IncrLast12M);
		},

		//ex_dogus: value help for issue filter field
		handleValueHelp: function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
			// create value help dialog
			if (!this._valueHelpDialog) {
				Fragment.load({
					id: "valueHelpDialog",
					name: "com.doehler.PricingMasterFile.fragments.Issues",
					controller: this
				}).then(function (oValueHelpDialog) {
					this._valueHelpDialog = oValueHelpDialog;
					this.getView().addDependent(this._valueHelpDialog);
					this._openValueHelpDialog(sInputValue);
				}.bind(this));
			} else {
				this._openValueHelpDialog(sInputValue);
			}
		},

		//ex_dogus: open issue dialog
		_openValueHelpDialog: function (sInputValue) {
			// create a filter for the binding
			this._valueHelpDialog.getBinding("items").filter([new Filter(
				"text",
				FilterOperator.Contains,
				sInputValue
			)]);

			// open value help dialog filtered by the input value
			this._valueHelpDialog.open(sInputValue);
		},

		//ex_dogus: searching in issue dialog
		_handleValueHelpSearch: function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new Filter(
				"text",
				FilterOperator.Contains,
				sValue
			);
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		//ex_dogus: close / confirm issue dialog
		_handleValueHelpClose: function (evt) {
			var aSelectedItems = evt.getParameter("selectedItems"),
				oMultiInput = this.byId("multiInputIssuesKpis"),
				line,
				that = this;

			if (aSelectedItems && aSelectedItems.length > 0) {
				aSelectedItems.forEach(function (oItem) {
					line = sap.ui.getCore().getModel("globalModel").getProperty(oItem.getBindingContext("globalModel").getPath());

					switch (line.key) {
					case 0:
						that._filterMKpis.setProperty("/REDP", "X");
						break;
					case 1:
						that._filterMKpis.setProperty("/REDC", "X");
						break;
					case 2:
						that._filterMKpis.setProperty("/LAST_PRICE_1", "X");
						break;
					case 3:
						that._filterMKpis.setProperty("/LAST_PRICE_2", "X");
						break;
					case 4:
						that._filterMKpis.setProperty("/ACCOUNT_INCREASE", "X");
						break;
					case 5:
						that._filterMKpis.setProperty("/GAP_TO_TARGET", "X");

						break;
					default:
					}

					oMultiInput.addToken(new sap.m.Token({
						key: line.key,
						text: line.text
					}));
				});
			}
		},

		//ex_dogus: clear all filter values in header area
		clearSelectionFields: function () {
			var controls, i;
			this._filterMKpis.setData({
				CUSTOMER_LOW_KPIS: "",
				CUSTOMER_KPIS: "",
				CUSTOMER_HIGH_KPIS: "",
				CUSTOMER_NI_LOW_KPIS: "",
				CUSTOMER_NI_HIGH_KPIS: "",
				ARTICLE_KPIS: "",
				B2BL1_KPIS: "",
				B2BL2_KPIS: "",
				B2BL3_KPIS: "",
				B2BL4_KPIS: "",
				UNAME_KPIS: "",
				STATUS_KPIS: "",
				AREA_KPIS: "",
				REGION_KPIS: "",
				CUSTSEG_KPIS: "",
				AGG_TYP_KPIS: "",
				C_CUST_GRP4_KPIS: "",
				C_DACCMAN_KPIS: "",
				C_CUST_GRP2_KPIS: "",
				C_DOEBKUNDE_KPIS: "",
				REDC_KPIS: "",
				REDP_KPIS: "",
				LAST_PRICE_1_KPIS: "",
				LAST_PRICE_2_KPIS: "",
				GAP_TO_TARGET_KPIS: "",
				ACCOUNT_INCREASE_KPIS: "",
				TIER2_CUSTOMER_L1: "",
				GLOBAL_TEAM: "",
				SPECIAL_VARIANTS: ""
			});

			controls = this.getView().byId("searchGridId").getContent();
			for (i = 0; i < controls.length; i++) {
				if (controls[i].getMetadata().getName() === "sap.m.MultiInput") {
					controls[i].removeAllTokens();
					controls[i].destroyTokens();
				}
			}

		},

		//ex_dogus: getting data for table from backend
		onGo: function () {
			var params, resdata = [],
				multiIssues, data, userId, name,
				that = this;

			this._globalM.setProperty("/savedPriceSetting", []);
			this._clearGenericTiles();
			multiIssues = this.getView().byId("multiInputIssuesKpis");
			this._checkIssueFilter(multiIssues, this._filterMKpis);
			data = this._filterMKpis.getData();
			this._checkExistCustomerRole(data);
			userId = sap.ushell.Container.getUser().getId();
			name = userId === "EX_DOGUS" || userId === "DEFAULT_USER" ? "HEADER_KPIS" : "HEADER_KPIS";

			if (data.CUSTOMER !== "") {
				data.CUSTOMER_LOW = data.CUSTOMER;
				data.CUSTOMER_HIGH = "";
				data.RUNTIME = "1";
				params = {
					"HANDLERPARAMS": {
						"FUNC": name
					},
					"INPUTPARAMS": [data]
				};
				dbcontext.callServer(params, function (oModel) {
					resdata = [];
					resdata = oModel.getProperty("/PRICEREQHDR");
					that._updateTimeJob(oModel.getData()["UPDATE_INFO"]);
					if (resdata.length === 0) {
						MessageBox.warning("Results not found for provided selection");
						return;
					}

					that._fillTable(resdata);
				}, this, null, null, true, true);
			} else {
				that._callDataTwice(data);
			}
		},

		//ex_dogus: if customer is initial then go to backend function twice
		_callDataTwice: function (data) {
			var low_customer_1 = "1",
				low_customer_2 = "50000",
				params, name = "HEADER_KPIS",
				promise1, result1, result2,
				that = this;

			data.CUSTOMER_LOW = low_customer_1;
			data.CUSTOMER_HIGH = low_customer_2;
			data.RUNTIME = "1";

			params = {
				"HANDLERPARAMS": {
					"FUNC": name
				},
				"INPUTPARAMS": [data]
			};

			sap.ui.core.BusyIndicator.show(0);

			promise1 = new Promise(function (resolved, rejected) {
				dbcontext.callServer(params, function (oModel) {
					result1 = oModel.getProperty("/PRICEREQHDR");
					resolved(result1);
				}, that, null, null, true, false);
			});

			data.CUSTOMER_LOW_KPIS = low_customer_1;
			data.CUSTOMER_HIGH_KPIS = low_customer_2;
			data.RUNTIME = "2";

			promise1.then(function (result1) {

				data.CUSTOMER_LOW_KPIS = low_customer_1;
				data.CUSTOMER_HIGH_KPIS = low_customer_2;
				data.RUNTIME = "2";

				params = {
					"HANDLERPARAMS": {
						"FUNC": name
					},
					"INPUTPARAMS": [data]
				};
				dbcontext.callServer(params, function (oModel2) {
					result2 = oModel2.getProperty("/PRICEREQHDR");
					result1 = result1.concat(result2);
					sap.ui.core.BusyIndicator.hide();
					if (result1.length === 0) {
						MessageBox.warning("Results not found for provided selection");
						return;
					}
					that._fillTable(result1);

				}, that, null, null, true, false);
			});
		},

		//ex_dogus: filling table 
		_fillTable: function (resdata) {
			var that = this,
				oldData;
			resdata = formulas._calculateRowsForKPIs(resdata);
			that._calculateValues(resdata);
			that._globalM.setProperty("/resDataKpis", resdata);
			// resdata = that._prepareTableData(resdata);
			// resdata = that._loadPriceScalesForViews(resdata);
			// sap.ui.getCore().getModel("psresultM").setData(resdata);
			// that._globalM.setProperty("/savedPriceSetting", []);
			// that._calculateValues(resdata);

			// jQuery.sap.delayedCall(1500, this, function () {
			// 	oldData = $.extend(true, [], resdata);
			// 	that._globalM.setProperty("/oldData", oldData);
			// });

		},

		//ex_dogus: open dialog t show cost data
		onPressCostChange: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("psresultM").getPath(),
				line = sap.ui.getCore().getModel("psresultM").getProperty(path);
			this._openCostDrivers(line, oEvent, this);
		},

		//ex_dogus: set 0 to all kpis in header
		_clearGenericTiles: function () {
			//	this._table.setSelectedIndex(-1);
			//CM1 Based on sales results
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResultsKpis", 0);
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResults_EURKGKpis", 0);
			//GM Based on Sales Results
			this._globalM.setProperty("/gmTileValueBasedOnSalesResultsKpis", 0);
			this._globalM.setProperty("/gmTileValueBasedOnSalesResults_EURKGKpis", 0);
			//General Information
			this._globalM.setProperty("/volumeValueKpis", 0);
			this._globalM.setProperty("/salesGrossValueKpis", 0);
			//Gap to Target
			this._globalM.setProperty("/gapToTarget_EURKGKpis", 0);
			this._globalM.setProperty("/gapToTarget_PERCKpis", 0);

			this._globalM.setProperty("/sumCM1TileKpis", 0);
			this._globalM.setProperty("/CM1PercentageKpis", 0);

			//Sales Results Tile
			this._globalM.setProperty("/salesGrossValueKpis", 0);
			this._globalM.setProperty("/gmTileValueBasedOnSalesResultsKpis", 0);
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResultsKpis", 0);
			this._globalM.setProperty("/CM1PercentageKpis", 0);

			//General Information Tile
			this._globalM.setProperty("/volumeValueKpis", 0);
			this._globalM.setProperty("/salesGrossKGValueKpis", 0);
			this._globalM.setProperty("/gmTileValueBasedOnSalesResults_EURKGKpis", 0);
			this._globalM.setProperty("/cm1TileValueBasedOnSalesResults_EURKGKpis", 0);

			//Gap to target tile
			this._globalM.setProperty("/totalGapToTargetKpis", 0);
			this._globalM.setProperty("/gapToTarget_EURKGKpis", 0);
			this._globalM.setProperty("/gapToTarget_PERCKpis", 0);

			//cost change gross tile
			this._globalM.setProperty("/costChangeGrossTotalKpis", 0);
			this._globalM.setProperty("/totalCostChange_DecrKpis", 0);
			this._globalM.setProperty("/totalCostChange_IncrKpis", 0);

			//Gap to target tile last 12m
			this._globalM.setProperty("/totalGapToTargetLast12MKpis", 0);
			this._globalM.setProperty("/gapToTarget_EURKGKpis", 0);
			this._globalM.setProperty("/gapToTarget_PERCKpis", 0);

			//cost change gross tile 12m
			this._globalM.setProperty("/costChangeGrossTotalKpis", 0);
			this._globalM.setProperty("/totalCostChange_DecrLast12MKpis", 0);
			this._globalM.setProperty("/totalCostChange_IncrLast12MKpis", 0);
		},

		//ex_dogus: save changed line
		onSavePR: function () {
			var sendingDataArray = [],
				sendingData = {},
				newPrice = [],
				thiz = this,
				savedData = [],
				priceMaster = [];

			savedData = this._globalM.getProperty("/savedPriceSetting");

			if (savedData.length === 0) {
				MessageBox.warning("There is no changed line!");
				return;
			}

			for (var i = 0; i < savedData.length; i++) {
				newPrice.push(savedData[i].NEW_PRICE);
				priceMaster.push(savedData[i].PRICE_MASTER);
			}

			sendingData.NEW_PRICE = newPrice;
			sendingData.PRICE_MASTER = priceMaster;
			sendingData.LV_ROL = this._globalM.getProperty("/ROLEINDEX");
			sendingDataArray.push(sendingData);

			var params = {
				"HANDLERPARAMS": {
					"FUNC": "SAVE_PRC_MASTER"
				},
				"INPUTPARAMS": sendingDataArray
			};

			dbcontext.callServer(params, function (oModel) {
				thiz.handleServerMessages(oModel, function (status) {
					if (status === "S") {
						thiz.onGo();
					}
				});
			}, this);
		},

		//ex_dogus: send the selected lines to quotation
		onSendQuotation: function (oEvent) {
			var table = this._table,
				data = [];
			data = this._getSendingDataForQuotation(table, null);

			if (data === undefined) return;
			var thiz = this,
				params;
			jQuery.sap.delayedCall(500, this, function () {
				params = {
					"HANDLERPARAMS": {
						"FUNC": "SEND_QUOT"
					},
					"INPUTPARAMS": data.data
				};
				dbcontext.callServer(params, function (oModel) {
					thiz.handleServerMessages(oModel, function (status) {
						if (status === "S") {
							thiz.onGo();
						}
					});
				}, this);
			});
		},

		//ex_dogus: send the selected lines to approval
		onSendApproval: function () {
			var data = [],
				sText,
				table = this._table;

			data = this._getSendingDataForApproval(table, null);
			if (data === undefined) return;

			if (data.isAppNeededInitial) {

				if (!this.oSubmitDialog) {
					this.oSubmitDialog = new Dialog({
						type: DialogType.Message,
						title: "Confirm",
						content: [
							new Label({
								text: "Why you want to start the workflow?",
								labelFor: "submissionNoteMain"
							}),
							new TextArea("submissionNoteMain", {
								maxLength: 200,
								width: "100%",
								placeholder: "Add note (required)",
								liveChange: function (oEvent) {
									sText = oEvent.getParameter("value");
									this.oSubmitDialog.getBeginButton().setEnabled(sText.length > 0);
								}.bind(this)
							})
						],
						beginButton: new Button({
							type: ButtonType.Emphasized,
							text: "Submit",
							enabled: false,
							press: function () {
								sText = Core.byId("submissionNoteMain").getValue();
								data.data.forEach(function (line) {
									if (line.APP_NEEDED === "")
										line.APP_NEEDED_DESC = sText;
								});
								this.oSubmitDialog.close();
								this._callSendApprovalFunc(data);
							}.bind(this)
						}),
						endButton: new Button({
							text: "Cancel",
							press: function () {
								this.oSubmitDialog.close();
							}.bind(this)
						})
					});
				} else
					Core.byId("submissionNoteMain").setValue("");

				this.oSubmitDialog.open();

			} else
				this._callSendApprovalFunc(data);

		},

		//ex_dogus: turn the changed values into initial state
		onCancelPR: function () {
			var oldData = this._globalM.getProperty("/oldData");
			var oldData2 = $.extend(true, [], oldData);
			sap.ui.getCore().getModel("psresultM").setData(oldData2);
			this._globalM.setProperty("/savedPriceSetting", []);
			this._calculateValues(oldData2);
		},

		//ex_dogus: send approval function is triggered
		_callSendApprovalFunc: function (data) {
			var thiz = this,
				params;

			if (data.dataForDacman.length) {

				if (!data.isInitialWorkflow) {
					MessageBox.warning("You can not select the lines that have In progress, Approved or Rejected workflow status!");
				} else {
					params = {
						"HANDLERPARAMS": {
							"FUNC": "GET_DACCMAN_TO_USER"
						},
						"INPUTPARAMS": data.dataForDacman
					};
					dbcontext.callServer(params, function (oModel) {
						var resultData = oModel.getData().OUTPUTDATA,
							lineResult = [];

						data.dataForNotApproval.forEach(function (itm) {
							lineResult = resultData.filter(function (resLine) {
								return resLine.C_DACCMAN === itm.PRICE_MASTER.C_DACCMAN;
							});
							if (lineResult.length)
								itm.PRICE_MASTER.RECIPIENT = lineResult[0].USRID;

						});

						thiz._createMailDialogForApproval(thiz, data.dataForNotApproval, "customHeaderMainId");
					}, this);
				}

			}

			if (!data.data.length) return;

			jQuery.sap.delayedCall(500, this, function () {
				params = {
					"HANDLERPARAMS": {
						"FUNC": "SEND_APPROVAL"
					},
					"INPUTPARAMS": data.data
				};
				dbcontext.callServer(params, function (oModel) {
					thiz.handleServerMessages(oModel, function (status) {
						if (status === "S") {
							thiz.onGo();
						}

					});
				}, this);
			});
		},

		//ex_dogus: go to create request page of Pricing Tool App
		goToCreatePriceRequestPage: function (oEvent) {
			var table = this._table,
				that = this,
				createRequestLink =
				"/sap/bc/ui5_ui5/ui2/ushell/shells/abap/FioriLaunchpad.html#zdoehlersempr-display?tag=1&/CreateRequest",
				stringfyJSON,
				indicator = 0,
				aSelIndex = table.getSelectedIndices(),
				aIndices = table.getBinding().aIndices,
				newSelIndex = [],
				context,
				path,
				position = 1,
				customerNumber,
				filteredCustomer = [],
				allData = [],
				row,
				createPriceReqItems = [],
				userId = sap.ushell.Container.getUser().getId();

			var finSalesPrice, freight_out, comission3rd;

			createRequestLink = location.host + createRequestLink;

			if (!aSelIndex.length) {
				MessageToast.show("Select at least one record");
			} else {
				aSelIndex.forEach(function (selIndex) {
					newSelIndex.push(aIndices[selIndex]);
				});

				aSelIndex = newSelIndex;
				aSelIndex.forEach(function (index, ind) {
					row = that._resultM.getData()[index];
					//path = context.getPath();
					//row = context.getProperty(path);
					createPriceReqItems.push({
						Customer: row.PRICE_MASTER.CUSTOMER,
						ArticleNumber: row.PRICE_MASTER.MATERIAL,
						Title: "PMT – Price Renewal"
					});

					if (row.PRICE_MASTER.DPRSVATD > 0) {
						finSalesPrice = 0;
						finSalesPrice = row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT);
						freight_out = 0;
						freight_out = row.NEW_PRICE.MOQ_NEW * ((row.PRICE_MASTER.DRPSFRTATD * row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSVATD));
						comission3rd = 0;
						comission3rd = (row.PRICE_MASTER.DPRSRSC3PATD) / (row.PRICE_MASTER.DPRSVATD) / (row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS) *
							100;

					}
					if (row.PRICE_MASTER.DPRSVYTD > 0) {
						finSalesPrice = 0;
						finSalesPrice = row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT);
						freight_out = 0;
						freight_out = row.NEW_PRICE.MOQ_NEW * ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSVYTD));
						comission3rd = 0;
						comission3rd = (row.PRICE_MASTER.DPRSRSC3P) / (row.PRICE_MASTER.DPRSVYTD) / (row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS) *
							100;

					}
					if ((row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) > 0) {
						finSalesPrice = 0;
						finSalesPrice = row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT);
						freight_out = 0;
						freight_out = row.NEW_PRICE.MOQ_NEW * ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY *
							row
							.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER
							.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY));
						comission3rd = 0;
						comission3rd = (row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY - row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER
							.DPRSVYTD +
							row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS) * 100;

					}

					if (row.PRICE_MASTER.DPRSVVJ > 0) {
						finSalesPrice = 0;
						finSalesPrice = row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT);
						freight_out = 0;
						freight_out = row.NEW_PRICE.MOQ_NEW * ((row.PRICE_MASTER.DRPSFRTPY * row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSVVJ));
						comission3rd = 0;
						comission3rd = (row.PRICE_MASTER.DPRSRSC3PPY) / (row.PRICE_MASTER.DPRSVVJ) / (row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS) *
							100;
					}

					// finSalesPrice = 0;
					// finSalesPrice = row.NEW_PRICE.NEW_PRICE_PLAN / parseFloat(row.PRICE_MASTER.DCONDUNIT);
					// freight_out = 0;
					// freight_out =
					// 	row.NEW_PRICE.MOQ_NEW * ((row.PRICE_MASTER.DRPSFRT * row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER.DRPSFRTPY *
					// 		row
					// 		.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DRPSFRTYPY * row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSVYTD + row.PRICE_MASTER
					// 		.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY));
					// comission3rd = 0;
					// comission3rd = (row.PRICE_MASTER.DPRSRSC3P + row.PRICE_MASTER.DPRSRSC3PPY -
					// 	row.PRICE_MASTER.DPRSRSC3PYPY) / (row.PRICE_MASTER
					// 	.DPRSVYTD +
					// 	row.PRICE_MASTER.DPRSVVJ - row.PRICE_MASTER.DPRSVYTDPY) / (row.PRICE_MASTER.DPRSCPAGC / row.PRICE_MASTER.DPRDIVIS) * 100;

					if (isNaN(comission3rd) || comission3rd === Infinity || comission3rd === -Infinity)
						comission3rd = 0;
					if (isNaN(freight_out) || freight_out === Infinity || freight_out === -Infinity)
						freight_out = 0;
					allData.push({
						POSNR: position++,
						READY: false,
						FROM_PQ: false,
						DATA_MATERIAL: false,
						DATA_CUSTOMER: false,
						PQNUM: row.LAST_REQUEST,
						CUSTOMER: row.PRICE_MASTER.CUSTOMER, // Customer
						MATERIAL: row.PRICE_MASTER.MATERIAL, // Material
						SHIP_TO: row.PRICE_MASTER.SHIP_TO, // Ship to
						PAYER: isNaN(parseInt(row.PRICE_MASTER.PAYER).toString()) ? "" : parseInt(row.PRICE_MASTER.PAYER).toString(), // Payer 
						SALESORG: row.PRICE_MASTER.SALESORG, // Sales Org
						DISTR_CHAN: row.PRICE_MASTER.DISTR_CHAN, // Disturbition Channel
						ORIGINATOR: "",
						INCOTERMS: row.PRICE_MASTER.INCOTERMS,
						PMNTTRMS: row.PRICE_MASTER.PMNTTRMS,
						DCUKYCP: row.PRICE_MASTER.DCUKYCP, //Quotation Currency dcukyoc
						ZWAERS2: row.PRICE_MASTER.DCUKYPC, //Quotation Currency
						ZWAERS2Q: row.PRICE_MASTER.DCUKYCP, //Quotation Currency
						PLANT: row.PRICE_MASTER.DOEWERK,
						TARGET_VOLUME: row.NEW_PRICE.TARGET_VOLUME,
						CUSTOMER_TARGET_VOLUME: row.NEW_PRICE.TARGET_VOLUME,
						CUSTOMER_TARGET_PRICE: 0,
						ZQUANTITY2: row.NEW_PRICE.MOQ_NEW, // Order size in no. of units ---------------->will be taken from data preparation function
						SHIP_COND: row.PRICE_MASTER.SHIP_COND,
						DOEKMEIN2: row.PRICE_MASTER.DOEKMEIN2, // alternative uom
						ZCOMM1A: comission3rd, // Commission 3rd party
						ZCOM1: 0, // Commission 3rd party %
						UNIT: row.PRICE_MASTER.UNIT,
						ZFREIGHT1: parseFloat(freight_out.toFixed(2)), //freight-out ---------------->will be taken from data preparation function
						VISIBLE_ZFREIGHT1: false,
						ZFINSALES: finSalesPrice, //final sales price                 ---------------->will be taken from data preparation function
						PRICE_VALID_FROM: row.NEW_PRICE.PRICE_VALID_FROM,
						PRICE_VALID_TO: row.NEW_PRICE.PRICE_VALID_TO,
						TITLE: "",
						OPP_ID: "",
						ZWAERS3AQ: 0, //Exchange Rate,
						ZPRICEVALIDITY: "", // RM SPOT,
						ZZKALK03: "", // Microbiology
						ZZ_GEBART: "", //packaging, 
						ZDOSAGE: 0,
						ZCOST2A: 0, //Target Cost-in-Use Doehler,
						ZSUBCURR: "",
						ZINCO2: "", // Place of fulfillment,
						ZFRTWGHT: 0, //Freight weight,
						ZFIXTIMDEL: "", //Fixed time delivery,
						ZABS: "", //Calculate on average batch size,
						MEINS: row.PRICE_MASTER.UNIT,
						ZCOST8A: 0, //Payment terms discount,
						ZLAST: 0, // debit discount,
						ZBONUS1A: "", // No. of rebate agreement,
						ZBONUS1: 0, // Rebates,
						ZCUSTBONUS1: "", //No. of rebate agreement, perc
						ZCUSTBONUS: 0, // Rebates perc,
						ZOTHEXP: 0, //other cost,
						ZLOSTCST: 0, //lost interest,
						ZFNCECST: 0, //financial cost,
						ZPRICE_INT7: 0, //document cost
						ZCOST7B_PRCT_MAN: 0, //Import duty in % (to customer) (manual)
						ZCOST7B_KG_MAN: 0, //Import duty per kg (to customer) (manual)
						ZCOST7F_MAN: 0, //Other import cost per kg (to customer) (manual),
						ZCOST7D: 0, // VAT,
						ZIC_BIZ1: "", //Business Type,
						ZIC_BIZ5: 0, //Order size subsidiary in no. of units 
						ZIC_BIZ7: "", //Shipping Condition to subsidiary
						ZIC_BIZ2: "", //Incoterms with subsidiary
						ZIC_BIZ10: 0, //IC-Freight/Freight Drop shipment ,
						ZIC_BIZ10_BORDER_MAN: 0, //Thereof freight till Customs Depar
						ZIC_BIZ12_MAN: 0, //Import duty in % (to subsidiary) (manual)
						ZIC_BIZ13_MAN: 0, //Import duty in per kg (to subsidiary) (manual)
						ZIC_BIZ14_MAN: 0, //Other import cost per kg (to subsidiary) (manual) 
						ZCOST7C_KG_MAN: 0, //Excise taxes per kg (to subsidiary) (manual) 
						ZFREIGHT3: 0, //Freight from warehouse to customer ,
						ZKZSHE: "", // sea harbour visible if it is X,
						ZHARBOUR: "",
						MAT_LENGTH: "",
						SEA_HARBOUR: "",
						ZCONT1: "", // filling quantity,
						ZQUANTITY3: 0, //order size,
						ZQUANTITY2MIN: row.DETAIL.AUMNG, //minumum order size,
						ZPAL2: "", // Pallet Y / N
						PALLET: "", // Pallet,
						TRIGGER_Q: "",
						TRIGGER_A: "",
						ZMATNR: "",
						ZMATNR_BULK: "",
						ZZEINR: 0,
						ZBRIX1: 0,
						ZZ_XVFKZ: "",
						MAT_DESC: "", //description
						PACK_DESC: "" //packaging description
					});

				});

				if ((userId === "EX_UZUNB" || userId === "EX_SAYDAMZ" || userId === "LUCZAKK" || userId === "SCONDOL" ||
						userId === "SASCHA" || userId === "EX_YILMAZO" || userId === "EX_YILMAZA" || userId === "EX_DOGUS" || userId === "FISCHEA" ||
						userId === "MATTHEC" || userId === "GRAULIM" || userId === "SCHULZL" || userId === "KALBA") && (window.location.hostname ===
						"dafe1ci.da.doehler.com" || window.location.hostname === "dafe2ci.da.doehler.com") ||
					userId === "DEFAULT_USER") {} else {

					if (createPriceReqItems.length > 1) {
						customerNumber = createPriceReqItems[0].Customer;
						filteredCustomer = createPriceReqItems.filter(function (line) {
							return line.Customer === customerNumber;
						});
						if (filteredCustomer.length < createPriceReqItems.length) {
							MessageBox.warning("Please select only one customer to create a price request");
							return;
						}
					}
				}

				table.setSelectedIndex(-1);
				stringfyJSON = JSON.stringify(createPriceReqItems);
				createRequestLink = location.protocol + "//" +
					createRequestLink;
				createRequestLink += "?CustomerArticles=" + encodeURI(stringfyJSON);

				var stringData = JSON.stringify(allData);
				if (userId === "DEFAULT_USER" || userId === "EX_UZUNB" || userId === "EX_SAYDAMZ" || userId === "LUCZAKK" || userId === "SCONDOL" ||
					userId === "SASCHA" || userId === "EX_YILMAZO" || userId === "EX_YILMAZA" || userId === "EX_DOGUS" || userId === "FISCHEA" ||
					userId === "MATTHEC" || userId === "GRAULIM" || userId === "SCHULZL" || userId === "KALBA") {
					var link = window.location.href + "&/List?tab=" + stringData;
					if (window.location.hostname === "dafe1ci.da.doehler.com" || window.location.hostname === "dafe2ci.da.doehler.com" || userId ===
						"DEFAULT_USER") {

						MessageBox.information(
							"How do you want to proceed with the RFQ creation? \n Create via BOT is recommended for requests with many articles and tender calculation. \n Create directly in Pricing Tool is recommended for smaller and straightforward requests (e.g. price renewal for a few articles).", {
								actions: ["Create via BOT", "Create directly in Pricing Tool"],
								onClose: function (sAction) {
									if (sAction === "Create via BOT")
										sap.m.URLHelper.redirect(link, true);
									else
										sap.m.URLHelper.redirect(createRequestLink, true);
								}
							});

					} else
						sap.m.URLHelper.redirect(createRequestLink, true);
				} else
					sap.m.URLHelper.redirect(createRequestLink, true);
			}
		},

		//ex_dogus: dowmloading excel
		onExport: function () {
			var data = [];
			var userId = sap.ushell.Container.getUser().getId();
			var tableData = this._resultM.getData();
			var copiedData = tableData;
			var aIndices = this._table.getBinding("rows").aIndices;

			if (copiedData.length === aIndices.length) {
				data = copiedData;
			} else {
				aIndices.forEach(function (ind) {
					data.push(copiedData[ind]);
				});
			}

			if (userId === "EX_DOGUS" || userId === "DEFAULT_USER")
				excel._downloadExcel("PSI", this._table, data);
			else
				excel._downloadExcel("PSI", this._table, data);

		},

		// //ex_dogus:go to tender page with selected lines
		// onPressTenderView: function () {
		// 	var tenderViewBtn = this.getView().byId("tenderBtnId"),
		// 		table = this._table,
		// 		customerNumber, filteredCustomer = [],
		// 		aSelIndex = table.getSelectedIndices(),
		// 		newSelIndex = [],
		// 		aIndices = table.getBinding().aIndices,
		// 		context, path, row, stringfyJSON,
		// 		items = [];
		// 	tenderViewBtn.setPressed(true);

		// 	var that = this;

		// 	if (!aSelIndex.length) {
		// 		MessageToast.show("Select at least one record");
		// 	} else {
		// 		aSelIndex.forEach(function (selIndex) {
		// 			newSelIndex.push(aIndices[selIndex]);
		// 		});

		// 		aSelIndex = newSelIndex;
		// 		aSelIndex.forEach(function (index, ind) {
		// 			row = that._resultM.getData()[index];
		// 			//path = context.getPath();
		// 			//row = context.getProperty(path);
		// 			items.push({
		// 				aSelIndex: index,
		// 				Customer: row.PRICE_MASTER.CUSTOMER,
		// 				Material: row.PRICE_MASTER.MATERIAL,
		// 				Ship_To: row.PRICE_MASTER.SHIP_TO,
		// 				Dis_Chnl: row.PRICE_MASTER.DISTR_CHAN,
		// 				SalesOrg: row.PRICE_MASTER.SALESORG,
		// 				C_DOEBKUNDE: row.PRICE_MASTER.C_DOEBKUNDE
		// 			});
		// 		});

		// 		customerNumber = items[0].C_DOEBKUNDE;
		// 		filteredCustomer = items.filter(function (line) {
		// 			return line.C_DOEBKUNDE === customerNumber;
		// 		});
		// 		if (filteredCustomer.length < items.length) {
		// 			MessageBox.warning("Please select customer(s) that have the same reporting customer!");
		// 			return;
		// 		}

		// 		table.setSelectedIndex(-1);
		// 		stringfyJSON = JSON.stringify(items);
		// 		sap.ui.getCore().getModel("tenderM").setData([]);
		// 		this.getRouter().navTo("Tender", {
		// 			query: {
		// 				tab: stringfyJSON
		// 			}
		// 		}, true /*without history*/ );
		// 	}

		// },

		/*------------------------------------------------------------------------ START OF VARIANT MANAGEMENT --------------------------------------------------------- */
		oCC: null,
		currTableData: null,
		oTPC: null,
		tableId: "priceSettingInfromationTableId",
		onSettings: function () {
			this.oTPC.openDialog();
			return;
			var thiz = this;
			sap.ushell.Container.getService("Personalization").getContainer("com.doehler.PricingMasterFile").then(function (oCC) {
				this.oCC = oCC;
				oCC.delItem(thiz.tableId);
				oCC.save().then(function () {
					Log.info("save", oCC.getItemKeys());
				});
			});
		},

		initVariant: function () {
			var thiz = this;
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var oVM = this.getView().byId("tableVMId");
			oVM.setModel(new sap.ui.model.json.JSONModel());
			// set initial standard variant
			this.setStandardVariant(tableId, function (oCC) {
				oVM.addVariantItem(new sap.ui.comp.variants.VariantItem({
					key: "default",
					text: "default"
				}));
				oVM.removeVariantItem(thiz.getVariantByKey(oVM, "default")); // fix default 
				thiz.oCC = oCC;
				var oItem = oCC.getItemValue(tableId);
				oVM.getModel().setData(oItem.items); // set data in model
				oVM.setInitialSelectionKey(oItem.defaultKey); // set initial default
				oVM.setDefaultVariantKey(oItem.defaultKey); // set initial default
				thiz.setPersoData(oTable, oItem[oItem.defaultKey].data); // apply data
				thiz.currTableData = oItem[oItem.defaultKey].data; // apply first data to currTableData

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oItem[oItem.defaultKey].rowCount === undefined ? 6 : oItem[oItem.defaultKey].rowCount);
				oTable.setVisibleRowCountMode(oItem[oItem.defaultKey].rowCount === undefined ? "Interactive" : "Fixed");

				// attach perso to get current change data
				thiz.oTPC = thiz.getPersoService(oTable, function (data) {
					thiz.currTableData = data; // store data temp
					if (oVM.getSelectionKey() != "*standard*") {
						oVM.currentVariantSetModified(true); // make other variant editable
					}
				});
			});
		},

		/* on select variant */
		onSelectVariant: function (oEvent) {
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			var selKey = oEvent.getParameters().key;
			var oItem = this.oCC.getItemValue(tableId);
			this.setPersoData(oTable, oItem[selKey].data); // apply data

			oTable.setVisibleRowCountMode("Fixed");
			oTable.setVisibleRowCount(oItem[selKey].rowCount === undefined ? 6 : oItem[selKey].rowCount);
			oTable.setVisibleRowCountMode(oItem[selKey].rowCount === undefined ? "Interactive" : "Fixed");

		},
		/* on save new variant */
		onSaveVariant: function (oEvent) {
			var thiz = this;
			var oCC = this.oCC;
			var key = oEvent.getParameters().key;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var tableId = this.tableId;
			var oTable = this.getView().byId(this.tableId);
			// 2 check if varName is available or not if not then create
			var ovar = oCC.getItemValue(tableId);
			if (this.currTableData) {
				if (oEvent.getParameters().def) { // check if default key present
					ovar["defaultKey"] = key; // set default key
				}
				if (!oEvent.getParameters().overwrite) { //if not overwrite then push new item
					ovar.items.push({
						key: key,
						text: varName
					});
				}
				ovar[key] = { // add new variant key with data
					key: key,
					text: varName,
					data: thiz.currTableData,
					rowCount: oTable.getVisibleRowCount()
				};

				oTable.setVisibleRowCountMode("Fixed");
				oTable.setVisibleRowCount(oTable.getVisibleRowCount());

				oCC.setItemValue(tableId, ovar); // set updated obj 
				oCC.save();
			}
		},
		/* on manage VM */
		// { items:[{ key:"", text:"" }], key:{ key:"", text:"" }}
		onManageVM: function (oEvent) {
			var oCC = this.oCC;
			var tableId = this.tableId;
			var ovar = oCC.getItemValue(tableId);
			// Rename
			var renameKeys = oEvent.getParameters().renamed;
			if (renameKeys.length > 0) {
				ovar.items.forEach(function (item) {
					renameKeys.forEach(function (reitem) {
						if (reitem.key === item.key) {
							item.text = reitem.name;
							ovar[item.key].text = reitem.name;
						}
					});
				});
			}
			// Delete
			var deletedKeys = oEvent.getParameters().deleted;
			if (deletedKeys.length > 0) {
				for (var i = ovar.items.length - 1; i >= 0; i--) {
					for (var j = 0; j < deletedKeys.length; j++) {
						if (ovar.items[i] && (ovar.items[i].key === deletedKeys[j])) {
							ovar.items.splice(i, 1);
							delete ovar[deletedKeys[j]];
						}
					}
				}
			}
			ovar["defaultKey"] = oEvent.getParameters().def; // Default
			oCC.setItemValue(tableId, ovar);
			oCC.save(); // save all
		},
		/* get variant name by key */
		getVariantName: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item.getText();
				}
			});
			return selItem;
		},
		/* get variant item by key */
		getVariantByKey: function (oVM, selKey) {
			var aItems = oVM.getVariantItems();
			var selItem = "";
			aItems.forEach(function (item) {
				if (selKey == item.getKey()) {
					selItem = item;
				}
			});
			return selItem;
		},
		/*------------------------------------------------------------------------ END OF VARIANT MANAGEMENT --------------------------------------------------------- */

		/*************************************************Start Header Search Variant******************************************************* ****************/
		oCCHeader: null,
		initSearchVariant: function () {
			var that = this;
			var oVM = this.getView().byId("searchFilterVMIdPSInf");
			var itemName = oVM.data("itemName"); // get item name
			oVM.setModel(new sap.ui.model.json.JSONModel()); // set model
			this.fixVariant(oVM); // fix variant 
			var data = sap.ui.getCore().getModel("psfilterM").getData();
			this.setFilterVariant(itemName, "*standard*", null, data, false, function (oCC) { // create item
				that.oCCHeader = oCC;
				that.setVariantList(oCC, oVM); // set variant list
				//that.addSearchFilter();
			}, function () {
				//that.addSearchFilter();
			});
		},

		/* set variant list from backend */
		setVariantList: function (oCC, oVM) {
			var itemName = oVM.data("itemName");
			var ovar = oCC.getItemValue(itemName);
			if (ovar.hasOwnProperty("items")) {
				oVM.getModel().setData(ovar.items);
			}
			// set inital default key
			// oVM.setInitialSelectionKey(ovar.defaultKey);
			// oVM.setDefaultVariantKey(ovar.defaultKey);
			if (this._globalM.getProperty("/AutoS") !== true) {
				oVM.setInitialSelectionKey(ovar.defaultKey);
				oVM.setDefaultVariantKey(ovar.defaultKey);
				sap.ui.getCore().getModel("psfilterM").setData(ovar[ovar.defaultKey]);
				this.createToken();
			}
		},

		/* on select variant */
		onSelectVariantPSI: function (oEvent) {

			this.clearSelectionFields(); // clear previous value
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			var ovar = oCC.getItemValue(itemName);
			var selKey = oEvent.getParameters().key;
			sap.ui.getCore().getModel("psfilterM").setData(ovar[selKey]);
			this.createToken();
		},

		/* on save variant */
		onSaveVariantPSI: function (oEvent) {
			var thiz = this;
			var itemName = oEvent.getSource().data("itemName");
			var key = oEvent.getParameters().key;
			var bDefault = oEvent.getParameters().def;
			var varName = this.getVariantName(oEvent.getSource(), key);
			var data = sap.ui.getCore().getModel("psfilterM").getData();
			this.setFilterVariant(itemName, key, varName, data, bDefault, function (oCC) {
				thiz.oCCHeader = oCC;
			});
		},

		/* on manage variant */
		onManageVMPSI: function (oEvent) {
			var oCC = this.oCCHeader;
			var itemName = oEvent.getSource().data("itemName");
			this.setManageVM(oEvent, oCC, itemName);
		},

		/* create token based on selected values */
		createToken: function () {
			var aControls = this.getView().byId("searchGridId").getContent();
			aControls.forEach(function (item) {
				if (item.getMetadata().getName() === "com.doehler.PricingMasterFile.customControls.MultiValueHelpControl" || item.getMetadata()
					.getName() ===
					"sap.m.MultiInput") {
					if (item.getMetadata().getName() === "sap.m.MultiInput")
						item.destroyTokens();
					if (item.getId().indexOf("multiInputIssuesKpis") <= -1) {
						if (item.getMultiSelect() && item.getSelectedValues()) {
							var arr = item.getSelectedValues().split(",");
							arr.forEach(function (value) {
								if (value != "") {
									item.addToken(new sap.m.Token({
										key: value,
										text: value
									}));
								}
							});
						}
					} else {
						if (sap.ui.getCore().getModel("psfilterM").getData().REDP === "X") {
							item.addToken(new sap.m.Token({
								key: 0,
								text: "Reds(position)"
							}));
						}
						if (sap.ui.getCore().getModel("psfilterM").getData().REDC === "X") {
							item.addToken(new sap.m.Token({
								key: 1,
								text: "Reds(customer)"
							}));
						}
						if (sap.ui.getCore().getModel("psfilterM").getData().LAST_PRICE_1 === "X") {
							item.addToken(new sap.m.Token({
								key: 2,
								text: "Last price change(last price change > 1 year"
							}));
						}
						if (sap.ui.getCore().getModel("psfilterM").getData().LAST_PRICE_2 === "X") {
							item.addToken(new sap.m.Token({
								key: 3,
								text: "Last price change(last price change > 2 year"
							}));
						}
						if (sap.ui.getCore().getModel("psfilterM").getData().GAP_TO_TARGET === "X") {
							item.addToken(new sap.m.Token({
								key: 4,
								text: "L & I-Account increase"
							}));
						}
						if (sap.ui.getCore().getModel("psfilterM").getData().ACCOUNT_INCREASE === "X") {
							item.addToken(new sap.m.Token({
								key: 5,
								text: "GAP to Target"
							}));
						}

					}
				}
			});
		},

		/*************************************************End Header Search Variant***********************************************************************/

		afterRender: false,
		onAfterRendering: function () {
			if (this.afterRender) {
				return;
			}
			// this.loadDropdown();
			// this.afterRender = true;
			// this.initVariant();
			// this.initSearchVariant();
		}

		/*-------------------------END OF VARIANT MANAGEMENT ------------------------*/
	});

});