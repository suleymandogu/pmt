sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/doehler/PricingMasterFile/model/models"
], function (UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("com.doehler.PricingMasterFile.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			models.initModel();
			models.initDropdown();
			models.getUser();
			models.getReportHostname(); // get report hostname and portno
			models.getUserCustomer();
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);
			// enable routing
			this.getRouter().initialize();
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
		}
	});
});