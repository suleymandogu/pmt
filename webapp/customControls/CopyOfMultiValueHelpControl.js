sap.ui.define([
	"sap/m/MultiInput",
	"sap/m/SearchField",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/table/Column", 'sap/m/Input', 'sap/m/Tokenizer', 'sap/m/Token', 'sap/m/library', 'sap/ui/core/EnabledPropagator',
	'sap/ui/base/ManagedObjectMetadata',
	'sap/ui/Device', 'sap/m/Popover', 'sap/m/List', 'sap/m/Title', 'sap/m/Bar', 'sap/m/Toolbar', 'sap/m/StandardListItem',
	'sap/ui/core/ResizeHandler',
	'sap/ui/core/IconPool', 'sap/m/MultiInputRenderer', "sap/ui/dom/containsOrEquals", "sap/ui/events/KeyCodes", 'sap/ui/core/InvisibleText',
	"sap/ui/thirdparty/jquery", "sap/ui/dom/jquery/cursorPos", "sap/ui/dom/jquery/control"
], function (MultiInput, SearchField, Filter, FilterOperator, Column, I, T, a, l, E, M, D, P, L, b, B, c, S, R, d, f, g, K, h, q) {
	"use strict";
	var j = l.PlacementType,
		k = l.ListMode;
	var m = I.extend("sap.m.MultiInput", {
		metadata: {
			library: "sap.m",
			designtime: "sap/m/designtime/MultiInput.designtime",
			properties: {
				enabled: {
					type: "boolean",
					defaultValue: true
				},

				serverSide: {
					type: "boolean",
					defaultValue: true
				},

				fieldName: {
					type: "string"
				},

				descFieldName: {
					type: "string"
				},

				kotab: {
					type: "string"
				},

				relFieldModel: {
					type: "string"
				},

				relFieldName1: {
					type: "string"
				},

				relFieldName2: {
					type: "string"
				},

				relFieldName3: {
					type: "string"
				},
				relFieldName4: {
					type: "string"
				},

				relFieldName5: {
					type: "string"
				},

				relFieldName6: {
					type: "string"
				},

				relFieldName7: {
					type: "string"
				},

				relFieldName8: {
					type: "string"
				},
				ctrlBindingPath: {
					type: "string"
				},

				treeEnabled: {
					type: "boolean",
					defaultValue: false
				},

				manualDataEnabled: {
					type: "boolean",
					defaultValue: false
				},

				dataFunction: {
					type: "object"
				},

				multiSelect: {
					type: "boolean",
					defaultValue: false
				},

				selectedValues: {
					type: "string"
				},
				controller: {
					type: "object"
				},
				desc: {
					type: "string"
				},

				enableMultiLineMode: {
					type: "boolean",
					group: "Behavior",
					defaultValue: false
				},
				maxTokens: {
					type: "int",
					group: "Behavior"
				}
			},
			aggregations: {
				tokens: {
					type: "sap.m.Token",
					multiple: true,
					singularName: "token"
				},
				tokenizer: {
					type: "sap.m.Tokenizer",
					multiple: false,
					visibility: "hidden"
				}
			},
			events: {
				onSelectValue: {
					parameters: {
						selectedValue: "string"
					}
				},

				onInitialize: {
					parameters: {

					}
				},

				onGetData: {
					parameters: {
						fnSuccess: "function"
					}
				},
				onSelectToken: {
					parameters: {

					}
				},

				tokenChange: {
					parameters: {
						type: {
							type: "string"
						},
						token: {
							type: "sap.m.Token"
						},
						tokens: {
							type: "sap.m.Token[]"
						},
						addedTokens: {
							type: "sap.m.Token[]"
						},
						removedTokens: {
							type: "sap.m.Token[]"
						}
					}
				},
				tokenUpdate: {
					allowPreventDefault: true,
					parameters: {
						type: {
							type: "string"
						},
						addedTokens: {
							type: "sap.m.Token[]"
						},
						removedTokens: {
							type: "sap.m.Token[]"
						}
					}
				}
			},
			dnd: {
				draggable: false,
				droppable: true
			}
		}
	});

	E.apply(m.prototype, [true]);
	var r = sap.ui.getCore().getLibraryResourceBundle("sap.m");
	m.prototype.init = function () {
		this._bShowListWithTokens = false;
		I.prototype.init.call(this);
		this._bIsValidating = false;
		this._tokenizer = new T();
		this._tokenizer._setAdjustable(true);
		this.setAggregation("tokenizer", this._tokenizer);
		this._tokenizer.attachTokenChange(this._onTokenChange, this);
		this._tokenizer.attachTokenUpdate(this._onTokenUpdate, this);
		this._tokenizer._handleNMoreIndicatorPress(this._handleIndicatorPress.bind(this));
		this._tokenizer.addEventDelegate({
			onThemeChanged: this._handleInnerVisibility.bind(this)
		}, this);
		this.setShowValueHelp(true);
		this.setShowSuggestion(true);
		this.attachSuggestionItemSelected(this._onSuggestionItemSelected, this);
		this.attachLiveChange(this._onLiveChange, this);
		this.attachValueHelpRequest(this._onValueHelpRequested, this);
		this._getValueHelpIcon().setProperty("visible", true, true);
		this._modifySuggestionPicker();

		MultiInput.prototype.init.apply(this, arguments);

		this.attachValueHelpRequest(m._valueHelpRequest.bind(this));
		this.attachChange(m._valueHelpChange.bind(this));
		this.attachSubmit(m._valueHelpSubmit.bind(this));
		this.attachLiveChange(m._valueHelpChange.bind(this));
		this.attachTokenChange(m._onUpdateTokens.bind(this));
		this.setShowValueHelp(true);
		this.setEnableMultiLineMode(true);
		var thiz = this;
		this.addValidator(function (args) {
			window.setTimeout(function () {
				args.asyncCallback(new sap.m.Token({
					key: args.text,
					text: args.text
				}));
				thiz.setSelectedValues(thiz.getSelectedValues());
				thiz.closeMultiLine();
			}, 0);
			return sap.m.MultiInput.WaitForAsyncValidation;
		});

	};

	m.prototype.onAfterRendering = function () {};

	m._onUpdateTokens = function () {
		this.setSelectedValues(this.getSelectedKeys());
	};

	m._valueHelpSubmit = function (oEvent) {
		var oValue = oEvent.getParameter("value");
		if (this.getMultiSelect()) {
			/*this.addToken(new sap.m.Token({
			    key: oValue,
			    text: oValue
			}));
			this.setValue("");

			this.setSelectedValues(this.getSelectedKeys());*/
			//this.closeMultiLine();
		} else
			this.setValue(oValue.toUpperCase());

	};

	m._valueHelpChange = function (oEvent) {
		if (!this.getMultiSelect()) {
			var oValue = oEvent.getParameter("value");
			this.setValue(oValue);
		}
	};

	m.prototype.getSelectedKeys = function () {
		var result = "";

		if (!this.getMultiSelect())
			return result;

		var oTokens = this.getTokens();

		for (var i = 0; i < oTokens.length; i++) {
			result = result + ',' + oTokens[i].getKey();
		}

		if (result !== "")
			result = result.substr(1, result.length);

		return result;
	};

	m._valueHelpRequest = function () {

		this.fireEvent("onInitialize");
		var thiz = this;

		if (this.getManualDataEnabled()) {
			this.fireEvent("onGetData", {
				fnSuccess: this._prepareValueHelpDialog
			});

		} else {
			var callFunc = this.getDataFunction();

			this.setBusy(true);

			callFunc(this.getController(),
				this.getFieldName(),
				this.getKotab(),
				this._getRelatedFieldModelData(),
				this._getServerSideModelData(),
				function (oModel, controller) {
					thiz.setBusy(false);
					thiz.getController().getView().setModel(oModel, "valueHelpModel");

					if (thiz.getTreeEnabled()) {
						thiz._prepareValueHelpTreeDialog(thiz.getId());
					} else {
						thiz._prepareValueHelpDialog();
					}
				});
		}
	};

	m.prototype._getServerSideModelData = function (searchTerm) {
		var oModel = {
			NoOfEntry: 100,
			SearchTerm: ""
		};

		if (this.getServerSide()) {
			oModel.SearchTerm = searchTerm;
		}

		return oModel;
	};

	m.prototype._getRelatedFieldModelData = function () {
		/*
		 * Related Field Configuration----
		 * If the current control should filter the values by using the value from another field in the model
		 * Then by using the related properties we will get the values and pass them to the API
		 */

		var relFldValue1,
			relFldValue2,
			relFldValue3,
			relFldValue4,
			relFldValue5,
			relFldValue6,
			relFldValue7,
			relFldValue8 = "";

		if (this.getRelFieldModel()) {

			var oRelFieldModel = this.getController().getView().getModel(this.getRelFieldModel());

			if (this.getRelFieldName1()) {
				relFldValue1 = oRelFieldModel.getProperty(this.getRelFieldName1());
			}

			if (this.getRelFieldName2()) {
				relFldValue2 = oRelFieldModel.getProperty(this.getRelFieldName2());
			}

			if (this.getRelFieldName3()) {
				relFldValue3 = oRelFieldModel.getProperty(this.getRelFieldName3());
			}
			if (this.getRelFieldName4()) {
				relFldValue4 = oRelFieldModel.getProperty(this.getRelFieldName4());
			}

			if (this.getRelFieldName5()) {
				relFldValue5 = oRelFieldModel.getProperty(this.getRelFieldName5());
			}

			if (this.getRelFieldName6()) {
				relFldValue6 = oRelFieldModel.getProperty(this.getRelFieldName6());
			}

			if (this.getRelFieldName7()) {
				relFldValue7 = oRelFieldModel.getProperty(this.getRelFieldName7());
			}

			if (this.getRelFieldName8()) {
				relFldValue8 = oRelFieldModel.getProperty(this.getRelFieldName8());
			}

		}

		var oRelFldModel = {
			RelFldValue1: relFldValue1,
			RelFldValue2: relFldValue2,
			RelFldValue3: relFldValue3,
			RelFldValue4: relFldValue4,
			RelFldValue5: relFldValue5,
			RelFldValue6: relFldValue6,
			RelFldValue7: relFldValue7,
			RelFldValue8: relFldValue8
		};

		return oRelFldModel;
	};

	m.prototype._onValueHelpSearch = function (oEvent) {
		var thiz = this;
		var searchStr = this.getTreeEnabled() ? oEvent.getParameter("query") : oEvent.getParameter("value");
		if (this.getFieldName() === "MATERIAL" && searchStr !== "")
			searchStr = searchStr.replace(/\./g, '');

		/*
		 * If the server side mode is enabled, we send request to the server to get the data by using the search term
		 */
		if (this.getServerSide()) {

			var callFunc = this.getDataFunction();

			callFunc(this.getController(),
				this.getFieldName(),
				this.getKotab(),
				this._getRelatedFieldModelData(),
				this._getServerSideModelData(searchStr),
				function (oModel, controller) {
					thiz.getController().getView().setModel(oModel, "valueHelpModel");
				});

		} else {
			/*
			 * If we are in client mode, then the search feature will work on the client
			 */

			var filters = "";
			var fieldNames = this.getTreeEnabled() ? ["PRODH", "VTEXT", "CHILDS/PRODH", "CHILDS/VTEXT"] : this.getController().getView().getModel(
				"valueHelpModel").getProperty("/F4_STRUC_DYN");
			var f4Filter = [];

			if (searchStr && searchStr.length > 0) {
				for (var i = 0; i < fieldNames.length; i++) {
					f4Filter.push(new sap.ui.model.Filter(fieldNames[i], sap.ui.model.FilterOperator.Contains, searchStr));
				}
				filters = new sap.ui.model.Filter({
					filters: f4Filter,
					and: false
				});
			}

			var oBinding = this.getTreeEnabled() ? this._oTree.getBinding("rows") : oEvent.getSource().getBinding("items");
			oBinding.filter(filters);
		}
	};

	m.prototype._onValueHelpConfirm = function (oEvent) {
		if (this.getTreeEnabled()) {
			var selectedIndx = oEvent.getParameter("rowIndex");

			if (this._selectedIndex === undefined || this._selectedIndex !== selectedIndx) {
				this._selectedIndex = selectedIndx;
				this._selectedTreeId = this.getModel("valueHelpModel").getProperty(oEvent.getParameter("rowContext").getPath()).PRODH;
				this._selectedTreeDesc = this.getModel("valueHelpModel").getProperty(oEvent.getParameter("rowContext").getPath()).VTEXT;
				this._oTree.clearSelection();
				this._oTree.setSelectedIndex(selectedIndx);
			}
		} else {

			if (this.getMultiSelect()) {
				this._setTokens(oEvent.getParameters().selectedItems);
			} else {

				var custData = oEvent.getParameters().selectedItem.getCustomData();
				var dispValue = custData[0].getValue();
				this.setValue(dispValue);

				//console.log(this.getModel("valueHelpModel").getData());

				if (this.getDescFieldName() !== "" && this.getDescFieldName() !== undefined) {
					//var descValue = custData[1].getValue();
					//this.getController().getObj(this.getId() + this.getDescFieldName()).setText(descValue)
					//this.setDescription(descValue);
				}
			}
			this.fireEvent("onSelectToken");
			this._destroyDialog();
		}
	};
	m.prototype._onSelectTokenfn = function () {
		return this.getSelectedKeys();
	};

	m.prototype._setTokens = function (selectedRows) {
		//this.removeAllTokens();
		for (var i = 0; i < selectedRows.length; i++) {
			var oValue = selectedRows[i].getCustomData()[0].getValue();
			var oText = selectedRows[i].getCustomData()[1].getValue();

			// if (selectedRows[i].getParent().getId() !== undefined) {
			// 	if (selectedRows[i].getParent().getId().indexOf("priceSettingUAMInputIdDialog") > -1 ||
			// 		selectedRows[i].getParent().getId().indexOf("priceSettingUCSInputIdDialog") > -1)
			// 		oValue = selectedRows[i].getCustomData()[1].getValue();
			// }

			this.addToken(new sap.m.Token({
				key: oValue,
				text: oText
			}));
		}
	};

	m.prototype._onValueHelpClose = function () {
		this._destroyDialog();
	};

	m.prototype._destroyDialog = function () {
		this.getController().getView().removeDependent(this._oDialog);
		this._oDialog.destroy();
	};

	m.prototype._prepareValueHelpTreeDialog = function (popupId) {
		// var thiz = this;
		// var oModel = this.getController().getView().getModel("valueHelpModel");

		this._oTree = new sap.ui.table.TreeTable({
			rows: "{path:'valueHelpModel>/PRODHIERARCHY', parameters: {arrayNames:['CHILDS']}}",
			selectionMode: "MultiToggle",
			enableSelectAll: false,
			width: "100%",
			enableColumnReordering: false,
			expandFirstLevel: false,
			rowSelectionChange: this._onValueHelpConfirm.bind(this),
			//				toolbar : new sap.m.Toolbar({
			//					content : new sap.m.SearchField({
			//						width : "100%",
			//						selectOnFocus : false,
			//						search : this._onValueHelpSearch.bind(this)
			//					})
			//				}),
			columns: [
				new Column("", {
					label: new sap.m.Label({
						text: "ID"
					}),
					template: new sap.m.Text("", {
						text: "{valueHelpModel>PRODH}"
					})
				}),
				new Column({
					label: new sap.m.Label({
						text: "Name"
					}),
					template: new sap.m.Text({
						text: "{valueHelpModel>VTEXT}"
					})
				})
			]
		});

		this._oDialog = new sap.m.Dialog({
			busyIndicatorDelay: 0,
			title: "Select Dialog",
			content: [this._oTree],
			contentWidth: "600px",
			beginButton: new sap.m.Button({
				text: that.getController().getText("select"),
				press: function () {
					if (that._selectedTreeId) {
						that.setValue(that._selectedTreeId);

						if (that.getDescFieldName() !== "") {
							that.getController().getObj(that.getId() + that.getDescFieldName()).setText(that._selectedTreeDesc);
						}

						that._destroyDialog();
					}
				}
			}),
			endButton: new sap.m.Button({
				text: that.getController().getText("cancel"),
				press: function () {
					that._destroyDialog();
				}
			}),
			afterClose: function () {
				that._destroyDialog();
			}
		}).addStyleClass("sapUiSizeCompact");

		this.getController().getView().addDependent(this._oDialog);
		this._oDialog.open();
	};

	m.prototype._prepareValueHelpDialog = function (popupId) {
		var thiz = this;
		var popupId = this.getId();
		var oModel = this.getController().getView().getModel("valueHelpModel");

		this._oDialog = new sap.m.TableSelectDialog(popupId + "Dialog", {
			//liveChange : this._onValueHelpSearch.bind(this),
			search: this._onValueHelpSearch.bind(this),
			confirm: this._onValueHelpConfirm.bind(this),
			//close: this._onValueHelpClose.bind(this),
			cancel: this._onValueHelpClose.bind(this),
			multiSelect: this.getMultiSelect(),
			growingThreshold: 100,
			rememberSelections: true,
			draggable: true,
			resizable: true
		});

		// sap.ui.getCore().getModel("globalModel").setProperty("/popupId", popupId);

		var columnsModel = oModel.getProperty("/F4_DESCR_DYN");
		var tableCells = [],
			customData = [];

		/*
		 * Add Columns and Cells of the Table
		 */
		columnsModel.forEach(function (aColumn) {
			thiz._oDialog.addColumn(
				new sap.m.Column({
					header: new sap.m.Label({
						text: aColumn.MDTXT,
						design: "Bold",
						wrapping: true
					})
				})
			);

			tableCells.push(new sap.m.Text({
				text: "{valueHelpModel>" + aColumn.FIELDNAME + "}"
			}));
		});

		/*
		 * Store the primary key and value of the current field control to use after the dialog being closed
		 */
		var oKey = oModel.getProperty("/F4_FIELD_DYN");
		customData.push(new sap.ui.core.CustomData({
			key: oKey,
			value: "{valueHelpModel>" + oKey + "}"
		}));

		/*
		 * If the description field is available, store the value of the text in custom data
		 */
		if (this.getDescFieldName() !== "") {
			customData.push(new sap.ui.core.CustomData({
				key: this.getDescFieldName(),
				value: "{valueHelpModel>" + this.getDescFieldName() + "}"
			}));
		}

		/*
		 * Bind data to the table
		 */
		this._oDialog.bindItems("valueHelpModel>/F4_VALUE_DYN", new sap.m.ColumnListItem({
			cells: tableCells,
			customData: customData
		}));

		/*
		 * Configure dialog
		 */
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getController().getView(), this._oDialog);
		this._oDialog._table.mProperties.growingScrollToLoad = false;
		this.getController().getView().addDependent(this._oDialog);
		this._oDialog.open();
	};
	m.prototype.exit = function () {
		I.prototype.exit.call(this);
		if (this._oSelectedItemPicker) {
			this._oSelectedItemPicker.destroy();
			this._oSelectedItemPicker = null;
		}
		if (this._oSelectedItemsList) {
			this._oSelectedItemsList.destroy();
			this._oSelectedItemsList = null;
		}
		if (this._getReadOnlyPopover()) {
			var o = this._getReadOnlyPopover();
			o.destroy();
			o = null;
		}
		this._deregisterResizeHandler();
	};
	m.prototype.onBeforeRendering = function () {
		I.onBeforeRendering.apply(this, arguments);
		this._tokenizer.setEnabled(this.getEnabled());
	};
	m.prototype.onAfterRendering = function () {
		this._bTokenIsValidated = false;
		this._tokenizer.scrollToEnd();
		this._registerResizeHandler();
		this._tokenizer.setMaxWidth(this._calculateSpaceForTokenizer());
		this._handleNMoreAccessibility();
		this._handleInnerVisibility();
		this._syncInputWidth(this._tokenizer);
		I.prototype.onAfterRendering.apply(this, arguments);
	};
	m.prototype._handleInnerVisibility = function () {
		var H = this._tokenizer._hasMoreIndicator();
		this[H ? "_setValueInvisible" : "_setValueVisible"].call(this);
	};
	m.prototype.oninput = function (e) {
		I.prototype.oninput.call(this, e);
		this._manageListsVisibility(false);
		this._getSelectedItemsPicker().close();
	};
	m.prototype._registerResizeHandler = function () {
		if (!this._iResizeHandlerId) {
			this._iResizeHandlerId = R.register(this, this._onResize.bind(this));
		}
		if (!this._iTokenizerResizeHandler) {
			this._iTokenizerResizeHandler = R.register(this._tokenizer, this._onResize.bind(this));
		}
	};
	m.prototype._deregisterResizeHandler = function () {
		if (this._iResizeHandlerId) {
			R.deregister(this._iResizeHandlerId);
			this._iResizeHandlerId = null;
		}
		if (this._iTokenizerResizeHandler) {
			R.deregister(this._iTokenizerResizeHandler);
			this._iTokenizerResizeHandler = null;
		}
	};
	m.prototype._onResize = function () {
		this._tokenizer.setMaxWidth(this._calculateSpaceForTokenizer());
		this._handleInnerVisibility();
		this._syncInputWidth(this._tokenizer);
		this._handleNMoreAccessibility();
		this._registerResizeHandler();
	};
	m.prototype._onTokenChange = function (e) {
		this.fireTokenChange(e.getParameters());
		this.invalidate();
		if (e.getParameter("type") === "removed") {
			this._tokenizer._useCollapsedMode(false);
		}
		if ((this._oSuggestionPopup && this._oSuggestionPopup.isOpen()) || this._bUseDialog) {
			this._fillList();
		}
		if (this._bUseDialog) {
			this._manageListsVisibility(true);
		}
	};
	m.prototype._onTokenUpdate = function (e) {
		var i = this.fireTokenUpdate(e.getParameters());
		if (!this.getTokens().length) {
			this.$().find("input").focus();
		}
		if (!i) {
			e.preventDefault();
		} else {
			this.invalidate();
		}
	};
	m.prototype._onSuggestionItemSelected = function (e) {
		var i = null,
			t = null,
			o = this._tokenizer.getTokens().length;
		if (this.getMaxTokens() && o >= this.getMaxTokens() || this._bValueHelpOpen) {
			return;
		}
		if (this._hasTabularSuggestions()) {
			i = e.getParameter("selectedRow");
		} else {
			i = e.getParameter("selectedItem");
			if (i) {
				t = new a({
					text: i.getText(),
					key: i.getKey()
				});
			}
		}
		if (i && !this._bTokenIsAdded) {
			var n = this.getValue();
			this._tokenizer._addValidateToken({
				text: n,
				token: t,
				suggestionObject: i,
				validationCallback: this._validationCallback.bind(this, o)
			});
		}
		if (this._bUseDialog) {
			var N = this._tokenizer.getTokens().length;
			if (o < N) {
				this.setValue("");
			}
			if (this._getSuggestionsList() instanceof sap.m.Table) {
				this._getSuggestionsList().addStyleClass("sapMInputSuggestionTableHidden");
			} else {
				this._getSuggestionsList().destroyItems();
			}
			var s = this._oSuggestionPopup.getScrollDelegate();
			if (s) {
				s.scrollTo(0, 0, 0);
			}
			this._oSuggPopover._oPopupInput.focus();
		}
		this._bTokenIsAdded = false;
	};
	m.prototype._onValueHelpRequested = function () {
		this._bValueHelpOpen = true;
	};
	m.prototype._onLiveChange = function (e) {
		this._tokenizer._removeSelectedTokens();
	};
	m.prototype._setValueInvisible = function () {
		this.$("inner").css("opacity", "0");
	};
	m.prototype._setValueVisible = function () {
		this.$("inner").css("opacity", "1");
	};
	m.prototype.onmousedown = function (e) {
		if (e.target == this.getDomRef('content')) {
			e.preventDefault();
			e.stopPropagation();
		}
	};
	m.prototype._openMultiLineOnDesktop = function () {};
	m.prototype.openMultiLine = function () {};
	m.prototype.closeMultiLine = function () {};
	m.prototype.getScrollDelegate = function () {
		return this._tokenizer._oScroller;
	};
	m.prototype.onBeforeRendering = function () {
		I.prototype.onBeforeRendering.apply(this, arguments);
		this._deregisterResizeHandler();
	};
	m.prototype.addValidator = function (v) {
		this._tokenizer.addValidator(v);
	};
	m.prototype.removeValidator = function (v) {
		this._tokenizer.removeValidator(v);
	};
	m.prototype.removeAllValidators = function () {
		this._tokenizer.removeAllValidators();
	};
	m.prototype.onsapnext = function (e) {
		if (e.isMarked()) {
			return;
		}
		var F = q(document.activeElement).control()[0];
		if (!F) {
			return;
		}
		if (this._tokenizer === F || this._tokenizer.$().find(F.$()).length > 0) {
			this._scrollAndFocus();
		}
	};
	m.prototype.onsapbackspace = function (e) {
		if (this.getCursorPosition() > 0 || !this.getEditable() || this.getValue().length > 0) {
			return;
		}
		if (!e.isMarked()) {
			T.prototype.onsapbackspace.apply(this._tokenizer, arguments);
		}
		if (e.isMarked("forwardFocusToParent")) {
			this.focus();
		}
		e.preventDefault();
		e.stopPropagation();
	};
	m.prototype.onsapdelete = function (e) {
		if (!this.getEditable()) {
			return;
		}
		if (this.getValue() && !this._completeTextIsSelected()) {
			return;
		}
		if (e.isMarked("forwardFocusToParent")) {
			this.focus();
		}
	};
	m.prototype.onkeydown = function (e) {
		if (!this.getEnabled()) {
			return;
		}
		if (e.which === K.TAB) {
			this._tokenizer._changeAllTokensSelection(false);
		}
		if ((e.ctrlKey || e.metaKey) && e.which === K.A) {
			if (this._tokenizer.getTokens().length > 0) {
				this._tokenizer.focus();
				this._tokenizer._changeAllTokensSelection(true);
				e.preventDefault();
			}
		}
		if ((e.ctrlKey || e.metaKey) && (e.which === K.C || e.which === K.INSERT)) {
			this._tokenizer._copy();
		}
		if (((e.ctrlKey || e.metaKey) && e.which === K.X) || (e.shiftKey && e.which === K.DELETE)) {
			if (this.getEditable()) {
				this._tokenizer._cut();
			} else {
				this._tokenizer._copy();
			}
		}
	};
	m.prototype.onpaste = function (e) {
		var o, i, v = [],
			A = [];
		if (this.getValueHelpOnly()) {
			return;
		}
		if (window.clipboardData) {
			o = window.clipboardData.getData("Text");
		} else {
			o = e.originalEvent.clipboardData.getData('text/plain');
		}
		var s = this._tokenizer._parseString(o);
		if (s.length <= 1) {
			return;
		}
		setTimeout(function () {
			if (s) {
				if (this.fireEvent("_validateOnPaste", {
						texts: s
					}, true)) {
					var n = "";
					for (i = 0; i < s.length; i++) {
						if (s[i]) {
							var t = this._convertTextToToken(s[i], true);
							if (t) {
								v.push(t);
							} else {
								n = s[i];
							}
						}
					}
					this.updateDomValue(n);
					for (i = 0; i < v.length; i++) {
						if (this._tokenizer._addUniqueToken(v[i])) {
							A.push(v[i]);
						}
					}
					if (A.length > 0) {
						this.fireTokenUpdate({
							addedTokens: A,
							removedTokens: [],
							type: T.TokenUpdateType.Added
						});
					}
				}
				if (A.length) {
					this.cancelPendingSuggest();
				}
			}
		}.bind(this), 0);
	};
	m.prototype._convertTextToToken = function (t, C) {
		var e = null,
			i = null,
			n = null,
			o = this._tokenizer.getTokens().length;
		if (!this.getEditable()) {
			return null;
		}
		t = t.trim();
		if (!t) {
			return null;
		}
		if (this._getIsSuggestionPopupOpen() || C) {
			if (this._hasTabularSuggestions()) {
				i = this._oSuggestionTable._oSelectedItem;
			} else {
				i = this._getSuggestionItem(t);
			}
		}
		if (i && i.getText && i.getKey) {
			n = new a({
				text: i.getText(),
				key: i.getKey()
			});
		}
		e = this._tokenizer._validateToken({
			text: t,
			token: n,
			suggestionObject: i,
			validationCallback: this._validationCallback.bind(this, o)
		});
		return e;
	};
	m.prototype._validationCallback = function (o, v) {
		var n = this._tokenizer.getTokens().length;
		this._bIsValidating = false;
		if (v) {
			this.setValue("");
			this._bTokenIsValidated = true;
			if (this._bUseDialog && this._oSuggPopover && this._oSuggPopover._oPopupInput && (o < n)) {
				this._oSuggPopover._oPopupInput.setValue("");
			}
		}
	};
	m.prototype.onsapprevious = function (e) {
		if (this._getIsSuggestionPopupOpen()) {
			return;
		}
		if (this.getCursorPosition() === 0) {
			if (e.srcControl === this) {
				T.prototype.onsapprevious.apply(this._tokenizer, arguments);
			}
		}
		if (e.keyCode === K.ARROW_UP) {
			e.preventDefault();
		}
	};
	m.prototype._scrollAndFocus = function () {
		this._tokenizer.scrollToEnd();
		this.$().find("input").focus();
	};
	m.prototype.onsaphome = function (e) {
		if (!this.getFocusDomRef().selectionStart) {
			T.prototype.onsaphome.apply(this._tokenizer, arguments);
		}
	};
	m.prototype.onsapend = function (e) {
		if (e.isMarked("forwardFocusToParent")) {
			this.focus();
		}
	};
	m.prototype.onsapenter = function (e) {
		if (I.prototype.onsapenter) {
			I.prototype.onsapenter.apply(this, arguments);
		}
		var v = true;
		if (this._oSuggestionPopup && this._oSuggestionPopup.isOpen()) {
			if (this._hasTabularSuggestions()) {
				v = !this._oSuggestionTable.getSelectedItem();
			} else {
				v = !this._getSuggestionsList().getSelectedItem();
			}
		}
		if (v) {
			this._validateCurrentText();
		}
		if (!this.getEditable() && this._tokenizer._hasMoreIndicator() && e.target === this.getFocusDomRef()) {
			this._handleIndicatorPress();
		}
		this.focus();
	};
	m.prototype._checkFocus = function () {
		return this.getDomRef() && g(this.getDomRef(), document.activeElement);
	};
	m.prototype.onsapfocusleave = function (e) {
		var p = this._oSuggestionPopup,
			s = this._oSelectedItemPicker,
			n = false,
			N = false,
			i = this._checkFocus(),
			o, F;
		if (p.isA("sap.m.Popover")) {
			if (e.relatedControlId) {
				o = sap.ui.getCore().byId(e.relatedControlId).getFocusDomRef();
				n = g(p.getFocusDomRef(), o);
				N = g(this._tokenizer.getFocusDomRef(), o);
				if (s) {
					F = g(s.getFocusDomRef(), o);
				}
			}
		}
		if (!N && !n) {
			this._tokenizer.scrollToEnd();
		}
		I.prototype.onsapfocusleave.apply(this, arguments);
		if (this._bIsValidating || this._bValueHelpOpen) {
			return;
		}
		if (!this._bUseDialog && !n && e.relatedControlId !== this.getId() && !N) {
			this._validateCurrentText(true);
		}
		if (!this._bUseDialog && this.getEditable()) {
			if (i || n) {
				return;
			}
		}
		if (!F && !N) {
			this._tokenizer._useCollapsedMode(true);
		}
		this._handleInnerVisibility();
	};
	m.prototype._onDialogClose = function () {
		this.setAggregation("tokenizer", this._tokenizer);
		this._tokenizer.setReverseTokens(false);
		this._tokenizer.invalidate();
	};
	m.prototype.ontap = function (e) {
		if (document.activeElement === this._$input[0] || document.activeElement === this._tokenizer.getDomRef()) {
			this._tokenizer.selectAllTokens(false);
		}
		if (e && e.isMarked("tokenDeletePress")) {
			return;
		}
		I.prototype.ontap.apply(this, arguments);
	};
	m.prototype._onclick = function (e) {
		if (this._bUseDialog && this.getTokens().length) {
			this._openSuggestionsPopover();
		}
	};
	m.prototype.onfocusin = function (e) {
		this._deregisterResizeHandler();
		this._bValueHelpOpen = false;
		if (e.target === this.getFocusDomRef()) {
			I.prototype.onfocusin.apply(this, arguments);
		}
		if (!this._bUseDialog && this.getEditable() && (!e.target.classList.contains("sapMInputValHelp") && !e.target.classList.contains(
				"sapMInputValHelpInner"))) {
			if (this._oSuggestionPopup && this._oSuggestionPopup.isOpen()) {
				return;
			}
			this._tokenizer._useCollapsedMode(false);
			this._setValueVisible();
			this._tokenizer.scrollToEnd();
		}
	};
	m.prototype.onsapescape = function (e) {
		this._tokenizer.selectAllTokens(false);
		this.selectText(0, 0);
		I.prototype.onsapescape.apply(this, arguments);
	};
	m.prototype._validateCurrentText = function (e) {
		var t = this.getValue(),
			o = this._tokenizer.getTokens().length;
		if (!t || !this.getEditable()) {
			return;
		}
		t = t.trim();
		if (!t) {
			return;
		}
		var i = null;
		if (e || this._getIsSuggestionPopupOpen()) {
			if (this._hasTabularSuggestions()) {
				i = this._oSuggestionTable._oSelectedItem;
			} else {
				i = this._getSuggestionItem(t, e);
			}
		}
		var n = null;
		if (i && i.getText && i.getKey) {
			n = new a({
				text: i.getText(),
				key: i.getKey()
			});
			this._bTokenIsAdded = true;
		}
		if (!this.getMaxTokens() || this.getTokens().length < this.getMaxTokens()) {
			this._bIsValidating = true;
			this._tokenizer._addValidateToken({
				text: t,
				token: n,
				suggestionObject: i,
				validationCallback: this._validationCallback.bind(this, o)
			});
		}
	};
	m.prototype.getCursorPosition = function () {
		return this._$input.cursorPos();
	};
	m.prototype._completeTextIsSelected = function () {
		var i = this._$input[0];
		if (i.selectionStart !== 0) {
			return false;
		}
		if (i.selectionEnd !== this.getValue().length) {
			return false;
		}
		return true;
	};
	m.prototype._getIsSuggestionPopupOpen = function () {
		return this._oSuggPopover && this._oSuggPopover._oPopover && this._oSuggPopover._oPopover.isOpen();
	};
	m.prototype.setEditable = function (e) {
		e = this.validateProperty("editable", e);
		var t = this._getTokensList();
		if (e === this.getEditable()) {
			return this;
		}
		if (I.prototype.setEditable) {
			I.prototype.setEditable.apply(this, arguments);
		}
		this._tokenizer.setEditable(e);
		if (e) {
			if (this._bUseDialog) {
				this._oSuggPopover._oPopover.addContent(t);
			} else {
				this._getSelectedItemsPicker().addContent(t);
			}
			t.setMode(k.Delete);
		} else {
			t.setMode(k.None);
			this._getReadOnlyPopover().addContent(t);
		}
		return this;
	};
	m.prototype._findItem = function (t, e, n, G) {
		if (!t) {
			return;
		}
		if (!(e && e.length)) {
			return;
		}
		t = t.toLowerCase();
		var o = e.length;
		for (var i = 0; i < o; i++) {
			var p = e[i];
			var s = G(p);
			if (!s) {
				continue;
			}
			s = s.toLowerCase();
			if (s === t) {
				return p;
			}
			if (!n && s.indexOf(t) === 0) {
				return p;
			}
		}
	};
	m.prototype._getSuggestionItem = function (t, e) {
		var n = null;
		var o = null;
		if (this._hasTabularSuggestions()) {
			n = this.getSuggestionRows();
			o = this._findItem(t, n, e, function (p) {
				var s = p.getCells();
				var u = null;
				if (s) {
					var i;
					for (i = 0; i < s.length; i++) {
						if (s[i].getText) {
							u = s[i].getText();
							break;
						}
					}
				}
				return u;
			});
		} else {
			n = this.getSuggestionItems();
			o = this._findItem(t, n, e, function (o) {
				return o.getText();
			});
		}
		return o;
	};
	m.prototype.clone = function () {
		var C;
		this.detachSuggestionItemSelected(this._onSuggestionItemSelected, this);
		this.detachLiveChange(this._onLiveChange, this);
		this._tokenizer.detachTokenChange(this._onTokenChange, this);
		this._tokenizer.detachTokenUpdate(this._onTokenUpdate, this);
		this.detachValueHelpRequest(this._onValueHelpRequested, this);
		C = I.prototype.clone.apply(this, arguments);
		this.attachSuggestionItemSelected(this._onSuggestionItemSelected, this);
		this.attachLiveChange(this._onLiveChange, this);
		this._tokenizer.attachTokenChange(this._onTokenChange, this);
		this._tokenizer.attachTokenUpdate(this._onTokenUpdate, this);
		this.attachValueHelpRequest(this._onValueHelpRequested, this);
		return C;
	};
	m.getMetadata().forwardAggregation("tokens", {
		getter: function () {
			return this._tokenizer;
		},
		aggregation: "tokens",
		forwardBinding: true
	});
	m.prototype.getPopupAnchorDomRef = function () {
		return this.getDomRef("content");
	};
	m.prototype.setTokens = function (t) {
		var v, V = [],
			i;
		if (Array.isArray(t)) {
			for (i = 0; i < t.length; i++) {
				v = this.validateAggregation("tokens", t[i], true);
				M.addAPIParentInfoBegin(t[i], this, "tokens");
				V.push(v);
			}
			this._tokenizer.setTokens(V);
			for (i = 0; i < t.length; i++) {
				M.addAPIParentInfoEnd(t[i]);
			}
		} else {
			throw new Error("\"" + t + "\" is of type " + typeof t + ", expected array for aggregation tokens of " + this);
		}
		return this;
	};
	m.TokenChangeType = {
		Added: "added",
		Removed: "removed",
		RemovedAll: "removedAll",
		TokensChanged: "tokensChanged"
	};
	m.WaitForAsyncValidation = "sap.m.Tokenizer.WaitForAsyncValidation";
	m.prototype.getDomRefForValueStateMessage = m.prototype.getPopupAnchorDomRef;
	m.prototype.updateInputField = function (n) {
		I.prototype.updateInputField.call(this, n);
		this.setDOMValue('');
		if (this._oSuggPopover._oPopupInput) {
			this._oSuggPopover._oPopupInput.setDOMValue('');
		}
	};
	m.prototype.onChange = function (e, p, n) {
		p = p || this.getChangeEventParams();
		if (!this.getEditable() || !this.getEnabled()) {
			return;
		}
		var v = this._getInputValue(n);
		if (v === this._lastValue) {
			this._bCheckDomValue = false;
			return;
		}
		if (!this._bTokenIsValidated) {
			this.setValue(v);
			v = this.getValue();
			this._lastValue = v;
		}
		this.fireChangeEvent(v, p);
		return true;
	};
	m.prototype.getAccessibilityInfo = function () {
		var t = this.getTokens().map(function (o) {
			return o.getText();
		}).join(" ");
		var i = I.prototype.getAccessibilityInfo.apply(this, arguments);
		i.type = r.getText("ACC_CTR_TYPE_MULTIINPUT");
		i.description = ((i.description || "") + " " + t).trim();
		return i;
	};
	m.prototype._modifySuggestionPicker = function () {
		var t = this,
			e, s;
		if (!this._bUseDialog) {
			return;
		}
		this._oSuggPopover._oPopover.addContent(this._getTokensList());
		this._oSuggPopover._oPopover.attachBeforeOpen(function () {
			e = t.getTokens();
			s = e.length ? true : false;
			t._manageListsVisibility(s);
			t._fillList();
			t._updatePickerHeaderTitle();
		}).attachAfterClose(function () {
			t._tokenizer._useCollapsedMode(true);
			t._bShowListWithTokens = false;
		});
	};
	m.prototype._modifyPopupInput = function (p) {
		var t = this;
		p.addEventDelegate({
			oninput: t._manageListsVisibility.bind(t, false),
			onsapenter: function (e) {
				if (p.getValue()) {
					t._closeSuggestionPopup();
				}
				t._validateCurrentText();
				t._setValueInvisible();
				t.onChange(e, null, p.getValue());
			}
		});
		return p;
	};
	m.prototype._hasShowSelectedButton = function () {
		return true;
	};
	m.prototype.forwardEventHandlersToSuggPopover = function (s) {
		I.prototype.forwardEventHandlersToSuggPopover.apply(this, arguments);
		s.setShowSelectedPressHandler(this._handleShowSelectedPress.bind(this));
	};
	m.prototype._handleShowSelectedPress = function (e) {
		this._bShowListWithTokens = e.getSource().getPressed();
		this._manageListsVisibility(this._bShowListWithTokens);
	};
	m.prototype._onBeforeOpenTokensPicker = function () {
		var p = this._getSelectedItemsPicker(),
			o = this.getDomRef(),
			w;
		this._setValueInvisible();
		this._fillList();
		if (o && p) {
			w = (o.offsetWidth / parseFloat(l.BaseFontSize)) + "rem";
			p.setContentMinWidth(w);
		}
	};
	m.prototype._onAfterCloseTokensPicker = function () {
		if (this._oSuggPopover && !this.getValue()) {
			this._tokenizer._useCollapsedMode(true);
			this._setValueInvisible();
		}
	};
	m.prototype.getDialogTitle = function () {
		var p = this._oSuggPopover._oPopover,
			H = p && p.getCustomHeader();
		if (H) {
			return H.getContentMiddle()[0];
		}
		return null;
	};
	m.prototype._updatePickerHeaderTitle = function () {
		var o, e;
		e = this.getLabels();
		if (e.length) {
			o = e[0];
			if (o && (typeof o.getText === "function")) {
				this.getDialogTitle().setText(o.getText());
			}
		} else {
			this.getDialogTitle().setText(r.getText("COMBOBOX_PICKER_TITLE"));
		}
	};
	m.prototype._openSelectedItemsPicker = function () {
		if (this._bUseDialog) {
			this._oSuggPopover._oPopover.open();
		} else {
			var p = this._getSelectedItemsPicker();
			if (p) {
				p.open();
			}
		}
		this._manageListsVisibility(true);
		this._setValueVisible();
		return this;
	};
	m.prototype._getTokensList = function () {
		if (!this._oSelectedItemsList) {
			this._oSelectedItemsList = this._createTokensList();
		}
		return this._oSelectedItemsList;
	};
	m.prototype._getSuggestionsList = function () {
		return this._oSuggPopover && this._oSuggPopover._oList;
	};
	m.prototype._createTokensList = function () {
		return new L({
			width: "auto",
			mode: k.Delete
		}).attachDelete(this._handleNMoreItemDelete, this);
	};
	m.prototype._filterTokens = function (v) {
		this._getTokensList().getItems().forEach(function (i) {
			if (i.getTitle().toLowerCase().indexOf(v) > -1) {
				i.setVisible(true);
			} else {
				i.setVisible(false);
			}
		});
	};
	m.prototype.getFilterSelectedButton = function () {
		return this._getSuggestionsPopover().getFilterSelectedButton();
	};
	m.prototype._manageListsVisibility = function (s) {
		this._getTokensList().setVisible(s);
		this._getSuggestionsList() && this._getSuggestionsList().setVisible(!s);
		if (this._bUseDialog) {
			this.getFilterSelectedButton().setPressed(s);
		}
	};
	m.prototype._mapTokenToListItem = function (t) {
		if (!t) {
			return null;
		}
		var o = new S({
			selected: true
		});
		o.setTitle(t.getText());
		o.data("tokenId", t.getId());
		return o;
	};
	m.prototype._fillList = function () {
		var t = this.getTokens(),
			o;
		if (!t) {
			return;
		}
		this._getTokensList().removeAllItems();
		for (var i = 0, e = t.length; i < e; i++) {
			var n = t[i],
				o = this._mapTokenToListItem(n);
			this._getTokensList().addItem(o);
		}
	};
	m.prototype._handleIndicatorPress = function () {
		this._bShowListWithTokens = true;
		if (this.getEditable()) {
			this._openSelectedItemsPicker();
		} else {
			this._fillList();
			this._manageListsVisibility(true);
			this._getReadOnlyPopover().openBy(this._tokenizer._oIndicator[0]);
		}
	};
	m.prototype._handleNMoreItemDelete = function (e) {
		var o = e.getParameter("listItem"),
			s = o && o.data("tokenId"),
			t;
		t = this.getTokens().filter(function (i) {
			return i.getId() === s;
		})[0];
		if (t && t.getEditable()) {
			this._tokenizer._onTokenDelete(t);
			this._getTokensList().removeItem(o);
		}
		this.focus();
	};
	m.prototype._handleNMoreAccessibility = function () {
		var i = h.getStaticId("sap.m", "MULTICOMBOBOX_OPEN_NMORE_POPOVER");
		var H = this.getAriaLabelledBy().indexOf(i) !== -1;
		if (!this.getEditable() && this._tokenizer._hasMoreIndicator()) {
			!H && this.addAriaLabelledBy(i);
		} else {
			H && this.removeAriaLabelledBy(i);
		}
	};
	m.prototype._getSelectedItemsPicker = function () {
		if (this._oSelectedItemPicker) {
			return this._oSelectedItemPicker;
		}
		this._oSelectedItemPicker = this._createDropdown();
		if (!this._bUseDialog) {
			this._oSelectedItemPicker.setHorizontalScrolling(false).attachBeforeOpen(this._onBeforeOpenTokensPicker, this).attachAfterClose(this._onAfterCloseTokensPicker,
				this).addContent(this._getTokensList());
		}
		return this._oSelectedItemPicker;
	};
	m.prototype._createDropdown = function () {
		var o = new P(this._getDropdownSettings());
		o.setInitialFocus(this);
		this._decoratePopover(o);
		return o;
	};
	m.prototype._getReadOnlyPopover = function () {
		if (!this._oReadOnlyPopover) {
			this._oReadOnlyPopover = this._createReadOnlyPopover();
		}
		return this._oReadOnlyPopover;
	};
	m.prototype._createReadOnlyPopover = function () {
		return new P({
			showArrow: true,
			placement: j.Auto,
			showHeader: false,
			contentMinWidth: "auto"
		}).addStyleClass("sapMMultiInputReadOnlyPopover");
	};
	m.prototype._decoratePopover = function (p) {
		var t = this;
		p.open = function () {
			return this.openBy(t);
		};
	};
	m.prototype._getDropdownSettings = function () {
		return {
			showArrow: false,
			showHeader: false,
			placement: j.VerticalPreferredBottom,
			offsetX: 0,
			offsetY: 0,
			bounce: false
		};
	};
	m.prototype._calculateSpaceForTokenizer = function () {
		if (this.getDomRef()) {
			var s, C = this.getDomRef().offsetWidth,
				i = this.$().find(".sapMInputDescriptionWrapper").width(),
				e = this._calculateIconsSpace(),
				o = this.$().find(".sapMInputBaseInner"),
				n = ["min-width", "padding-right", "padding-left"],
				p = n.reduce(function (A, t) {
					return A + (parseInt(o.css(t)) || 0);
				}, 0);
			s = C - (e + p + i);
			s = s < 0 ? 0 : s;
			return s + "px";
		} else {
			return null;
		}
	};
	m.prototype._syncInputWidth = function (t) {
		var F = this.getDomRef('inner'),
			s, i;
		if (!F || (t && !t.getDomRef())) {
			return;
		}
		s = this._calculateIconsSpace();
		i = Math.ceil(t.getDomRef().getBoundingClientRect().width);
		F.style.width = 'calc(100% - ' + Math.floor(s + i) + "px";
	};
	return m;
});