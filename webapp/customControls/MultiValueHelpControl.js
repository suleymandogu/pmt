/*!
 * OpenUI5
 * (c) Copyright 2009-2020 SAP SE or an SAP affiliate company.
 * Licensed under the Apache License, Version 2.0 - see LICENSE.txt.
 */
sap.ui.define(["sap/m/MultiInput",
	"sap/m/SearchField",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/table/Column", 'sap/m/Input', 'sap/m/Tokenizer', 'sap/m/Token', 'sap/m/library', 'sap/ui/core/EnabledPropagator',
	'sap/ui/base/ManagedObject',
	'sap/ui/base/ManagedObjectMetadata', 'sap/ui/base/ManagedObjectObserver', 'sap/ui/Device', 'sap/m/Popover', 'sap/m/List', 'sap/m/Title',
	'sap/m/Bar',
	'sap/m/Toolbar', 'sap/m/StandardListItem', 'sap/ui/core/ResizeHandler', 'sap/ui/core/IconPool', 'sap/m/MultiInputRenderer',
	"sap/ui/dom/containsOrEquals", "sap/m/inputUtils/completeTextSelected", "sap/ui/events/KeyCodes", 'sap/ui/core/InvisibleText',
	"sap/ui/thirdparty/jquery", "sap/ui/dom/jquery/cursorPos", "sap/ui/dom/jquery/control"
], function (MultiInput, SearchField, Filter, FilterOperator, Column, I, T, a, l, E, M, b, c, D, P, L, d, B, f, S, R, g, h, j, k, K, m, q) {
	"use strict";
	var n = l.TokenizerRenderMode;
	var o = I.extend("sap.m.MultiInput", {
		metadata: {
			library: "sap.m",
			designtime: "sap/m/designtime/MultiInput.designtime",
			properties: {
				enabled: {
					type: "boolean",
					defaultValue: true
				},

				serverSide: {
					type: "boolean",
					defaultValue: true
				},

				fieldName: {
					type: "string"
				},

				descFieldName: {
					type: "string"
				},

				kotab: {
					type: "string"
				},

				relFieldModel: {
					type: "string"
				},

				relFieldName1: {
					type: "string"
				},

				relFieldName2: {
					type: "string"
				},

				relFieldName3: {
					type: "string"
				},
				relFieldName4: {
					type: "string"
				},

				relFieldName5: {
					type: "string"
				},

				relFieldName6: {
					type: "string"
				},

				relFieldName7: {
					type: "string"
				},

				relFieldName8: {
					type: "string"
				},
				ctrlBindingPath: {
					type: "string"
				},

				treeEnabled: {
					type: "boolean",
					defaultValue: false
				},

				manualDataEnabled: {
					type: "boolean",
					defaultValue: false
				},

				dataFunction: {
					type: "object"
				},

				multiSelect: {
					type: "boolean",
					defaultValue: false
				},

				selectedValues: {
					type: "string"
				},
				controller: {
					type: "object"
				},
				desc: {
					type: "string"
				},

				enableMultiLineMode: {
					type: "boolean",
					group: "Behavior",
					defaultValue: false
				},
				maxTokens: {
					type: "int",
					group: "Behavior"
				}
			},
			aggregations: {
				tokens: {
					type: "sap.m.Token",
					multiple: true,
					singularName: "token"
				},
				tokenizer: {
					type: "sap.m.Tokenizer",
					multiple: false,
					visibility: "hidden"
				}
			},
			events: {
				onSelectValue: {
					parameters: {
						selectedValue: "string"
					}
				},

				onInitialize: {
					parameters: {

					}
				},

				onGetData: {
					parameters: {
						fnSuccess: "function"
					}
				},
				onSelectToken: {
					parameters: {

					}
				},
				tokenChange: {
					parameters: {
						type: {
							type: "string"
						},
						token: {
							type: "sap.m.Token"
						},
						tokens: {
							type: "sap.m.Token[]"
						},
						addedTokens: {
							type: "sap.m.Token[]"
						},
						removedTokens: {
							type: "sap.m.Token[]"
						}
					}
				},
				tokenUpdate: {
					allowPreventDefault: true,
					parameters: {
						type: {
							type: "string"
						},
						addedTokens: {
							type: "sap.m.Token[]"
						},
						removedTokens: {
							type: "sap.m.Token[]"
						}
					}
				}
			},
			dnd: {
				draggable: false,
				droppable: true
			}
		}
	});
	E.apply(o.prototype, [true]);
	var r = sap.ui.getCore().getLibraryResourceBundle("sap.m");
	o.prototype.init = function () {
		var t = this;
		this._bShowListWithTokens = false;
		I.prototype.init.call(this);
		this._bIsValidating = false;
		var e = new T({
			renderMode: n.Narrow,
			tokenDelete: this._tokenDelete.bind(this)
		});
		e.updateTokens = function () {
			this.destroyTokens();
			this.updateAggregation("tokens");
		};
		this.setAggregation("tokenizer", e);
		e.getTokensPopup().setInitialFocus(this).attachBeforeOpen(this._onBeforeOpenTokensPicker.bind(this)).attachAfterClose(this._onAfterCloseTokensPicker
			.bind(this))._getPopup().setAutoCloseAreas([e, this]);
		this.setAggregation("tokenizer", e);
		this._oTokenizerObserver = new c(function (C) {
			var s = C.mutation;
			var i = C.child;
			switch (s) {
			case "insert":
				this.fireTokenChange({
					type: sap.m.Tokenizer.TokenChangeType.Added,
					token: i,
					tokens: [i],
					removedTokens: []
				});
				break;
			case "remove":
				var p = C.object.getTokens().length ? sap.m.Tokenizer.TokenChangeType.Removed : sap.m.Tokenizer.TokenChangeType.RemovedAll;
				this.fireTokenChange({
					type: p,
					token: i,
					removedTokens: [i]
				});
				break;
			default:
				break;
			}
			this.invalidate();
		}.bind(this));
		this._oTokenizerObserver.observe(e, {
			aggregations: ["tokens"]
		});
		this._bShowListWithTokens = false;
		this._bIsValidating = false;
		e.addEventDelegate({
			onThemeChanged: this._handleInnerVisibility.bind(this),
			onAfterRendering: function () {
				if (this.isMobileDevice() && this.getEditable()) {
					e.addStyleClass("sapMTokenizerIndicatorDisabled");
				} else {
					e.removeStyleClass("sapMTokenizerIndicatorDisabled");
				}
				this._syncInputWidth(e);
				this._handleInnerVisibility();
				this._handleNMoreAccessibility();
				this._registerTokenizerResizeHandler();
			}.bind(this)
		}, this);
		this._aTokenValidators = [];
		this.setShowValueHelp(true);
		this.setShowSuggestion(true);
		this._getSuggestionsPopoverInstance()._oPopover.attachBeforeOpen(function () {
			if (t.isMobileDevice() !== true) {
				return;
			}
			var i = e._getTokensList();
			e._fillTokensList(i);
			this.addContent(i);
			t._manageListsVisibility(!!e.getTokens().length);
		});
		this.attachSuggestionItemSelected(this._onSuggestionItemSelected, this);
		this.attachLiveChange(this._onLiveChange, this);
		this.attachValueHelpRequest(this._onValueHelpRequested, this);
		this._getValueHelpIcon().setProperty("visible", true, true);
		this._onResize = this._onResize.bind(this);

		/**Custom***/
		MultiInput.prototype.init.apply(this, arguments);

		this.attachValueHelpRequest(o._valueHelpRequest.bind(this));
		this.attachChange(o._valueHelpChange.bind(this));
		this.attachSubmit(o._valueHelpSubmit.bind(this));
		this.attachLiveChange(o._valueHelpChange.bind(this));
		this.attachTokenChange(o._onUpdateTokens.bind(this));
		this.setShowValueHelp(true);
		this.setEnableMultiLineMode(true);
		var thiz = this;
		this.addValidator(function (args) {
			window.setTimeout(function () {
				args.asyncCallback(new sap.m.Token({
					key: args.text,
					text: args.text
				}));
				thiz.setSelectedValues(thiz.getSelectedValues());
				thiz.closeMultiLine();
			}, 0);
			return sap.m.MultiInput.WaitForAsyncValidation;
		});
	};

	/**Custom**/
	o.prototype.onAfterRendering = function () {};

	o._onUpdateTokens = function () {
		this.setSelectedValues(this.getSelectedKeys());
	};

	o._valueHelpSubmit = function (oEvent) {
		var oValue = oEvent.getParameter("value");
		if (this.getMultiSelect()) {
			/*this.addToken(new sap.m.Token({
			    key: oValue,
			    text: oValue
			}));
			this.setValue("");

			this.setSelectedValues(this.getSelectedKeys());*/
			//this.closeMultiLine();
		} else
			this.setValue(oValue.toUpperCase());

	};

	o._valueHelpChange = function (oEvent) {
		if (!this.getMultiSelect()) {
			var oValue = oEvent.getParameter("value");
			this.setValue(oValue);
		}
	};

	o.prototype.getSelectedKeys = function () {
		var result = "";

		if (!this.getMultiSelect())
			return result;

		var oTokens = this.getTokens();

		for (var i = 0; i < oTokens.length; i++) {
			result = result + ',' + oTokens[i].getKey();
		}

		if (result !== "")
			result = result.substr(1, result.length);

		return result;
	};

	o._valueHelpRequest = function () {

		this.fireEvent("onInitialize");
		var thiz = this;

		if (this.getManualDataEnabled()) {
			this.fireEvent("onGetData", {
				fnSuccess: this._prepareValueHelpDialog
			});

		} else {
			var callFunc = this.getDataFunction();

			this.setBusy(true);

			callFunc(this.getController(),
				this.getFieldName(),
				this.getKotab(),
				this._getRelatedFieldModelData(),
				this._getServerSideModelData(),
				function (oModel, controller) {
					thiz.setBusy(false);
					thiz.getController().getView().setModel(oModel, "valueHelpModel");

					if (thiz.getTreeEnabled()) {
						thiz._prepareValueHelpTreeDialog(thiz.getId());
					} else {
						thiz._prepareValueHelpDialog();
					}
				});
		}
	};

	o.prototype._getServerSideModelData = function (searchTerm) {
		var oModel = {
			NoOfEntry: 100,
			SearchTerm: ""
		};

		if (this.getServerSide()) {
			oModel.SearchTerm = searchTerm;
		}

		return oModel;
	};

	o.prototype._getRelatedFieldModelData = function () {
		/*
		 * Related Field Configuration----
		 * If the current control should filter the values by using the value from another field in the model
		 * Then by using the related properties we will get the values and pass them to the API
		 */

		var relFldValue1,
			relFldValue2,
			relFldValue3,
			relFldValue4,
			relFldValue5,
			relFldValue6,
			relFldValue7,
			relFldValue8 = "";

		if (this.getRelFieldModel()) {

			var oRelFieldModel = this.getController().getView().getModel(this.getRelFieldModel());

			if (this.getRelFieldName1()) {
				relFldValue1 = oRelFieldModel.getProperty(this.getRelFieldName1());
			}

			if (this.getRelFieldName2()) {
				relFldValue2 = oRelFieldModel.getProperty(this.getRelFieldName2());
			}

			if (this.getRelFieldName3()) {
				relFldValue3 = oRelFieldModel.getProperty(this.getRelFieldName3());
			}
			if (this.getRelFieldName4()) {
				relFldValue4 = oRelFieldModel.getProperty(this.getRelFieldName4());
			}

			if (this.getRelFieldName5()) {
				relFldValue5 = oRelFieldModel.getProperty(this.getRelFieldName5());
			}

			if (this.getRelFieldName6()) {
				relFldValue6 = oRelFieldModel.getProperty(this.getRelFieldName6());
			}

			if (this.getRelFieldName7()) {
				relFldValue7 = oRelFieldModel.getProperty(this.getRelFieldName7());
			}

			if (this.getRelFieldName8()) {
				relFldValue8 = oRelFieldModel.getProperty(this.getRelFieldName8());
			}

		}

		var oRelFldModel = {
			RelFldValue1: relFldValue1,
			RelFldValue2: relFldValue2,
			RelFldValue3: relFldValue3,
			RelFldValue4: relFldValue4,
			RelFldValue5: relFldValue5,
			RelFldValue6: relFldValue6,
			RelFldValue7: relFldValue7,
			RelFldValue8: relFldValue8
		};

		return oRelFldModel;
	};

	o.prototype._onValueHelpSearch = function (oEvent) {
		var thiz = this;
		var searchStr = this.getTreeEnabled() ? oEvent.getParameter("query") : oEvent.getParameter("value");
		if (this.getFieldName() === "MATERIAL" && searchStr !== "")
			searchStr = searchStr.replace(/\./g, '');

		/*
		 * If the server side mode is enabled, we send request to the server to get the data by using the search term
		 */
		if (this.getServerSide()) {

			var callFunc = this.getDataFunction();

			callFunc(this.getController(),
				this.getFieldName(),
				this.getKotab(),
				this._getRelatedFieldModelData(),
				this._getServerSideModelData(searchStr),
				function (oModel, controller) {
					thiz.getController().getView().setModel(oModel, "valueHelpModel");
				});

		} else {
			/*
			 * If we are in client mode, then the search feature will work on the client
			 */

			var filters = "";
			var fieldNames = this.getTreeEnabled() ? ["PRODH", "VTEXT", "CHILDS/PRODH", "CHILDS/VTEXT"] : this.getController().getView().getModel(
				"valueHelpModel").getProperty("/F4_STRUC_DYN");
			var f4Filter = [];

			if (searchStr && searchStr.length > 0) {
				for (var i = 0; i < fieldNames.length; i++) {
					f4Filter.push(new sap.ui.model.Filter(fieldNames[i], sap.ui.model.FilterOperator.Contains, searchStr));
				}
				filters = new sap.ui.model.Filter({
					filters: f4Filter,
					and: false
				});
			}

			var oBinding = this.getTreeEnabled() ? this._oTree.getBinding("rows") : oEvent.getSource().getBinding("items");
			oBinding.filter(filters);
		}
	};

	o.prototype._onValueHelpConfirm = function (oEvent) {
		if (this.getTreeEnabled()) {
			var selectedIndx = oEvent.getParameter("rowIndex");

			if (this._selectedIndex === undefined || this._selectedIndex !== selectedIndx) {
				this._selectedIndex = selectedIndx;
				this._selectedTreeId = this.getModel("valueHelpModel").getProperty(oEvent.getParameter("rowContext").getPath()).PRODH;
				this._selectedTreeDesc = this.getModel("valueHelpModel").getProperty(oEvent.getParameter("rowContext").getPath()).VTEXT;
				this._oTree.clearSelection();
				this._oTree.setSelectedIndex(selectedIndx);
			}
		} else {

			if (this.getMultiSelect()) {
				this._setTokens(oEvent.getParameters().selectedItems);
			} else {

				var custData = oEvent.getParameters().selectedItem.getCustomData();
				var dispValue = custData[0].getValue();
				this.setValue(dispValue);

				//console.log(this.getModel("valueHelpModel").getData());

				if (this.getDescFieldName() !== "" && this.getDescFieldName() !== undefined) {
					//var descValue = custData[1].getValue();
					//this.getController().getObj(this.getId() + this.getDescFieldName()).setText(descValue)
					//this.setDescription(descValue);
				}
			}
			this.fireEvent("onSelectToken");
			this._destroyDialog();
		}
	};
	o.prototype._onSelectTokenfn = function () {
		return this.getSelectedKeys();
	};

	o.prototype._setTokens = function (selectedRows) {
		//this.removeAllTokens();
		for (var i = 0; i < selectedRows.length; i++) {
			var oValue = selectedRows[i].getCustomData()[0].getValue();
			var oText = selectedRows[i].getCustomData()[1].getValue();

			// if (selectedRows[i].getParent().getId() !== undefined) {
			// 	if (selectedRows[i].getParent().getId().indexOf("priceSettingUAMInputIdDialog") > -1 ||
			// 		selectedRows[i].getParent().getId().indexOf("priceSettingUCSInputIdDialog") > -1)
			// 		oValue = selectedRows[i].getCustomData()[1].getValue();
			// }

			this.addToken(new sap.m.Token({
				key: oValue,
				text: oText
			}));
		}
	};

	o.prototype._onValueHelpClose = function () {
		this._destroyDialog();
	};

	o.prototype._destroyDialog = function () {
		this.getController().getView().removeDependent(this._oDialog);
		this._oDialog.destroy();
	};

	o.prototype._prepareValueHelpTreeDialog = function (popupId) {
		// var thiz = this;
		// var oModel = this.getController().getView().getModel("valueHelpModel");

		this._oTree = new sap.ui.table.TreeTable({
			rows: "{path:'valueHelpModel>/PRODHIERARCHY', parameters: {arrayNames:['CHILDS']}}",
			selectionMode: "MultiToggle",
			enableSelectAll: false,
			width: "100%",
			enableColumnReordering: false,
			expandFirstLevel: false,
			rowSelectionChange: this._onValueHelpConfirm.bind(this),
			//				toolbar : new sap.m.Toolbar({
			//					content : new sap.m.SearchField({
			//						width : "100%",
			//						selectOnFocus : false,
			//						search : this._onValueHelpSearch.bind(this)
			//					})
			//				}),
			columns: [
				new Column("", {
					label: new sap.m.Label({
						text: "ID"
					}),
					template: new sap.m.Text("", {
						text: "{valueHelpModel>PRODH}"
					})
				}),
				new Column({
					label: new sap.m.Label({
						text: "Name"
					}),
					template: new sap.m.Text({
						text: "{valueHelpModel>VTEXT}"
					})
				})
			]
		});

		this._oDialog = new sap.m.Dialog({
			busyIndicatorDelay: 0,
			title: "Select Dialog",
			content: [this._oTree],
			contentWidth: "600px",
			beginButton: new sap.m.Button({
				text: that.getController().getText("select"),
				press: function () {
					if (that._selectedTreeId) {
						that.setValue(that._selectedTreeId);

						if (that.getDescFieldName() !== "") {
							that.getController().getObj(that.getId() + that.getDescFieldName()).setText(that._selectedTreeDesc);
						}

						that._destroyDialog();
					}
				}
			}),
			endButton: new sap.m.Button({
				text: that.getController().getText("cancel"),
				press: function () {
					that._destroyDialog();
				}
			}),
			afterClose: function () {
				that._destroyDialog();
			}
		}).addStyleClass("sapUiSizeCompact");

		this.getController().getView().addDependent(this._oDialog);
		this._oDialog.open();
		
	};

	o.prototype._prepareValueHelpDialog = function (popupId) {
		var thiz = this;
		var popupId = this.getId();
		var oModel = this.getController().getView().getModel("valueHelpModel");
		var fieldname = this.getFieldName();
		var dialogTitle = "";
		if (fieldname === "SPECIAL_VARIANTS"){ dialogTitle = "Selected variant/variants will be excluded!" }

		this._oDialog = new sap.m.TableSelectDialog(popupId + "Dialog", {
			//liveChange : this._onValueHelpSearch.bind(this),
			search: this._onValueHelpSearch.bind(this),
			confirm: this._onValueHelpConfirm.bind(this),
			//close: this._onValueHelpClose.bind(this),
			cancel: this._onValueHelpClose.bind(this),
			multiSelect: this.getMultiSelect(),
			title : dialogTitle ,
			titleAlignment : "Center",
			growingThreshold: 100,
			rememberSelections: true,
			draggable: true,
			resizable: true
		});

		// sap.ui.getCore().getModel("globalModel").setProperty("/popupId", popupId);

		var columnsModel = oModel.getProperty("/F4_DESCR_DYN");
		var tableCells = [],
			customData = [];

		/*
		 * Add Columns and Cells of the Table
		 */
		columnsModel.forEach(function (aColumn) {
			thiz._oDialog.addColumn(
				new sap.m.Column({
					header: new sap.m.Label({
						text: aColumn.MDTXT,
						design: "Bold",
						wrapping: true
					})
				})
			);

			tableCells.push(new sap.m.Text({
				text: "{valueHelpModel>" + aColumn.FIELDNAME + "}"
			}));
		});

		/*
		 * Store the primary key and value of the current field control to use after the dialog being closed
		 */
		var oKey = oModel.getProperty("/F4_FIELD_DYN");
		customData.push(new sap.ui.core.CustomData({
			key: oKey,
			value: "{valueHelpModel>" + oKey + "}"
		}));

		/*
		 * If the description field is available, store the value of the text in custom data
		 */
		if (this.getDescFieldName() !== "") {
			customData.push(new sap.ui.core.CustomData({
				key: this.getDescFieldName(),
				value: "{valueHelpModel>" + this.getDescFieldName() + "}"
			}));
		}

		/*
		 * Bind data to the table
		 */
		this._oDialog.bindItems("valueHelpModel>/F4_VALUE_DYN", new sap.m.ColumnListItem({
			cells: tableCells,
			customData: customData
		}));

		/*
		 * Configure dialog
		 */
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getController().getView(), this._oDialog);
		this._oDialog._table.mProperties.growingScrollToLoad = false;
		this.getController().getView().addDependent(this._oDialog);
		this._oDialog.open();
	};
	/**Custom**/
	o.prototype.exit = function () {
		this._deregisterResizeHandler();
		this._deregisterTokenizerResizeHandler();
		this._oTokenizerObserver.disconnect();
		this._oTokenizerObserver.destroy();
		this._oTokenizerObserver = null;
		I.prototype.exit.call(this);
	};
	o.prototype.onAfterRendering = function () {
		var t = this.getAggregation("tokenizer");
		this._bTokenIsValidated = false;
		t.setMaxWidth(this._calculateSpaceForTokenizer());
		t.scrollToEnd();
		this._registerResizeHandler();
		I.prototype.onAfterRendering.apply(this, arguments);
	};
	o.prototype._tokenDelete = function (e) {
		this._deleteTokens(e.getParameter("tokens"), e.getParameters());
	};
	o.prototype._deleteTokens = function (e, O) {
		var t = this.getAggregation("tokenizer");
		var i = 0;
		var p = O.keyCode === K.BACKSPACE;
		var s = e[e.length - 1];
		var F = e[0];
		i = this.getTokens().indexOf(p ? F : s);
		t.focusToken(i, O, function () {
			this.focus();
		}.bind(this));
		this.fireTokenUpdate({
			type: T.TokenUpdateType.Removed,
			addedTokens: [],
			removedTokens: e
		});
		e.filter(function (u) {
			return this.getEditable() && this.getEnabled() && u.getEditable();
		}.bind(this)).forEach(function (u) {
			u.destroy();
		});
		if (this.getTokens().length === 0) {
			t.getTokensPopup().close();
		}
		if (!O.keyCode) {
			this.focus();
		}
	};
	o.prototype._handleInnerVisibility = function () {
		var H = !!this.getAggregation("tokenizer").getHiddenTokensCount();
		this._setValueVisible(!H);
	};
	o.prototype.oninput = function (e) {
		this.setProperty("selectedKey", '', true);
		I.prototype.oninput.call(this, e);
		if (e.isMarked("invalid") || !this.getEditable()) {
			return;
		}
		this._setValueVisible(true);
		this._manageListsVisibility(false);
		this.getAggregation("tokenizer").getTokensPopup().close();
	};
	o.prototype._registerResizeHandler = function () {
		if (!this._iResizeHandlerId) {
			this._iResizeHandlerId = R.register(this, this._onResize);
		}
	};
	o.prototype._deregisterResizeHandler = function () {
		if (this._iResizeHandlerId) {
			R.deregister(this._iResizeHandlerId);
			this._iResizeHandlerId = null;
		}
	};
	o.prototype._registerTokenizerResizeHandler = function () {
		if (!this._iTokenizerResizeHandler) {
			this._iTokenizerResizeHandler = R.register(this.getAggregation("tokenizer"), this._onResize);
		}
	};
	o.prototype._deregisterTokenizerResizeHandler = function () {
		if (this._iTokenizerResizeHandler) {
			R.deregister(this._iTokenizerResizeHandler);
			this._iTokenizerResizeHandler = null;
		}
	};
	o.prototype._onResize = function () {
		this.getAggregation("tokenizer").setMaxWidth(this._calculateSpaceForTokenizer());
	};
	o.prototype._onSuggestionItemSelected = function (e) {
		var t = this.getAggregation("tokenizer"),
			i = null,
			p = null,
			O = t.getTokens().length;
		if (this.getMaxTokens() && O >= this.getMaxTokens() || this._bValueHelpOpen) {
			return;
		}
		if (this._hasTabularSuggestions()) {
			i = e.getParameter("selectedRow");
		} else {
			i = e.getParameter("selectedItem");
			if (i) {
				p = new a({
					text: M.escapeSettingsValue(i.getText()),
					key: M.escapeSettingsValue(i.getKey())
				});
			}
		}
		if (i && !this._bTokenIsAdded) {
			var s = this.getValue();
			this.addValidateToken({
				text: s,
				token: p,
				suggestionObject: i,
				validationCallback: this._validationCallback.bind(this, O)
			});
		}
		if (this.isMobileDevice()) {
			var N = t.getTokens().length;
			if (O < N) {
				this.setValue("");
			}
			if (this._getSuggestionsList() instanceof sap.m.Table) {
				this._getSuggestionsList().addStyleClass("sapMInputSuggestionTableHidden");
			} else {
				this._getSuggestionsList().destroyItems();
			}
			var u = this.getAggregation("tokenizer").getScrollDelegate();
			if (u) {
				u.scrollTo(0, 0, 0);
			}
			this._getSuggestionsPopoverInstance()._oPopupInput.focus();
		}
		this._bTokenIsAdded = false;
	};
	o.prototype._onValueHelpRequested = function () {
		this._bValueHelpOpen = true;
	};
	o.prototype._onLiveChange = function (e) {
		var C = this.getAggregation("tokenizer").getTokens().every(function (t) {
			return t.getSelected();
		});
		if (!C) {
			return;
		}
		this.removeAllTokens();
	};
	o.prototype._setValueVisible = function (v) {
		var V = v ? "1" : "0";
		this.$("inner").css("opacity", V);
	};
	o.prototype.onmousedown = function (e) {
		if (e.target == this.getDomRef('content')) {
			e.preventDefault();
			e.stopPropagation();
		}
	};
	o.prototype.openMultiLine = function () {};
	o.prototype.closeMultiLine = function () {};
	o.prototype.showItems = function () {
		I.prototype.showItems.apply(this, arguments);
		this._manageListsVisibility(false);
	};
	o.prototype.onBeforeRendering = function () {
		var t = this.getAggregation("tokenizer");
		var e = t._getTokensList();
		I.prototype.onBeforeRendering.apply(this, arguments);
		this._hideTokensOverLimit();
		t.setEnabled(this.getEnabled());
		t._fillTokensList(e);
	};
	o.prototype._hideTokensOverLimit = function () {
		if (!this.getMaxTokens()) {
			return;
		}
		this.getTokens().forEach(function (t, i) {
			if (i >= this.getMaxTokens()) {
				return t.setVisible(false);
			}
			return t.setVisible(true);
		}, this);
	};
	o.prototype.onsapnext = function (e) {
		var t = this.getAggregation("tokenizer");
		if (e.isMarked()) {
			return;
		}
		var F = q(document.activeElement).control()[0];
		if (!F) {
			return;
		}
		if (t === F || t.$().find(F.$()).length > 0) {
			t.scrollToEnd();
			this.$().find("input").trigger("focus");
		}
	};
	o.prototype.onsapbackspace = function (e) {
		var v = this.getValue();
		var i = this.getFocusDomRef() === document.activeElement;
		var t = this.getTokens();
		var p = t[t.length - 1];
		if (v === "" && i && p && e.srcControl === this) {
			var A = t.filter(function (s) {
				return s.getSelected();
			}).length === t.length;
			if (A) {
				return this._deleteTokens(t, {
					keyCode: K.BACKSPACE
				});
			}
			p.focus();
			e.preventDefault();
		}
	};
	o.prototype.onsapdelete = function (e) {
		if (!this.getEditable()) {
			return;
		}
		if (this.getValue() && !k(this.getFocusDomRef())) {
			return;
		}
		if (e.isMarked("forwardFocusToParent")) {
			this.focus();
		}
	};
	o.prototype.onkeydown = function (e) {
		var t = this.getAggregation("tokenizer");
		if (!this.getEnabled()) {
			return;
		}
		if (e.which === K.TAB) {
			t.selectAllTokens(false);
		}
		if ((e.ctrlKey || e.metaKey) && e.which === K.A && t.getTokens().length > 0) {
			t.focus();
			t.selectAllTokens(true);
			e.preventDefault();
		}
		if ((e.ctrlKey || e.metaKey) && (e.which === K.C || e.which === K.INSERT)) {
			t._copy();
		}
		if (((e.ctrlKey || e.metaKey) && e.which === K.X) || (e.shiftKey && e.which === K.DELETE)) {
			if (this.getEditable()) {
				t._cut();
			} else {
				t._copy();
			}
		}
		if ((e.ctrlKey || e.metaKey) && e.which === K.I && t.getTokens().length) {
			t._togglePopup(t.getTokensPopup());
			e.preventDefault();
		}
	};
	o.prototype.onpaste = function (e) {
		var O, i, s, A = [];
		if (this.getValueHelpOnly()) {
			return;
		}
		if (window.clipboardData) {
			O = window.clipboardData.getData("Text");
		} else {
			O = e.originalEvent.clipboardData.getData('text/plain');
		}
		s = O.split(/\r\n|\r|\n/g);
		if (s.length <= 1) {
			return;
		}
		setTimeout(function () {
			if (s) {
				if (this.fireEvent("_validateOnPaste", {
						texts: s
					}, true)) {
					var p = "";
					for (i = 0; i < s.length; i++) {
						if (s[i]) {
							var t = this._convertTextToToken(s[i], true);
							if (this._addUniqueToken(t)) {
								A.push(t);
							} else {
								p = s[i];
							}
						}
					}
					this.updateDomValue(p);
					if (A.length > 0) {
						this.fireTokenUpdate({
							addedTokens: A,
							removedTokens: [],
							type: T.TokenUpdateType.Added
						});
						this.fireTokenChange({
							addedTokens: A,
							removedTokens: [],
							type: T.TokenChangeType.TokensChanged
						});
					}
				}
				if (A.length) {
					this.cancelPendingSuggest();
				}
			}
		}.bind(this), 0);
	};
	o.prototype._validationCallback = function (O, v) {
		var N = this.getAggregation("tokenizer").getTokens().length;
		var s = this._getSuggestionsPopoverInstance();
		this._bIsValidating = false;
		if (v) {
			this.setValue("");
			this._bTokenIsValidated = true;
			if (this.isMobileDevice() && s && s._oPopupInput && (O < N)) {
				s._oPopupInput.setValue("");
			}
		}
	};
	o.prototype.onsapprevious = function (e) {
		if (this._getIsSuggestionPopupOpen()) {
			return;
		}
		if (this._$input.cursorPos() === 0) {
			if (e.srcControl === this) {
				T.prototype.onsapprevious.apply(this.getAggregation("tokenizer"), arguments);
			}
		}
		if (e.keyCode === K.ARROW_UP) {
			e.preventDefault();
		}
	};
	o.prototype.onsaphome = function (e) {
		if (!this.getFocusDomRef().selectionStart) {
			T.prototype.onsaphome.apply(this.getAggregation("tokenizer"), arguments);
		}
	};
	o.prototype.onsapend = function (e) {
		if (e.isMarked("forwardFocusToParent")) {
			this.focus();
		}
	};
	o.prototype.onsapenter = function (e) {
		if (I.prototype.onsapenter) {
			I.prototype.onsapenter.apply(this, arguments);
		}
		var v = true,
			t = this.getAggregation("tokenizer");
		if (this._getIsSuggestionPopupOpen()) {
			if (this._hasTabularSuggestions()) {
				v = !this._oSuggestionTable.getSelectedItem();
			} else {
				v = !this._getSuggestionsList().getSelectedItem();
			}
		}
		if (v) {
			this._validateCurrentText();
		}
		if (e && e.setMarked && this._bTokenIsValidated) {
			e.setMarked();
		}
		if (!this.getEditable() && t.getHiddenTokensCount() && e.target === this.getFocusDomRef()) {
			t._togglePopup(t.getTokensPopup());
		}
		this.focus();
	};
	o.prototype.onsapfocusleave = function (e) {
		var p = this._getSuggestionsPopoverPopup(),
			t = this.getAggregation("tokenizer"),
			s = t.getTokensPopup(),
			N = false,
			i = false,
			u = this.getDomRef() && j(this.getDomRef(), document.activeElement),
			v, F;
		if (p && p.isA("sap.m.Popover")) {
			if (e.relatedControlId) {
				v = sap.ui.getCore().byId(e.relatedControlId).getFocusDomRef();
				N = j(p.getFocusDomRef(), v);
				i = j(t.getFocusDomRef(), v);
				if (s) {
					F = j(s.getFocusDomRef(), v);
				}
			}
		}
		I.prototype.onsapfocusleave.apply(this, arguments);
		if (this._bIsValidating || this._bValueHelpOpen) {
			return;
		}
		if (!this.isMobileDevice() && !N && e.relatedControlId !== this.getId() && !i) {
			this._validateCurrentText(true);
		}
		if (!this.isMobileDevice() && this.getEditable()) {
			if (u || N) {
				return;
			}
		}
		if (!F && !i) {
			s.isOpen() && !this.isMobileDevice() && t._togglePopup(s);
			t.setRenderMode(n.Narrow);
		}
		this._handleInnerVisibility();
	};
	o.prototype.ontap = function (e) {
		var t = this.getAggregation("tokenizer");
		if (document.activeElement === this._$input[0] || document.activeElement === t.getDomRef()) {
			t.selectAllTokens(false);
		}
		if (e && e.isMarked("tokenDeletePress")) {
			return;
		}
		I.prototype.ontap.apply(this, arguments);
	};
	o.prototype.onfocusin = function (e) {
		var t = this.getAggregation("tokenizer");
		this._deregisterTokenizerResizeHandler();
		this._bValueHelpOpen = false;
		if (e.target === this.getFocusDomRef()) {
			I.prototype.onfocusin.apply(this, arguments);
			if (t.hasOneTruncatedToken() && this.getEnabled() && this.getEditable()) {
				t.getTokens()[0].setSelected(false);
				!this.isMobileDevice() && t.setFirstTokenTruncated(false);
			}
		}
		if (!this.isMobileDevice() && this.getEditable() && e.target === this.getDomRef("inner") && !(this._getIsSuggestionPopupOpen())) {
			t.setRenderMode(n.Loose);
			this._setValueVisible(true);
		}
		this._registerResizeHandler();
	};
	o.prototype.onsapescape = function (e) {
		var t = this.getAggregation("tokenizer"),
			p = t.getTokensPopup();
		this.getAggregation("tokenizer").selectAllTokens(false);
		this.selectText(0, 0);
		if (p.isOpen()) {
			t._togglePopup(p);
		}
		I.prototype.onsapescape.apply(this, arguments);
	};
	o.prototype._getIsSuggestionPopupOpen = function () {
		var s = this._getSuggestionsPopoverInstance(),
			e = this._getSuggestionsPopoverPopup();
		return s && e && e.isOpen();
	};
	o.prototype.setEditable = function (e) {
		var t = this.getAggregation("tokenizer");
		e = this.validateProperty("editable", e);
		if (e === this.getEditable()) {
			return this;
		}
		if (I.prototype.setEditable) {
			I.prototype.setEditable.apply(this, arguments);
		}
		t.setEditable(e);
		return this;
	};
	o.prototype._findItem = function (t, e, p, G) {
		if (!t) {
			return;
		}
		if (!(e && e.length)) {
			return;
		}
		t = t.toLowerCase();
		var s = e.length;
		for (var i = 0; i < s; i++) {
			var u = e[i];
			var v = G(u);
			if (!v) {
				continue;
			}
			v = v.toLowerCase();
			if (v === t) {
				return u;
			}
			if (!p && v.indexOf(t) === 0) {
				return u;
			}
		}
	};
	o.prototype._getSuggestionItem = function (t, e) {
		var p = null;
		var s = null;
		if (this._hasTabularSuggestions()) {
			p = this.getSuggestionRows();
			s = this._findItem(t, p, e, function (u) {
				var v = u.getCells();
				var w = null;
				if (v) {
					var i;
					for (i = 0; i < v.length; i++) {
						if (v[i].getText) {
							w = v[i].getText();
							break;
						}
					}
				}
				return w;
			});
		} else {
			p = this.getSuggestionItems();
			s = this._findItem(t, p, e, function (s) {
				return s.getText();
			});
		}
		return s;
	};
	o.prototype.clone = function () {
		var C;
		this.detachSuggestionItemSelected(this._onSuggestionItemSelected, this);
		this.detachLiveChange(this._onLiveChange, this);
		this.detachValueHelpRequest(this._onValueHelpRequested, this);
		C = I.prototype.clone.apply(this, arguments);
		this.attachSuggestionItemSelected(this._onSuggestionItemSelected, this);
		this.attachLiveChange(this._onLiveChange, this);
		this.attachValueHelpRequest(this._onValueHelpRequested, this);
		return C;
	};
	o.getMetadata().forwardAggregation("tokens", {
		getter: function () {
			return this.getAggregation("tokenizer");
		},
		aggregation: "tokens",
		forwardBinding: true
	});
	o.prototype.getPopupAnchorDomRef = function () {
		return this.getDomRef("content");
	};
	o.prototype.setTokens = function (t) {
		if (!Array.isArray(t)) {
			return;
		}
		this.removeAllTokens();
		t.forEach(function (e) {
			b.addAPIParentInfoBegin(e, this, "tokens");
		}, this);
		t.forEach(function (e) {
			this.addToken(e);
		}, this);
		t.forEach(function (e) {
			b.addAPIParentInfoEnd(e);
		}, this);
		this.fireTokenChange({
			type: sap.m.Tokenizer.TokenChangeType.TokensChanged,
			addedTokens: t,
			removedTokens: []
		});
		return this;
	};
	o.TokenChangeType = {
		Added: "added",
		Removed: "removed",
		RemovedAll: "removedAll",
		TokensChanged: "tokensChanged"
	};
	o.WaitForAsyncValidation = "sap.m.MultiInput.WaitForAsyncValidation";
	o.prototype.getDomRefForValueStateMessage = o.prototype.getPopupAnchorDomRef;
	o.prototype.updateInputField = function (N) {
		I.prototype.updateInputField.call(this, N);
		var s = this._getSuggestionsPopoverInstance();
		this.setDOMValue('');
		if (s._oPopupInput) {
			s._oPopupInput.setDOMValue('');
		}
	};
	o.prototype.onChange = function (e, p, N) {
		p = p || this.getChangeEventParams();
		if (!this.getEditable() || !this.getEnabled()) {
			return;
		}
		var v = this._getInputValue(N);
		if (v === this.getLastValue()) {
			this._bCheckDomValue = false;
			return;
		}
		if (!this._bTokenIsValidated) {
			this.setValue(v);
			v = this.getValue();
			this.setLastValue(v);
		}
		this.fireChangeEvent(v, p);
		return true;
	};
	o.prototype.getAccessibilityInfo = function () {
		var t = this.getTokens().map(function (e) {
			return e.getText();
		}).join(" ");
		var i = I.prototype.getAccessibilityInfo.apply(this, arguments);
		i.type = r.getText("ACC_CTR_TYPE_MULTIINPUT");
		i.description = ((i.description || "") + " " + t).trim();
		return i;
	};
	o.prototype._modifyPopupInput = function (p) {
		var t = this;
		p.addEventDelegate({
			oninput: t._manageListsVisibility.bind(t, false),
			onsapenter: function (e) {
				if (p.getValue()) {
					t._closeSuggestionPopup();
				}
				t._validateCurrentText();
				t._setValueVisible(false);
				t.onChange(e, null, p.getValue());
			}
		});
		return p;
	};
	o.prototype._hasShowSelectedButton = function () {
		return true;
	};
	o.prototype.forwardEventHandlersToSuggPopover = function (s) {
		I.prototype.forwardEventHandlersToSuggPopover.apply(this, arguments);
		s.setShowSelectedPressHandler(this._handleShowSelectedPress.bind(this));
	};
	o.prototype._handleShowSelectedPress = function (e) {
		this._bShowListWithTokens = e.getSource().getPressed();
		this._manageListsVisibility(this._bShowListWithTokens);
	};
	o.prototype._onBeforeOpenTokensPicker = function () {
		var t = this.getAggregation("tokenizer"),
			p = t.getTokensPopup(),
			e = this.getDomRef(),
			i = this.getEditable(),
			C, s;
		this._setValueVisible(false);
		this._manageListsVisibility(true);
		if (e && p) {
			C = parseInt(p.getContentWidth());
			s = e.offsetWidth > C ? e.offsetWidth : C;
			s = ((t.getTokens().length === 1) || !i) ? "auto" : (s / parseFloat(l.BaseFontSize)) + "rem";
			p.setContentWidth(s);
		}
	};
	o.prototype._onAfterCloseTokensPicker = function () {
		if (document.activeElement !== this.getDomRef("inner")) {
			this.getAggregation("tokenizer").setRenderMode(n.Narrow);
		}
	};
	o.prototype.getDialogTitle = function () {
		var p = this._getSuggestionsPopoverPopup(),
			H = p && p.getCustomHeader();
		if (H) {
			return H.getContentMiddle()[0];
		}
		return null;
	};
	o.prototype._updatePickerHeaderTitle = function () {
		var e, i;
		i = this.getLabels();
		if (i.length) {
			e = i[0];
			if (e && (typeof e.getText === "function")) {
				this.getDialogTitle().setText(e.getText());
			}
		} else {
			this.getDialogTitle().setText(r.getText("COMBOBOX_PICKER_TITLE"));
		}
	};
	o.prototype._getSuggestionsList = function () {
		var s = this._getSuggestionsPopoverInstance();
		return s && s._oList;
	};
	o.prototype._getSuggestionsPopoverInstance = function () {
		return this._oSuggPopover;
	};
	o.prototype._getSuggestionsPopoverPopup = function () {
		return this._oSuggestionPopup;
	};
	o.prototype._manageListsVisibility = function (s) {
		if (!this.isMobileDevice()) {
			return;
		}
		this.getAggregation("tokenizer")._getTokensList().setVisible(s);
		this._getSuggestionsList() && this._getSuggestionsList().setVisible(!s);
		this._getSuggestionsPopover().getFilterSelectedButton().setPressed(s);
	};
	o.prototype._handleNMoreAccessibility = function () {
		var i = m.getStaticId("sap.m", "MULTICOMBOBOX_OPEN_NMORE_POPOVER"),
			F = this.getFocusDomRef(),
			A = (F && F.getAttribute("aria-labelledby")),
			e = A ? A.split(" ") : [],
			N = e.indexOf(i),
			p = this.getEnabled(),
			s = !this.getEditable() && this.getAggregation("tokenizer").getHiddenTokensCount();
		if (s && N === -1) {
			e.push(i);
			p && this.getFocusDomRef().setAttribute("aria-keyshortcuts", "Enter");
		} else if (N !== -1 && !s) {
			e.splice(N, 1);
			this.getFocusDomRef().removeAttribute("aria-keyshortcuts");
		}
		if (F && e.length) {
			F.setAttribute("aria-labelledby", e.join(" ").trim());
		}
	};
	o.prototype._calculateSpaceForTokenizer = function () {
		var e = this.getDomRef();
		if (e) {
			var s, i = this.$().find(".sapMInputDescriptionWrapper"),
				p = this.$().find(".sapMInputBaseInner"),
				C = e.offsetWidth || 0,
				t = i.width() || 0,
				u = this._calculateIconsSpace(),
				v = ["min-width", "padding-right", "padding-left"],
				w = v.reduce(function (A, x) {
					return A + (parseInt(p.css(x)) || 0);
				}, 0);
			s = C - (u + w + t);
			s = s < 0 ? 0 : s;
			return s + "px";
		} else {
			return null;
		}
	};
	o.prototype._syncInputWidth = function (t) {
		var F = this.getDomRef('inner'),
			s, i;
		if (!F || (t && !t.getDomRef())) {
			return;
		}
		s = this._calculateIconsSpace();
		i = Math.ceil(t.getDomRef().getBoundingClientRect().width);
		F.style.width = 'calc(100% - ' + Math.floor(s + i) + "px";
	};
	o.prototype.isValueHelpOnlyOpener = function (t) {
		return [this._$input[0], this._getValueHelpIcon().getDomRef()].indexOf(t) > -1;
	};
	o.prototype._shouldTriggerSuggest = function () {
		var s = I.prototype._shouldTriggerSuggest.apply(this, arguments);
		return s && !this._bShowListWithTokens;
	};
	o.prototype.addValidator = function (v) {
		if (typeof (v) === "function") {
			this._aTokenValidators.push(v);
		}
	};
	o.prototype.removeValidator = function (v) {
		var i = this._aTokenValidators.indexOf(v);
		if (i !== -1) {
			this._aTokenValidators.splice(i, 1);
		}
	};
	o.prototype.removeAllValidators = function () {
		this._aTokenValidators = [];
	};
	o.prototype.getValidators = function () {
		return this._aTokenValidators;
	};
	o.prototype.addValidateToken = function (p, v) {
		var t = this._validateToken(p, v),
			A = this._addUniqueToken(t, p.validationCallback);
		if (A) {
			this.fireTokenUpdate({
				addedTokens: [t],
				removedTokens: [],
				type: T.TokenUpdateType.Added
			});
			this.fireTokenChange({
				addedTokens: [t],
				removedTokens: [],
				type: T.TokenChangeType.TokensChanged
			});
		}
	};
	o.prototype._validateToken = function (p, v) {
		var t = p.token,
			V = p.validationCallback,
			s = p.suggestionObject,
			e = t && t.getText(),
			u = e ? e : p.text,
			w;
		v = v ? v : this._aTokenValidators;
		w = v.length;
		if (!w) {
			if (!t && V) {
				V(false);
			}
			return t;
		}
		for (var i = 0; i < w; i++) {
			t = v[i]({
				text: u,
				suggestedToken: t,
				suggestionObject: s,
				asyncCallback: this._getAsyncValidationCallback(v, i, u, s, V)
			});
			if (!t) {
				if (V) {
					V(false);
				}
				return null;
			}
			if (t === o.WaitForAsyncValidation) {
				return null;
			}
		}
		return t;
	};
	o.prototype._addUniqueToken = function (t, v) {
		if (!t) {
			return false;
		}
		var e = !this._tokenExists(t);
		e && this.addToken(t);
		if (v) {
			v(e);
		}
		return e;
	};
	o.prototype._tokenExists = function (t) {
		var e = this.getTokens(),
			p = e.length,
			s = t && t.getKey();
		if (!s) {
			return false;
		}
		for (var i = 0; i < p; i++) {
			if (e[i].getKey() === s) {
				return true;
			}
		}
		return false;
	};
	o.prototype._convertTextToToken = function (t, C) {
		var e = this.getAggregation("tokenizer"),
			O = e.getTokens().length,
			i = this._configureTokenOptions(t, false, C),
			v = i.text,
			p = i.item,
			s = i.token;
		if (!v) {
			return null;
		}
		return this._validateToken({
			text: v,
			token: s,
			suggestionObject: p,
			validationCallback: this._validationCallback.bind(this, O)
		});
	};
	o.prototype._validateCurrentText = function (e) {
		var t = this.getAggregation("tokenizer"),
			O = t.getTokens().length,
			i = this._configureTokenOptions(this.getValue(), e),
			v = i.text,
			p = i.item,
			s = i.token;
		if (!v) {
			return null;
		}
		if (p) {
			this._bTokenIsAdded = true;
		}
		if (!this.getMaxTokens() || this.getTokens().length < this.getMaxTokens()) {
			this._bIsValidating = true;
			this.addValidateToken({
				text: v,
				token: s,
				suggestionObject: p,
				validationCallback: this._validationCallback.bind(this, O)
			});
		}
	};
	o.prototype._configureTokenOptions = function (v, e, p) {
		var i, t;
		if (v && this.getEditable()) {
			v = v.trim();
		}
		if (v && (e || p || this._getIsSuggestionPopupOpen())) {
			if (this._hasTabularSuggestions()) {
				i = this._oSuggestionTable.getSelectedItem();
			} else {
				i = this._getSuggestionItem(v, e);
			}
		}
		if (i && i.getText && i.getKey) {
			t = new a({
				text: i.getText(),
				key: i.getKey()
			});
		}
		return {
			text: v,
			item: i,
			token: t
		};
	};
	o.prototype._getAsyncValidationCallback = function (v, V, i, s, e) {
		var t = this;
		return function (p) {
			if (p) {
				p = t.addValidateToken({
					text: i,
					token: p,
					suggestionObject: s,
					validationCallback: e
				}, v.slice(V + 1));
			} else {
				e && e(false);
			}
		};
	};
	return o;
});