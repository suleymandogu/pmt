sap.ui.define([
	"sap/ui/core/Control"
], function (Control) {
	"use strict";
	return Control.extend("com.doehler.PricingMasterFile.customControls.test", {
		metadata: {
			properties: {
				value: {
					type: "string",
					defaultValue: ""
				}
			}
		},

		aggregations: {},

		init: function () {},

		setValue: function (iValue) {
			this.invalidate();
			this.setProperty("value", iValue, true);
		},

		renderer: function (oRM, oControl) {
			oRM.write("<div height='600px' width='600px' ");
			oRM.writeControlData(oControl);
			oRM.writeClasses();
			oRM.write(">");

			oRM.write("<iframe width='100%' height='600px'");
			oRM.writeAttributeEscaped("src", oControl.getProperty("value"));
			oRM.write("/>")
			oRM.write("</iframe>");

			oRM.write("</div>");
		}
	});
});