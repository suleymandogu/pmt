sap.ui.define([
	"sap/m/MultiInput",
	"sap/m/SearchField",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/table/Column"
], function (MultiInput, SearchField, Filter, FilterOperator, Column) {
	"use strict";

	var ValueHelpControl = MultiInput.extend("com.doehler.pr.customControls.ValueHelpControl", {
		metadata: {
			properties: {
				enabled: {
					type: "boolean",
					defaultValue: true
				},

				serverSide: {
					type: "boolean",
					defaultValue: true
				},

				fieldName: {
					type: "string"
				},

				descFieldName: {
					type: "string"
				},

				descFieldName2: {
					type: "string"
				},

				kotab: {
					type: "string"
				},

				relFieldModel: {
					type: "string"
				},

				relFieldName1: {
					type: "string"
				},

				relFieldName2: {
					type: "string"
				},

				relFieldName3: {
					type: "string"
				},
				relFieldName4: {
					type: "string"
				},

				relFieldName5: {
					type: "string"
				},

				relFieldName6: {
					type: "string"
				},

				relFieldName7: {
					type: "string"
				},

				relFieldName8: {
					type: "string"
				},

				relFieldName9: {
					type: "string"
				},

				relFieldName10: {
					type: "string"
				},

				relFieldName11: {
					type: "string"
				},

				relFieldName12: {
					type: "string"
				},

				ctrlBindingPath: {
					type: "string"
				},

				treeEnabled: {
					type: "boolean",
					defaultValue: false
				},

				manualDataEnabled: {
					type: "boolean",
					defaultValue: false
				},

				dataFunction: {
					type: "object"
				},

				multiSelect: {
					type: "boolean",
					defaultValue: false
				},
				enableMultiLineMode: {
					type: "boolean",
					defaultValue: true
				},
				selectedValues: {
					type: "string"
				},
				controller: {
					type: "object"
				},
				desc: {
					type: "string"
				},
				showDesc: {
					type: "boolean",
					defaultValue: true
				},
				directRefValue: {
					type: "boolean",
					defaultValue: false
				}
			},

			aggregations: {},

			events: {
				onSelectValue: {
					parameters: {
						selectedValue: "string"
					}
				},

				onInitialize: {
					parameters: {

					}
				},

				onGetData: {
					parameters: {
						fnSuccess: "function"
					}
				},
				onSelectToken: {
					parameters: {
						fnSuccess: "function"
					}
				}
			}
		},
		/*setValue: function (iValue) {
    		this.setProperty("value", iValue, true);
    	},*/
		renderer: function (oRm, oControl) {
			sap.m.MultiInputRenderer.render(oRm, oControl);
			var desc = oControl.getDesc();
			if (oControl.getShowDesc()) {
				jQuery("#" + oControl.getId() + "_desc").remove();
				if (desc) {
					oRm.write("<span id='" + oControl.getId() + "_desc'");
					//oRm.writeControlData(oControl);
					//oRm.writeAttribute("id", oControl.getId() + "_desc"); 
					oRm.addClass("f4desc");
					oRm.writeClasses();
					oRm.write(">" + desc + "</span>");
				}
			} else {
				jQuery("#" + oControl.getId() + "_desc").hide();
			}
		}
	});

	ValueHelpControl.prototype.init = function () {
		MultiInput.prototype.init.apply(this, arguments);

		this.attachValueHelpRequest(ValueHelpControl._valueHelpRequest.bind(this));
		this.attachChange(ValueHelpControl._valueHelpChange.bind(this));
		this.attachSubmit(ValueHelpControl._valueHelpSubmit.bind(this))
		this.attachLiveChange(ValueHelpControl._valueHelpChange.bind(this));
		this.attachTokenChange(ValueHelpControl._onUpdateTokens.bind(this));
		this.setShowValueHelp(true);
		this.setEnableMultiLineMode(true);

		/*this.addValidator(function(args){
			window.setTimeout(function(){
				args.asyncCallback(new sap.m.Token({text: args.text}));
			},500);
			return sap.m.MultiInput.WaitForAsyncValidation;
		});*/
	};

	ValueHelpControl.prototype.onAfterRendering = function () {
		//console.log(this.getValue(), this.sId);
		/*  var value = this.getValue();
		  this.setProperty("value", value, true);*/
	};

	ValueHelpControl._onUpdateTokens = function () {
		this.setSelectedValues(this.getSelectedKeys());
	};

	ValueHelpControl._valueHelpSubmit = function (oEvent) {

			var oValue = oEvent.getParameter("value");
			if (this.getMultiSelect()) {
				this.addToken(new sap.m.Token({
					key: oValue,
					text: oValue
				}));
				this.setValue("");

				this.setSelectedValues(this.getSelectedKeys());
			} else
				this.setValue(oValue.toUpperCase());
		},

	ValueHelpControl._valueHelpChange = function (oEvent) {
			if (!this.getMultiSelect()) {
				var oValue = oEvent.getParameter("value");
				this.setValue(oValue);
			}
		};

	ValueHelpControl.prototype.getSelectedKeys = function () {
			var result = "";

			if (!this.getMultiSelect())
				return result;

			var oTokens = this.getTokens();

			for (var i = 0; i < oTokens.length; i++) {
				result = result + ',' + oTokens[i].getKey();
			}

			if (result !== "")
				result = result.substr(1, result.length);

			return result;
		},
		ValueHelpControl.oControl = null; ///////
	ValueHelpControl._valueHelpRequest = function (oEvent) {
		ValueHelpControl.oControl = oEvent.getSource(); //////
		sap.ui.getCore().getModel("globalModel").setProperty("/pressTableValueControl", oEvent.getSource());
		this.fireEvent("onInitialize");
		var thiz = this;

		if (this.getManualDataEnabled()) {
			this.fireEvent("onGetData", {
				fnSuccess: this._prepareValueHelpDialog
			});

		} else {
			var callFunc = this.getDataFunction();

			this.setBusy(true);

			callFunc(this.getController(),
				this.getFieldName(),
				this.getKotab(),
				this._getRelatedFieldModelData(oEvent),
				this._getServerSideModelData(),
				function (oModel, controller) {
					thiz.setBusy(false);
					thiz.getController().getView().setModel(oModel, "valueHelpModel");

					if (thiz.getTreeEnabled()) {
						thiz._prepareValueHelpTreeDialog(thiz.getId());
					} else {
						thiz._prepareValueHelpDialog();
					}
				});
		}
	};

	ValueHelpControl.prototype._getServerSideModelData = function (searchTerm) {
		var oModel = {
			NoOfEntry: 100,
			SearchTerm: ""
		}

		if (this.getServerSide()) {
			oModel.SearchTerm = searchTerm;
		}

		return oModel;
	};

	ValueHelpControl.prototype._getRelatedFieldModelData = function (oEvent) {
		/*
		 * Related Field Configuration----
		 * If the current control should filter the values by using the value from another field in the model
		 * Then by using the related properties we will get the values and pass them to the API
		 */

		var relFldValue1,
			relFldValue2,
			relFldValue3,
			relFldValue4,
			relFldValue5,
			relFldValue6,
			relFldValue7,
			relFldValue8,
			relFldValue9,
			relFldValue10,
			relFldValue11,
			relFldValue12 = "";

		if (this.getDirectRefValue() && oEvent !== undefined) {
			var source = oEvent.getSource();
			//relFldValue1 = this.getRelFieldName1();
			if (source.getRelFieldName1()) {
				relFldValue1 = source.getRelFieldName1();
			}

			if (source.getRelFieldName2()) {
				relFldValue2 = source.getRelFieldName2();
			}

			if (source.getRelFieldName3()) {
				relFldValue3 = source.getRelFieldName3();
			}
			if (source.getRelFieldName4()) {
				relFldValue4 = source.getRelFieldName4();
			}

			if (source.getRelFieldName5()) {
				relFldValue5 = source.getRelFieldName5();
			}

			if (source.getRelFieldName6()) {
				relFldValue6 = source.getRelFieldName6();
			}

			if (source.getRelFieldName7()) {
				relFldValue7 = source.getRelFieldName7();
			}

			if (source.getRelFieldName8()) {
				relFldValue8 = source.getRelFieldName8();
			}

			if (source.getRelFieldName9()) {
				relFldValue9 = source.getRelFieldName9();
			}

			if (source.getRelFieldName10()) {
				relFldValue10 = source.getRelFieldName10();
			}
			if (source.getRelFieldName11()) {
				relFldValue11 = source.getRelFieldName11();
			}
			if (source.getRelFieldName12()) {
				relFldValue12 = source.getRelFieldName12();
			}
		} else if (this.getRelFieldModel()) {

			var oRelFieldModel = this.getController().getView().getModel(this.getRelFieldModel());
			if (this.getRelFieldName1()) {
				relFldValue1 = oRelFieldModel.getProperty(this.getRelFieldName1());
			}

			if (this.getRelFieldName2()) {
				relFldValue2 = oRelFieldModel.getProperty(this.getRelFieldName2());
			}

			if (this.getRelFieldName3()) {
				relFldValue3 = oRelFieldModel.getProperty(this.getRelFieldName3());
			}
			if (this.getRelFieldName4()) {
				relFldValue4 = oRelFieldModel.getProperty(this.getRelFieldName4());
			}

			if (this.getRelFieldName5()) {
				relFldValue5 = oRelFieldModel.getProperty(this.getRelFieldName5());
			}

			if (this.getRelFieldName6()) {
				relFldValue6 = oRelFieldModel.getProperty(this.getRelFieldName6());
			}

			if (this.getRelFieldName7()) {
				relFldValue7 = oRelFieldModel.getProperty(this.getRelFieldName7());
			}

			if (this.getRelFieldName8()) {
				relFldValue8 = oRelFieldModel.getProperty(this.getRelFieldName8());
			}

			if (this.getRelFieldName9()) {
				relFldValue9 = oRelFieldModel.getProperty(this.getRelFieldName9());
			}

			if (this.getRelFieldName10()) {
				relFldValue10 = oRelFieldModel.getProperty(this.getRelFieldName10());
			}
			if (this.getRelFieldName11()) {
				relFldValue11 = oRelFieldModel.getProperty(this.getRelFieldName11());
			}
			if (this.getRelFieldName12()) {
				relFldValue12 = oRelFieldModel.getProperty(this.getRelFieldName12());
			}

		}

		var oRelFldModel = {
			RelFldValue1: relFldValue1,
			RelFldValue2: relFldValue2,
			RelFldValue3: relFldValue3,
			RelFldValue4: relFldValue4,
			RelFldValue5: relFldValue5,
			RelFldValue6: relFldValue6,
			RelFldValue7: relFldValue7,
			RelFldValue8: relFldValue8,
			RelFldValue9: relFldValue9,
			RelFldValue10: relFldValue10,
			RelFldValue11: relFldValue11,
			RelFldValue12: relFldValue12
		}

		return oRelFldModel;
	};

	ValueHelpControl.prototype._onValueHelpSearch = function (oEvent) {
		var thiz = this;
		var searchStr = this.getTreeEnabled() ? oEvent.getParameter("query") : oEvent.getParameter("value");

		/*
		 * If the server side mode is enabled, we send request to the server to get the data by using the search term
		 */
		if (this.getServerSide()) {

			var callFunc = this.getDataFunction();

			callFunc(this.getController(),
				this.getFieldName(),
				this.getKotab(),
				this._getRelatedFieldModelData(),
				this._getServerSideModelData(searchStr),
				function (oModel, controller) {
					thiz.getController().getView().setModel(oModel, "valueHelpModel");
				});

		} else {
			/*
			 * If we are in client mode, then the search feature will work on the client
			 */

			var filters = "";
			var fieldNames = this.getTreeEnabled() ? ["PRODH", "VTEXT", "CHILDS/PRODH", "CHILDS/VTEXT"] : this.getController().getView().getModel(
				"valueHelpModel").getProperty("/F4_STRUC_DYN");
			var f4Filter = [];

			if (searchStr && searchStr.length > 0) {
				for (var i = 0; i < fieldNames.length; i++) {
					f4Filter.push(new sap.ui.model.Filter(fieldNames[i], sap.ui.model.FilterOperator.Contains, searchStr));
				}
				filters = new sap.ui.model.Filter({
					filters: f4Filter,
					and: false
				});
			}

			var oBinding = this.getTreeEnabled() ? this._oTree.getBinding("rows") : oEvent.getSource().getBinding("items");
			oBinding.filter(filters);
		}
	};

	ValueHelpControl.prototype._onValueHelpConfirm = function (oEvent) {

		if (this.getTreeEnabled()) {
			var selectedIndx = oEvent.getParameter("rowIndex");

			if (this._selectedIndex === undefined || this._selectedIndex !== selectedIndx) {
				this._selectedIndex = selectedIndx;
				this._selectedTreeId = this.getModel("valueHelpModel").getProperty(oEvent.getParameter("rowContext").getPath()).PRODH;
				this._selectedTreeDesc = this.getModel("valueHelpModel").getProperty(oEvent.getParameter("rowContext").getPath()).VTEXT;
				this._oTree.clearSelection();
				this._oTree.setSelectedIndex(selectedIndx);
			}
		} else {

			if (this.getMultiSelect()) {
				this._setTokens(oEvent.getParameters().selectedItems);
			} else {
				var index = this.getProperty("fieldName") === "CREATEDBY" ? 1 : 0;

				var custData = oEvent.getParameters().selectedItem.getCustomData();
				var dispValue = custData[index].getValue();
				var dispValue2 = custData[index].getValue();

				// if (this.getProperty("fieldName") === "CREATEDBY" && custData.length === 3) {
				// 	dispValue2 = dispValue2 + " " + custData[2].getValue();
				// 	dispValue = custData[0].getValue();
				// }

				this.setValue(dispValue);
				this.setDesc(dispValue2);
				if (this.getDirectRefValue()) {
					ValueHelpControl.oControl.setValue(dispValue); ////////////
				}
				//console.log(this.getModel("valueHelpModel").getData());
				// var descValue = "";

				if (this.getDescFieldName() !== "" && this.getDescFieldName() !== undefined) {
					//var descValue = custData[1].getValue();
					//this.getController().getObj(this.getId() + this.getDescFieldName()).setText(descValue)
					//this.setDescription(descValue);
				}
				var isPackagingSelected = this.getProperty("fieldName") === "PACKAGING";
				if (isPackagingSelected === true) {
					var packagingData = oEvent.getParameters().selectedItem.getCustomData();
					var packagingValue = packagingData[isPackagingSelected].getValue();
					this.setValue(packagingValue);
				}

			}
			if (this.getShowDesc()) {
				this.setDesc(oEvent.getParameters().selectedItem.getCells()[1].getText());
			}
			this.fireEvent("onSelectToken", {
				fnSuccess: this._prepareValueHelpDialog
			});

			this._destroyDialog();
		}
	};
	ValueHelpControl.prototype._onSelectTokenfn = function () {
		return this.getSelectedKeys();
	};

	ValueHelpControl.prototype._setTokens = function (selectedRows) {
		//this.removeAllTokens();

		for (var i = 0; i < selectedRows.length; i++) {
			var oValue = selectedRows[i].getCustomData()[0].getValue();
			var oText = selectedRows[i].getCustomData()[1].getValue();

			this.addToken(new sap.m.Token({
				key: oValue,
				text: oText
			}));
		}
	};

	ValueHelpControl.prototype._onValueHelpClose = function () {
		this._destroyDialog();
	};

	ValueHelpControl.prototype._destroyDialog = function () {
		this.getController().getView().removeDependent(this._oDialog);
		this._oDialog.destroy();
	};

	ValueHelpControl.prototype._prepareValueHelpTreeDialog = function (popupId) {
		var that = this;
		// var oModel = this.getController().getView().getModel("valueHelpModel");

		this._oTree = new sap.ui.table.TreeTable({
			rows: "{path:'valueHelpModel>/PRODHIERARCHY', parameters: {arrayNames:['CHILDS']}}",
			selectionMode: "MultiToggle",
			enableSelectAll: false,
			width: "100%",
			enableColumnReordering: false,
			expandFirstLevel: false,
			rowSelectionChange: this._onValueHelpConfirm.bind(this),
			//				toolbar : new sap.m.Toolbar({
			//					content : new sap.m.SearchField({
			//						width : "100%",
			//						selectOnFocus : false,
			//						search : this._onValueHelpSearch.bind(this)
			//					})
			//				}),
			columns: [
				new Column("", {
					label: new sap.m.Label({
						text: "ID"
					}),
					template: new sap.m.Text("", {
						text: "{valueHelpModel>PRODH}"
					})
				}),
				new Column({
					label: new sap.m.Label({
						text: "Name"
					}),
					template: new sap.m.Text({
						text: "{valueHelpModel>VTEXT}"
					})
				})
			]
		});

		this._oDialog = new sap.m.Dialog({
			busyIndicatorDelay: 0,
			title: "Select Dialog",
			content: [this._oTree],
			contentWidth: "600px",
			beginButton: new sap.m.Button({
				text: that.getController().getText("select"),
				press: function () {
					if (that._selectedTreeId) {
						that.setValue(that._selectedTreeId);

						if (that.getDescFieldName() !== "") {
							that.getController().getObj(that.getId() + that.getDescFieldName()).setText(that._selectedTreeDesc);
						}

						that._destroyDialog();
					}
				}
			}),
			endButton: new sap.m.Button({
				text: that.getController().getText("cancel"),
				press: function () {
					that._destroyDialog();
				}
			}),
			afterClose: function () {
				that._destroyDialog();
			}
		}).addStyleClass("sapUiSizeCompact");

		this.getController().getView().addDependent(this._oDialog);
		this._oDialog.open();
	};

	ValueHelpControl.prototype._prepareValueHelpDialog = function (popupId) {
		var thiz = this;
		var that = thiz;
		var popupId = this.getId();
		var oModel = this.getController().getView().getModel("valueHelpModel");

		this._oDialog = new sap.m.TableSelectDialog(popupId + "Dialog", {
			//liveChange : this._onValueHelpSearch.bind(this),
			search: this._onValueHelpSearch.bind(this),
			confirm: this._onValueHelpConfirm.bind(this),
			close: this._onValueHelpClose.bind(this),
			cancel: this._onValueHelpClose.bind(this),
			multiSelect: this.getMultiSelect(),
			draggable: true,
			resizable: true
		});

		var columnsModel = oModel.getProperty("/F4_DESCR_DYN");
		var tableCells = [],
			customData = [];

		/*
		 * Add Columns and Cells of the Table
		 */
		columnsModel.forEach(function (aColumn) {
			that._oDialog.addColumn(
				new sap.m.Column({
					header: new sap.m.Label({
						text: aColumn.MDTXT,
						design: "Bold",
						wrapping: true
					})
				})
			);

			tableCells.push(new sap.m.Text({
				text: "{valueHelpModel>" + aColumn.FIELDNAME + "}"
			}));
		});

		/*
		 * Store the primary key and value of the current field control to use after the dialog being closed
		 */
		var oKey = oModel.getProperty("/F4_FIELD_DYN");
		customData.push(new sap.ui.core.CustomData({
			key: oKey,
			value: "{valueHelpModel>" + oKey + "}"
		}));

		/*
		 * If the description field is available, store the value of the text in custom data
		 */
		if (this.getDescFieldName() !== "") {
			customData.push(new sap.ui.core.CustomData({
				key: this.getDescFieldName(),
				value: "{valueHelpModel>" + this.getDescFieldName() + "}"
			}));
		}
		if (this.getDescFieldName2() !== "") {
			customData.push(new sap.ui.core.CustomData({
				key: this.getDescFieldName2(),
				value: "{valueHelpModel>" + this.getDescFieldName2() + "}"
			}));
		}

		/*
		 * Bind data to the table
		 */
		this._oDialog.bindItems("valueHelpModel>/F4_VALUE_DYN", new sap.m.ColumnListItem({
			cells: tableCells,
			customData: customData
		}));

		/*
		 * Configure dialog
		 */
		jQuery.sap.syncStyleClass("sapUiSizeCompact", this.getController().getView(), this._oDialog);
		this._oDialog._table.mProperties.growingScrollToLoad = false;
		this.getController().getView().addDependent(this._oDialog);
		this._oDialog.open();
	};

	return ValueHelpControl;
});

/* To add tokens
var oInput = this.getView().byId("statusVHId");
oInput.addToken(new sap.m.Token({key:"Q",text:"Q"}));
oInput.setSelectedValues("Q");
*/