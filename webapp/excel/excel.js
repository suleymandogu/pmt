 sap.ui.define([
 	'sap/ui/model/json/JSONModel',
 	'sap/ui/export/library',
 	'sap/ui/export/Spreadsheet',
 	'sap/m/MessageToast',
 	'sap/base/Log'
 ], function (JSONModel, exportLibrary, Spreadsheet, MessageToast, Log) {
 	"use strict";
 	var EdmType = exportLibrary.EdmType;
 	return {

 		_downloadExcel: function (name, table, tableData) {
 			var aCols, oSettings, oSheet,
 				dial = new sap.m.BusyDialog();
 			aCols = this.createColumnConfig(table, tableData);
 			oSettings = {
 				count: 150000,
 				workbook: {
 					columns: aCols.aCols,
 					context: {
 						title: 'Price Setting Information',
 					}
 				},
 				dataSource: tableData,
 				fileName: name === "PSI" ? "priceSettingInformation.xlsx" : name === "BUM" ? "buMeasure.xlsx" : name === "PSM" ?
 					"pricingMeasures.xslx" : name === "RP" ? "redPosition.xslx" : name === "RC" ? "redCustomer.xslx" : name === "List" ? "list.xslx" : "tenderView.xslx"
 			};

 			oSheet = new Spreadsheet(oSettings);
 			oSheet.onprogress = function (iValue) {
 				console.log(iValue);
 			};
 			oSheet.build()
 				.then(function () {
 					MessageToast.show('Spreadsheet export has finished');
 				})
 				.finally(oSheet.destroy);

 		},

 		createColumnConfig: function (oTable, tableData) {
 			var that = this,
 				aCols = [],
 				aColumns = oTable.getColumns(),
 				columnText = "",
 				type = "",
 				property = "",
 				buActionList = sap.ui.getCore().getModel("buActionDD");

 			aColumns.forEach(function (oColumn) {
 				if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
 					if (oColumn.mSkipPropagation.template) {
 						columnText = oColumn.getLabel().getText();
 						if (oColumn.getLabel().getCustomData().length) {

 							type = oColumn.getLabel().getCustomData()[0].getValue();
 							property = oColumn.getLabel().getCustomData()[1].getValue();

 							aCols = that._prepareRows(oTable, columnText, property, type, aCols);

 						}
 					}
 				}
 			});

 			return {
 				aCols: aCols
 			};
 		},

 		_prepareRows: function (oTable, columnText, property, type, aCols) {
 			var gModel = sap.ui.getCore().getModel("globalModel");
 			var role = gModel.getProperty("/UIROLE");

 			switch (type) {
 			case "textType":
 				if (property === "NEW_PRICE/APPROVAL_NAME") {
 					aCols.push({
 						label: columnText,
 						property: ['PRICE_MASTER/WORKLOW_STATUS_TT', 'NEW_PRICE/APPROVAL_NAME'],
 						type: EdmType.String,
 						width: 20,
 						wrap: true,
 						textAlign: "Center",
 						template: '{0} {1}'
 					});
 				} else {
 					if (property === "PRICE_MASTER/DCUKYCP") {
 						aCols.push({
 							label: columnText,
 							property: ['PRICE_MASTER/DCUKYCP', 'PRICE_MASTER/DCONDUNIT', 'PRICE_MASTER/DOEKMEIN2'],
 							type: EdmType.String,
 							width: 20,
 							wrap: true,
 							textAlign: "Center",
 							template: '{0} / {1} {2}'
 						});
 					} else {
 						if (property === "READY") {
 							aCols.push({
 								property: "READY_T",
 								label: "READY",
 								width: 20,
 								type: EdmType.String,
 								wrap: true,
 								textAlign: "Center"
 							});
 						} else {
 							aCols.push({
 								property: property,
 								label: columnText,
 								width: 20,
 								type: EdmType.String,
 								wrap: true,
 								textAlign: "Center"
 							});
 						}
 					}

 				}

 				break;

 			case "numberType":
 				if (property === "CM1Review") {
 					aCols.push({
 						property: "PRICE_MASTER/cm1L12m",
 						label: columnText + "(% CM1)",
 						width: 18,
 						type: EdmType.Number,
 					});
 					aCols.push({
 						property: "PRICE_MASTER/DPRSTMPRO",
 						label: columnText + "% target",
 						width: 18,
 						type: EdmType.Number
 					});
 					aCols.push({
 						property: "PRICE_MASTER/gapToTargetPercDetail",
 						label: columnText + "(% delta)",
 						width: 18,
 						type: EdmType.Number
 					});
 				} else {

 					if (property === "PRICE_MASTER/DPRSCPAIN") {
 						aCols.push({
 							property: "PRICE_MASTER/DPRSCPAIN",
 							label: columnText + "(curr. price)",
 							width: 18,
 							type: EdmType.Number
 						});
 						if (oTable.getId().split("--").length > 0) {
 							if (oTable.getId().split("--")[1] !== "tenderTableId") {

 								aCols.push({
 									property: "PRICE_MASTER/PRICE3_EXCEL",
 									label: columnText + "(target)",
 									width: 18,
 									type: EdmType.Number
 								});
 							}
 						}

 					} else {
 						aCols.push({
 							property: property,
 							label: columnText,
 							type: EdmType.Number
 						});

 					}

 				}

 				break;

 			case "Date":

 				//Price Validty
 				if (property === "PRICE_MASTER/QUOT_FROM" || property === "PRICE_MASTER/QUOT_TO") {
 					aCols.push({
 						label: columnText,
 						type: EdmType.Date,
 						property: property,
 						wrap: true,
 						textAlign: "Center",
 						inputFormat: 'yyyymmdd',
 						width: 25
 					});
 					// aCols.push({
 					// 	label: columnText + " To",
 					// 	type: EdmType.Date,
 					// 	property: "PRICE_MASTER/QUOT_TO",
 					// 	wrap: true,
 					// 	textAlign: "Center",
 					// 	inputFormat: 'yyyymmdd',
 					// 	width: 25
 					// });
 				} else {
 					if (property === "PRICE_MASTER/DAPCHANGEDON_EXCEL") {
 						aCols.push({
 							label: columnText,
 							type: EdmType.Date,
 							property: property,
 							width: 25,
 							wrap: true,
 							textAlign: "Center",
 							inputFormat: 'yyyymmdd'
 						});
 						// aCols.push({
 						// 	property: "PRICE_MASTER/PRICE_CHANGE_INDICATOR",
 						// 	label: "Last price change Indicator",
 						// 	width: 20,
 						// 	type: EdmType.String,
 						// 	wrap: true,
 						// 	textAlign: "Center"
 						// });
 					} else {
 						aCols.push({
 							label: columnText,
 							type: EdmType.Date,
 							property: property,
 							width: 25,
 							wrap: true,
 							textAlign: "Center",
 							inputFormat: property === "PRICE_VALID_FROM" || property === "PRICE_VALID_OLD" || property === "CUSTOMER_VALID_FROM" ||
 								property === "CUSTOMER_VALID_TO" || property === "NEW_PRICE/BU_DEADLINE" ||
 								property === "NEW_PRICE/PRICE_VALID_FROM" || property ===
 								"NEW_PRICE/PRICE_VALID_TO" || property === "NEW_PRICE/PRICING_DEADLINE" ? 'yyyy-MM-dd' : 'yyyymmdd'
 						});
 					}
 				}
 				break;
 			default:
 			}
 			return aCols;
 		},

 		/****************************************************************************************/

 		prepareRowsArray: function (aColumns, oRecord) {
 			var aRow = [];
 			var thiz = this,
 				path,
 				sValue;

 			aColumns.forEach(function (oColumn) {
 				if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped") && oColumn.getLabel().getCustomData().length) {
 					path = oColumn.getLabel().getCustomData()[1].getValue();
 					if (path !== "" && path !== undefined) {
 						if (path === "CM1Review") {
 							path = "PRICE_MASTER/DPRSCM1PERC";
 							sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 							aRow.push(sValue);
 							path = "PRICE_MASTER/DPRSTMPRO";
 							sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 							aRow.push(sValue);
 							path = "PRICE_MASTER/gapToTargetPercDetail";
 							sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 							aRow.push(sValue);
 						} else {
 							if (path === "PRICE_MASTER/DPRSCPAIN" || path === "PRICE_MASTER/DPRSCPAIN_tender") {
 								var isTender = false;
 								if (path === "PRICE_MASTER/DPRSCPAIN_tender") {
 									isTender = true;
 								}
 								path = "PRICE_MASTER/DPRSCPAIN";
 								sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 								aRow.push(sValue);
 								if (!isTender) {
 									path = "PRICE_MASTER/PRICE3";
 									sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 									aRow.push(sValue);
 								}

 							} else {
 								if (path === "PRICE_MASTER/GAPTOTARGET_PERC") {
 									var path2 = "PRICE_MASTER/NOTVALIDGAPTOTARGET_PERC";
 									sValue = oRecord[path2.split("/")[0]][path2.split("/")[1]];
 									if (sValue === "---") {
 										sValue = 0;
 										aRow.push(sValue);
 									} else {
 										sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 										aRow.push(sValue);
 									}

 								} else {
 									sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 									if (path === "PRICE_MASTER/DAPCHANGEDON") {
 										if (!sValue || sValue === "00000000") {
 											sValue = "";
 										}

 										sValue = sValue.substring(6, 8) + "." + sValue.substring(4, 6) + "." + sValue.substring(0, 4);
 									}
 									aRow.push(sValue);
 									if (path === "PRICE_MASTER/QUOT_FROM") {
 										path = "PRICE_MASTER/QUOT_TO";
 										sValue = oRecord[path.split("/")[0]][path.split("/")[1]];
 										aRow.push(sValue);
 									}
 								}
 							}
 						}
 					}
 				}
 			});

 			return aRow;
 		},

 		prepareColumnsArray: function (aColumns) {
 			var aRow = [];
 			aColumns.forEach(function (oColumn) {
 				if (oColumn.getProperty("visible") && !oColumn.getProperty("grouped")) {
 					if (oColumn.mSkipPropagation.template) {
 						var oTemplate = oColumn.getAggregation("template");
 						if (oTemplate.getMetadata().getName() === "sap.m.HBox" || oTemplate.getMetadata().getName() === "sap.m.VBox") {
 							if (oColumn.getAggregation("label").getText()) {
 								if (oColumn.getAggregation("label").getText() === "CM1 Review") {
 									aRow.push(oColumn.getAggregation("label").getText() + "(% CM1/ytd)");
 									aRow.push(oColumn.getAggregation("label").getText() + "% target");
 									aRow.push(oColumn.getAggregation("label").getText() + "(% delta)");
 								} else if (oColumn.getAggregation("label").getText() === "Price Validity Old") {
 									aRow.push(oColumn.getAggregation("label").getText() + " From");
 									aRow.push(oColumn.getAggregation("label").getText() + " To");
 								} else if (oColumn.getAggregation("label").getText() === "Prices") {
 									aRow.push(oColumn.getAggregation("label").getText() + "(curr. price)");
 									aRow.push(oColumn.getAggregation("label").getText() + "(at curr. cost)");
 									aRow.push(oColumn.getAggregation("label").getText() + "(target)");
 								} else {
 									aRow.push(oColumn.getAggregation("label").getText());
 								}

 							}
 						} else {
 							if (oTemplate.mBindingInfos.icon || oTemplate.mBindingInfos.text || oTemplate.mBindingInfos.value || oTemplate.mBindingInfos.selectedKey) {
 								if (oColumn.getAggregation("label").getText()) {
 									aRow.push(oColumn.getAggregation("label").getText());
 								}
 							}
 						}
 					}
 				}
 			});
 			return aRow;
 		},

 		prepareExcelData: function (aModelData, table) {
 			var oTable = table;
 			var aRow = [];
 			var aExcelData = [];
 			var thiz = this;
 			if (oTable) {
 				var aColumns = oTable.getColumns();
 				if (aColumns) {
 					aRow = this.prepareColumnsArray(aColumns);
 				}
 				if (aRow.length) {
 					aExcelData.push(aRow);
 					var length = aModelData.length;
 					for (var t = 0; t < length; t++) {
 						var oRecord = aModelData[t];
 						aRow = [];
 						console.log(t);
 						aRow = thiz.prepareRowsArray(aColumns, oRecord);
 						if (aRow.length) {
 							aExcelData.push(aRow);
 						}
 					}
 				}
 			};
 			return aExcelData;
 		},

 		downloadBlob: function (sFileName, oBlob) {
 			if (typeof window.navigator.msSaveBlob !== "undefined") {
 				window.navigator.msSaveBlob(oBlob, sFileName);
 			} else {
 				const oURL = window.URL || window.webkitURL;
 				const oTempURL = oURL.createObjectURL(oBlob);

 				let oLinkObject = document.createElement("a");
 				oLinkObject.style = "display: none";

 				if (typeof oLinkObject.download !== "undefined") {
 					oLinkObject.href = oTempURL;
 					oLinkObject.download = sFileName;
 					document.body.appendChild(oLinkObject);
 					oLinkObject.click();
 				} else {
 					window.open(oTempURL, "_blank");
 				}
 			}
 		},

 		dataConversion: function (sData) {
 			if (typeof ArrayBuffer !== "undefined") {
 				let oBuf = new ArrayBuffer(sData.length);
 				let aView = new Uint8Array(oBuf);
 				for (let i = 0; i !== sData.length; ++i) {
 					aView[i] = sData.charCodeAt(i) & 0xFF;
 				}
 				return oBuf;
 			} else {
 				let oBuf = new Array(sData.length);
 				for (let i = 0; i !== sData.length; ++i) {
 					oBuf[i] = sData.charCodeAt(i) & 0xFF;
 				}
 				return oBuf;
 			}
 		},

 		_downloadExcel2: function (name, table, tableData) {
 			var excelTitle, aPROverData, length = 0,
 				aExcelData,
 				data, oWorkbook, oWorkSheet, oRange;
 			length;
 			aPROverData = tableData;
 			excelTitle = name;

 			if (aPROverData && aPROverData.length) {
 				oWorkbook = {
 					"Sheets": {
 						"Sheet1": "",
 						"Sheet2": "",
 						"Sheet3": "",
 						"Sheet4": ""
 					},
 					"Props": {},
 					"SSF": {},
 					"SheetNames": []
 				};

 				oWorkSheet = {};
 				oRange = {
 					s: {
 						c: 0,
 						r: 0
 					},
 					e: {
 						c: 0,
 						r: 0
 					}
 				};

 				aExcelData = this.prepareExcelData(aPROverData, table);
 				length = aExcelData.length;
 				if (length) {

 					for (var nRowNum = 0; nRowNum !== length; ++nRowNum) {
 						if (oRange.s.r > nRowNum) {
 							oRange.s.r = nRowNum;
 						}
 						if (oRange.e.r < nRowNum) {
 							oRange.e.r = nRowNum
 						}
 						for (var nColNum = 0; nColNum !== aExcelData[nRowNum].length; ++nColNum) {
 							if (oRange.e.r < nRowNum) {
 								oRange.e.r = nRowNum
 							}
 							if (oRange.e.c < nColNum) {
 								oRange.e.c = nColNum
 							}
 							var oCell = {
 								v: aExcelData[nRowNum][nColNum],
 								s: {
 									font: {
 										name: "Algerian",
 										sz: 10,
 										bold: true,
 										color: "3FFF00FF"
 									}
 								}
 							};
 							if (oCell.v === null) {
 								continue;
 							}
 							var oCellRef = XLSX.utils.encode_cell({
 								c: nColNum,
 								r: nRowNum
 							});
 							if (typeof oCell.v === "number") {
 								oCell.t = "n";
 							} else if (typeof oCell.v === "boolean") {
 								oCell.t = "b";
 							} else {
 								oCell.t = "s";
 							}
 							oWorkSheet[oCellRef] = oCell;
 						}
 					}
 					oWorkSheet["!ref"] = XLSX.utils.encode_range(oRange);

 					// add worksheet to workbook
 					oWorkbook.SheetNames.push("Sheet1");
 					oWorkbook.Sheets["Sheet1"] = oWorkSheet;

 				}
 				oWorkbook.Props = {
 					Title: excelTitle,
 					LastAuthor: sap.ushell.Container.getUser().getFullName(),
 					CreatedDate: new Date()
 				};

 				var oWB = XLSX.write(oWorkbook, {
 					type: "binary",
 					bookType: "xlsx",
 					WTF: 1
 				});
 				var oWData = this.dataConversion(oWB);
 				this.downloadBlob("Document", new Blob([oWData], {
 					type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64"
 				}));

 			}
 		},

 		_downloadExcelNew: function (name, table, tableData) {
 			var oTable = table;
 			var arrData1 = typeof tableData != 'object' ? JSON.parse(tableData) : tableData;
 			var arrData = arrData1;
 			var CSV = '';
 			CSV += "sep=,\r\n"; // for comma seperator settings
 			var oColumns1 = oTable.getColumns();
 			var oColumns2 = oTable.getColumns();
 			var oColumns = null;

 			var preventData = [];

 			oColumns = oColumns1.filter(function (oColumn) {
 				if (oColumn.getVisible()) {
 					return true;
 				} else {
 					return false;
 				}
 			});

 			var row = "";
 			oColumns.forEach(function (item) {
 				if (item.getLabel().getText()) {
 					if (item.getLabel().getText() === "CM1 Review") {
 						row += item.getLabel().getText() + "(% CM1/ytd)" + ',';
 						row += item.getLabel().getText() + "% target" + ',';
 						row += item.getLabel().getText() + "(% delta)" + ',';
 					} else if (item.getLabel() === "Price Validity Old") {
 						row += item.getLabel().getText() + " From" + ',';
 						row += item.getLabel().getText() + " To" + ',';
 					} else if (item.getLabel() === "Prices") {
 						row += item.getLabel().getText() + "(curr. price)" + ',';
 						row += item.getLabel().getText() + "(at curr. cost)" + ',';
 						row += item.getLabel().getText() + "(target)" + ',';
 					} else {
 						row += item.getLabel().getText() + ',';
 					}
 				}

 			});
 			row = row.slice(0, -1);
 			CSV += row + '\r\n';

 			for (var i = 0; i < arrData.length; i++) {
 				if (arrData[i].STUFE === 0) {
 					continue;
 				} // skip 0 level
 				var row = "";
 				oColumns.forEach(function (item) {

 					if (item.getLabel().getCustomData().length) {
 						var pro = item.getLabel().getCustomData()[1].getValue();
 						if (pro != "") {
 							if (pro === "PRICE_MASTER/GAPTOTARGET_PERC_EXCEL") {
 								var path2 = "PRICE_MASTER/NOTVALIDGAPTOTARGET_PERC";
 								var sValue = arrData[i][path2.split("/")[0]][path2.split("/")[1]];
 								if (sValue === "---") {
 									sValue = 0;
 									row += '"' + sValue + '",';
 								} else {
 									row += '"' + arrData[i][pro.split("/")[0]][pro.split("/")[1]] + '",';
 								}

 							} else {
 								if (pro === "PRICE_MASTER/GAPTOTARGET_VALUE") {
 									var path2 = "PRICE_MASTER/NOTVALIDGAPTOTARGET_VALUE";
 									var sValue = arrData[i][path2.split("/")[0]][path2.split("/")[1]];
 									if (sValue === "---") {
 										sValue = 0;
 										row += '"' + sValue + '",';
 									} else {
 										row += '"' + arrData[i][pro.split("/")[0]][pro.split("/")[1]] + '",';
 									}

 								} else {
 									if (pro === "CM1Review") {
 										pro = "PRICE_MASTER/DPRSCM1PERC";
 										row += '"' + arrData[i][pro.split("/")[0]][pro.split("/")[1]] + '",';
 										pro = "PRICE_MASTER/DPRSTMPRO";
 										row += '"' + arrData[i][pro.split("/")[0]][pro.split("/")[1]] + '",';
 										pro = "PRICE_MASTER/gapToTargetPercDetail";
 										row += '"' + arrData[i][pro.split("/")[0]][pro.split("/")[1]] + '",';
 									} else
 										row += '"' + arrData[i][pro.split("/")[0]][pro.split("/")[1]] + '",';
 								}

 							}
 						} else {
 							row += '" ",';
 						}
 					}

 				});
 				row.slice(0, row.length - 1);
 				CSV += row + '\r\n';
 			}
 			if (CSV == '') {
 				alert("Invalid data");
 				return;
 			}
 			var fileName = "BOM_Article_";
 			//fileName += ReportTitle.replace(/ /g, "_");
 			var date = new Date();
 			fileName += "_" + date.getDay() + "_" + date.getMonth() + "_" + date.getFullYear() + "_" + date.getHours() + date.getMinutes() +
 				date.getSeconds();
 			var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
 			var link = document.createElement("a");
 			link.href = uri;

 			//set the visibility hidden so it will not effect on your web-layout
 			link.style = "visibility:hidden";
 			link.download = fileName + ".csv";

 			//this part will append the anchor tag and remove it after automatic click
 			document.body.appendChild(link);
 			link.click();
 			document.body.removeChild(link);

 		}

 	};
 });