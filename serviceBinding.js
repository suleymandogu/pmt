function initModel() {
	var sUrl = "/sap/opu/odata/sap/ZVTX_PRICING_TOOL_KPI_SRV/";
	var oModel = new sap.ui.model.odata.ODataModel(sUrl, true);
	sap.ui.getCore().setModel(oModel);
}